<?php

namespace App\Http\Controllers;

use App\Mail\OrderComplete;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'invoice.cfadmin.index' : 'home.index';

        $invoices = Invoice::orderBy('created_at','desc')->get();

        return view($view, [
            'invoices' => $invoices,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $customers = Customer::where('status', 1)->get();

        return view('invoice.cfadmin.create',[
            'customers' => $customers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = null;
        //new customer
        if($request->input('customer_type') == 1){
            $customer = new Customer();
            $customer->name = $request->input('cust_name');
            $customer->tel = $request->input('cust_tel');
            $customer->email = $request->input('cust_email');
            $customer->address1 = $request->input('cust_address');
            $customer->status = 1;
            $customer->save();
        }
        else{
            $customer = Customer::find($request->input('customer'));

        }
        $invoice = new Invoice();
        $invoice->customer_id = $customer->id;
        $invoice->name = $request->input('name');
        $invoice->payment = '';
        $invoice->price = $request->input('price');
        $invoice->due_date = Carbon::createFromFormat('d/m/Y',$request->input('payment_due'))->format('Y-m-d');
        $invoice->status = 1;
        $invoice->remark = $request->input('remark');
        $invoice->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $view = ($this->isAdminRequest()) ? 'invoice.cfadmin.detail' : 'invoice.detail';

        $invoice = Invoice::find($id);

        return view($view, [
            'invoice' => $invoice,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function previewemail($id)
    {
        $invoice = Invoice::find($id);
        return new \App\Mail\Invoice($invoice->voucher->toArray());
    }

    public function preview(Request $request, $prefix, $no)
    {
        $voucher = Voucher::where('voucher_prefix', $prefix)->where('voucher_no', $no)->first();

        return view('payment.invoice', [
            'voucher' => $voucher
        ]);
    }

    public function approve($id)
    {
        $invoice = Invoice::find($id);
        $invoice->status = 3;
        $invoice->voucher->status = 9;
        $invoice->voucher->save();
        $invoice->save();
        //TODO new Mail approve payment via bank transfer & wechat pay to customer mail
        //Mail::to('lilybluecatz@gmail.com')->send(new \App\Mail\Invoice($invoice->voucher->toArray()));
        Mail::to('lilybluecatz@gmail.com')->send(new OrderComplete($invoice->voucher->toArray()));
        return redirect('cfadmin/invoice/' . $invoice->id);
    }

    public function reject($id)
    {
        $invoice = Invoice::find($id);
        $invoice->status = 4;
        $invoice->voucher->status = 4;
        $invoice->voucher->save();
        $invoice->save();

        return redirect('cfadmin/invoice/' . $invoice->id);
    }

    public function confirm($id)
    {
        $invoice = Invoice::find($id);
        $invoice->status = 1;
        $invoice->voucher->status = 7;
        $invoice->voucher->save();
        $invoice->save();

        Mail::to('lilybluecatz@gmail.com')->send(new \App\Mail\Invoice($invoice->voucher->toArray()));
        return redirect('cfadmin/invoice/' . $invoice->id);
    }

}
