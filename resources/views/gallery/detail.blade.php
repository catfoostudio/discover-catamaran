<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Gallery</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap" style="padding-bottom: 20px;">
            <h1>{{ $gallery->name }}</h1>
            {!!  $gallery->description !!}
        </div>
    </header>
    <!-- //Intro -->

    <!-- Gallery -->
    <div class="gallery" id="gallery">
        @foreach($gallery->images as $image)
            <figure class="one-fourth" data-src="{{ asset($image->path) }}">
                <img src="{{ asset($image->path) }}" alt="" />
                <figcaption>
                    <span class="icojam_zoom_in" style="padding-top: 20%;"></span>
                    {{--<div>--}}
                    {{--<h5>BOL GOLDEN CAPE</h5>--}}
                    {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>--}}
                    {{--</div>--}}
                </figcaption>
            </figure>
        @endforeach
    </div>
    <!-- //Gallery -->
</main>
<!-- //Main -->

<!-- Bottom Sidebar -->
@include('template.bottom_sidebar')
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/lightGallery.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#gallery").lightGallery({
            download:false
        });
    });
</script>
</body>
</html>