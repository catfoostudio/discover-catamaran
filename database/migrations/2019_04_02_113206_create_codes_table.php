<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->dateTime('startdate');
            $table->dateTime('enddate');
            $table->tinyInteger('status');
            $table->integer('voucher_prefix_id')->unsigned()->nullable();
            $table->foreign('voucher_prefix_id')->references('id')->on('voucher_prefixes');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('discounttype_id')->unsigned()->nullable();
            $table->foreign('discounttype_id')->references('id')->on('discounttypes');
            $table->integer('value');
            $table->integer('min_purchase');
            $table->integer('max_discount');
            $table->integer('adult_value');
            $table->integer('child_value');
            $table->integer('private_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
