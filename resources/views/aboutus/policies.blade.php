<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Policies</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>POLICIES ( BOOKING-PAYMENT-REFUND)</h1>
            <p></p>
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    <div class="box-white" style="padding-top: 40px;">

                        <h3>DAY TRIP PROGRAM</h3>
                       <ul>
                           <li>1. SELECT THE DAY TRIP PROGRAM THAT YOU PREFER, CHECK AVAILABILITY IN THE CALENDER</li>
                           <li>2. FILL IN IN YOUR INFORMATION AS REQUIRED AND PROCESS TO THE PAYMENT VIA PAYPAL<br>
                               * IF PAYPAL IS NOT PREFFERED, WESTERN UNION OR BANK TRANSFER IS ALTERNATIVE. PLEASE EMAIL US YOUR TRAVEL DETAILS AND OUR QUOTATION/INVOICE WILL BE ISSUED WITHIN 24HR</li>
                           <li>3. AFTER THE PAYMENT IS COMPLETED VIA PAYPAL, A VOUCHER WILL BE SENT TO YOUR EMAIL WITHIN 5MINUTES. FOR OTHER PAYMENT METHODS,  A VOUCHER WILL BE SENT TO YOUR EMAIL WITHIN 12HR AFTER WE RECEIVE YOUR PAYMENT SLIP. </li>
                           <li>4. FULL REFUND WHEN WE HAVE TO CANCEL THE TRIP DUE TO BAD WEATHERS OR ETC REASON. <br>
                               50% REFUND WHEN THE GUEST CANCEL OR CHANGE DATE LESS THAN 14 DAYS IN ADVANCE<br>
                               NO REFUND WHEN THE GUEST CANCEL OR CHANGE DATE LESS THAN 7 DAYS IN ADVANCE.</li>
                       </ul>
                        <h3>PRIVATE CHARTER</h3>
                        <ul>
                            <li>1. SELECT THE FLEET THAT YOU PREFER, CHECK AVAILABILITY IN THE CALENDER</li>
                            <li>2. PLEASE EMAIL US YOUR TRAVEL DETAILS AND OUR QUOTATION/INVOICE WILL BE ISSUED WITHIN 24HR. PAYMENT CAN BE PROCESSED VIA PAYPAL/ WESTERN UNION/ BANK TRANSFER.</li>
                            <li>3. AFTER THE PAYMENT IS COMPLETED, A VOUCHER WILL BE SENT TO YOUR EMAIL WITHIN 12HR AFTER WE RECEIVE YOUR PAYMENT SLIP. </li>
                            <li>4. FULL REFUND WHEN WE HAVE TO CANCEL THE TRIP DUE TO BAD WEATHERS OR ETC REASON. </li>
                            <li>  50% REFUND WHEN THE GUEST CANCEL OR CHANGE DATE LESS THAN 30 DAYS IN ADVANCE<br>
                                NO REFUND WHEN THE GUEST CANCEL OR CHANGE DATE LESS THAN 14 DAYS IN ADVANCE.</li>
                        </ul>

                        <p>***To cancel or revise your booking, please email us your request and refer to your voucher number***</p>
                    </div>
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
{{--<div class="wrap center">--}}
{{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
{{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
{{--dates for our convenience - brilliant!” </p>--}}
{{--<p>- Johnatan Davidson</p>--}}
{{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>