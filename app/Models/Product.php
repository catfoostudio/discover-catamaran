<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function tourcompanies()
    {
        return $this->belongsToMany('App\Models\Tourcompany','tourcompany_product')->using(TourcompanyProduct::class)->withPivot('id','status','remark');
    }

    public function units()
    {
        return $this->belongsToMany('App\Models\Unit','product_unit')->withPivot('id','price','remark');
    }

    public function zones()
    {
    	return $this->hasMany('App\Models\Zone');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Image','image_product');
    }

    public function totalprice()
    {
        
    }

    public function totalPriceDiscount($product_id, $tourcompany_id, $adult, $child, $infant, $travel_date,$discountcode)
    {
        $total = 0;

        $product = Product::find($product_id);

        //Retrieve discount price by Discount code
        $code = null;
        if(!empty($discountcode)){
            $code = Code::where('code', $discountcode)->where('status',1)->whereDate('startdate','<=',Carbon::now())->whereDate('enddate', '>=',Carbon::now())->first();

            if(empty($code))
            {
                return redirect('premiumdaytrip/'.$product->id)->withErrors(['discount' => 'Discount Code Incorrect or Expired'])->withInput();
            }
        }

        return $total;
    }

}
