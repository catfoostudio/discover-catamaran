@extends('cfadmin')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        {{--<div class="row">--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-aqua">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Customers</h3>--}}
                        {{--<p>ข้อมูลลูกค้า</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/photo/album/1') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-green">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Hotels</h3>--}}
                        {{--<p>ข้อมูลโรงแรม</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-stats-bars"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/photo/album/2') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-yellow">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Tickets</h3>--}}

                        {{--<p>ข้อมูลตั๋วเดินทาง</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-person-add"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/photo/album/3') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-red">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Restaurants</h3>--}}
                        {{--<p>ข้อมูลร้านอาหาร</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-pie-graph"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/photo/album/4') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-black">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Tours</h3>--}}
                        {{--<p>ข้อมูลทัวร์</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-pie-graph"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/country') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-maroon">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Packages</h3>--}}
                        {{--<p>ข้อมูลแพคเกจ</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-pie-graph"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/country') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-sm-6 col-xs-12">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-purple">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>Vouchers</h3>--}}
                        {{--<p>ข้อมูลวอชเชอร์</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-pie-graph"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{ url('cfadmin/country') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
        {{--</div>--}}
        <!-- /.row -->

    </section>

</div>
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard2.js"></script>
@endsection
