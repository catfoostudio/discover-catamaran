<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cfadmin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function postAdminLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email|exists:users,email,role_id,1',
            'password' => 'required'
        ]);

        $credentials = $request->only( 'email', 'password' );

        if(Auth::attempt($credentials))
        {
            return redirect('/cfadmin');
        }
        else
        {
            // Your logic of invalid credentials.
            return 'Invalid Credentials';
        }
    }

    public function index()
    {
        return view('auth.login',[

        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('cfadmin/login');
    }
}
