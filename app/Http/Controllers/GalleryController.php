<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Image;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'gallery.cfadmin.index' : 'gallery.index';

        $galleries = Gallery::where('status' , 1)->get();

        return view($view, [
            'galleries' => $galleries,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'gallery.cfadmin.create' : 'gallery.index';

        return view($view, [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $this->validate($request, [
            'name' => 'required|max:191',
            'description' => 'required',
        ]);

        $gallery = new Gallery();
        $gallery->name = $input['name'];
        $gallery->description = $input['description'];
        $gallery->save();

        $request->session()->flash('alert-success', 'Gallery was successful created!');
        return redirect('cfadmin/gallery/'.$gallery->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($gallery)
    {
        //dd($gallery);
        $view = ($this->isAdminRequest()) ? 'gallery.cfadmin.detail' : 'gallery.detail';

        //$gallery = Gallery::find($gallery);
//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'gallery' => $gallery,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $gallery)
    {
        $input = $request->input();
        $this->validate($request, [
            'name' => 'required|max:191',
            'description' => 'required',
        ]);

        $gallery->name = $input['name'];
        $gallery->description = $input['description'];
        $gallery->save();

        $request->session()->flash('alert-success', "Gallery \"".$gallery->name."\"(".$gallery->id.") was successful updated!");
        return redirect('cfadmin/gallery/'.$gallery->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reorder()
    {
        $gallery = Gallery::find(1);
        return view('gallery.cfadmin.reordertest', [
            'gallery' => $gallery
        ]);
    }

    public function orderprocess(Request $request)
    {
        //dd($request->input()['ids']);
        $input = $request->input();
        $gallery = Gallery::find($input['galleryid']);
        $ids = explode(",", $input['ids']);
        for ($i = 0; $i < count($ids); $i++) {

            $image = Image::find($ids[$i]);
            $images[$image->id] = ['status' => 1, 'order' => $i+1];
        }

        $gallery->images()->sync($images);
        $gallery->save();
    }

    public function addgalleryimage(Request $request)
    {
        $input = $request->input();
        $gallery = Gallery::find($input['id']);

        // open file a image resource
        $img = \Intervention\Image\Facades\Image::make(asset($input['path']));
        $img->fit(800, 600);
        $filename = time().'_'.$gallery->id.'.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);

        $image = new Image();
        $image->path = 'crop_images/'.$filename;
        $image->status = 1;
        $image->save();

        $gallery->images()->attach($image->id, ['status' => 1, 'order' => $gallery->images()->count()]);
        $gallery->save();

        return $image->id;
    }

    public function removegalleryimage(Request $request)
    {
        $input = $request->input();

        $gallery = Gallery::find($input['galleryid']);
        $gallery->images()->detach($input['id']);
        $gallery->save();

    }
}
