<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tourcompany extends Model
{

    protected $appends = array('telfull','faxfull');

    protected function getTelfullAttribute()
    {
        if(empty($this->tel)){
            return 'No data';
        }
        else{
            return $this->tel_code.'-'.$this->tel;
        }

    }

    protected function getFaxfullAttribute()
    {
        if(empty($this->fax)){
            return 'No data';
        }
        else{
            return $this->fax_code.'-'.$this->fax;
        }

    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'tourcompany_product')->using(TourcompanyProduct::class)->withPivot('id','status','remark');
    }
}
