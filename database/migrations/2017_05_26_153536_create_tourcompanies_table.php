<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourcompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tourcompanies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tel_code')->nullable();
            $table->string('tel');
            $table->string('mobile_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax_code')->nullable();
            $table->string('fax')->nullable();
            $table->string('email');
            $table->string('facebook_url')->nullable();
            $table->string('description')->nullable();
            $table->string('line')->nullable();
            $table->string('other_contact')->nullable();
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('postcode')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tourcompanies');
    }
}
