<?php

namespace App\Http\Controllers;

use App\Models\Dclifestyle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'dclifestyle.cfadmin.index' : 'dclifestyle.index';

        $dclifestyles = Dclifestyle::where('status' , 1)->get();

        return view($view, [
            'dclifestyles' => $dclifestyles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'dclifestyle.cfadmin.create' : 'dclifestyle.index';

        return view($view, [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $this->validate($request, [
            'name' => 'required|max:191',
            'description' => 'required',
            'content' => 'required',
        ]);

        //dd($input);
        $dclifestyle = new Dclifestyle();
        $dclifestyle->name = $input['name'];
        $dclifestyle->description = $input['description'];
        $dclifestyle->content = $input['content'];
        $dclifestyle->image = $input['image'];
        $dclifestyle->type = $input['type'];
        $dclifestyle->status = 1;
        $dclifestyle->user_id = Auth::user()->id;
        $dclifestyle->save();

        $img = \Intervention\Image\Facades\Image::make(asset($input['image']));
        $time = time();
        //Original
        $img->fit(870, 580);
        $filename = $time.'_'.$dclifestyle->id.'.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);
        $dclifestyle->crop_image = 'crop_images/'.$filename;
        $dclifestyle->save();

        $request->session()->flash('alert-success', 'Gallery was successful created!');
        return redirect('cfadmin/dclifestyle/'.$dclifestyle->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($dclifestyle)
    {
        //
        $view = ($this->isAdminRequest()) ? 'dclifestyle.cfadmin.detail' : 'dclifestyle.detail';

        //dd($dclifestyle);
        return view($view, [
            'dclifestyle' => $dclifestyle,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dclifestyle)
    {
        //
        $input = $request->input();

        $this->validate($request, [
            'name' => 'required|max:191',
            'description' => 'required',
            'content' => 'required',
        ]);

        //dd($input);
        //dd($dclifestyle);
        $dclifestyle->name = $input['name'];
        $dclifestyle->description = $input['description'];
        $dclifestyle->content = $input['content'];
        $dclifestyle->image = $input['image'];
        $dclifestyle->type = $input['type'];
        $dclifestyle->status = 1;
        $dclifestyle->user_id = Auth::user()->id;
        $dclifestyle->save();

        $img = \Intervention\Image\Facades\Image::make(asset($input['image']));
        $time = time();
        //Original
        $img->fit(870, 580);
        $filename = $time.'_'.$dclifestyle->id.'.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);
        $dclifestyle->crop_image = 'crop_images/'.$filename;
        $dclifestyle->save();

        $request->session()->flash('alert-success', 'Gallery was successful created!');
        return redirect('cfadmin/dclifestyle/'.$dclifestyle->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($dclifestyle)
    {
        //
        $dclifestyle->status = 0;
        $dclifestyle->save();

        return  redirect("cfadmin/dclifestyle");
    }
}
