@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Invoice
                <small>#{{$invoice->id}} </small>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="#">Invoices</a></li>
                <li class="active">Invoice #{{ $invoice->id }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        Discover Catamaran

                        <small class="pull-right">Date: {{$invoice->created_at}}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <b>Status :</b> {!! $invoice->statusbadge  !!}
                    {{--From--}}
                    {{--<address>--}}
                        {{--<strong>Discover Catamaran</strong><br>--}}
                        {{--20/135 (Boat lagoon) M.2 <br>T.Kohkaew A.Muangphuket Phuket 83000<br>--}}
                        {{--Phone: +66(0)94-597-6111 / +66(0)94-597-6222 <br> +66(0)87-461-9555<br>--}}
                        {{--Email: discovercatamaran@gmail.com--}}
                    {{--</address>--}}
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    {{--To--}}
                    <address>
                        {{--<strong>{{$invoice->customer->name}}</strong><br>--}}
                        {{--{{$invoice->customer->address1}}<br>--}}
                        {{--Phone: {{$invoice->customer->tel}}<br>--}}
                        {{--Email: {{$invoice->customer->email}}--}}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>Invoice #{{$invoice->id}}</b><br>
                    <br>
                    {{--<b>Order ID:</b> 4F3S8J<br>--}}
                    <b>Payment
                        Due:</b> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$invoice->due_date)->format('d/m/Y H:i')}}<br>
                    {{--<b>Account:</b> 968-34567--}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Product</th>
                            {{--<th>Serial #</th>--}}
                            {{--<th>Description</th>--}}
                            <th>Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>
                                <b>Product:</b> {{ $invoice->voucher->product->name }}<br>
                                <b>Travel date:</b> {{ $invoice->voucher->traveldate }}<br>
                                <b>Issue to:</b> {{ $invoice->voucher->tourcompany->name }}</td>
                            {{--<td>455-981-221</td>--}}
                            {{--<td></td>--}}
                            <td>฿{{number_format($invoice->price,2)}}</td>
                        </tr>
                        <tr style="height: 37px;">
                            <td></td>
                            <td></td>
                            {{--<td>455-981-221</td>--}}
                            {{--<td></td>--}}
                            <td></td>
                        </tr>
                        <tr style="height: 37px;">
                            <td></td>
                            <td></td>
                            {{--<td>455-981-221</td>--}}
                            {{--<td></td>--}}
                            <td></td>
                        </tr>
                        <tr style="height: 37px;">
                            <td></td>
                            <td></td>
                            {{--<td>455-981-221</td>--}}
                            {{--<td></td>--}}
                            <td></td>
                        </tr>
                        <tr style="height: 37px;">
                            <td></td>
                            <td></td>
                            {{--<td>455-981-221</td>--}}
                            {{--<td></td>--}}
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
                    <p class="lead">Remark</p>
                    <textarea class="form-control" rows="5">{{$invoice->remark}}</textarea>
                    {{--<p class="lead">Payment Methods:</p>--}}
                    {{--<img src="../../dist/img/credit/visa.png" alt="Visa">--}}
                    {{--<img src="../../dist/img/credit/mastercard.png" alt="Mastercard">--}}
                    {{--<img src="../../dist/img/credit/american-express.png" alt="American Express">--}}
                    {{--<img src="../../dist/img/credit/paypal2.png" alt="Paypal">--}}

                    {{--<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">--}}
                    {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg--}}
                    {{--dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.--}}
                    {{--</p>--}}
                </div>
                <!-- /.col -->
                <div class="col-xs-6">
                    <p class="lead">Amount
                        Due {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$invoice->due_date)->format('d/m/Y H:i')}}</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td>฿{{number_format($invoice->price,2)}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Discount:</th>
                                <td>฿{{number_format($invoice->discount,2)}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Transfer:</th>
                                <td>฿{{number_format($invoice->transfer,2)}}</td>
                            </tr>
                            <tr>
                                <th style="width:50%">Fee(Omise add 3.5%):</th>
                                <td>฿{{number_format($invoice->fee,2)}}</td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td>฿{{number_format($invoice->grandtotal,2)}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    {!! Form::open(['method' => 'POST' , 'action' => ['InvoiceController@approve' , $invoice] , 'style' => 'display:inline;margin-right:15px;']) !!}
                    <button type="button" class="btn btn-success btn-approve-invoice"><i class="fa fa-check"></i> Approve</button>
                    {!! Form::close() !!}
                    {!! Form::open(['method' => 'POST' , 'action' => ['InvoiceController@reject' , $invoice] , 'style' => 'display:inline;margin-right:15px;']) !!}
                    <button type="button" class="btn btn-danger btn-reject-invoice"><i class="fa fa-times"></i> Reject</button>
                    {!! Form::close() !!}
                    {!! Form::open(['method' => 'POST' , 'action' => ['InvoiceController@confirm' , $invoice] , 'style' => 'display:inline']) !!}
                    <button type="button" class="btn btn-warning btn-agent-invoice"><i class="fa fa-check"></i> Confirm</button>
                    {!! Form::close() !!}
                    <a target="_blank"
                       href="{{ url('cfadmin/payment/invoice/' . $invoice->voucher->voucher_prefix . '/' . $invoice->voucher->voucher_no) }}">
                        <button type="button" class="btn btn-primary pull-right"><i class="fa fa-file-text"></i>
                            Preview Invoice
                        </button>
                    </a>
                    <a target="_blank" href="{{url('cfadmin/invoice/previewemail/'.$invoice->id)}}">
                        <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                            <i class="fa fa-envelope"></i> Preview Email
                        </button>
                    </a>
                </div>
            </div>
        </section>
        <!-- /.content -->
        <div class="clearfix"></div>
    </div>
    <div class="approve-modal">
        <div class="modal modal-success" id="confirm-approve-invoice-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-approve">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->
    <div class="approve-modal">
        <div class="modal modal-warning" id="confirm-agent-invoice-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-agent">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->
    <div class="approve-modal">
        <div class="modal modal-danger" id="confirm-reject-invoice-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-reject">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('plugins/lightbox/ekko-lightbox.js') }}"></script>
    <script>
        $(function () {

            $('.btn-approve-invoice').on('click', function () {
                $('#confirm-approve-invoice-modal').modal('show');
                $('#confirm-approve-invoice-modal').find('.modal-title').text('Warning!');
                $('#confirm-approve-invoice-modal').find('.modal-body').text('Are you sure you want to approve this invoice?');

                var form = $(this).parent();
                $('#confirm-approve').on('click', function () {
                    form.submit();
                });

            });

            $('.btn-agent-invoice').on('click', function () {
                var confirmInvoiceModal = $('#confirm-agent-invoice-modal');
                confirmInvoiceModal.modal('show');
                confirmInvoiceModal.find('.modal-title').text('Warning!');
                confirmInvoiceModal.find('.modal-body').text('Are you sure you want to confirm this invoice?');

                var form = $(this).parent();
                $('#confirm-agent').on('click', function () {
                    form.submit();
                });

            });

            $('.btn-reject-invoice').on('click', function () {
                $('#confirm-reject-invoice-modal').modal('show');
                $('#confirm-reject-invoice-modal').find('.modal-title').text('Warning!');
                $('#confirm-reject-invoice-modal').find('.modal-body').text('Are you sure you want to reject this invoice?');

                var form = $(this).parent();
                $('#confirm-reject').on('click', function () {
                    form.submit();
                });

            });
        });
    </script>
@endsection