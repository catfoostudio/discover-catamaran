<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('enquires', function (Blueprint $table) {
            $table->increments('id');
            $table->date('departdate');
            $table->date('traveldate');
            $table->string('name');
            $table->string('tel');
            $table->string('email');
            $table->string('line');
            $table->string('wechatid');
            $table->text('enquire')->nullable();
            $table->smallInteger('adult');
            $table->smallInteger('child');
            $table->smallInteger('infant');
            $table->tinyInteger('transfer');
            $table->string('zone');
            $table->string('hotel');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('enquires');
    }
}
