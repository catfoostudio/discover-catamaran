<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Hotel;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'hotel.cfadmin.index' : 'home.index';

        $hotels = Hotel::where('status' , 1)->get();
        return view($view, [
            'hotels' => $hotels,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $view = ($this->isAdminRequest()) ? 'hotel.cfadmin.create' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'countries' => $countries,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
            'tel' => 'required',
            'address1' => 'required',
        ]);

        $input = $request->input();

        $hotel = new Hotel();

        //hotel info. Req.
        $hotel->name = $input['name'];
        $hotel->address1 = $input['address1'];
        $hotel->tel = $input['tel'];

        //Not Req.
        $hotel->country_id = $input['country'];
        if (!empty($input['other_contact']))
            $hotel->other_contact = $input['other_contact'];
        if (!empty($input['description']))
            $hotel->description = $input['description'];

        //Reservation info.
        $hotel->contact_name = $input['contact_name'];
        $hotel->contact_mobile = $input['contact_mobile'];
        $hotel->contact_line = $input['contact_line'];
        $hotel->email = $input['email'];

        $hotel->status = 1;
        $hotel->save();

        $request->session()->flash('alert-success', 'Add hotel was successful updated!');
        return redirect("cfadmin/hotel");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hotel)
    {
        //
        $view = ($this->isAdminRequest()) ? 'hotel.cfadmin.detail' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'hotel' => $hotel,
            'countries' => $countries,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $hotel)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
            'tel' => 'required',
            'address1' => 'required',
        ]);

        $input = $request->input();

        //Hotel info. Req.
        $hotel->name = $input['name'];
        $hotel->tel = $input['tel'];
        $hotel->address1 = $input['address1'];

        //not Req.
        $hotel->country_id = $input['country'];
        if (!empty($input['other_contact']))
            $hotel->other_contact = $input['other_contact'];
        if (!empty($input['description']))
            $hotel->description = $input['description'];

        //Reservation info.
        $hotel->contact_name = $input['contact_name'];
        $hotel->contact_mobile = $input['contact_mobile'];
        $hotel->contact_line = $input['contact_line'];
        $hotel->email = $input['email'];
        $hotel->status = 1;
        $hotel->save();

        $request->session()->flash('alert-success', 'Hotel was successful updated!');
        return redirect("cfadmin/hotel");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $hotel)
    {
        //
        $hotel->status = 0;
        $hotel->save();

        $request->session()->flash('alert-success', 'Hotel was successful updated!');
        return redirect("cfadmin/hotel");

    }
}
