<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Private Charter</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>Private Charter</h1>

        </div>
    </header>
    <!-- //Intro -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row" style="display: flex;flex-flow: row wrap;">
            @foreach($products as $product)
                <!-- OneHalf -->
                <div class="one-third" style="display: flex;">
                    <div class="box-white" style="padding: 0;">
                       <img src="{{ count($product->images) > 0 ? explode('.',$product->images()->orderBy('order')->first()->path)[0].'_800x600.jpg': 'images/image-soon.png' }}" />
                        <div class="full-width" style="padding: 0px 20px 0px 10px;overflow: hidden;">
                        <h2 style="padding: 5px 0px 0px 0px;height: 40px;">{{ $product->name }}</h2>
                        </div>
                        <div class="full-width" style="padding: 0px 0px 0px 10px;">
{{--                            <span style="font-size: 13px">{{ !empty($product->destination) ? $product->destination : '??? + ???'}}</span>--}}
                            <span style="font-size: 13px">Starting Price: {{ number_format($product->units()->orderBy('price')->first()->pivot->price) }} Baht</span>
                        </div>
                        <div class="full-width" style="padding: 10px;">
                        <div style="border-top: 1px dotted #d4d4d4;padding-top: 10px;" >{{ !empty($product->max_pax) ? 'MAX '.$product->max_pax.' PAX' : 'MAX 0 PAX'}}
                            <a href="{{ url('privatecharter/'.$product->id) }}" title="more info" class="button gold smaill" style="float:right;">More</a>
                        </div></div>
                    </div>
                </div>
                <!-- //OneHalf -->
                @endforeach
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- Content -->
    {{--<div class="content boxed grid4">--}}
        {{--@foreach($products as $product)--}}
        {{--<!-- Item -->--}}
        {{--<article class="one-three">--}}
            {{--<figure class="one-half heightfix"><a href="{{ url('premiumdaytrip/'.$product->id) }}">--}}
                    {{--<img src="{{ !empty($product->images()->orderBy('order')->get()->first()['path']) ? asset($product->images()->orderBy('order')->get()->first()['path']) : asset('uploads/img.jpg') }}" alt="destination" />--}}
                {{--</a></figure>--}}
            {{--<div class="one-half heightfix">--}}
                {{--<div class="text">--}}
                    {{--<h3><a href="destinations-single.html">{{ $product->name }}</a></h3>--}}
                    {{--<p>From the top to the bottom of the Caribbean, Sailor has it covered from St.Lucia, Antigua, Grenada, St.Martin and British Virgin Islands.</p>--}}
                    {{--<a href="{{ url('premiumdaytrip/'.$product->id) }}" class="more" title="Read more">Read more</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</article>--}}
        {{--@endforeach--}}
        {{--<!-- //Item -->--}}

    {{--</div>--}}
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>