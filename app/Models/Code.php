<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    //

    public function voucherprefix()
    {
        return $this->belongsTo('App\Models\VoucherPrefix', 'voucher_prefix_id');
    }

    public function discounttype()
    {
        return $this->belongsTo('App\Models\Discounttype', 'discounttype_id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }

    public function optional()
    {
        return $this->belongsTo('App\Models\Optional', 'optional_id');
    }
}
