@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Product
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/product') }}">Product</a></li>
                <li class="active">Add Product</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>ขออภัย!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'POST' , 'action' => ['ProductController@store']]) !!}
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="radio" name="product_type" value="1" checked>
                                        <label for="inputEmail3">Online Stock</label>
                                        <input type="radio" name="product_type" value="2">
                                        <label for="inputEmail3">Online Schedule</label>
                                        <input type="radio" name="product_type" value="3">
                                        <label for="inputEmail3">No Stock</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="per_head" value="1">
                                        <label for="inputEmail3">Per head</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Name</label>
                                        <input type="text" class="form-control" name="name"  value="{{ old('name') }}">
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Max Pax</label>
                                        <input type="text" class="form-control" name="max_pax"  value="{{ old('max_pax') }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Destination</label>
                                        <input type="text" class="form-control" name="destination"  value="{{ old('destination') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Tour Company</label>
                                        <select class="form-control select2" style="width: 100%;" name="tourcompany[]" multiple="multiple">
                                            @foreach($tourcompanies as $tourcompany)
                                                <option
                                                    @for($i = 0; $i < count(old('tourcompany')); $i++)
                                                    @if(old('tourcompany.'.$i) == $tourcompany->id)
                                                        selected="selected"
                                                    @endif
                                                    @endfor
                                                    data-custom="{{ $tourcompany->name }}"  value="{{ $tourcompany->id }}">{{ $tourcompany->name }} [{{ $tourcompany->tel_code.$tourcompany->tel }}] [{{ $tourcompany->fax_code.$tourcompany->fax }}]</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        {{--<label for="inputEmail3">Zone</label>--}}
                                        {{--<select class="form-control select2" style="width: 100%;" name="zone[]" multiple="multiple">--}}
                                            {{--@foreach($zones as $zone)--}}
                                                {{--<option data-custom="{{ $zone->name }}"  value="{{ $zone->id }}">{{ $zone->name }} [Time {{  date("H:i", strtotime($zone->pickuptime)) }}] [Price {{ $zone->price }}]</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="unitsection">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Unit</label>
                                        <button type="button" style="margin-left: 5px;" id="btn_add_unit">Add Unit</button>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Price</label>

                                    </div>
                                </div>
                                @if(!empty( old('unit')))
                                    @for($i = 0; $i < count(old('unit')); $i++)
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control select2" style="width: 100%;" name="unit[]">
                                                    @foreach($units as $unit)
                                                        <option  {{ old('unit.'.$i) == $unit->id ? 'selected="selected"' : ''  }}  data-custom="{{ $unit->name }}"  value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="price[]"  value="{{ old('price.'.$i, 1) }}">
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                            <div class="form-group" id="zonesection">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="inputEmail3">Zone</label>
                                        <button type="button" style="margin-left: 5px;" id="btn_add_zone">Add Zone</button>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputEmail3">Price</label>

                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputEmail3">Time</label>
                                    </div>
                                </div>
                                @if(!empty( old('zone')))
                                    @for($i = 0; $i < count(old('zone')); $i++)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="zone_name[]"  value="{{ old('zone_name.'.$i, 1) }}">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="zone_price[]"  value="{{ old('zone_price.'.$i, 1) }}">
                                            </div>
                                            <div class="col-md-4">
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>
                                                            <input type="text" class="form-control timepicker" name="zone_time[]" value="{{ old('zone_time.'.$i) }}">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Short description</label>
                                        <textarea class="form-control" name="short_description"  >{{ old('short_description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Description</label>
                                        <textarea class="form-control" name="description"  >{{ old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="form-group" id="gallery_section" data-index="0">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<label for="inputEmail3">Gallery</label> <input type="button" value="Add" id="btn_add_image" />--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Remark</label>
                                        <textarea class="form-control" name="remark" >{{ old('remark') }}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Add</button>
                            <a href="{{ url('cfadmin/product') }}"><button type="button" class="btn btn-default pull-right">Cancel</button></a>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({
                tags: true,
                templateSelection: function (selection) {
                    return jQuery(selection.element).data('custom');
                },
                createTag: function(params) {
                    return undefined;
                }
            });

            var options = {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            };
            $('.lfm').filemanager('image');
            $('textarea[name="description"]').ckeditor(options);
            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                    {
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        startDate: moment().subtract(29, 'days'),
                        endDate: moment()
                    },
                    function (start, end) {
                        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }
            );

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                minuteStep: 1,
            });

            $("#btn_add_unit").on('click', function () {
                $('#unitsection').append('<div class="row"> <div class="col-md-6"> <select class="form-control select2" style="width: 100%;" name="unit[]">@foreach($units as $unit)<option data-custom="{{ $unit->name }}"  value="{{ $unit->id }}">{{ $unit->name }}</option>@endforeach</select> </div> <div class="col-md-5"> <input type="text" class="form-control" name="price[]"  value="{{ old('name') }}"> </div><div class="col-md-1"> <button type="button" class="btn btn-box-tool remove-row"><i class="fa fa-remove"></i></button> </div> </div>');
                $('.remove-row').on('click', function () {
                    $(this).parent().parent().remove();
                });
                $(".select2").select2({
                    tags: true,
                    templateSelection: function (selection) {
                        return jQuery(selection.element).data('custom');
                    },
                    createTag: function(params) {
                        return undefined;
                    }
                });
            });


            $("#btn_add_zone").on('click', function () {
                $('#zonesection').append('<div class="row"> <div class="col-md-4"> <input type="text" class="form-control" name="zone_name[]"  value="{{ old('zone') }}"> </div> <div class="col-md-4"> <input type="text" class="form-control" name="zone_price[]"  value="{{ old('zone') }}"> </div><div class="col-md-3"> <div class="bootstrap-timepicker"> <div class="form-group"> <div class="input-group"> <div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div> <input type="text" class="form-control timepicker" name="zone_time[]" > </div> </div> </div> </div><div class="col-md-1"> <button type="button" class="btn btn-box-tool remove-row"><i class="fa fa-remove"></i></button> </div>');
                $('.remove-row').on('click', function () {
                    $(this).parent().parent().remove();
                });
                $(".select2").select2({
                    tags: true,
                    templateSelection: function (selection) {
                        return jQuery(selection.element).data('custom');
                    },
                    createTag: function(params) {
                        return undefined;
                    }
                });

                $(".timepicker").timepicker({
                    showInputs: false,
                    minuteStep: 1,
                });
            });

            $('#btn_add_image').on('click' , function () {
//                alert($('#gallery_section').data('index'));
                $('#gallery_section').data('index', $('#gallery_section').data('index')+1);

//                alert($('#gallery_section').data('index'));

                $('#gallery_section').append('<div class="row">\n' +
                    '                                    <div class="col-md-12">\n' +
                    '                                        <div class="input-group">\n' +
                    '                                           <span class="input-group-btn">\n' +
                    '                                             <a data-input="thumbnail'+$('#gallery_section').data('index')+'" data-preview="holder" class="btn btn-primary lfm">\n' +
                    '                                               <i class="fa fa-picture-o"></i> Choose\n' +
                    '                                             </a>\n' +
                    '                                           </span>\n' +
                    '                                            <input id="thumbnail'+$('#gallery_section').data('index')+'" class="form-control" type="text" name="filepath[]">\n' +
                    '                                        </div>\n' +
                    //                    '                                        <img id="holder2" style="margin-top:15px;max-height:100px;">\n' +
                    '                                    </div>\n' +
                    '                                </div>');
                $('.lfm').filemanager('image');
            });

            $('input[name="per_head"]').on('change' , function () {
                if(this.checked){
                    $('#unitsection').html('<div class="row">\n' +
                        '                                    <div class="col-md-6">\n' +
                        '                                        <label for="inputEmail3">Unit</label>\n' +
                        '                                    </div>\n' +
                        '                                    <div class="col-md-6">\n' +
                        '                                        <label for="inputEmail3">Price</label>\n' +
                        '\n' +
                        '                                    </div>\n' +
                        '                                </div>');
                    $('#unitsection').append('<div class="row"> <div class="col-md-6"> <select class="form-control select2" style="width: 100%;" name="unit[]"><option data-custom="Adult"  value="1">Adult</option></select> </div> <div class="col-md-6"> <input type="text" class="form-control" name="price[]"  value="{{ old('name') }}"> </div><div class="col-md-1"> </div> </div>');
                    $('#unitsection').append('<div class="row"> <div class="col-md-6"> <select class="form-control select2" style="width: 100%;" name="unit[]"><option data-custom="Child"  value="2">Child</option></select> </div> <div class="col-md-6"> <input type="text" class="form-control" name="price[]"  value="{{ old('name') }}"> </div><div class="col-md-1"> </div> </div>');
                    $('#unitsection').append('<div class="row"> <div class="col-md-6"> <select class="form-control select2" style="width: 100%;" name="unit[]"><option data-custom="Infant (0-3yr)"  value="3">Infant (0-3yr)</option></select> </div> <div class="col-md-6"> <input type="text" class="form-control" name="price[]"  value="{{ old('name') }}"> </div><div class="col-md-1"> </div> </div>');
                }
                else{
                    $('#unitsection').html('<div class="row">\n' +
                        '                                    <div class="col-md-6">\n' +
                        '                                        <label for="inputEmail3">Unit</label>\n' +
                    '                                        <button type="button" style="margin-left: 5px;" id="btn_add_unit">Add Unit</button>\n' +
                    '                                    </div>\n' +
                        '                                    <div class="col-md-6">\n' +
                        '                                        <label for="inputEmail3">Price</label>\n' +
                        '\n' +
                        '                                    </div>\n' +
                        '                                </div>');

                    $("#btn_add_unit").on('click', function () {
                        $('#unitsection').append('<div class="row"> <div class="col-md-6"> <select class="form-control select2" style="width: 100%;" name="unit[]">@foreach($units as $unit)<option data-custom="{{ $unit->name }}"  value="{{ $unit->id }}">{{ $unit->name }}</option>@endforeach</select> </div> <div class="col-md-5"> <input type="text" class="form-control" name="price[]"  value="{{ old('name') }}"> </div><div class="col-md-1"> <button type="button" class="btn btn-box-tool remove-row"><i class="fa fa-remove"></i></button> </div> </div>');
                        $('.remove-row').on('click', function () {
                            $(this).parent().parent().remove();
                        });
                        $(".select2").select2({
                            tags: true,
                            templateSelection: function (selection) {
                                return jQuery(selection.element).data('custom');
                            },
                            createTag: function(params) {
                                return undefined;
                            }
                        });
                    });
                }
            });
        });
    </script>
@endsection
