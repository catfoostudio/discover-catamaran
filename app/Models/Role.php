<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    public function isAdmin()
    {
        return $this->code == 'admin' ? true : false; // this looks for an admin column in your users table
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
