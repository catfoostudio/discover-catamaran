<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    //
    protected $table = 'product_unit';

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }
}
