<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $appends = array('customername', 'hotelname' , 'statustext' , 'statusbadge');

    protected $guarded = [];

    public function getCustomernameAttribute()
    {
        if(!empty($this->customer))
            return $this->customer->name;
        else
            return 'No customer';
    }

    public function getStatusTextAttribute()
    {
        $string = '';

        if($this->status == 1){
            $string = 'Await';
        }
        elseif($this->status == 2){
            $string = 'Confirmed';
        }
        elseif($this->status == 3){
            $string = 'Completed';
        }
        elseif($this->status == 4){
            $string = 'Cancelled';
        }
        elseif($this->status == 5){
            $string = 'No Show';
        }
        elseif($this->status == 6){
            $string = 'No Launch';
        }
        elseif($this->status == 7){
            $string = 'Payment Pending';
        }
        elseif($this->status == 8){
            $string = 'Payment Pending Over Due';
        }
        elseif($this->status == 9){
            $string = 'Payment Completed';
        }
        elseif($this->status == 10){
            $string = 'Payment Review';
        }
        else{
            $string = 'Error Status';
        }

        return $string;
    }

    public function getStatusBadgeAttribute()
    {
        if($this->status == 1){
            $string = '<span class="label label-warning" style="font-size: 100%;font-weight: normal;">Await</span>';
        }
        elseif($this->status == 2){
            $string = '<span class="label label-success" style="font-size: 100%;font-weight: normal;">Confirmed</span>';
        }
        elseif($this->status == 3){
            $string = '<span class="label label-success" style="font-size: 100%;font-weight: normal;">Completed</span>';
        }
        elseif($this->status == 4){
            $string = '<span class="label label-danger" style="font-size: 100%;font-weight: normal;">Cancelled</span>';
        }
        elseif($this->status == 5){
            $string = '<span class="label label-success" style="font-size: 100%;font-weight: normal;">No Show</span>';
        }
        elseif($this->status == 6){
            $string = '<span class="label label-danger" style="font-size: 100%;font-weight: normal;">No Launch</span>';
        }
        elseif($this->status == 7){
            $string = '<span class="label label-warning" style="font-size: 100%;font-weight: normal;">Payment Pending</span>';
        }
        elseif($this->status == 8){
            $string = '<span class="label label-danger" style="font-size: 100%;font-weight: normal;">Payment Pending Over Due</span>';
        }
        elseif($this->status == 9){
            $string = '<span class="label label-success" style="font-size: 100%;font-weight: normal;">Payment Completed</span>';
        }
        elseif($this->status == 10){
            $string = '<span class="label label-info" style="font-size: 100%;font-weight: normal;">Payment Review</span>';
        }
        else{
            $string = 'Error Status';
        }

        return $string;
    }

//    public function getUnitsAttribute()
//    {
//        /*$string = '';
//        foreach ($this->voucherproductunits as $u){
//            $productunit = ProductUnit::find($u->product_unit_id);
//            $unit = Unit::find($productunit->unit_id);
//            $string .= $unit->name.':'.$u->amount.', ';
//        }
//        return $string;*/
//        return 'work in progress';
//    }

    public function getHotelnameAttribute()
    {
        if(!empty($this->hotel))
            return $this->hotel->name;
        else
            return 'No hotel';
    }

    //
    public function stock()
    {
        return $this->belongsTo('App\Models\Stock');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function tourcompany()
    {
        return $this->belongsTo('App\Models\Tourcompany');
    }

    public function voucherproductunits()
    {
        return $this->belongsToMany('App\Models\VoucherProductUnit' , 'voucher_product_unit');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function charge()
    {
        return $this->hasOne('App\Models\Charge');
    }

    public function code()
    {
        return $this->belongsTo('App\Models\Code');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }
}
