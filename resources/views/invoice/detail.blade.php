<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body >
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Discover Catamaran
                    <small class="pull-right">Date: {{$invoice->created_at}}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>Discover Catamaran</strong><br>
                    20/135 (Boat lagoon) M.2 <br>T.Kohkaew A.Muangphuket Phuket 83000<br>
                    Phone: +66(0)94-597-6111 / +66(0)94-597-6222 <br> +66(0)87-461-9555<br>
                    Email:  discovercatamaran@gmail.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{$invoice->customer->name}}</strong><br>
                    {{$invoice->customer->address1}}<br>
                    Phone: {{$invoice->customer->tel}}<br>
                    Email: {{$invoice->customer->email}}
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #{{$invoice->id}}</b><br>
                <br>
                {{--<b>Order ID:</b> 4F3S8J<br>--}}
                <b>Payment Due:</b> {{\Carbon\Carbon::createFromFormat('Y-m-d',$invoice->due_date)->format('d/m/Y')}}<br>
                {{--<b>Account:</b> 968-34567--}}
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        {{--<th>Serial #</th>--}}
                        {{--<th>Description</th>--}}
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>{{$invoice->name}}</td>
                        {{--<td>455-981-221</td>--}}
                        {{--<td></td>--}}
                        <td>฿{{number_format($invoice->price,2)}}</td>
                    </tr>
                    <tr style="height: 37px;">
                        <td></td>
                        <td></td>
                        {{--<td>455-981-221</td>--}}
                        {{--<td></td>--}}
                        <td></td>
                    </tr>
                    <tr style="height: 37px;">
                        <td></td>
                        <td></td>
                        {{--<td>455-981-221</td>--}}
                        {{--<td></td>--}}
                        <td></td>
                    </tr>
                    <tr style="height: 37px;">
                        <td></td>
                        <td></td>
                        {{--<td>455-981-221</td>--}}
                        {{--<td></td>--}}
                        <td></td>
                    </tr>
                    <tr style="height: 37px;">
                        <td></td>
                        <td></td>
                        {{--<td>455-981-221</td>--}}
                        {{--<td></td>--}}
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods:</p>
                <img src="../../dist/img/credit/visa.png" alt="Visa">
                <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="../../dist/img/credit/american-express.png" alt="American Express">
                <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                {{--<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">--}}
                {{--Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg--}}
                {{--dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.--}}
                {{--</p>--}}
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">Amount Due {{\Carbon\Carbon::createFromFormat('Y-m-d',$invoice->due_date)->format('d/m/Y')}}</p>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>฿{{number_format($invoice->price,2)}}</td>
                        </tr>
                        {{--<tr>--}}
                        {{--<th>Tax (9.3%)</th>--}}
                        {{--<td>$10.34</td>--}}
                        {{--</tr>--}}
                        {{--<tr>--}}
                        {{--<th>Shipping:</th>--}}
                        {{--<td>$5.80</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <th>Total:</th>
                            <td>฿{{number_format($invoice->price,2)}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a onclick="window.print();" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
                </button>
                {{--<button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">--}}
                    {{--<i class="fa fa-download"></i> Generate PDF--}}
                {{--</button>--}}
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
