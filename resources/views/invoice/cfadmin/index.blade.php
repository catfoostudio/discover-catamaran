@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Invoices
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                {{--<li><a href="#">โรงแรม</a></li>--}}
                <li class="active">Invoices</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Invoice list</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Invoice No.</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Due Date</th>
                                    <th>Create Date</th>
                                </tr>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Invoice No.</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Due Date</th>
                                    <th>Create Date</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Invoice No.</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Due Date</th>
                                    <th>Create Date</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ url('cfadmin/invoice/'.$invoice->id) }}"><button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button></a>
                                                {!! Form::open(['method' => 'DELETE' , 'action' => ['InvoiceController@destroy' , $invoice] , 'style' => 'display:inline', 'id' => 'content_'.$invoice->id]) !!}
                                                {!! Form::hidden('invoice_id' , $invoice->id) !!}
                                                <button type="button" class="btn btn-danger btn-flat btn-delete-invoice" data-invoiceid="{{ $invoice->id }}"><i class="fa fa-trash"></i></button>
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                        <td>{{ $invoice->id }}</td>
                                        <td>{{ $invoice->voucher->voucher_prefix.'-'.str_pad($invoice->voucher->voucher_no,4,"0",STR_PAD_LEFT) }}</td>
                                        <td>{{ $invoice->payment }}</td>
                                        <td>{!! $invoice->StatusBadge !!}</td>
                                        <td>{{ number_format($invoice->grandtotal,2) }}</td>
                                        <td>{{ $invoice->due_date }}</td>
                                        <td>{{ $invoice->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->

                    </div>
                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="delete-modal">
        <div class="modal modal-danger" id="confirm-delete-invoice-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-delete">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('plugins/lightbox/ekko-lightbox.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {

            $('.btn-delete-invoice').on('click', function () {
                $('#confirm-delete-invoice-modal').modal('show');
                $('#confirm-delete-invoice-modal').find('.modal-title').text('Warning!');
                $('#confirm-delete-invoice-modal').find('.modal-body').text('Are you sure you want to delete this record?');

                var form = $(this).parent();
                $hotel_id = $(this).data('hotelid');
                $('#confirm-delete').on('click' , function () {
                    form.submit();
                });

                console.log();

            });

            // Setup - add a text input to each header cell
            $('#example1 thead tr:eq(1) th').each( function () {
                var title = $('#example1 thead tr:eq(0) th').eq( $(this).index() ).text();
                if(title == 'ID')
                    $(this).html( '<input type="text" placeholder="'+title+'" style="width: 50px;" />' );
                else
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            } );

            var table = $('#example1').DataTable({
                orderCellsTop: true,

                "bScrollCollapse": true,
                "order": [[ 7, "desc" ]],
                "columnDefs": [
                    { width: 70, targets: 0 },
                    {
                        "targets": [ 1 ],
                        "visible": false
                    }
                ]
            });

            // Apply the search
            table.columns().every(function (index) {
                console.log(index);
                $('#example1 thead tr:eq(1) th:eq(' + index + ') input').on('keyup change', function () {

                    table.column($(this).parent().index() + ':visible')
                        .search(this.value)
                        .draw();
                });
            });

            $('#example1 tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#example1 tbody').on('dblclick', 'tr', function () {

                var data = table.row( this ).data();
                window.location = '{{ url('cfadmin/invoice/') }}/'+data[1];
//                alert( 'You clicked on '+data[0]+'\'s row' );
            } );

//            $('#button').click( function () {
//                table.row('.selected').remove().draw( false );
//            } );




            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
        });
    </script>

@endsection
