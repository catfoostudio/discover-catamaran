<?php

namespace App\Http\Controllers;

use App\Exports\VouchersExport;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $users = User::all();
        return view('report.cfadmin.index', [
            'users' => $users,
            'products' => $products,
        ]);

    }

    public function report(Request $request)
    {
        $input = $request->input();

        $user_id = User::find($input['user'])->id;
        $product_id = User::find($input['product'])->id;

        if($input['type'] == 1){
            return (new VouchersExport)->specific($input['month'],$input['year'],$user_id,$product_id)->download('voucher.xlsx');
        }
        else{
            return (new VouchersExport)->period($input['start'],$input['end'],$user_id,$product_id)->download('voucher.xlsx');
        }

    }

    public function specific(Request $request){

        $input = $request->input();

        return (new VouchersExport)->specific($input['month'],$input['year'],$input['prefix'])->download('voucher.xlsx');
    }

    public function period(Request $request)
    {
        $input = $request->input();
        return (new VouchersExport)->period($input['start'],$input['end'],$input['prefix'])->download('voucher.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
