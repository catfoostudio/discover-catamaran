<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Voucher;
use App\Models\VoucherPrefix;
use App\Models\VoucherProductUnit;
use App\Models\Zone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class EnquireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /*
     * Check Reservation, collect data to session & Cal total price. Wait customer to confirm.
     * Only product Premium day trip with per head unit(Adult, Child and Infant)
     */
    public function confirm(Request $request)
    {

        //Validate form data
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
            'travel_date' => 'required|after:'.date(Carbon::yesterday()),
            'name' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'adult' => 'required|numeric',
            'child' => 'required|numeric',
            'infant' => 'required|numeric',
            'product' => 'required',
            'tourcompany' => 'required',
            'terms' => 'required',
        ]);

        $input = $request->input();

        /*************************************************/
        $product = Product::find($input['product']);
        $tourcompany_id = $input['tourcompany'];
        $adult = $input['adult'];
        $child = $input['child'];
        $infant = $input['infant'];
        $travel_date = $input['travel_date'];
        $paymentType = $input['payment']; //credit, bank, wechat
        $discountCode = $input['discount'];

        //Sum all seat (Adult + child + infant)
        $totalunitamount = 0;
        $totalprice = 0;
        $adultProductUnit = null;
        $childProductUnit = null;
        $infantProductUnit = null;
        $fee = 3.5;
        $invoiceDays = null;
        $grandtotalprice = 0;
        $fee_price = 0;
        $transfer_price = 0;
        $discountByType = 0;
        $discountOverall = 0;

        if ($product->per_head != 0) {
            $totalunitamount = $adult + $child + $infant;
            $adultProductUnit = Product::has('units')->find($product->id)->units->find(1)->pivot;
            $adultprice = $adultProductUnit->price;

            $childProductUnit = Product::has('units')->find($product->id)->units->find(2)->pivot;
            $childprice = $childProductUnit->price;

            $infantProductUnit = Product::has('units')->find($product->id)->units->find(3)->pivot;
            $infantprice = $infantProductUnit->price;

            //Calculate totalprice before discount
            $totaladultprice = $adultprice * $adult;
            $totalchildprice = $childprice * $child;
            $totalinfant = $infantprice;
            $totalprice = $totaladultprice + $totalchildprice + $totalinfant;
        }

        //Retrieve discount price by Discount code
        $code = null;
        if (!empty($discountCode)) {
            $code = Code::where('code', $discountCode)->where('status', 1)->whereDate('startdate', '<=', Carbon::now())->whereDate('enddate', '>=', Carbon::now())->first();

            if (empty($code)) {
                //If code is not available or not found redirect and return error to customer
                return Redirect::back()->withErrors(['discount' => 'Discount Code Incorrect or Expired'])->withInput();

            } else if ($totalprice < $code->min_purchase) {
                //If promocode has minimum purchase require check first.
                return Redirect::back()->withErrors(['discount' => 'Your purchase not meet minimum purchase to use this discount code.'])->withInput();

            } else {
                //If use promo code. Rrefix is change to prefix that promo code setting.
                $prefix = $code->voucherprefix->name;
                //If optional is set on is promo code get number of invoice days that promocode pick
                if (!empty($code->optional)) {
                    $invoiceDays = $code->optional->number_value;
                }

                //Calculate DAY TRIP discount per type
                $discountByType = ($code->adult_value * $adult) + ($code->child_value * $child);
                $totalPriceAfterDiscountByType = $totalprice - $discountByType;

                //Calculate overall discount
                //This will discount the price after deduct price per type then will calculate this after
                //get total price after discount per type already which if no discount per type is the same value as totalprice
                $discountOverall = 0;
                if ($code->discounttype->type_value == 'PERCENT') {
                    //Calculate in percent from total that above explain
                    $discountOverall = $totalPriceAfterDiscountByType * $code->value / 100;
                } else {
                    //If just a value TH BAHT just grab that value
                    $discountOverall = $code->value;
                }

                //If setting max discount should check first before deduct from totalprice
                //This check will apply only DISCOUNT VALUE. But discount per type is not check yet.
                if ($code->max_discount != 0) {
                    $discountOverall = ($discountOverall > $code->max_discount) ? $code->max_discount : $discountOverall;
                }

                //Apply discount that we already calculate again to totalprice that we discoount by per type before this.
                $totalafterdiscount = $totalPriceAfterDiscountByType - $discountOverall;

            }

        } else {
            //No discount just grab totalprice from base
            $totalafterdiscount = $totalprice;
        }

        if ($product->premium_day_trip_flag == 1) {

            $tourproduct = Product::has('tourcompanies')->find($product->id)->tourcompanies->find($tourcompany_id)->pivot;
            $stock = $tourproduct->stocks()->where('reserved_date', Carbon::createFromFormat('Y-m-d', $travel_date)->toDateString())->first();

            //Stock is initialized and available
            if (!empty($stock)) {

                //seat that customer want to reserved (adult +child+ infant)
                $seat = $totalunitamount;

                //check stock seat is enough seat for new reservation
                if ($stock->max_seat >= ($stock->remaining_seat + $seat)) {

                    if ($input['transfer'] != 'notf') {
                        //If customer choose with transfer setting detail and price here.
                        $zone = Zone::find($input['zone']);
                        $transfer_price = $zone->price * ($adult + $child);
                    }

                    //save total discount value here
                    $discount_price = $discountByType + $discountOverall;

                    //Add Transfer cost to totalprice after discount
                    $totalafterdiscount += $transfer_price;


                    //Total price will add 3.5% for Omise If use credit card
                    if ($paymentType == 'credit') {
                        //Credit card use the totalprice after discount to calculate fee 3.5% for omise
                        $fee_price = ($totalafterdiscount * $fee / 100);
                        $grandtotalprice = $totalafterdiscount + $fee_price;

                    } else {
                        $fee_price = 0;
                        //Bank transfer and wechat pay no need to add omise fee price
                        $grandtotalprice = $totalafterdiscount;
                    }

                } else {
                    return Redirect::back()->withErrors(['Stock full!'])->withInput();
                }
            } else {
                return Redirect::back()->withErrors(['Stock not initial!'])->withInput();
            }
        }
        /*************************************************/

        //Clear before store new data
        $this->clearreservedsession();

        //Kept data in session
        $request->session()->put('in-process', true);
        $request->session()->put('travel_date', $input['travel_date']);
        $request->session()->put('name', $input['name']);
        $request->session()->put('tel', $input['tel']);
        $request->session()->put('email', $input['email']);
        $request->session()->put('adult', $input['adult']);
        $request->session()->put('child', $input['child']);
        $request->session()->put('infant', $input['infant']);
        $request->session()->put('product', $input['product']);
        $request->session()->put('tourcompany', $input['tourcompany']);
        $request->session()->put('line', $input['line']);
        $request->session()->put('wechat', $input['wechat']);
        $request->session()->put('transfer', $input['transfer']);
        $request->session()->put('zone', $input['zone']);
        $request->session()->put('hotel', $input['hotel']);
        $request->session()->put('payment', $input['payment']);
        $request->session()->put('discount', $input['discount']);
        $request->session()->put('totalprice', $totalprice);
        $request->session()->put('grandtotalprice', $grandtotalprice);
        $request->session()->put('discount_price', $discount_price);
        $request->session()->put('fee_price', $fee_price);
        $request->session()->put('transfer_price', $transfer_price);

        return view('confirm.confirm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
         *
         * DONT USE THIS FUNCTION RIGHT NOW
         *
         */

        $input = $request->input();

        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
             'adult' => 'required|numeric',
            'child' => 'required|numeric',
            'infant' => 'required|numeric',
            'name' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'travel_date' => 'required',
            'product' => 'required',
            'tourcompany' => 'required',
        ]);


//        $resultrecaptcha =

        $line = $input['line'];
        $wechat = $input['wechat'];
        $enquire = $input['enquire'];
        $travel_date = $input['travel_date'];
        $tel = $input['tel'];

        if(!empty($input['product'])) {
            $product = Product::find($input['product']);

            if ($product->per_head != 1) {
                $validate['unit_amount.*'] = 'required';
            }
        }

        //if($input['unit_id'])

        //$this->validate($request, $validate);

        //     dd($request->input());
        $customers = Customer::where('tel' , $tel)->get();

        if(count($customers) > 0){
            $customer = $customers->first();
//            $customer->remark = $input['name'];
        }
        else{
            $customer = new Customer();
            $customer->name = $input['name'];
            $customer->tel = $input['tel'];
            $customer->customer_code = '0';
            $customer->remark = '';
            $customer->status = 1;
            $customer->save();
        }

        $voucher = new Voucher();
        $voucher->confirmation_no = '';
        $voucher->confirm_by = '';
        $voucher->remark = '';
        $voucher->product_note = '';

        $voucher->adult = $input['adult'];
        $voucher->child = $input['child'];
        $voucher->infant = $input['infant'];

        $voucher->transfer = $input['transfer'];

        if($input['transfer'] == 'wot'){
            $voucher->pickuptime_text = 'To be confirmed';
        }
        else{
            $voucher->product_note = $input['hotel'].' : '.$input['zone'];
        }

//        if(!empty($input['pickuptime']))
//            $voucher->pickuptime = date("H:i", strtotime($input['pickuptime']));

        $voucher->traveldate = Carbon::createFromFormat('Y-m-d', $input['travel_date'])->toDateString();

        $voucher->product_id = $input['product'];
        $voucher->tourcompany_id = $input['tourcompany'];

        $voucher->user_id = Auth::user()->id;
        $voucher->customer_id = $customer->id;


        $voucher_prefix = VoucherPrefix::all()->first();
        $voucher->voucher_prefix = $voucher_prefix->name;
        $voucher_prefix->amount += 1;
        $voucher->voucher_no = $voucher_prefix->amount + 1;

        $voucher->status = 1;

        $product = Product::find($voucher->product_id);

        $totalunitamount = 0;
        if ($product->per_head == 0) {

            for ($i = 0; $i < count($input['unit_amount']); $i++) {
                //$units[$input['unit_id'][$i]] = ['status' => 1, 'price' => $input['price'][$i]];
                $totalunitamount += $input['unit_amount'][$i];
            }
        }
        else{
            $totalunitamount = $input['adult'] + $input['child'] + $input['infant'];
        }

        if ($product->premium_day_trip_flag == 1) {
            //$tourcompany = Tourcompany::findOrFail(1);
            //$tourproduct = $tourcompany->products()->where('products.id', $voucher->product_id)->first()->pivot;
            //$stock = $tourproduct->stocks()->where('reserved_date', $voucher->traveldate)->first();

            $tourproduct = Product::has('tourcompanies')->find($input['product'])->tourcompanies->find($input['tourcompany'])->pivot;

            $stock = $tourproduct->stocks()->where('reserved_date' , Carbon::createFromFormat('Y-m-d', $input['travel_date'])->toDateString())->first();

            //dd($input);

            if (!empty($stock)) {

                $seat = $totalunitamount;

                //dd(($stock->remaining_seat + $seat));
                if ($stock->max_seat >= ($stock->remaining_seat + $seat)) {
                    $stock->remaining_seat += $seat;
                    $stock->save();
                    $voucher_prefix->save();
                    $voucher->stock_id = $stock->id;
                    $voucher->save();

                    if ($product->per_head == 0) {
                        for ($i = 0; $i < count($input['unit_amount']); $i++) {
                            $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                            $voucher_product_unit = new VoucherProductUnit();
                            $voucher_product_unit->product_unit_id = $product_unit->id;
                            $voucher_product_unit->amount = $input['unit_amount'][$i];
                            $voucher_product_unit->status = 1;
                            $voucher_product_unit->voucher_id = $voucher->id;
                            $voucher_product_unit->save();
                        }
                    }
                    else{
                        //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                        $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['adult'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['child'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['infant'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();
                    }

                    $request->session()->flash('alert-success', 'cut stock!');
                } else {
                    return Redirect::back()->withErrors(['stock full!'])->withInput();
                }
            } else {
                // $request->session()->flash('alert-danger', 'stock not initial!');

                return Redirect::back()->withErrors(['stock not initial!'])->withInput();
            }
        }
        else if($product->premium_day_trip_flag == 2){
            $request->session()->flash('alert-success', 'Project was successful updated!');
            $voucher_prefix->save();
            $voucher->save();
        }
        else if($product->premium_day_trip_flag == 3) {

            $request->session()->flash('alert-success', 'Project was successful updated!');
            $voucher_prefix->save();
            $voucher->save();


            if ($product->per_head == 0) {
                for ($i = 0; $i < count($input['unit_amount']); $i++) {
                    $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                    $voucher_product_unit = new VoucherProductUnit();
                    $voucher_product_unit->product_unit_id = $product_unit->id;
                    $voucher_product_unit->amount = $input['unit_amount'][$i];
                    $voucher_product_unit->status = 1;
                    $voucher_product_unit->voucher_id = $voucher->id;
                    $voucher_product_unit->save();
                }
            }
            else{
                //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['adult'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['child'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['infant'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();
            }
        }

        return redirect("premiumdaytrip/".$product->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function clearreservedsession()
    {
        //Clear session data
        Session::forget([
            'in-process',
            'travel_date',
            'name',
            'tel',
            'email',
            'adult',
            'child',
            'infant',
            'product',
            'tourcompany',
            'line',
            'wechat',
            'transfer',
            'zone',
            'hotel',
            'payment',
            'discount',
            'totalprice',
            'grandtotalprice',
        ]);
    }
}
