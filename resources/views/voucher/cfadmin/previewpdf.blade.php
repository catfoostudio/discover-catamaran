{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}

{{--<head>--}}
    {{--<meta charset="utf-8">--}}
    {{--<title>A4</title>--}}

{{--</head>--}}

{{--<!-- Set "A5", "A4" or "A3" for class name -->--}}
{{--<!-- Set also "landscape" if you need -->--}}
{{--<body >--}}

{{--<!-- Each sheet element should have the class "sheet" -->--}}
{{--<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->--}}


    {{--<!-- Write HTML just like a web page -->--}}


    {{--<table>--}}
        {{--<tr><td>--}}
                {{--<img src="{{ url('images/discover.jpg') }}" style="width: 100px">--}}
            {{--</td>--}}
        {{--<td>--}}
            {{--Discover Catamaran<br>--}}
            {{--129/8 M.1 T.Sakoo A.Thalang Phuket Thailand 83110<br>--}}
            {{--TEL: 094-597-6111(Tha) /094-597-6333(Chi) /085-199-0268(Eng)<br>--}}
            {{--Fax: 076-328-499 E-mail: discovercatamaran@gmail.com<br>--}}
        {{--</td></tr>--}}
    {{--</table>--}}
{{--<table style="border: black 1px solid;width: auto">--}}
    {{--<tr style="border: black 1px solid;">--}}
        {{--<td style="width: 500px;">--}}
            {{--Customer Detail:--}}
        {{--</td>--}}
        {{--<td>--}}
            {{--Voucher No: TS-2133--}}
        {{--</td>--}}
    {{--</tr>--}}

{{--</table>--}}
{{--<table>--}}
    {{--<tr>--}}
        {{--<td style="width: 500px;">--}}
            {{--Name: MA LI<br>--}}
            {{--Adult: 12 Child: 0 Infant: 0<br>--}}
            {{--Hotel: Chalong Pier<br>--}}
        {{--</td>--}}
        {{--<td>--}}
            {{--Tel: -<br>--}}
            {{--Transfer: 2/W 1/W NO T/F<br>--}}
            {{--Pickup Time : 18:00<br>--}}
        {{--</td>--}}
    {{--</tr>--}}
{{--</table>--}}
{{--</body>--}}


@extends('cfadmin')
@section('content')
    <div class="content-wrapper">
    <section class="content-header">
            <h1>
                Preview Voucher
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="#">Voucher</a></li>
                <li class="active">Preview Voucher</li>
            </ol>
        </section>
        <style type="text/css">
            @font-face {
                font-family: BrowaUPC;
                src: url("{{ asset('fonts/BrowaUPC.ttf') }}");
            }
            @font-face {
                font-family: BrowabUPC;
                src: url("{{ asset('fonts/BrowabUPC.ttf') }}");
            }
            @page { margin: 0px; }
            body {
            }
            label{
                /*font-family: BrowabUPC;*/
            }
            .title{
                /*font-size:35px;*/
                /*font-family: BrowabUPC*/
            }
            #page-wrap {
                width: 750px;
                margin: 0 auto;
                /*font-family: BrowaUPC;*/
                /*font-size: 22px;*/

            }
            .center-justified {
                text-align: justify;
                margin: 0 auto;
                width: 30em;
            }
            table.outline-table {
                border: 1px solid;
                border-spacing: 0;
            }
            tr{
                line-height: 14px;
            }
            tr.border-bottom td, td.border-bottom {
                border-bottom: 1px solid;
            }
            tr.border-top td, td.border-top {
                border-top: 1px solid;
            }
            tr.border-right td, td.border-right {
                border-right: 1px solid;
            }
            tr.border-right td:last-child {
                border-right: 0px;
            }
            tr.center td, td.center {
                text-align: center;
                vertical-align: text-top;
            }
            td.pad-left {
                padding-left: 5px;
            }
            tr.right-center td, td.right-center {
                text-align: right;
                padding-right: 50px;
            }
            tr.right td, td.right {
                text-align: right;
            }
            .grey {
                background:grey;
            }
        </style>
    <!-- Content Wrapper. Contains page content -->
    <div id="page-wrap" style="height: 550px;background-color: white;padding: 20px;">
        <table width="100%">
            <tbody>
            <tr>
                <td width="30%">
                    <img src="{{ asset('images/dct-author.png') }}" style="width: 100px;margin-left: 45px;"> <!-- your logo here -->
                </td>
                <td width="70%">
                    <label class="title">Discover Catamaran</label><br>
                    <label class="font">20/135 (Boat lagoon) M.2 T.Kohkaew A.Muangphuket Phuket 83000</label><br>
                    <label class="font">Tel: +66(0)94-597-6111 /+66(0)94-597-6222 / +66(0)87-461-9555</label><br>
                    <label class="font">Fax: +66(0)76-606-678 E-mail: discovercatamaran@gmail.com</label><br>
                </td>
            </tr>
            </tbody>
        </table>
        <table width="100%" class="outline-table" style="margin-top: 5px;">
            <tbody>
            <tr class="border-bottom">
                <td class="pad-left"  width="60%"><label>Customer Detail:</label></td>
                <td class="pad-left"><label>Voucher No: {{ $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT) }}</label></td>
            </tr>
            {{--<tr class="border-bottom border-right center">--}}
            {{--<td width="45%"><strong>Name: </strong>MA LI</td>--}}
            {{--<td width="25%"><strong>Tax</strong></td>--}}
            {{--<td width="30%"><strong>Amount (INR)</strong></td>--}}
            {{--</tr>--}}
            <tr >
                <td class="pad-left"><label>Name: </label>{{ $voucher->customer->name }}</td>
                <td class="pad-left"><label>Tel.: </label>{{ $voucher->customer->tel }}</td>
            </tr>
            <tr >
                <td class="pad-left">
                    <label>Adult: </label>{{ $voucher->adult }}
                    <label> Child: </label>{{ $voucher->child }}
                    <label> Infant: </label>{{ $voucher->infant }}

                </td>
                <td class="pad-left border-left"><label>Transfer:</label>
                    @if($voucher->transfer == '2w')
                        2 Way
                    @elseif($voucher->transfer == '1win')
                        1 Way-in
                    @elseif($voucher->transfer == '1wout')
                        1 Way-out
                    @elseif($voucher->transfer == 'notf')
                        No Transfer
                    @endif

                </td>
            </tr>
            <tr >
                <td class="pad-left"><label>Hotel:</label> {{ (!empty($voucher->hotel)) ? $voucher->hotel->name : '' }}</td>
                <td class="pad-left"><label>Pickup Time :</label> {{ $voucher->pickuptime_text }}</td>
                {{--//date("H:i", strtotime($voucher->pickuptime))--}}
            </tr>
            {{--<tr class="border-right">--}}
            {{--<td class="pad-left">&nbsp;</td>--}}
            {{--<td class="right border-top">Tax</td>--}}
            {{--<td class="right border-top">Rs. 1236</td>--}}
            {{--</tr>--}}
            {{--<tr class="border-right">--}}
            {{--<td class="pad-left">&nbsp;</td>--}}
            {{--<td class="right border-top">Total</td>--}}
            {{--<td class="right border-top">Rs. 11,236</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
        <table width="100%" class="outline-table" style="margin-top: 5px;overflow: hidden;height: 112px;">
            <tbody>
            <tr class="border-bottom border-right">
                <td colspan="3"><label>Detail:</label></td>
            </tr>
            <tr class="border-right center">
                <td colspan="3" style="overflow: hidden"><label>{{ $voucher->traveldate }}<br>
                        {{ $voucher->product->name }}</label>
                    @if($voucher->product->per_head != 1)
                        - <?php $string = ''; ?>
                        @foreach($voucherproductunits as $vpu)
                            <?php

                            $pu = \App\Models\ProductUnit::find($vpu->product_unit_id);
                            $string .= $pu->unit->name.': '.$vpu->amount.' / ';
                            ?>
                        @endforeach
                        {{ $string }}<br>
                    @endif
                    <div style="height: 33px;">{!! preg_replace("/\r\n|\r|\n/",'<br/>',$voucher->product_note)  !!}</div>
                </td>
            </tr>
            </tbody>
        </table>
        <table width="100%" class="outline-table" style="margin-top: 5px;">
            <tbody>
            <tr class="border-bottom">
                <td colspan="3" class="pad-left"><label>Booking Ref:</label></td>
            </tr>
            <tr >
                <td class="pad-left" width="60%" colspan="2"><label>Issue to: </label>{{ $voucher->tourcompany->name }}</td>
                <td class="pad-left"><label>Issue by:</label>    {{ $voucher->user->name }}</td>
            </tr>
            <tr >
                <td class="pad-left" colspan="2"><label>Tel: </label>{{ $voucher->tourcompany->tel }}</td>
                <td class="pad-left"><label>Tel:</label> {{ $voucher->user->tel }}</td>
            </tr>
            {{--<tr>--}}
            {{--<td class="pad-left"><strong> Fax: </strong>{{ $voucher->tourcompany->fax }}</td>--}}
            {{--<td class="pad-left"><strong>Fax:</strong> {{ $voucher->user->fax }}</td>--}}
            {{--</tr>--}}
            <tr>
                <td class="pad-left">
                    <label>Confirmed By:</label> {{ $voucher->confirm_by }}

                </td>
                <td>
                    <label>Confirmation No.:</label>{{ $voucher->confirmation_no }}
                </td>
                <td class="pad-left">
                    <label>Voucher Date:</label> {{ $voucher->updated_at }}
                </td>
            </tr>
            </tbody>
        </table>
        <table width="100%" class="outline-table" style="margin-top: 5px;overflow: hidden;height: 115px">
            <tbody>
            <tr class="border-bottom">
                <td class="pad-left"><label>Remark:</label></td>
            </tr>
            <tr >
                <td class="pad-left"><label>Hotel: </label>
                    {{ (!empty($voucher->hotel)) ? $voucher->hotel->tel.' '.$voucher->hotel->adderss1 : '' }}
                </td>
            </tr>
            <tr >
                <td class="pad-left"><label>Remark: </label><div style="height: 43px;">{!! preg_replace("/\r\n|\r|\n/",'<br/>',$voucher->remark); !!}<br></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
        <div align="center" style="margin-top: 20px;">
        <a href="{{ url('cfadmin/voucher') }}"><button class="btn btn-primary" style="margin-right:10px;">Back to Voucher list</button></a>
            <a href="{{ url('cfadmin/voucher/pdf/'.$voucher->id) }}" target="_blank"><button class="btn btn-danger">Print PDF</button></a>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
    {{--<!-- Select2 -->--}}
    {{--<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>--}}
    {{--<!-- InputMask -->--}}
    {{--<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>--}}
    {{--<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>--}}
    {{--<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>--}}
    {{--<!-- date-range-picker -->--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>--}}
    {{--<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>--}}
    {{--<!-- bootstrap datepicker -->--}}
    {{--<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>--}}
    {{--<!-- bootstrap color picker -->--}}
    {{--<script src="{{ asset('plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>--}}
    {{--<!-- bootstrap time picker -->--}}
    {{--<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>--}}
    {{--<!-- SlimScroll 1.3.0 -->--}}
    {{--<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>--}}
    {{--<!-- iCheck 1.0.1 -->--}}
    {{--<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>--}}
    {{--<!-- FastClick -->--}}
    {{--<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>--}}
    {{--<!-- AdminLTE App -->--}}
    {{--<script src="{{ asset('dist/js/app.min.js') }}"></script>--}}


@endsection
