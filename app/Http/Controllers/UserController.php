<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Role;
use App\Models\VoucherPrefix;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'user.cfadmin.index' : 'home.index';

        $users = User::all();
        return view($view, [
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'user.cfadmin.create' : 'home.index';

        $countries = Country::all();
        $roles = Role::all();
        $voucherprefixes = VoucherPrefix::all();
        return view($view, [
            'countries' => $countries,
            'roles' => $roles,
            'voucherprefixes' => $voucherprefixes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        //
        $view = ($this->isAdminRequest()) ? 'user.cfadmin.detail' : 'home.index';
        $roles = Role::all();
        $voucherprefixes = VoucherPrefix::all();
        //$voucherprefix = VoucherPrefix::all();

//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'user' => $user,
            'roles' => $roles,
            'voucherprefixes' => $voucherprefixes,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
