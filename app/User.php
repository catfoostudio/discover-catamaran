<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id' , 'voucher_prefix_id' , 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'telfull', 'faxfull'
    ];

    protected function getTelfullAttribute()
    {
        if (empty($this->tel)) {
            return 'No data';
        } else {
            return $this->tel_code . '-' . $this->tel;
        }

    }

    protected function getFaxfullAttribute()
    {
        if (empty($this->fax)) {
            return 'No data';
        } else {
            return $this->fax_code . '-' . $this->fax;
        }

    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function voucherprefix()
    {
        return $this->belongsTo('App\Models\VoucherPrefix', 'voucher_prefix_id');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }
}
