<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'customer.cfadmin.index' : 'home.index';

        $customers = Customer::all();
        return view($view, [
            'customers' => $customers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $view = ($this->isAdminRequest()) ? 'customer.cfadmin.create' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'countries' => $countries,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
//            'tel_code' => 'required',
            'tel' => 'required',
//            'email' => 'required',
//            'address1' => 'required',
        ]);


        $input = $request->input();


        $customer = new Customer();
        $customer->name = $input['name'];
        $customer->country_id = $input['country'];
//        $customer->tel_code = $input['tel_code'];
        $customer->tel = $input['tel'];
//        $customer->email = $input['email'];
//        $customer->address1 = $input['address1'];
        $customer->customer_code = '001';


        if (!empty($input['first_name']))
            $customer->first_name = $input['first_name'];
        if (!empty($input['last_name']))
            $customer->last_name = $input['last_name'];
        if (!empty($input['license_id']))
            $customer->license_id = $input['license_id'];
        if (!empty($input['id_card_no']))
            $customer->id_card_no = $input['id_card_no'];
        if (!empty($input['passport_no']))
            $customer->passport_no = $input['passport_no'];
        if (!empty($input['fax_code']))
            $customer->fax_code = $input['fax_code'];
        if (!empty($input['fax']))
            $customer->fax = $input['fax'];
        if (!empty($input['mobile_code']))
            $customer->mobile_code = $input['mobile_code'];
        if (!empty($input['mobile']))
            $customer->mobile = $input['mobile'];
        if (!empty($input['facebook_url']))
            $customer->facebook_url = $input['facebook_url'];
        if (!empty($input['facebook_id']))
            $customer->facebook_id = $input['facebook_id'];
        if (!empty($input['line']))
            $customer->line = $input['line'];
        if (!empty($input['other_contact']))
            $customer->other_contact = $input['other_contact'];
        if (!empty($input['address2']))
            $customer->address2 = $input['address2'];
        if (!empty($input['postcode']))
            $customer->postcode = $input['postcode'];
        if (!empty($input['remark']))
            $customer->remark = $input['remark'];
        $customer->status = 1;
        $customer->save();

        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/customer");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($customer)
    {
        //
        $view = ($this->isAdminRequest()) ? 'customer.cfadmin.detail' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'customer' => $customer,
            'countries' => $countries,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $customer)
    {
        //
        //
        $this->validate($request, [
            'name' => 'required|max:191',
//            'tel_code' => 'required',
            'tel' => 'required',
//            'email' => 'required',
//            'address1' => 'required',
        ]);

        $input = $request->input();

        $customer->name = $input['name'];
        $customer->country_id = $input['country'];
//        $customer->tel_code = $input['tel_code'];
        $customer->tel = $input['tel'];
//        $customer->email = $input['email'];
//        $customer->address1 = $input['address1'];
                        
        if (!empty($input['first_name']))
            $customer->first_name = $input['first_name'];
        if (!empty($input['last_name']))
            $customer->last_name = $input['last_name'];
        if (!empty($input['license_id']))
            $customer->license_id = $input['license_id'];
        if (!empty($input['id_card_no']))
            $customer->id_card_no = $input['id_card_no'];
        if (!empty($input['passport_no']))
            $customer->passport_no = $input['passport_no'];
        if (!empty($input['fax_code']))
            $customer->fax_code = $input['fax_code'];
        if (!empty($input['fax']))
            $customer->fax = $input['fax'];
        if (!empty($input['mobile_code']))
            $customer->mobile_code = $input['mobile_code'];
        if (!empty($input['mobile']))
            $customer->mobile = $input['mobile'];
        if (!empty($input['facebook_url']))
            $customer->facebook_url = $input['facebook_url'];
        if (!empty($input['facebook_id']))
            $customer->facebook_id = $input['facebook_id'];
        if (!empty($input['line']))
            $customer->line = $input['line'];
        if (!empty($input['other_contact']))
            $customer->other_contact = $input['other_contact'];
        if (!empty($input['address2']))
            $customer->address2 = $input['address2'];
        if (!empty($input['postcode']))
            $customer->postcode = $input['postcode'];
        if (!empty($input['remark']))
            $customer->remark = $input['remark'];
        $customer->status = 1;
        $customer->save();

        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/customer");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
