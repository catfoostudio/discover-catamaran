@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Discount Types
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/discounttype') }}">Discount Types</a></li>
                <li class="active">Add Discount Types</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        {!! Form::open(['method' => 'PUT' , 'action' => ['DiscounttypeController@update', $discounttype]]) !!}
                        <div class="box-header with-border">
                            <h3 class="box-title">Discount Type Info.</h3> <input value="THB" type="radio" style="margin-left: 10px;" name="type_value" checked> <label>THB</label> <input value="PERCENT" type="radio" name="type_value" style="margin-left: 10px;" {{ $discounttype->type_value == 'PERCENT' ? 'checked' : '' }}> <label>Percent %</label>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Discount Type</label>
                                        <input type="text" name="name" class="form-control" value="{{ $discounttype->name }}">
                                    </div>
                                    <div class="col-md-6">
                                        <div id="discounttype_value">
                                            <label for="inputEmail3">Value</label>
                                            <input type="text" class="form-control" name="value" value="{{ $discounttype->value }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="discounttype_minpurchase">
                                            <label for="inputEmail3">MIN Purchase</label>
                                            <input type="text" class="form-control" name="min_purchase" value="{{ $discounttype->min_purchase }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="discounttype_maxdiscount">
                                            <label for="inputEmail3">MAX Discount</label>
                                            <input type="text" class="form-control" name="max_discount" value="{{ $discounttype->max_discount }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="discounttype_adult">
                                            <label for="inputEmail3">DAYTRIP (Adult)</label>
                                            <input type="text" class="form-control" name="adult_value" value="{{ $discounttype->adult_value }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="discounttype_child">
                                            <label for="inputEmail3">DAYTRIP (Child)</label>
                                            <input type="text" class="form-control" name="child_value" value="{{ $discounttype->child_value }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="discounttype_private">
                                            <label for="inputEmail3">Private Charter</label>
                                            <input type="text" class="form-control" name="private_value"  value="{{ $discounttype->private_value }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="optional">
                            </div>

                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Optional</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12" id="optionals">
                                        @foreach($optionals as $optional)
                                            <input id="checkbox_{{$optional->id}}" name="optional[]" type="checkbox" value="{{$optional->id}}" {{ in_array($optional->id, $discounttype->optionals()->pluck('optionals.id')->toArray()) ? 'checked' : '' }}> <label for="checkbox_{{$optional->id}}">{{ $optional->name }}</label><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Save</button>
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({});
            $('#lfm').filemanager('image');
            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                minuteStep: 1,
            });


        });
    </script>
@endsection
