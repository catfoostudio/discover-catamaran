<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function getStatusTextAttribute()
    {
        $string = '';

        if($this->status == 1){
            $string = 'Pending Payment';
        }
        elseif($this->status == 2){
            $string = 'Payment Review';
        }
        elseif($this->status == 3){
            $string = 'Completed';
        }
        elseif($this->status == 4){
            $string = 'Cancelled';
        }
        elseif($this->status == 5){
            $string = 'Waiting Confirm';
        }
        elseif($this->status == 6){
            $string = 'Agent Invoice';
        }
        else{
            $string = 'Error Status';
        }

        return $string;
    }

    public function getStatusBadgeAttribute()
    {
        if($this->status == 1){
            $string = '<span class="label label-warning" style="font-size: 100%;font-weight: normal;">Pending Payment</span>';
        }
        elseif($this->status == 2){
            $string = '<span class="label label-info" style="font-size: 100%;font-weight: normal;">Payment Review</span>';
        }
        elseif($this->status == 3){
            $string = '<span class="label label-success" style="font-size: 100%;font-weight: normal;">Completed</span>';
        }
        elseif($this->status == 4){
            $string = '<span class="label label-danger" style="font-size: 100%;font-weight: normal;">Cancelled</span>';
        }
        elseif($this->status == 5){
            $string = '<span class="label label-default" style="font-size: 100%;font-weight: normal;">Waiting Confirm</span>';
        }
        elseif($this->status == 5){
            $string = '<span class="label label-warning" style="font-size: 100%;font-weight: normal;">Agent Invoice</span>';
        }
        else{
            $string = 'Error Status';
        }

        return $string;
    }

    public function voucher()
    {
        return $this->hasOne('App\Models\Voucher');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}
