@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Stock
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Calendar</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body no-padding">
                            <!-- THE CALENDAR -->
                            <div id="calendar"></div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="stockLeft" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="stockLeftLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <h4>Tour Product</h4>
                    <select class="form-control select2" style="width: 100%;" name="product">

                        @foreach($tourcompanywithproduct as $tour)
                            @foreach($tour->products as $product)
                                <option value="{{ $product->name }}">{{ $product->name.' : '.$tour->name }}</option>
                            @endforeach
                        @endforeach
                    </select>
                    {!! Form::hidden('date', '') !!}
                    <h4>Remaining</h4>
                     {{ Form::number('remaining', 0, ['min' => 0,  'onkeypress' => "return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"]) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSaveRemaining">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/select2.full.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            /* initialize the external events
             -----------------------------------------------------------------*/
            function ini_events(ele) {
                ele.each(function () {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });

                });
            }

            ini_events($('#external-events div.external-event'));

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date();
            var d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear();

            var currentEventDate = new Date();

            var allevents = [
                {
                    title: 'Product name - 4',
                    start: new Date(y, m, 1),
                    allDay: true,
                    backgroundColor: "#dd4b39", //red
                    borderColor: "#dd4b39" //red
                },
                {
                    title: 'Product name - 12',
                    start: new Date(y, m, d - 5),
//                    end: new Date(y, m, d - 2),
                    allDay: true,
                    backgroundColor: "#00a65a", //yellow
                    borderColor: "#00a65a" //yellow
                },
                {
                    title: 'Product name - 2',
                    start: new Date(y, m, d, 10, 30),
                    allDay: true,
                    backgroundColor: "#dd4b39", //Blue
                    borderColor: "#dd4b39" //Blue
                },
                {
                    title: 'Product name - 21',
                    start: new Date(y, m, d, 12, 0),
//                    end: new Date(y, m, d, 14, 0),
                    allDay: true,
                    backgroundColor: "#00a65a", //Info (aqua)
                    borderColor: "#00a65a" //Info (aqua)
                },
                {
                    title: 'Product name - all reserved',
                    start: new Date(y, m, d + 1, 19, 0),
//                    end: new Date(y, m, d + 1, 22, 30),
                    allDay: true,
                    backgroundColor: "black", //Success (green)
                    borderColor: "black" //Success (green)
                },
                {
                    title: 'Product name - 25',
                    start: new Date(y, m, 28),
//                    end: new Date(y, m, 29),
//                    url: 'http://google.com/',
                    allDay: true,
                    backgroundColor: "#00a65a", //Primary (light-blue)
                    borderColor: "#00a65a" //Primary (light-blue)
                }
            ];

            $('#calendar').fullCalendar({
                dayClick: function(date, jsEvent, view) {

//                    alert('Clicked on: ' + date.format());

//                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

//                    alert('Current view: ' + view.name);
                    currentEventDate = date;
                    $('#stockLeftLabel').text(date.format('MMMM Do YYYY'));
                    $('#stockLeft').modal('show');

                    // change the day's background color just for fun
                    //  $(this).css('background-color', 'red');

                },
                eventClick: function(calEvent, jsEvent, view) {

//                    alert('Event: ' + calEvent.title);
//                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                    alert('View: ' + view.name);
                  //  currentEventDate = date;
                    $('#stockLeftLabel').text(calEvent.title + ' : ' +calEvent.start.format('MMMM Do YYYY'));
                    $('#stockLeft').modal('show');
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
                //Random default events
                // events: allevents,
                // editable: true,
                // droppable: true, // this allows things to be dropped onto the calendar !!!
                // drop: function (date, allDay) { // this function is called when something is dropped
                //
                //     // retrieve the dropped element's stored Event Object
                //     var originalEventObject = $(this).data('eventObject');
                //
                //     // we need to copy it, so that multiple events don't have a reference to the same object
                //     var copiedEventObject = $.extend({}, originalEventObject);
                //
                //     // assign it the date that was reported
                //     copiedEventObject.start = date;
                //     copiedEventObject.allDay = allDay;
                //     copiedEventObject.backgroundColor = $(this).css("background-color");
                //     copiedEventObject.borderColor = $(this).css("border-color");
                //
                //     // render the event on the calendar
                //     // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                //     $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                //
                //     // is the "remove after drop" checkbox checked?
                //     if ($('#drop-remove').is(':checked')) {
                //         // if so, remove the element from the "Draggable Events" list
                //         $(this).remove();
                //     }
                //
                // }
            });

            $('#btnSaveRemaining').on('click', function () {

                var formData = new FormData();
                formData.append('country', $('select[name="product"]').val());
                formData.append('remaining', $("input[name='remaining']").val());
                formData.append('date', $("input[name='date']").val());

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: 'stock',
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: formData,
                    success: function(data) {
                        if(data.success) {
                            window.location = '';//Desired location for redirect
                        } else {
                            // If the signin wasn't successful.
                        }
                    },
                    error: function(data) {
                        // If you got an error code.
                    }
                });


                var newEvent = new Object();
                var remaining = parseInt($("input[name='remaining']").val());
                var product = $("select[name='product']").select2('data')[0].text;


                newEvent.start = currentEventDate;
                newEvent.allDay = true;
                if(remaining == 0){
                    newEvent.backgroundColor = 'black';
                    newEvent.borderColor = 'black';
                    newEvent.title =  product + ' - all reserved';
                }
                else if(remaining <= 9){
                    newEvent.backgroundColor = '#dd4b39';
                    newEvent.borderColor = '#dd4b39';
                    newEvent.title = product +' - '+ remaining;
                }
                else if(remaining >= 99){
                    newEvent.backgroundColor = '#00a65a';
                    newEvent.borderColor = '#00a65a';
                    newEvent.title = product +' - '+ 99;
                }
                else{
                    newEvent.backgroundColor = '#00a65a';
                    newEvent.borderColor = '#00a65a';
                    newEvent.title = product +' - '+ remaining;
                }

                $('#calendar').fullCalendar( 'renderEvent', newEvent );
                $('#stockLeft').modal('hide');
            });


            /* ADDING EVENTS */
            var currColor = "#3c8dbc"; //Red by default
            //Color chooser button
            var colorChooser = $("#color-chooser-btn");
            $("#color-chooser > li > a").click(function (e) {
                e.preventDefault();
                //Save color
                currColor = $(this).css("color");
                //Add color effect to button
                $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
            });
            $("#add-new-event").click(function (e) {
                e.preventDefault();
                //Get value and make sure it is not null
                var val = $("#new-event").val();
                if (val.length == 0) {
                    return;
                }

                //Create events
                var event = $("<div />");
                event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
                event.html(val);
                $('#external-events').prepend(event);

                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new-event").val("");
            });
        });
    </script>

@endsection
