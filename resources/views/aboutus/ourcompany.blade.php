<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Our Company</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>Our Company</h1>
            <p>Discover Catamaran - The Original Premium Catamaran Excursion in Phuket</p>
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    <div class="box-white" style="padding-top: 40px;">

                        <p>
                            Discover Catamaran was founded in 2013 dedicated to provide inbound travel services and
                            hospitality on mid-high and high-end services and products.</p>
                        <p>Years of experience added with our creativity and thousands of happy customers have made us
                            one of the most remarkable high quality operator specializing in catamaran and tailor-made
                            vacations in Phuket.</p>

                        <p>We always try to bring the true values to our customers, and provide the best of the day</p>


                        <p><strong>Team</strong></p>

                        <p>We always take PEOPLE seriously, the key to success is a strong team and efficient teamwork.
                            We are always open to everyone, who are team-oriented and looking for an achievement in
                            his/her career.</p>

                        <p><strong>Services</strong></p>

                        <ul>
                            <li>
                                Premium joied day trip on catamaran
                            </li>
                            <li>
                                Premium Private charter on catamaran
                            </li>
                            <li>
                                Hotel booking service
                            </li>
                            <li>
                                Domestic ticketing
                            </li>
                            <li>
                                Transportation service ( Bus, van and car)
                            </li>
                            <li>
                                M.I.C.E. tours
                            </li>
                        </ul>


                        <p><strong>Service Standard</strong></p>

                        <p>We care every step of what we provide to customers, make sure we are able to deliver the best
                            possible and to assist customers during their vacation</p>
                        <ul>
                            <li>1. Listen to customers needs, ensure we deliver what the customers need</li>
                            <li>2. Plan the most suitable itinerary and arrangment according to customers' requirments</li>
                            <li>3. Select the best services and products to make customers' vacation unforgettable</li>
                        </ul>

                        <p><strong>Unprofit and social responsibility</strong></p>

                        <p>It is everyone's responsibility to help the society one's living in, as we are closely
                            involved in the industry that has great concern with natural environment, we initiatively
                            comply all environmental protection regulations and keep improving standard to protect our
                            environment</p>

                        <p>We are one of the earliest tour operators implied feeding fish prohibition on all our
                            catamaran excurisons</p>

                        <p>We take part in the local community supports and activites, e.g. 2015 donated educational
                            tools to poor condition students. 2014 donated for anti-drug soccer game held in Naiyang,
                            etc. We are ready and will always be dedicated to take part in the environmental protection
                            and to help building a healthier tourism in Thailand.</p>
                    </div>
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
    {{--<div class="wrap center">--}}
        {{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
        {{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
            {{--dates for our convenience - brilliant!” </p>--}}
        {{--<p>- Johnatan Davidson</p>--}}
    {{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>