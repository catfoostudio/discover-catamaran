<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'zone.cfadmin.index' : 'home.index';

        $zones = Zone::all();
        return view($view, [
            'zones' => $zones,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'zone.cfadmin.create' : 'home.index';

        $products = Product::all();

        return view($view, [
            'products' => $products,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // //
        $this->validate($request, [
            'name' => 'required|max:191',
            'price' => 'required',
            'pickuptime' => 'required',
        ]);


        $input = $request->input();

        $zone = new Zone();
        $zone->name = $input['name'];
        $zone->price = $input['price'];
        $zone->product_id = $input['product'];
        $zone->pickuptime = date("H:i", strtotime($input['pickuptime']));
        if (!empty($input['remark']))
            $zone->remark = $input['remark'];

        $zone->status = 1;
        $zone->save();
        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/zone");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($zone)
    {
        //
        $view = ($this->isAdminRequest()) ? 'zone.cfadmin.detail' : 'home.index';

        $products = Product::all();

//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'zone' => $zone,
            'products' => $products,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone)
    {
        $this->validate($request, [
            'name' => 'required|max:191',
            'price' => 'required',
            'pickuptime' => 'required',
        ]);


        $input = $request->input();

        $zone->name = $input['name'];
        $zone->price = $input['price'];
        $zone->product_id = $input['product'];
        $zone->pickuptime = date("H:i", strtotime($input['pickuptime']));
        if (!empty($input['remark']))
            $zone->remark = $input['remark'];

        $zone->status = 1;
        $zone->save();
        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/zone");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getzoneoption(Request $request)
    {

        $input = $request->input();

        if(!empty($input['id'])) {
            $zones = Zone::whereHas('product', function ($query) use ($input) {
                $query->where('products.id', $input['id']);
            })->get(['id', DB::raw('CONCAT(name, " ", DATE_FORMAT(pickuptime, "%h:%i %p")) as text')]);
            //DB::raw('CONCAT(First_Name, " ", Last_Name) AS full_name'))
        }
        else{
            $zones = Zone::all(['id', 'name as text']);
        }

        return $zones->toJson();
    }
}
