<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Confirm Reservation</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
{{--<header class="intro">--}}
{{--<div class="wrap">--}}
{{--<h1>Our Company</h1>--}}
{{--<p>Discover Catamaran - The Original Premium Catamaran Excursion in Phuket</p>--}}
{{--</div>--}}
{{--</header>--}}
<!-- //Intro -->
    <style>
        label {
            font-weight: bold;
        }

        .one-half {
            padding-bottom: 10px;
        }

        .one-third {
            padding-bottom: 10px;
        }
    </style>

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    <div class="box-white" style="padding-top: 40px;">
                        <h1 style="float: left;"><span
                                    style="font-weight: bold">DUE DATE: </span><br>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $voucher->invoice->due_date)->format('d F Y H:i') }}
                        </h1>
                        <h1 style="float: right;"><span
                                    style="font-weight: bold">INVOICE# :</span> {{ $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT) }}
                            <br>
                            <span style="font-weight: bold">STATUS: </span><br>{{ $voucher->statustext }}</h1>
                        <div class="row">
                            <div class="full-width" style="padding-bottom: 0px;">
                                <hr>
                                <h3 style="padding-bottom: 2px;">Information</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one-half">
                                <label>Product</label>
                                {{ $voucher->product->name }}
                            </div>
                            <div class="one-half">
                                <label>Travel Date</label>
                                {{ \Carbon\Carbon::createFromFormat('Y-m-d', $voucher->traveldate)->format('d F Y') }}
                            </div>
                            <div class="one-half">
                                <label>Name</label>
                                {{ $voucher->customer->name }}
                            </div>
                            <div class="one-half">
                                <label>Tel.</label>
                                {{ $voucher->customer->tel }}
                            </div>
                            <div class="one-half">
                                <label>E-mail</label>
                                {{ $voucher->customer->email }}
                            </div>
                            <div class="one-half">
                                <label>Line</label>
                                {{ (!empty($voucher->customer->line)) ? $voucher->customer->line : '-' }}
                            </div>
                            <div class="one-half">
                                <label>Wechat ID</label>
                                {{ (!empty($voucher->customer->wechat)) ? $voucher->customer->wechat : '-' }}
                            </div>
                        </div>
                        <hr>
                        <h3 style="padding-bottom: 2px;">Traveler</h3>
                        <div class="row">
                            <div class="one-third">
                                <label>Adult</label>
                                {{ $voucher->adult }}
                            </div>
                            <div class="one-third">
                                <label>Child 4-11 yrs</label>
                                {{ $voucher->child }}
                            </div>
                            <div class="one-third">
                                <label>Infant 1-3 yrs</label>
                                {{ $voucher->infant }}
                            </div>
                        </div>
                        <hr>
                        <h3 style="padding-bottom: 2px;">Transfer</h3>
                        <div class="row">
                            <div class="one-third">
                                <label>Transfer</label>
                                {{ (($voucher->transfer) == 'wt') ? 'With Transfer' : 'Without Transfer' }}
                            </div>
                            @if($voucher->transfer == 'wt')
                                <div class="one-third">
                                    <label>Zone</label>
                                    {{ \App\Models\Zone::find($voucher->zone_id)->name.' '.date("H:i", strtotime(\App\Models\Zone::find($voucher->zone_id)->pickuptime)) }}
                                </div>
                                <div class="one-third">
                                    <label>Hotel</label>
                                    {{ \App\Models\Hotel::find($voucher->hotel_id)->name }}
                                </div>
                            @endif
                        </div>
                        <hr>
                        <h3 style="padding-bottom: 2px;">Payment</h3>
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="one-half">
                                <label>Total Price</label>
                                {{ (!empty($voucher->invoice->grandtotal)) ? number_format($voucher->invoice->grandtotal,2).' Baht' : '0 Baht' }}
                                <label>Discount code</label>
                                {{ (!empty($voucher->code_id)) ? $voucher->code->code : '-' }}
                                <label> Payment type</label>
                                @if($voucher->invoice->payment == 'credit')
                                    Credit Card
                                @elseif($voucher->invoice->payment == 'bank')
                                    Bank Transfer
                                @elseif($voucher->invoice->payment == 'wechat')
                                    Wechat Pay
                                @endif
                            </div>
                            <div class="one-half">
                                <label>Bank Detail</label>

                            </div>

                        </div>
                        <div class="row">
                            <div class="full-width">
                                <a href="{{ url('payment/inform/'.$voucher->voucher_prefix.'/'.$voucher->voucher_no) }}">
                                    <button type="button" class="button gold medium">Inform Payment</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
{{--<div class="wrap center">--}}
{{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
{{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
{{--dates for our convenience - brilliant!” </p>--}}
{{--<p>- Johnatan Davidson</p>--}}
{{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $('#btn-cancel-order').click(function () {

    });
</script>
</body>
</html>