<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Contact</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>Contact us</h1>
            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea.</p>--}}
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content boxed grid2">
        <!-- Item -->
        <article class="full-width hentry">
            <div class="one-half">
                <div class="text">
                    <h4>Contact us</h4>
                    <p>Please complete the information below and we will respond to your inquiry as soon as possible. Your information will not be used for any other purposes. All fields are required.</p>

                    <form method="post" action="{{ action('ContactController@sendmail') }}" name="contactform" id="contactform2">
                        {{ csrf_field() }}
                        <fieldset>
                            <div id="message"></div>
                            <div class="full-width">
                                <label for="name">Name and surname</label>
                                <input type="text" id="name" name="name"/>
                            </div>

                            <div class="full-width">
                                <label for="email">E-mail address</label>
                                <input type="email" id="email" name="email"/>
                            </div>

                            <div class="full-width">
                                <label for="phone">Phone number</label>
                                <input type="number" id="phone" name="phone"/>
                            </div>

                            <div class="full-width">
                                <label for="comments">Your message</label>
                                <textarea id="comments" name="comments"></textarea>
                            </div>

                            <div class="full-width">
                                <input type="submit" value="Send message" class="button gold large" id="submit" />
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="one-half">
                {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d199666.8695132292!2d-121.4429125!3d38.56154050000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x809ac672b28397f9%3A0x921f6aaa74197fdb!2sSacramento%2C+CA%2C+USA!5e0!3m2!1sen!2shr!4v1426758068135" width="1000" height="800" frameborder="0" style="border:0"></iframe>--}}
                <div class="text">
                    <h6 style="margin-bottom: -20px">DISCOVER CATAMARAN CO.,LTD.</h6>
                    <ul class="social-list" style="line-height: 45px">
                        <li style="line-height: 25px;"><img src="{{ asset('images/dbd.png') }}" style="height:25px;display: inline-block;"> 0835561014024</li>
                        <li style="line-height: 25px;"><img src="{{ asset('images/ttt.png') }}" style="height:25px;display: inline-block;"> 83/1111</li>
                        <li style="line-height: 25px;"><img src="{{ asset('images/address.png') }}" style="height:25px;display: inline-block;"> 20/135 (Boat Lagoon) M2 Kohkaew, Muang, Phuket, Thailand 83000</li>
                        <li style="line-height: 25px;"><img src="{{ asset('images/tel.png') }}" style="height:25px;display: inline-block;"> <a href="tel:66-(0)80-076-1680">66-(0)80-076-1680</a> (EN/TH)<br><a href="tel:66-(0)87-461-9555">66-(0)87-461-9555</a> (CH/TH)</li>
                        <li style="line-height: 25px;"><img src="{{ asset('images/email.png') }}" style="height:25px;display: inline-block;"> <a href="mailto:discovercatamaran@gmail.com">discovercatamaran@gmail.com</a></li>
                        <li style="line-height: 25px;">Line: @catamaran</li>
                        <li style="line-height: 25px;">Wechat: allatphuket</li>
                    </ul>

                    {{--<a href="https://www.facebook.com/discovercatamaran" target="_blank"><i class="fa fa-facebook-official"></i></a>--}}
                    {{--<a href="https://instagram.com/discovercatamaran" target="_blank"><i class="fa fa-instagram"></i></a>--}}

                </div> </div>
        </article>
        <!-- //Item -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->
<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>