<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tourcompany_product_id')->unsigned()->nullable();
            $table->foreign('tourcompany_product_id')->references('id')->on('tourcompany_product')->onDelete('cascade');
            $table->date('reserved_date');
            $table->integer('remaining_seat');
            $table->integer('max_seat');
            $table->tinyInteger('status');
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
