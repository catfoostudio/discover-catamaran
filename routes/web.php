<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('lang', function () {
    return session()->get('locale');
});

Route::get('lang/{key}', function ($key) {
    session()->put('locale', $key);
    return redirect()->back();
});


// route–model binding
Route::model('voucher', App\Models\Voucher::class);
Route::model('hotel', App\Models\Hotel::class);
Route::model('customer', App\Models\Customer::class);
Route::model('product', App\Models\Product::class);
Route::model('tourcompany', App\Models\Tourcompany::class);
Route::model('zone', App\Models\Zone::class);
Route::model('user', App\User::class);
Route::model('unit', App\Models\Unit::class);
Route::model('voucherprefix', App\Models\VoucherPrefix::class);
Route::model('enquire', App\Models\Unit::class);
Route::model('gallery', App\Models\Gallery::class);
Route::model('dclifestyle', App\Models\Dclifestyle::class);

Route::get('premiumdaytrip','PremiumdaytripController@index');
Route::get('premiumdaytrip/{id}','PremiumdaytripController@show');
Route::get('otherproduct','PremiumdaytripController@otherproduct');

Route::get('destination','DestinationController@index');
Route::get('gallery','GalleryController@index');

Route::resource('gallery', 'GalleryController');
Route::resource('dclifestyle', 'BlogController');

Route::get('privatecharter','PrivateCharterController@index');
Route::get('privatecharter/{id}','PrivateCharterController@show');
Route::get('dclifestyle','BlogController@index');
Route::get('dclifestyle/{id}','BlogController@show');
Route::get('hotdeals','HotDealsController@index');
Route::get('contact','ContactController@index');
Route::post('sendmail','ContactController@sendmail');

Route::get('ourcompany','AboutusController@ourcompany');
Route::get('ourmission','AboutusController@ourmission');
Route::get('policies','AboutusController@policies');

Route::resource('enquire', 'EnquireController', [
    'except' => ['edit', 'update', 'destroy']
]);

Route::post('confirm/reservation', 'EnquireController@confirm');
Route::post('charge', 'ChargeController@charge');
Route::get('payment/success/{id}', 'ChargeController@success');

Route::get('payment/invoice/{prefix}/{no}', 'ChargeController@showinvoice');
Route::get('payment/inform/{prefix}/{no}', 'PaymentController@informwithno');
Route::post('payment/informed/{voucher}', 'PaymentController@inform');
Route::get('payment/refund/{id}', 'ChargeController@refund');
Route::get('invoice/{id}', 'InvoiceController@show');


Route::get('voucher/pdf/{id}' ,'VoucherController@pdf');

// admin routes

Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth']), function() {

    Route::get('/', 'CfAdminDashboardController@index');
});
Route::group(array('prefix' => 'cfadmin', 'middleware' => ['auth' ,'admin']), function()
{
    /*Route::resource('photo', 'PhotoController');
    Route::get('photo/album/{id}', 'PhotoController@album');
    Route::get('photo/create/{id}' , 'PhotoController@createbyalbum');
    Route::get('photo/{id}/status', 'PhotoController@status');
*/
    Route::get('getBasicObject', 'VoucherController@getBasicObject');
    Route::get('getBasicObjectData', 'VoucherController@getBasicObjectData');

    Route::get('test', function () {
        dd(\App\Models\Product::has('tourcompanies')->find(6)->tourcompanies->find(1)->pivot->stocks()->where('reserved_date' , '2020-12-03')->where('status', 1)->first());
    });

    Route::get('report', 'ReportController@index');
    Route::post('report', 'ReportController@report');

    Route::get('reorder', 'ProductController@reorder');
    Route::post('orderprocess', 'ProductController@orderprocess');
    Route::post('addproductimage', 'ProductController@addproductimage');
    Route::post('removeproductimage', 'ProductController@removeproductimage');

    Route::get('api/reordergallery', 'GalleryController@reorder');
    Route::post('api/orderprocessgallery', 'GalleryController@orderprocess');
    Route::post('api/addgalleryimage', 'GalleryController@addgalleryimage');
    Route::post('api/removegalleryimage', 'GalleryController@removegalleryimage');

    Route::get('api/tourcompanydata', 'TourcompanyController@getdata');
    Route::get('api/gettourcompanyoption', 'TourcompanyController@gettourcompanyoption');
    Route::get('api/getproductoption', 'ProductController@getproductoption');
    Route::get('api/getzoneoption', 'ZoneController@getzoneoption');
    Route::get('api/getunithtml', 'UnitController@getunithtml');
    Route::post('api/getstockdata', 'StockController@getstockdata');

    Route::get('api/getperhead', 'ProductController@getperhead');


    Route::get('api/getdiscounttypesetting', 'DiscounttypeController@getdiscounttypesetting');

    Route::resource('payment', 'PaymentController');
    Route::resource('voucher', 'VoucherController');
    Route::resource('hotel', 'HotelController');
    Route::resource('customer', 'CustomerController');
    Route::resource('stock', 'StockController');
    Route::resource('tourcompany', 'TourcompanyController');
    Route::resource('product', 'ProductController');
    Route::resource('user', 'UserController');
    Route::resource('zone', 'ZoneController');
    Route::resource('unit', 'UnitController');
    Route::resource('voucherprefix', 'VoucherPrefixController');
    Route::resource('gallery', 'GalleryController');
    Route::resource('dclifestyle', 'BlogController');
    Route::resource('code', 'CodeController');
    Route::resource('discounttype', 'DiscounttypeController');
    Route::resource('optional', 'OptionalController');
    Route::resource('invoice', 'InvoiceController');

    Route::post('discounttype/active/{id}' , 'DiscounttypeController@active');

    //Route::resource('enquire', 'EnquireController');
    Route::get('premiumdaytrip/stock', 'PremiumdaytripController@stock');
    Route::post('premiumdaytrip/getvoucherlist', 'PremiumdaytripController@getvoucherlist');
    Route::resource('premiumdaytrip', 'PremiumdaytripController');

    Route::post('voucher/updatestatus', 'VoucherController@updatestatus');
    Route::get('voucher/pdf/{id}', 'VoucherController@pdf');
    Route::get('voucher/previewpdf/{id}', 'VoucherController@previewpdf');
    //Route::get('voucher/album/{id}', 'PhotoController@album');
    //Route::get('voucher/create/{id}' , 'PhotoController@createbyalbum');
   // Route::get('voucher/{id}/status', 'PhotoController@status');

    Route::get('cfadmin/register', 'RegisterController@index');

    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
    // list all lfm routes here...

    Route::post('premiumdaytrip/updatestock' , 'PremiumdaytripController@updatestock');
    //Route::resource('country', 'CountryController');
   // Route::get('country/{id}/status', 'CountryController@status');

    Route::get('invoice/previewemail/{id}','InvoiceController@previewemail');
    Route::get('payment/invoice/{prefix}/{no}', 'InvoiceController@preview');
    Route::post('invoiceapprove/{id}', 'InvoiceController@approve');
    Route::post('invoiceconfirm/{id}', 'InvoiceController@confirm');
    Route::post('invoicereject/{id}', 'InvoiceController@reject');
});


Route::group(array('prefix' => 'cfadmin'), function()
{
//    Route::get('login', 'Auth\LoginController@index');
//    Route::post('login', 'Auth\LoginController@postAdminLogin');
//    Route::get('register', 'Auth\RegisterController@index');
//    Route::get('logout', 'Auth\LoginController@logout');
    Auth::routes();
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::resource('voucher', 'VoucherController');
});