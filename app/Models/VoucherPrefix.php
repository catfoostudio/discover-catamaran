<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherPrefix extends Model
{
    //
    protected $table = 'voucher_prefixes';

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
