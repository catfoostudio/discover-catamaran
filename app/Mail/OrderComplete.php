<?php

namespace App\Mail;

use App\Models\Voucher;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderComplete extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $input)
    {
        //
        $this->data = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $voucher = Voucher::find($this->data['id']);
        return $this->subject('Confirmation of Purchase from Discover Catamaran')
            ->view('charge.email-order-complete')
            ->with([
                'voucher' => $voucher,
            ]);
    }
}
