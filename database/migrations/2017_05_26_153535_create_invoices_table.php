<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('payment');
            $table->decimal('price');
            $table->decimal('vat3')->nullable();
            $table->decimal('vat7')->nullable();
            $table->decimal('discount')->nullable();
            $table->decimal('transfer')->nullable();
            $table->decimal('fee')->nullable();
            $table->decimal('grandtotal')->nullable();
            $table->date('date')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->tinyInteger('status');
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('invoices');
    }
}
