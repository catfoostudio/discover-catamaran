<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\Tourcompany;
use App\Models\Unit;
use App\Models\Zone;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'product.cfadmin.index' : 'home.index';

        $products = Product::where('status' , 1)->get();
        return view($view, [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $view = ($this->isAdminRequest()) ? 'product.cfadmin.create' : 'home.index';

        $tourcompanies = Tourcompany::all();
        $zones = Zone::all();
        $units = Unit::all();

        return view($view, [
            'tourcompanies' => $tourcompanies,
            'zones' => $zones,
            'units' => $units,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        //dd($request->has('premium_day_trip_flag'));


        $this->validate($request, [
            'name' => 'required|max:191',
            'unit' => 'required',
            'destination' => 'required',
            'tourcompany' => 'required',
            //'zone_name' => 'required',
        ]);

        $product = new Product();
        $product->name = $input['name'];
        $product->destination = $input['destination'];
        $product->short_description = $input['short_description'];
        $product->description = $input['description'];
        $product->max_pax = $input['max_pax'];

        if($request->has('product_type')){
            if($input['product_type'] == 1) {
                //premium day trip
                $product->premium_day_trip_flag = 1;
            }
            elseif ($input['product_type'] == 2){
                //private chater
                $product->premium_day_trip_flag = 2;
            }
            elseif($input['product_type'] == 3){
                //no stock
                $product->premium_day_trip_flag = 3;
            }
            else{
                $product->premium_day_trip_flag = 0;
            }
        }



        if($request->has('per_head')){
            $product->per_head = 1;
        }

        if (!empty($input['remark']))
            $product->remark = $input['remark'];

        $product->status = 1;
        $product->save();

        if (!empty($input['filepath'])) {
            for ($i = 0; $i < count($input['filepath']); $i++) {
                $image = new Image();
                $image->path = $input['filepath'][$i];
                $image->status = 1;
                $image->save();
                $images[$image->id] = ['status' => 1];
            }
            $product->images()->sync($images);
        }

        for ($i = 0; $i < count($input['tourcompany']); $i++) {
            $tourcompanies[$input['tourcompany'][$i]] = ['status' => 1];
        }
        $product->tourcompanies()->sync($tourcompanies);

        for ($i = 0; $i < count($input['unit']); $i++) {
            $units[$input['unit'][$i]] = ['status' => 1, 'price' => $input['price'][$i]];
        }
        $product->units()->sync($units);

        if (!empty($input['zone_name'])) {
            for ($i = 0; $i < count($input['zone_name']); $i++) {
                $zone = new Zone();
                $zone->name = $input['zone_name'][$i];
                $zone->price = $input['zone_price'][$i];
                $zone->pickuptime = date("H:i", strtotime($input['zone_time'][$i]));
                $zone->status = 1;
                $zone->save();
                $product->zones()->save($zone);
            }
        }
        //  $tourcompany = Tourcompany::find($input['tourcompany']);
     //   $product->tourcompanies()->sync($tourcompany->id , ['status' => 1]);

        if (!empty($input['images'])) {
            for ($i = 0; $i < count($input['images']); $i++) {
                $image = new Image();

            }
        }

        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/product/".$product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        //
        $view = ($this->isAdminRequest()) ? 'product.cfadmin.detail' : 'home.index';

        $tourcompanies = Tourcompany::all();
        $zones = Zone::all();
        //dd($product->zones);
        $units = Unit::all();

        //dd($product->units);
//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'product' => $product,
            'tourcompanies' => $tourcompanies,
            'units' => $units,
            'zones' => $zones,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product)
    {
        //dd($product);
        $input = $request->input();
//        dd($input);
        $this->validate($request, [
            'name' => 'required|max:191',
            'unit' => 'required',
            'destination' => 'required',
            'tourcompany' => 'required',
//            'zone_name' => 'required',
        ]);


        $product->name = $input['name'];
        $product->destination = $input['destination'];
        $product->short_description = $input['short_description'];
        $product->description = $input['description'];
        $product->max_pax = $input['max_pax'];
//        if($request->has('premium_day_trip_flag')){
//            $product->premium_day_trip_flag = 1;
//        }

        if($request->has('product_type')){
            if($input['product_type'] == 1) {
                //premium day trip
                $product->premium_day_trip_flag = 1;
            }
            elseif ($input['product_type'] == 2){
                //private chater
                $product->premium_day_trip_flag = 2;
            }
            elseif($input['product_type'] == 3){
                //no stock
                $product->premium_day_trip_flag = 3;
            }
            else{
                $product->premium_day_trip_flag = 0;
            }
        }

        if($request->has('per_head')){
            $product->per_head = 1;
        }

        if (!empty($input['remark']))
            $product->remark = $input['remark'];

        $product->status = 1;
        $product->save();

        if (!empty($input['filepath'])) {
            for ($i = 0; $i < count($input['filepath']); $i++) {
                $image = null;
                if (isset($input['image_id'][$i])) {
                    $image = Image::find($input['image_id'][$i]);
                }

                if (empty($image))
                    $image = new Image();

                $image->path = $input['filepath'][$i];
                $image->status = 1;
                $image->save();
                $images[$image->id] = ['status' => 1];
            }
            $product->images()->sync($images);
        }

        for ($i = 0; $i < count($input['tourcompany']); $i++) {
            $tourcompanies[$input['tourcompany'][$i]] = ['status' => 1];
        }
        $product->tourcompanies()->sync($tourcompanies);

        for ($i = 0; $i < count($input['unit']); $i++) {
            $units[$input['unit'][$i]] = ['status' => 1, 'price' => $input['price'][$i]];
        }
        $product->units()->sync($units);

      /*  $zones = Zone::whereIn('id', $input['zone_id'])->get();

        Zone::where('product_id' , $product->id)->delete();
*/

        if (!empty($input['zone_id'])) {
            $zone_ids = array();

            for ($i = 0; $i < count($input['zone_id']); $i++) {
                $zone = Zone::find($input['zone_id'][$i]);

                if (empty($zone)) {
                    $zone = new Zone();
                    $zone->name = $input['zone_name'][$i];
                    $zone->price = $input['zone_price'][$i];
                    $zone->pickuptime = date("H:i", strtotime($input['zone_time'][$i]));
                    $zone->status = 1;
                    $zone->save();
                    $product->zones()->save($zone);
                } else {
                    $zone->name = $input['zone_name'][$i];
                    $zone->price = $input['zone_price'][$i];
                    $zone->pickuptime = date("H:i", strtotime($input['zone_time'][$i]));
                    $zone->status = 1;
                    $zone->save();
                    $product->zones()->save($zone);
                }
                $zone_ids[] = $zone->id;
            }


            Zone::where('product_id', $product->id)->whereNotIn('id', $zone_ids)->delete();
        }
      /*
        for ($i = 0;!empty($input['zone_name']) && $i < count($input['zone_name']); $i++) {

            $zone = Zone::find($input['zone_id'][$i]);

            $zone->name = $input['zone_name'][$i];
            $zone->price = $input['zone_price'][$i];
            $zone->pickuptime =  date("H:i", strtotime($input['zone_time'][$i]));
            $zone->status = 1;
            $zone->save();
            $product->zones()->save($zone);
        }
*/
        //  $tourcompany = Tourcompany::find($input['tourcompany']);
        //   $product->tourcompanies()->sync($tourcompany->id , ['status' => 1]);

        $request->session()->flash('alert-success', 'Project was successful updated!');

        return redirect("cfadmin/product");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $product)
    {
        //
        //dd($hotel->name);
        $product->status = 0;
        $product->save();

        $request->session()->flash('alert-success', 'Product was successful updated!');
        return redirect("cfadmin/product");
    }

    public function getproductoption(Request $request)
    {
        $input = $request->input();
        if(!empty($input['id'])) {
            $products = Product::whereHas('tourcompanies', function ($query) use ($input) {
                $query->where('tourcompanies.id', $input['id']);
            })->get(['id', 'name as text']);
        }
        else{
            $products = Product::all(['id', 'name as text']);
        }
        return $products->toJson();
    }

    public function getperhead(Request $request)
    {
        $input = $request->input();
        if(!empty($input['id'])) {
            $product = Product::find($input['id']);
            return $product->per_head;
        }
        else{
            return false;
        }
    }

    public function reorder()
    {
        $product = Product::find(1);
        return view('product.cfadmin.reordertest', [
            'product' => $product
        ]);
    }

    public function orderprocess(Request $request)
    {

        $input = $request->input();

        $product = Product::find(1);
        $ids = explode(",", $input['ids']);
        for ($i = 0; $i < count($ids); $i++) {

            $image = Image::find($ids[$i]);
            $images[$image->id] = ['status' => 1, 'order' => $i+1];
        }

        $product->images()->sync($images);
        $product->save();
    }

    public function addproductimage(Request $request)
    {
        $input = $request->input();
        $product = Product::find($input['id']);

        // open file a image resource
        $img = \Intervention\Image\Facades\Image::make(asset($input['path']));
        $time = time();


        //Original
        $img->fit(1024, 600);
        $filename = $time.'_'.$product->id.'.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);

        $image = new Image();
        $image->path = 'crop_images/'.$filename;
        $image->status = 1;
        $image->save();

        //Thumbnail index
        $img->fit(800, 600);
        $filename = $time.'_'.$product->id.'_800x600.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);

        //Thumbnail in slider
        $img->fit(100, 50);
        $filename = $time.'_'.$product->id.'_100x50.jpg';
        $filepath = public_path('crop_images').'/'.$filename;
        $img->save($filepath);


        $product->images()->attach($image->id, ['status' => 1, 'order' => $product->images()->count()]);
        $product->save();

        return $image->id;
    }

    public function removeproductimage(Request $request)
    {
        $input = $request->input();

        $product = Product::find($input['productid']);
        $product->images()->detach($input['id']);
        $product->save();

        Image::find($input['id'])->delete();

    }
}
