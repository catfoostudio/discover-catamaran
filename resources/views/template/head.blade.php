<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
{{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
{{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
<title>Discover Catamaran - Premium day trip: {{ $product->name }} </title>

<link rel="stylesheet" href="{{ asset('css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
<link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/fullcalendar.js') }}"></script>


<style>
    .tab-content table td:first-child{width:auto}
</style>
