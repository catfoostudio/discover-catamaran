<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherProductUnit extends Model
{
    //
    protected $table = 'voucher_product_unit';


    public function vouchers() {
        return $this->belongsTo('App\Models\Voucher');
    }

    public function productunit(){
        return $this->belongsTo('App\Models\ProductUnit');
    }

}
