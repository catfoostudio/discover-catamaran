<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!-- PAGE TITLE -->
    <title>Discover Catamaran - Fun Pursuit Lifestyle</title>

    <!-- FavIcon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/images/favicons/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/images/favicons/apple-touch-icon-60x60.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicons/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('assets/images/favicons/favicon-16x16.png') }}" sizes="16x16">

    <!-- META-DATA -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- CSS:: FONTS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- CSS:: BOOTSTRAP -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <!-- CSS:: ANIMATION -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

    <!-- CSS:: DATEPICKER -->
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">

    <!-- JQUERY:: BxSlider.CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.bxslider.css') }}">


    <!-- CSS:: OWL -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}">

    <!-- CSS:: -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
</head>

<body>
<div class="parallax-image-wrapper parallax-image-wrapper-100" data-anchor-target="#dragons + .gap" data-bottom-top="transform:translate3d(0px, 200%, 0px)" data-top-bottom="transform:translate3d(0px, 0%, 0px)">
    <div class="parallax-image parallax-image-100" style="background-image:url(assets/images/header_bg_02.jpg)" data-anchor-target="#dragons + .gap" data-bottom-top="transform: translate3d(0px, -80%, 0px);" data-top-bottom="transform: translate3d(0px, 80%, 0px);">
    </div>
</div>
<div id="skrollr-body" class="y-header_strip y-header_01">
    <div  id="dragons">
        <div class="header y-header_outer">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-sm-2">
                        <a href="{{ url('/') }}" class="y-logo"><img src="{{ asset('dct-0012.png') }}" alt=""></a>
                    </div>
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 wow fadeInDown text-center" data-wow-duration="1s">
                        <div class="y-top_info_part">
                            <address>
                                <a href="tel:+4401234567890">English</a> /
                                <a href="tel:+4401234567890">ไทย</a> /
                                <a href="tel:+4401234567890">中文</a>
                            </address>
                            <div class="y-social">
                                <a class="y-fb fa fa-facebook" href="#"></a>
                                <a class="y-tw fa fa-twitter" href="#"></a>
                                <a class="y-gp fa fa-google-plus" href="#"></a>
                                <a class="y-yt fa fa-youtube" href="#"></a>
                                <a class="y-linkd fa fa-linkedin" href="#"></a>
                                <a class="y-insta fa fa-instagram" href="#"></a>
                            </div>
                        </div>
                        <div class="y-menu_outer wow fadeInDown" data-wow-duration="2s">
                            <div class="rmm style">
                                <ul>
                                    <li><a href="{{ url('/') }}">HOT DEALS</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="index_01.html">Home I</a></li>--}}
                                            {{--<li><a href="index_02.html">Home II</a></li>--}}
                                            {{--<li><a href="index_03.html">Home III</a></li>--}}
                                            {{--<li><a href="index_04.html">Home IV</a></li>--}}
                                            {{--<li><a href="index_05.html">Home V</a></li>--}}
                                            {{--<li><a href="index_06.html">Home VI</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('premiumdaytrip') }}">PREMIUM DAY TRIP</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">PRIVATE CHARTER</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="charter_listing.html">Charter Listing</a></li>--}}
                                            {{--<li><a href="charter_single.html">Single Charter</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">DESTINATION</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="sale_listing.html">Sale Listing</a></li>--}}
                                            {{--<li><a href="sale_single.html">Single Product</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">GALLERY</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="destination_listing.html">Destination Listing</a></li>--}}
                                            {{--<li><a href="destination_single.html">Single Destination</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">DC LIFESTYLE</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="post_listing.html">Blog Listing</a></li>--}}
                                            {{--<li><a href="post_single.html">Single Blog</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">Contact</a>
                                        {{--<ul>--}}
                                            {{--<li><a href="contact_01.html">Contact I</a></li>--}}
                                            {{--<li><a href="contact_02.html">Contact II</a></li>--}}
                                            {{--<li><a href="contact_03.html">Contact III</a></li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    <li>
                                        <a href="{{ url('/') }}">Other Product</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="y-home_slider" class="y-banner">
            <div>
                <img src="{{ asset('images/slider/_R4A1536.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/BH10_26-11-59_6207_0.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/BH14_10_8_161226_0004.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/CR-32.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/CR-40-2.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/Private_trip12-1-60_170114_0029.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/Pro_B1421-2-60_170222_0017.jpg') }}" class="img-responsive" alt="">
            </div>
            <div>
                <img src="{{ asset('images/slider/TH_-1787.jpg') }}" class="img-responsive" alt="">
            </div>
            {{--<div>--}}
                {{--<iframe width="1000px" height="560px" src="https://www.youtube.com/embed/eUHV2fMFSg?playlist=VIDEOID&controls=0&showinfo=0&loop=1&autoplay=1" frameborder="0"></iframe>--}}
            {{--</div>--}}
        </div>
        {{--<div id="y-header_form">--}}
            {{--<div class="container wow fadeInUp" data-wow-duration="2.5s">--}}
                {{--<div class="row y-header_form">--}}
                    {{--<h3>search charter yachts</h3>--}}
                    {{--<div class="clearfix row">--}}
                        {{--<form action="#" class="y-form">--}}
                            {{--<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">--}}
                                {{--<div>--}}
                                    {{--<i class="material-icons">access_time</i>--}}
                                    {{--<input type="text" name="checkin" id="y-check_in" placeholder="Check In">--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<i class="material-icons">alarm_on</i>--}}
                                    {{--<input type="text" name="checkout"  id="y-check_out" placeholder="Check Out">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">--}}
                                {{--<div>--}}
                                    {{--<i class="material-icons">location_on</i>--}}
                                    {{--<select class="custom-select">--}}
                                        {{--<option>Destination</option>--}}
                                        {{--<option>Destination I</option>--}}
                                        {{--<option>Destination II</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<i class="material-icons">directions_boat</i>--}}
                                    {{--<select class="custom-select">--}}
                                        {{--<option>Boat Type</option>--}}
                                        {{--<option>Boat Type I</option>--}}
                                        {{--<option>Boat Type II</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">--}}
                                {{--<div class="y-form_gap">--}}
                                    {{--<button class="y-button"><i class="material-icons">send</i> SEARCH FOR YACHT</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    {{--<div class="gap gap-100" style="background-image:url(assets/images/header_bg_02.jpg);"></div>--}}
</div>
<section id="y-our_services">
    <div class="y-our_services">
        <div class="row y-section_inner clearfix" style="padding-top: 0px;">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="y-service_inner">
                    <a href="#"><img src="{{ asset('images/home/b14_1142016_2601.jpg') }}" alt="" class="img-responsive"></a>
                    <div class="y-servie_info">
                        <h4><a href="#">HOT DEALS</a></h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="y-service_inner">
                    <a href="#"><img src="{{ asset('images/home/Pro_C_vip_1461.jpg') }}" alt="" class="img-responsive"></a>
                    <div class="y-servie_info">
                        <h4><a href="#">PREMIUM DAY TRIP</a></h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="y-service_inner">
                    <a href="#"><img src="{{ asset('images/home/Pro_C_vip_6435_0.jpg') }}" alt="" class="img-responsive"></a>
                    <div class="y-servie_info">
                        <h4><a href="#">PRIVATE CHARTER</a></h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="y-service_inner">
                    <a href="#"><img src="{{ asset('images/home/TH_-7505.jpg') }}" alt="" class="img-responsive"></a>
                    <div class="y-servie_info">
                        <h4><a href="#">DESTINATION</a></h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="y-book_today">
    <div class="y-book_today">
        <div class="container">
            <div class="row y-single_info_inner y-section_content" style="margin-top:0px;">
                <div class="col-lg-12 clearfix">
                    <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                            <h1 class="y-heading">About us</h1>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                            <ul class="pull-right">
                                <li><a href="index.html">Home</a></li>
                                <li><span>About us</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 clearfix">
                    <div class="y-corporate_block clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                            <img src="{{ asset('images/home/_R4A2057_2.jpg') }}" class="img-responsive" alt="">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            </p>
                            <ul>
                                <li>Lorem Ipsum is simply dummy text of the printing</li>
                                <li>when an unknown printer took a galley of type</li>
                                <li>Advice and Assistance Investing</li>
                                <li>Comprehensive Support for Your Business</li>
                                <li>Lorem Ipsum is simply dummy text of the printing</li>
                                <li>when an unknown printer took a galley of type</li>
                                <li>Advice and Assistance Investing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="y-popular_destination">
    <div class="y-popular_destination">
        <div class="container">
            <div class="row y-section_inner">
                <div class="y-section_content clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="text-center">Popular Packages</h2>
                        <div class="y-dest_list">
                            <div class="y-dest_list_single clearfix">
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 pull-right">
                                    <a href="#"><img src="{{ asset('images/home/package/catamaran_45.jpg') }}" class="img-responsive" alt=""></a>
                                </div>
                                <div class="col-sm-7 wow fadeInRight" data-wow-duration="1s">
                                    <h3><a href="#">POWER CATAMARAN SUNSET CRUISE : Pro A</a></h3>
                                    <h4>SUNSET CRUISE WITH SNORKELING IN MAITHON AND RACHA ISLAND</h4>
                                    <ul>
                                        <li><strong>SCHEDULE:</strong> Every Wed, Fri and Sun</li>
                                        <li><strong>TIME:</strong> 12:00-19:00</li>
                                        <li><strong>FLEET:</strong> Azure 3, Power Catamaran</li>
                                        <li><strong>INCLUDE:</strong> Light lunch, Thai buffet dinner, snorkeling tools with life jackets, sea-kayaking, inflatable toys, drinking water, soft drink, snacks, seasonal fruit and insurance</li>
                                    </ul>
                                    <a href="#" class="y-button">Read More</a>
                                </div>
                            </div>
                            <div class="y-dest_list_single clearfix">
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    <a href="#"><img src="{{ asset('images/home/package/14095751_1121090857984018_8852255239973163010_n.jpg') }}" class="img-responsive" alt=""></a>
                                </div>
                                <div class="col-sm-7 col-xs-12 pull-right wow fadeInRight" data-wow-duration="1s">
                                    <h3><a href="#">BABA HIDEAWAY SAILING CATAMARAN : BH10</a></h3>
                                    <h4>MAITHON ISLAND SNORKELING WITH GOURMET SET AT SRI PANWA BY SAILING CATAMARAN</h4>
                                    <ul>
                                        <li><strong>SCHEDULE:</strong> Everyday (Except Wed)</li>
                                        <li><strong>TIME:</strong> 10:00 - 14:30 </li>
                                        <li><strong>FLEET:</strong> Sailing Catamaran</li>
                                        <li><strong>INCLUDE:</strong> Set menu at Baba Poolclub, snorkeling tools with life jacket, dring water, soft drinks, snacks, seasonal fruit and insurance</li>
                                    </ul>
                                    <a href="#" class="y-button">Read More</a>
                                </div>
                            </div>
                            <div class="y-dest_list_single clearfix">
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 pull-right">
                                    <a href="#"><img src="{{ asset('images/home/package/CR-38.JPG') }}" class="img-responsive" alt=""></a>
                                </div>
                                <div class="col-sm-7 wow fadeInRight" data-wow-duration="1s">
                                    <h3><a href="#">SAILING CATAMARA DOLPHIN QUEST :Pro B10 VIP</a></h3>
                                    <h4>SNORKELING WITH MEAL ON MAITHON ISLAND BY SAILING CATAMARAN </h4>
                                    <ul>
                                        <li><strong>SCHEDULE:</strong> Everyday </li>
                                        <li><strong>TIME:</strong> 09:30 - 16:00 </li>
                                        <li><strong>FLEET:</strong> Sailing Catamaran</li>
                                        <li><strong>INCLUDE:</strong> Thai buffet meal with BBQ seafood, snorkeling tools with life jackets, dring water, soft drinks, seasonal fruit and insurance</li>
                                    </ul>
                                    <a href="#" class="y-button">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="y-client_says_section">
    <div class="y-client_says_section">
        <div id="y-client_says_text" class="container">
            <div class="row y-section_inner">
                <h2 class="text-center">WHAT CLIENTS SAY</h2>
                <div id="y-client_testimonial_carousel">
                    <div class="y-client_testimonials">
                        <p class="y-client_testimonial_text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.
                        </p>
                        <div class="y-client_testimonial_user">
                            <span>Bill Gates <em>(Web Design)</em></span>
                            <div class="y-client_testimonial_rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                        </div>
                    </div>
                    <div class="y-client_testimonials">
                        <p class="y-client_testimonial_text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus. Aliquam mollis blandit arcu scelerisque condimentum. Sed augue nisi pellentesque et tempor.
                        </p>
                        <div class="y-client_testimonial_user">
                            <span>Henry Ford <em>(CEO founder)</em></span>
                            <div class="y-client_testimonial_rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="y-client_testimonials">
                        <p class="y-client_testimonial_text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa odio, eleifend non dictum et, euismod vel mauris. Pellentesque tortor felis, maximus id laoreet a, congue ut diam. Integer volutpat mauris id ligula placerat vestibulum. Nunc laoreet rutrum elit, id fringilla metus.
                        </p>
                        <div class="y-client_testimonial_user">
                            <span>Coco Chanel <em>(Marketing)</em></span>
                            <div class="y-client_testimonial_rating">
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="y-blogs">
    <div class="y-blogs">
        <div class="container">
            <div class="row y-section_inner">
                <div class="y-section_content_full clearfix">
                    <h2 class="text-center">Our Blogs</h2>
                    <div class="y-feature_post row clearfix">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <img src="assets/images/img_5.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-6 pull-right y-blog_info">
                            <h3>
                                Lorem ipsum dolor sit amet
                            </h3>
                            <ul class="y-blog_general_info">
                                <li><span><i class="material-icons">access_time</i>30 September, 2015</span></li>
                                <li><span><i class="material-icons">account_circle</i><a href="#">admin</a></span></li>
                            </ul>
                            <h5>Ut enim ad minim veniam, quis nostrud exercitation ullamco</h5>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Ut enim ad minim veniam,
                            </p>
                            <p>
                                quis nostrud exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat.
                            </p>
                            <a href="#" class="y-button">Read More</a>
                        </div>
                    </div>
                    <div class="row clearfix y-blog_listing">
                        <div class="col-sm-4 col-xs-12">
                            <div class="y-info_pop">
                                <a href="#">Lorem ipsum dolor sit amet</a>
                                <span><i class="material-icons">access_time</i>30 September, 2016</span>
                            </div>
                            <img src="assets/images/blog_img_1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="y-info_pop">
                                <a href="#">Lorem ipsum dolor sit amet</a>
                                <span><i class="material-icons">access_time</i>30 September, 2016</span>
                            </div>
                            <img src="assets/images/blog_img_2.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="y-info_pop">
                                <a href="#">Lorem ipsum dolor sit amet</a>
                                <span><i class="material-icons">access_time</i>30 September, 2016</span>
                            </div>
                            <img src="assets/images/blog_img_3.jpg" alt="" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="y-footer">
    <div class="y-footer">
        <div class="container">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><i class="material-icons">perm_phone_msg</i> <span>Contact Us</span></h5>
                    <p>Lorem ipsum dolor.<br>Lorem ipsum dolor, sit amet</p>
                    <p>
                        <a href="tel:1-888-888-888"> 1-888-888-888</a><br>
                        <a href="mailto:site@site.com">site@site.com</a>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdiscovercatamaran%2F&tabs&width=262&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=463910590321509" width="262" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><span>Member Login</span></h5>
                    <form action="#" class="y-form">
                        <input type="text" placeholder="Your Email">
                        <input type="password" placeholder="Password">
                        <button class="y-button">Login</button>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h5><span>Message to us</span></h5>
                    <form action="#" class="y-form">
                        <input type="text" placeholder="Your Email">
                        <textarea placeholder="Message.."></textarea>
                        <button class="y-button">Send</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="y-footer_strip">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <p class="text-left">
                            Copyright 2017 Discover Catamaran, all rights reserved.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 y-social_links">
                        <p>
                            <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="y-back_to_top" id="y-back_to_top">
    <i class="fa fa-anchor"></i>
</div>
<div class="y-line"></div>
<div class="y-loading" id="y-loading">
    <img src="{{ asset('assets/images/loading.gif') }}" alt="">
</div>
<!-- JQUERY:: JQUERY.MIN.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>

<!-- JQUERY:: WOW.JS -->
<script type="text/javascript" src="{{ asset('assets/js/wow.min.js') }}"></script>

<!-- JQUERY:: MENU.JS -->
<script type="text/javascript" src="{{ asset('assets/js/responsivemultimenu.js') }}"></script>

<!-- JQUERY:: OWL.JS -->
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

<!-- JQUERY:: DATEPICKER.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.ui.js') }}"></script>

<!-- CUSTOM:: CUSTOM.JS -->
<script type="text/javascript" src="{{ asset('assets/js/custom.min.js') }}"></script>

<!-- JQUERY:: BxSlider.JS -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.fitvids.js') }}"></script>

</body>

</html>
