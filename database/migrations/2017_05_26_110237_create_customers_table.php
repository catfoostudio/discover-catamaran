<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_code')->nullable();
            $table->string('name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('license_id')->nullable();
            $table->string('id_card_no')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('tel_code')->nullable();
            $table->string('tel');
            $table->string('mobile_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax_code')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('line')->nullable();
            $table->string('wechat')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('postcode')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->text('remark')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('customers');
    }
}
