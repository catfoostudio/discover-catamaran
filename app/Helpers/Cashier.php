<?php
/**
 * Created by PhpStorm.
 * User: catfoo
 * Date: 2/21/19
 * Time: 5:24 PM
 */

namespace App\Helpers;

//require_once './vendor/omise/omise-php/lib/Omise.php';

define('OMISE_PUBLIC_KEY','pkey_test_5fjechtz8nqp0kvg050');//config('omise.omise_public_key'));
define('OMISE_SECRET_KEY','skey_test_5fjecif1qw9as9u0n1f');//config('omise.omise_secret_key'));
define('OMISE_API_VERSION',config('omise.omise_api_version'));

class Cashier
{

    public static function charge(Array $data)
    {
        return \OmiseCharge::create($data);
    }

    public static function retrieve($id)
    {
        return \OmiseCharge::retrieve($id);
    }
}