@extends('cfadmin')
@section('content')
    <style>
        body {
            background: #FFFFFF;
            margin: 0px;
            font-size: 14px;
            color: #4f5252;
            font-weight: 400;
        }
        .container{
            margin-top:50px;
            padding: 10px;
        }
        ul, ol, li {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #reorder-helper{
            margin: 18px 10px;
            padding: 10px;
        }
        .light_box {
            background: #efefef;
            padding: 20px;
            margin: 15px 0;
            text-align: center;
            font-size: 1.2em;
        }

        /* image gallery */
        .gallery{ width:100%; float:left; margin-top:20px;}
        .gallery ul{ margin:0; padding:0; list-style-type:none;}
        .gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
        .gallery img{ width:250px;}

        /* notice box */
        .notice, .notice a{ color: #fff !important; }
        .notice { z-index: 8888;padding: 10px;margin-top: 20px; }
        .notice a { font-weight: bold; }
        .notice_error { background: #E46360; }
        .notice_success { background: #657E3F; }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $gallery->name }}
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/gallery') }}">Galleries</a></li>
                <li class="active">{{ $gallery->name }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gallery Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['method' => 'PUT' , 'action' => ['GalleryController@update', $gallery]]) !!}
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="กรอกชื่อ"
                                               value="{{ $gallery->name }}">
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Description</label>
                                        <textarea maxlength="191" class="form-control" name="description"
                                                  placeholder="คำอธิบาย">{{ $gallery->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="gallery_section" data-index="{{ count($gallery->images) }}">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="inputEmail3">Gallery</label> <i>(for best display please make sure all images are same resolution)</i><br>
                                        <button type="button" class="reorder_link" >Reorder photos</button>
                                        <button type="button" class="reload" style="display: none"><img src="{{ asset('images/refresh-animated.gif') }}" /></button>
                                        <button type="button" id="saveReorder"  style="display: none">Save reordering</button>
                                    <button id="add_photo" type="button">Add photos</button>
                                    <div id="reorderHelper" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
                                    <div class="gallery">
                                        <ul class="reorder_ul reorder-photos-list">
                                            <?php
                                            // Include and create instance of db class
                                            //require_once 'DB.class.php';
                                            //$db = new DB();

                                            // Fetch all images from database

                                            //$images = \App\Models\Image::all();
                                            ?>
                                            @if(!empty($gallery->images))
                                                @foreach($gallery->images()->orderBy('order')->get() as $image)
                                                    <li id="image_li_{{ $image->id }}" class="ui-sortable-handle">
                                                        <img class="btn-delete-image" data-id="{{ $image->id }}" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer" />
                                                        <a href="javascript:void(0);" style="float:none;" class="image_link">
                                                            <img src="{{ asset($image->path) }}" alt="" style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">
                                                        </a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div></div>

                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Save</button>
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({
                tags: true,
                templateSelection: function (selection) {
                    return jQuery(selection.element).data('custom');
                },
                createTag: function (params) {
                    return undefined;
                }
            });
            var options = {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
            };


//            $('textarea[name="description"]').ckeditor(options);

            $('.lfm').filemanager('image');


            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                minuteStep: 1,
            });


            $('.remove-row').on('click', function () {
                $(this).parent().parent().remove();
            });

            $('#btn_add_image').on('click', function () {
//                alert($('#gallery_section').data('index'));
                $('#gallery_section').data('index', $('#gallery_section').data('index') + 1);

//                alert($('#gallery_section').data('index'));

                $('#gallery_section').append('<div class="row">\n' +
                    '                                    <div class="col-md-12">\n' +
                    '                                        <div class="input-group">\n' +
                    '                                           <span class="input-group-btn">\n' +
                    '                                             <a data-input="thumbnail' + $('#gallery_section').data('index') + '" data-preview="holder" class="btn btn-primary lfm">\n' +
                    '                                               <i class="fa fa-picture-o"></i> Choose\n' +
                    '                                             </a>\n' +
                    '                                           </span>\n' +
                    '                                            <input id="thumbnail' + $('#gallery_section').data('index') + '" class="form-control" type="text" name="filepath[]">\n' +
                    '                                        </div>\n' +
                    //                    '                                        <img id="holder2" style="margin-top:15px;max-height:100px;">\n' +
                    '                                    </div>\n' +
                    '                                </div>');
                $('.lfm').filemanager('image');
            });

        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('js/jquery.ui.touch-punch.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.reorder_link').on('click',function(){
                var btn_reorder = $(this);
                $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
//                $('.reorder_link').html('save reordering');
//                $('.reorder_link').attr("id","saveReorder");
                $("#reorderHelper").html( "1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished." ).removeClass('notice notice_error notice_success').addClass('light_box');
                $('#reorderHelper').slideDown('slow');

                $('.reorder_link').css('display','none');
                $('.reload').css('display','none');
                $('#saveReorder').css('display','inline');

                $('.image_link').attr("href","javascript:void(0);");
                $('.image_link').css("cursor","move");

            });

            $("#saveReorder").click(function( e ){
                if( !$("#saveReorder i").length ){
                    $('.reorder_link').css('display','none');
                    $('.reload').css('display','inline');
                    $('#saveReorder').css('display','none');
                    {{--$(this).html('').prepend('<img src="{{ asset('images/refresh-animated.gif') }}"/>');--}}
                    $("ul.reorder-photos-list").sortable('destroy');
                    $("#reorderHelper").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                    var h = [];
                    $("ul.reorder-photos-list li").each(function() {  h.push($(this).attr('id').substr(9));  });

                    $.ajax({
                        type: "POST",
                        url: "{{ url('cfadmin/api/orderprocessgallery') }}",
                        data: {ids: " " + h + "",galleryid:"{{$gallery->id}}"},
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        success: function(){
                            $("#reorderHelper").html( "Finish! Reordered Photos" ).removeClass('light_box').addClass('notice notice_success');
                            $('.reorder_link').css('display','inline');
                            $('.reload').css('display','none');
                            $('#saveReorder').css('display','none');
//                            btn_reorder.text('').prepend('Reorder photos');
//                            $('.reorder_link').removeAttr( "id" );
                        }
                    });
                    return false;
                }
                e.preventDefault();
            });

            var lfm = function(options, cb) {

                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = cb;
            }


            $('.btn-delete-image').click(function () {

                var imageObject = $(this).parent();

                $.ajax({
                    type: "POST",
                    url: "{{ url('cfadmin/api/removegalleryimage') }}",
                    data: {id: $(this).data('id'), galleryid: '{{ $gallery->id }}'},
                    headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    success: function(result){
                        imageObject.remove();
                    }
                });

            });



            $('#add_photo').click( function (e) {
                var btn_add_photo = $(this);
                lfm({type: 'image', prefix: ''}, function(url, path) {
                    //alert(url + ' ' + path);
                    btn_add_photo.html('').prepend('<img src="{{ asset('images/refresh-animated.gif') }}"/>');
                    $("#reorderHelper").html( "Processing Photo - This could take a moment. Please don't navigate away from this page." ).removeClass('notice notice_success light_box').addClass('notice notice_error');
                    $('#reorderHelper').slideDown('slow');
                    $.ajax({
                        type: "POST",
                        url: "{{ url('cfadmin/api/addgalleryimage') }}",
                        data: {path: path,id: '{{ $gallery->id }}'},
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        success: function(result){
                            $('.reorder-photos-list').append('<li id="image_li_'+result+'" class="ui-sortable-handle">\n' +
                                '                    <img class="btn-delete-image" data-id="'+result+'" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer" />\n' +
                                '                    <a href="javascript:void(0);" style="float:none;" class="image_link">\n' +
                                '                        <img src="'+url+'" alt="" style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">\n' +
                                '                    </a>\n' +
                                '                </li>');
                            btn_add_photo.html('').prepend('Add photo');
                            $("#reorderHelper").html( "Finish! Photo Added" ).removeClass('notice notice_success light_box').addClass('notice notice_success');
                        }
                    });


                });
            });
        });
    </script>
@endsection
