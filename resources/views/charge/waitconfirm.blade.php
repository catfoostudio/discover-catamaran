<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Confirm Reservation</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
{{--<header class="intro">--}}
{{--<div class="wrap">--}}
{{--<h1>Our Company</h1>--}}
{{--<p>Discover Catamaran - The Original Premium Catamaran Excursion in Phuket</p>--}}
{{--</div>--}}
{{--</header>--}}
<!-- //Intro -->
    <style>
        label{
            font-weight: bold;
        }
        .one-half{
            padding-bottom: 10px;
        }
        .one-third{
            padding-bottom: 10px;
        }
    </style>

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    <div class="box-white" style="padding-top: 40px;text-align: center">
                        <h1>THANK YOU FOR YOUR BOOKING</h1>
                        <p style="font-size: 20px;">
                            We will check your booking and send invoice as soon as possible
                        </p>
                    </div>
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
{{--<div class="wrap center">--}}
{{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
{{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
{{--dates for our convenience - brilliant!” </p>--}}
{{--<p>- Johnatan Davidson</p>--}}
{{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $('#btn-cancel-order').click(function () {

    });
</script>
</body>
</html>