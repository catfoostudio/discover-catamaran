<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Customer;
use App\Models\Hotel;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\Tourcompany;
use App\Models\Voucher;
use App\Models\VoucherPrefix;
use App\Models\VoucherProductUnit;
use App\Models\Zone;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class VoucherController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'voucher.cfadmin.index' : 'home.index';


        $vouchers = Voucher::where('traveldate', '>', Carbon::now()->subYear())->orderBy('updated_at', 'desc')->get();
        return view($view, [
            'vouchers' => $vouchers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = ($this->isAdminRequest()) ? 'voucher.cfadmin.create' : 'home.index';

        $countries = Country::where('status', 1)->get();
        $hotels = Hotel::where('status', 1)->get();
        $products = Product::where('status', 1)->get();
        $tourcompanies = Tourcompany::where('status', 1)->get();
        $customers = Customer::where('status', 1)->get();
        if (Auth::user()->role->code == 'admin') {
            $voucherprefixes = VoucherPrefix::where('status', 1)->get();
        } else {
            $voucherprefixes = Auth::user()->voucherprefix;
        }

        return view($view, [
            'countries' => $countries,
            'hotels' => $hotels,
            'products' => $products,
            'tourcompanies' => $tourcompanies,
            'customers' => $customers,
            'voucherprefixes' => $voucherprefixes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        //
        //dd($input);
        $validate = [
            'adult' => 'required|numeric',
            'child' => 'required|numeric',
            'infant' => 'required|numeric',
            //'pickuptime' => 'required',
            'travel_date' => 'required',
//            'product_note' => 'required',
//            'confirmation_no' => 'required',
            //'confirm_by' => 'required',
//            'unit_amount.*' => 'required',
            'product' => 'required',
            'tourcompany' => 'required',
        ];


        if ($input['customer_type'] == 1) {
            $validate['cust_name'] = 'required|max:191';
            $validate['cust_tel'] = 'required';
        } else {
            $validate['customer'] = 'required';
        }

        if (!empty($input['product'])) {
            $product = Product::find($input['product']);

            if ($product->per_head != 1) {
                $validate['unit_amount.*'] = 'required';
            }
        }

        //if($input['unit_id'])

        $this->validate($request, $validate);

        //     dd($request->input());

        if ($input['customer_type'] == 1) {

            $customer = new Customer();
            $customer->name = $input['cust_name'];
            $customer->tel = $input['cust_tel'];
            $customer->customer_code = '0';
            $customer->remark = '';
            $customer->status = 1;
            $customer->save();

        } else {

            $customer = Customer::find($input['customer']);
        }

        $voucher = new Voucher();

        $voucher->adult = $input['adult'];
        $voucher->child = $input['child'];
        $voucher->infant = $input['infant'];

        $voucher->transfer = $input['transfer'];

        if ($input['transfer'] == 'notf') {
            $voucher->pickuptime_text = 'To be confirmed';
        } else {
            if (!empty($input['pickuptime']))
                $voucher->pickuptime_text = $input['pickuptime'];
        }

        if (!empty($input['pickuptime']))
            $voucher->pickuptime = date("H:i", strtotime($input['pickuptime']));

        $voucher->traveldate = Carbon::createFromFormat('m/d/Y', $input['travel_date'])->toDateString();
        if (!empty($input['hotel']) && $input['transfer'] != 'notf') {
            $voucher->hotel_id = $input['hotel'];
        }

        $voucher->product_id = $input['product'];

        if (!empty($input['product_note'])) {
            $voucher->product_note = $input['product_note'];
        } else {
            $voucher->product_note = '';
        }

        $voucher->tourcompany_id = $input['tourcompany'];

        if (!empty($input['confirmation_no'])) {
            $voucher->confirmation_no = $input['confirmation_no'];
        } else {
            $voucher->confirmation_no = '';
        }

        if (!empty($input['confirm_by'])) {
            $voucher->confirm_by = $input['confirm_by'];
        } else {
            $voucher->confirm_by = '';
        }
        if (!empty($input['remark']))
            $voucher->remark = $input['remark'];
        else
            $voucher->remark = '';

        $voucher->user_id = Auth::user()->id;
        $voucher->customer_id = $customer->id;

        if (!empty($input['other_zone_flag'])) {
            $voucher->zone = $input['other_zone'];
        } else {
            $voucher->zone_id = $input['zone'];
        }


        $voucher_prefix = VoucherPrefix::find($input['voucher_prefix']);
        $voucher->voucher_prefix = $voucher_prefix->name;
        $voucher_prefix->amount += 1;
        $voucher->voucher_no = $voucher_prefix->amount + 1;

        $voucher->status = 1;

        $product = Product::find($voucher->product_id);

        $totalunitamount = 0;
        if ($product->per_head == 0) {

            for ($i = 0; $i < count($input['unit_amount']); $i++) {
                //$units[$input['unit_id'][$i]] = ['status' => 1, 'price' => $input['price'][$i]];
                $totalunitamount += $input['unit_amount'][$i];
            }
        } else {
            $totalunitamount = $input['adult'] + $input['child'] + $input['infant'];
        }

        if ($product->premium_day_trip_flag == 1) {
            //$tourcompany = Tourcompany::findOrFail(1);
            //$tourproduct = $tourcompany->products()->where('products.id', $voucher->product_id)->first()->pivot;
            //$stock = $tourproduct->stocks()->where('reserved_date', $voucher->traveldate)->first();

            $tourproduct = Product::has('tourcompanies')->find($input['product'])->tourcompanies->find($input['tourcompany'])->pivot;

            $stock = $tourproduct->stocks()->where('reserved_date', Carbon::createFromFormat('m/d/Y', $input['travel_date'])->toDateString())->first();

            //dd($input);

            if (!empty($stock)) {

                $seat = $totalunitamount;

                //dd(($stock->remaining_seat + $seat));
                if ($stock->max_seat >= ($stock->remaining_seat + $seat)) {
                    $stock->remaining_seat += $seat;
                    $stock->save();
                    $voucher_prefix->save();
                    $voucher->stock_id = $stock->id;
                    $voucher->save();

                    if ($product->per_head == 0) {
                        for ($i = 0; $i < count($input['unit_amount']); $i++) {
                            $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                            $voucher_product_unit = new VoucherProductUnit();
                            $voucher_product_unit->product_unit_id = $product_unit->id;
                            $voucher_product_unit->amount = $input['unit_amount'][$i];
                            $voucher_product_unit->status = 1;
                            $voucher_product_unit->voucher_id = $voucher->id;
                            $voucher_product_unit->save();
                        }
                    } else {
                        //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                        $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['adult'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['child'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['infant'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();
                    }

                    $request->session()->flash('alert-success', 'cut stock!');
                } else {
                    return Redirect::back()->withErrors(['stock full!'])->withInput();
                }
            } else {
                // $request->session()->flash('alert-danger', 'stock not initial!');

                return Redirect::back()->withErrors(['stock not initial!'])->withInput();
            }
        } else if ($product->premium_day_trip_flag == 2) {
            $request->session()->flash('alert-success', 'Project was successful updated!');
            $voucher_prefix->save();
            $voucher->save();
        } else if ($product->premium_day_trip_flag == 3) {

            $request->session()->flash('alert-success', 'Project was successful updated!');
            $voucher_prefix->save();
            $voucher->save();


            if ($product->per_head == 0) {
                for ($i = 0; $i < count($input['unit_amount']); $i++) {
                    $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                    $voucher_product_unit = new VoucherProductUnit();
                    $voucher_product_unit->product_unit_id = $product_unit->id;
                    $voucher_product_unit->amount = $input['unit_amount'][$i];
                    $voucher_product_unit->status = 1;
                    $voucher_product_unit->voucher_id = $voucher->id;
                    $voucher_product_unit->save();
                }
            } else {
                //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['adult'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['child'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['infant'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();
            }
        }

        $voucher = Voucher::find($voucher->id);
        $string = '';
        foreach (VoucherProductUnit::where('voucher_id', $voucher->id)->get() as $vpu) {
            $pu = ProductUnit::find($vpu->product_unit_id);

            $string .= $pu->unit->name . ': ' . $vpu->amount . ', ';
        }
        $voucherproductunits = VoucherProductUnit::where('voucher_id', $voucher->id)->get();
        $pdf = PDF::loadView('voucher.cfadmin.pdf', [
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ])->setPaper('a5', 'landscape');
        //return $pdf->stream();

        return view('voucher.cfadmin.previewpdf', [
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ]);

        //return redirect("cfadmin/voucher/pdf/" . $voucher->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($voucher)
    {
        //
        $view = ($this->isAdminRequest()) ? 'voucher.cfadmin.detail' : 'voucher.cfadmin.detail';

        $countries = Country::all();
        $hotels = Hotel::all();
        $products = Product::all();
        $tourcompanies = Tourcompany::all();
        $customers = Customer::all();
        if (Auth::user()->role->code == 'admin') {
            $voucherprefixes = VoucherPrefix::all();
        } else {
            $voucherprefixes = Auth::user()->voucherprefix;
        }

        $voucherproductunits = VoucherProductUnit::where('voucher_id', $voucher->id)->get();


        return view($view, [
            'countries' => $countries,
            'hotels' => $hotels,
            'products' => $products,
            'tourcompanies' => $tourcompanies,
            'customers' => $customers,
            'voucherprefixes' => $voucherprefixes,
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $voucher)
    {
        $input = $request->input();
        $validate = [
            'adult' => 'required|numeric',
            'child' => 'required|numeric',
            'infant' => 'required|numeric',
            'travel_date' => 'required',
        ];

        $product_id = $voucher->product_id;
        $tourcompany_id = $voucher->tourcompany_id;

        if (!empty($product_id)) {
            $product = Product::find($product_id);

            if ($product->per_head != 1) {
                $validate['unit_amount.*'] = 'required';
            }
        }

        $this->validate($request, $validate);

        if ($input['customer_type'] == 1) {

            $customer = new Customer();
            $customer->name = $input['cust_name'];
            $customer->tel = $input['cust_tel'];
            $customer->customer_code = '0';
            $customer->remark = '';
            $customer->status = 1;
            $customer->save();

        } else {

            $customer = Customer::find($input['customer']);
        }

        $voucher->transfer = $input['transfer'];

        if ($input['transfer'] == 'notf') {
            $voucher->pickuptime_text = 'To be confirmed';
            $voucher->hotel_id = null;
        } else {
            if (!empty($input['pickuptime']))
                $voucher->pickuptime_text = $input['pickuptime'];
        }

        if (!empty($input['pickuptime']))
            $voucher->pickuptime = date("H:i", strtotime($input['pickuptime']));


        if (!empty($input['hotel']) && $input['transfer'] != 'notf') {
            $voucher->hotel_id = $input['hotel'];
        }

        if (!empty($input['product_note'])) {
            $voucher->product_note = $input['product_note'];
        } else {
            $voucher->product_note = '';
        }

        if (!empty($input['confirmation_no'])) {
            $voucher->confirmation_no = $input['confirmation_no'];
        } else {
            $voucher->confirmation_no = '';
        }

        if (!empty($input['confirm_by'])) {
            $voucher->confirm_by = $input['confirm_by'];
        } else {
            $voucher->confirm_by = '';
        }
        if (!empty($input['remark']))
            $voucher->remark = $input['remark'];
        else
            $voucher->remark = '';

        $voucher->user_id = Auth::user()->id;
        $voucher->customer_id = $customer->id;

        if (!empty($input['other_zone_flag'])) {
            $voucher->zone = $input['other_zone'];
        } else {
            $voucher->zone_id = $input['zone'];
        }

        $old_traveldate = $voucher->traveldate;
        $voucher->traveldate = Carbon::createFromFormat('m/d/Y', $input['travel_date'])->toDateString();


        $product = Product::find($voucher->product_id);

        $totalunitamount = 0;
        if ($product->per_head == 0) {

            for ($i = 0; $i < count($input['unit_amount']); $i++) {
                //$units[$input['unit_id'][$i]] = ['status' => 1, 'price' => $input['price'][$i]];
                $totalunitamount += $input['unit_amount'][$i];
            }
        } else {
            $totalunitamount = $input['adult'] + $input['child'] + $input['infant'];

        }

        if ($product->premium_day_trip_flag == 1) {
            //$tourcompany = Tourcompany::findOrFail(1);
            //$tourproduct = $tourcompany->products()->where('products.id', $voucher->product_id)->first()->pivot;
            //$stock = $tourproduct->stocks()->where('reserved_date', $voucher->traveldate)->first();

            $tourproduct = Product::has('tourcompanies')->find($product_id)->tourcompanies->find($tourcompany_id)->pivot;

            $stock = $tourproduct->stocks()->where('reserved_date', Carbon::createFromFormat('m/d/Y', $input['travel_date'])->toDateString())->first();

            $old_stock = $tourproduct->stocks()->where('reserved_date', $old_traveldate)->first();

            if (!empty($stock)) {

                $seat = $totalunitamount;
                $oldamount = $voucher->adult + $voucher->child + $voucher->infant;

                if ($old_traveldate != Carbon::createFromFormat('m/d/Y', $input['travel_date'])->toDateString()) {
                    $old_stock->remaining_seat = $old_stock->remaining_seat - $oldamount;
                    $newseat = $stock->remaining_seat + $seat;
                } else {
                    $newseat = $stock->remaining_seat - $oldamount + $seat;
                }

                if ($stock->max_seat >= $newseat) {

                    //TODO: Update old stock too! save new stock remaining seat. But if old stock need to be update is not update right now.
                    $stock->remaining_seat = $newseat;
                    $stock->save();
                    $old_stock->save();



                    $voucher->stock_id = $stock->id;
                    $voucher->adult = $input['adult'];
                    $voucher->child = $input['child'];
                    $voucher->infant = $input['infant'];
                    $voucher->save();

                    if ($product->per_head == 0) {
                        for ($i = 0; $i < count($input['unit_amount']); $i++) {
                            $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                            $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                            $voucher_product_unit = new VoucherProductUnit();
                            $voucher_product_unit->product_unit_id = $product_unit->id;
                            $voucher_product_unit->amount = $input['unit_amount'][$i];
                            $voucher_product_unit->status = 1;
                            $voucher_product_unit->voucher_id = $voucher->id;
                            $voucher_product_unit->save();
                        }
                    } else {
                        //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                        $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                        $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['adult'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                        $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['child'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();

                        $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                        $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                        $voucher_product_unit->product_unit_id = $product_unit->id;
                        $voucher_product_unit->amount = $input['infant'];
                        $voucher_product_unit->status = 1;
                        $voucher_product_unit->voucher_id = $voucher->id;
                        $voucher_product_unit->save();
                    }

                    $request->session()->flash('alert-success', 'Voucher was successful updated!');
                } else {
                    return Redirect::back()->withErrors(['Stock full!'])->withInput();
                }
            } else {
                return Redirect::back()->withErrors(['stock not initial!'])->withInput();
            }
        } else if ($product->premium_day_trip_flag == 2) {
            $request->session()->flash('alert-success', 'Voucher was successful updated!');
            $voucher->save();
        } else if ($product->premium_day_trip_flag == 3) {
            $request->session()->flash('alert-success', 'Voucher was successful updated!');
            $voucher->save();

            if ($product->per_head == 0) {
                for ($i = 0; $i < count($input['unit_amount']); $i++) {
                    $product_unit = Product::has('units')->find($product->id)->units->find($input['unit_id'][$i])->pivot;
                    $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                            $voucher_product_unit = new VoucherProductUnit();
                    $voucher_product_unit->product_unit_id = $product_unit->id;
                    $voucher_product_unit->amount = $input['unit_amount'][$i];
                    $voucher_product_unit->status = 1;
                    $voucher_product_unit->voucher_id = $voucher->id;
                    $voucher_product_unit->save();
                }
            } else {
                //dd($product_unit = Product::has('units')->find($product->id)->units->where('name', 'Adult')->first()->pivot);
                $product_unit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['adult'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['child'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();

                $product_unit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                $voucher_product_unit = VoucherProductUnit::where('voucher_id', $voucher->id)->where('product_unit_id', $product_unit->id)->first();
//                        $voucher_product_unit = new VoucherProductUnit();
                $voucher_product_unit->product_unit_id = $product_unit->id;
                $voucher_product_unit->amount = $input['infant'];
                $voucher_product_unit->status = 1;
                $voucher_product_unit->voucher_id = $voucher->id;
                $voucher_product_unit->save();
            }
        }

        $voucher = Voucher::find($voucher->id);
        $string = '';
        foreach (VoucherProductUnit::where('voucher_id', $voucher->id)->get() as $vpu) {
            $pu = ProductUnit::find($vpu->product_unit_id);

            $string .= $pu->unit->name . ': ' . $vpu->amount . ', ';
        }
        $voucherproductunits = VoucherProductUnit::where('voucher_id', $voucher->id)->get();

        return view('voucher.cfadmin.previewpdf', [
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($voucher)
    {
        $voucher->status = 0;
        $voucher->save();

        return redirect("cfadmin/voucher");
    }

    public function pdf($id)
    {
        if($this->isAdminRequest()){
            $voucher = Voucher::find($id);
        }
        else{
            $voucher = Voucher::where('pdf_link',$id)->first();
        }


        $string = '';
        foreach (VoucherProductUnit::where('voucher_id', $voucher->id)->get() as $vpu) {
            $pu = ProductUnit::find($vpu->product_unit_id);

            $string .= $pu->unit->name . ': ' . $vpu->amount . ', ';
        }
        $voucherproductunits = VoucherProductUnit::where('voucher_id', $voucher->id)->get();
        $pdf = PDF::loadView('voucher.cfadmin.pdf', [
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ])->setPaper('a5', 'landscape');
        return $pdf->stream();

    }

    public function previewpdf($id)
    {
        $voucher = Voucher::find($id);

        $string = '';

        foreach (VoucherProductUnit::where('voucher_id', $id)->get() as $vpu) {
            $pu = ProductUnit::find($vpu->product_unit_id);

            $string .= $pu->unit->name . ': ' . $vpu->amount . ', ';
        }

        $countries = Country::all();
        $hotels = Hotel::all();
        $products = Product::all();
        $tourcompanies = Tourcompany::all();
        $customers = Customer::all();
        if (Auth::user()->role->code == 'admin') {
            $voucherprefixes = VoucherPrefix::all();
        } else {
            $voucherprefixes = Auth::user()->voucherprefix;
        }
        $voucherproductunits = VoucherProductUnit::where('voucher_id', $id)->get();

        return view('voucher.cfadmin.previewpdf', [
            'countries' => $countries,
            'hotels' => $hotels,
            'products' => $products,
            'tourcompanies' => $tourcompanies,
            'customers' => $customers,
            'voucherprefixes' => $voucherprefixes,
            'voucher' => $voucher,
            'voucherproductunits' => $voucherproductunits,
        ]);
    }

    public function updatestatus(Request $request)
    {
        $input = $request->input();

        $voucher_id = $input['voucher_id'];

        $voucher = Voucher::find($voucher_id);
        $string = "";
        
        if ($voucher->status == 4) {

        } else {

            $voucher->status = $input['status'];

            if ($input['status'] == 4) {


                $product = Product::find($voucher->product_id);

                $totalunitamount = 0;

                if ($product->premium_day_trip_flag == 1) {

                    $tourproduct = Product::has('tourcompanies')->find($voucher->product_id)->tourcompanies->find($voucher->tourcompany_id)->pivot;

                    $stock = $tourproduct->stocks()->where('reserved_date', Carbon::createFromFormat('Y-m-d', $voucher->traveldate)->toDateString())->first();

                    if (!empty($stock)) {

                        $seat = $totalunitamount;
                        $oldamount = $voucher->adult + $voucher->child + $voucher->infant;
                        $newseat = $stock->remaining_seat - $oldamount + $seat;

                        $string .= 'oldamount:'.$oldamount.'|oldstock:'.$stock->remaining_seat;

                        if ($stock->max_seat >= $newseat) {

                            $stock->remaining_seat = $newseat;
                            $stock->save();

//                            $request->session()->flash('alert-success', 'cut stock!');
                        } else {
                            return Redirect::back()->withErrors(['stock full!'])->withInput();
                        }
                    } else {
                        // $request->session()->flash('alert-danger', 'stock not initial!');

                        return Redirect::back()->withErrors(['stock not initial!'])->withInput();
                    }
                }


            }

            $voucher->save();
        }

        if ($voucher->status == 4 || $voucher->status == 6)
            $label = 'danger';
        elseif ($voucher->status == 1)
            $label = 'warning';
        else
            $label = 'success';

        return Response::json(array(
            'success' => true,
            'test' => $string,
            'result' => '<span class="label label-' . $label . '" style="font-size: 100%;font-weight: normal;">' . $voucher->statusText . '</span>',
        ));

    }

    public function getBasicObject()
    {
        return view('datatables.eloquent.basic-columns');
    }

    public function getBasicObjectData()
    {
        $vouchers = Voucher::select(['id', 'status', 'voucher_prefix', 'voucher_no', 'created_at', 'updated_at']);

        return Datatables::of($vouchers)
            ->addColumn('action', function ($voucher) {
                $result = '<div class="btn-group">';

                if ($voucher->status != 4 && $voucher->status != 6) {
                    $result .= '<select style = \'color: #000;\' class="voucher_status_select2" data-voucherid="' . $voucher->id . '">
                                                <option ' . (($voucher->status == 1) ? 'selected=selected' : '') . ' value="1">Await</option>
                                                <option ' . (($voucher->status == 2) ? 'selected=selected' : '') . ' value="2">Confirmed</option>
                                                <option ' . (($voucher->status == 3) ? 'selected=selected' : '') . ' value="3">Completed</option>
                                                <option ' . (($voucher->status == 5) ? 'selected=selected' : '') . ' value="5">No Show</option>
                                                <option ' . (($voucher->status == 4) ? 'selected=selected' : '') . ' value="4">Cancelled</option>
                                                <option ' . (($voucher->status == 6) ? 'selected=selected' : '') . ' value="6">No Launch</option>
                                            </select>';
                }

                $result .= '<a style="margin-left:5px;" href="'.url('cfadmin/voucher/'.$voucher->id).'"><button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button></a>
                            <a href="'.url('cfadmin/voucher/pdf/'.$voucher->id).'" target="_blank"><button type="button" class="btn btn-primary btn-flat"><i class="fa fa-file-pdf-o"></i></button></a>';

                return $result . '</div>';
            })
            ->editColumn('status', '{!! $statusbadge !!}')
            ->rawColumns(['status', 'action'])
            ->make(true);


    }
}
