@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Vouchers
                {{--<small>Preview</small>--}}
                <a class="btn btn-app" href="{{ url('cfadmin/voucher/create') }}">
                    <i class="fa fa-plus"></i> เพิ่ม
                </a>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                {{--<li><a href="#">โรงแรม</a></li>--}}
                <li class="active">Vouchers</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Vouchers Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Voucher No</th>
                                    <th>Customer</th>
                                    <th>Travel Date</th>
                                    <th>Product</th>
                                    <th>Hotel</th>
                                    <th>Pickup Time</th>
                                    <th>Tel.</th>
                                    <th>Confirm No.</th>
                                    <th>Confirmed By</th>
                                    <th>Tour Company</th>
                                    <th>User</th>
                                    <th>Created Date </th>
                                    <th>Updated Date </th>
                                    <th>Transfer</th>
                                </tr>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Voucher No</th>
                                    <th>Customer</th>
                                    <th>Travel Date</th>
                                    <th>Product</th>
                                    <th>Hotel</th>
                                    <th>Pickup Time</th>
                                    <th>Tel.</th>
                                    <th>Confirm No.</th>
                                    <th>Confirmed By</th>
                                    <th>Tour Company</th>
                                    <th>User</th>
                                    <th>Created Date </th>
                                    <th>Updated Date </th>
                                    <th>Transfer</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Action</th>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Voucher No</th>
                                    <th>Customer</th>
                                    <th>Travel Date</th>
                                    <th>Product</th>
                                    <th>Hotel</th>
                                    <th>Pickup Time</th>
                                    <th>Tel.</th>
                                    <th>Confirm No.</th>
                                    <th>Confirmed By</th>
                                    <th>Tour Company</th>
                                    <th>User</th>
                                    <th>Created Date </th>
                                    <th>Updated Date </th>
                                    <th>Transfer</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($vouchers as $voucher)
                                    <tr>
                                        <td align="right">
                                            <div class="btn-group">
                                                @if($voucher->status != 4 && $voucher->status != 6)
                                                    <select style='color: #000;' class="voucher_status_select" data-voucherid="{{ $voucher->id }}">
                                                        <option {{ $voucher->status == 1 ? 'selected=selected' : '' }} value="1">Await</option>
                                                        <option {{ $voucher->status == 2 ? 'selected=selected' : '' }} value="2">Confirmed</option>
                                                        <option {{ $voucher->status == 3 ? 'selected=selected' : '' }} value="3">Completed</option>
                                                        <option {{ $voucher->status == 5 ? 'selected=selected' : '' }} value="5">No Show</option>
                                                        <option {{ $voucher->status == 4 ? 'selected=selected' : '' }} value="4">Cancelled</option>
                                                        <option {{ $voucher->status == 6 ? 'selected=selected' : '' }} value="6">No Launch</option>
                                                    </select>
                                                @endif
                                                <a href="{{ url('cfadmin/voucher/'.$voucher->id) }}"><button type="button" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></button></a>
                                                <a href="{{ url('cfadmin/voucher/pdf/'.$voucher->id) }}" target="_blank"><button type="button" class="btn btn-primary btn-flat"><i class="fa fa-file-pdf-o"></i></button></a>

                                            </div>
                                        </td>
                                        <td>{{ $voucher->id }}</td>
                                        <td>
                                            {{--@if($voucher->status == 4)--}}
                                                {{--<span class="label label-danger" style="font-size: 100%;font-weight: normal;">Cancelled</span>--}}
                                            {{--@elseif($voucher->status == 6)--}}
                                                {{--<span class="label label-danger" style="font-size: 100%;font-weight: normal;">No Launch</span>--}}
                                            {{--@elseif($voucher->status == 1)--}}
                                                {{--<span class="label label-warning" style="font-size: 100%;font-weight: normal;">Await</span>--}}
                                            {{--@else--}}
                                                {{--<span class="label label-success" style="font-size: 100%;font-weight: normal;">{{ $voucher->statusText }}</span>--}}
                                            {{--@endif--}}
                                            {!! $voucher->StatusBadge !!}
                                        </td>
                                        <td>{{ $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT) }}</td>
                                        <td>{{ (!empty($voucher->customer)) ? $voucher->customer->name : '' }}</td>
                                        <td>{{ $voucher->traveldate }}</td>
                                        <td>{{ (!empty($voucher->product)) ? $voucher->product->name : '' }}</td>
                                        <td>{{ (!empty($voucher->hotel)) ? $voucher->hotel->name : '' }}</td>
                                        <td>{{ $voucher->pickuptime }}</td>
                                        <td>{{ (!empty($voucher->customer)) ? $voucher->customer->tel : '' }}</td>
                                        <td>{{ $voucher->confirmation_no }}</td>
                                        <td>{{ $voucher->confirm_by }}</td>
                                        <td>{{ (!empty($voucher->tourcompany)) ? $voucher->tourcompany->name : '' }}</td>
                                        <td>{{ (!empty($voucher->user)) ? $voucher->user->name : 'Online' }}</td>
                                        <td>{{ $voucher->created_at }}</td>
                                        <td>{{ $voucher->updated_at }}</td>
                                        <td>{{ $voucher->transfer }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->

                    </div>
                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="delete-modal">
        <div class="modal modal-danger" id="confirm-delete-voucher-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-outline pull-left" id="confirm-delete">Confirm</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
    <!-- /.delete-modal -->
@endsection

@section('script')
    <style>
        th, td {
            white-space: nowrap;
        }
    </style>
    <!-- jQuery 2.2.3 -->
    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {

            $('.btn-delete-voucher').on('click', function () {
                $('#confirm-delete-voucher-modal').modal('show');
                $('#confirm-delete-voucher-modal').find('.modal-title').text('Warning!');
                $('#confirm-delete-voucher-modal').find('.modal-body').text('Are you sure you want to delete this record?');

                var form = $(this).parent();
//                $hotel_id = $(this).data('hotelid');

                $('#confirm-delete').on('click' , function () {
                    form.submit();
                });

            });


            // Setup - add a text input to each header cell
            $('#example1 thead tr:eq(1) th').each( function () {


                var title = $('#example1 thead tr:eq(0) th').eq( $(this).index() ).text();

                if(title == 'ID'){
                    $(this).html( '<input type="text" placeholder="'+title+'" style="width: 50px;" />' );
                }
                else{
                    if($(this).index() == 0){
                        $(this).html( '<div style="width:160px;"></div>' );
                    }
                    else if($(this).index() == 2){
                        $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 80px;"  />' );
                    }
                    else if($(this).index() == 3){
                        $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 80px;"  />' );
                    }
                    else if($(this).index() == 5){
                        $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: 80px;"  />' );
                    }
                    else{
                        $(this).html( '<input type="text" placeholder="Search '+title+'" style="width: auto;"  />' );
                    }

                }

            } );

            {{--var table2 = $('#users-table').DataTable({--}}
                {{--orderCellsTop: true,--}}
                {{--"autoWidth": false,--}}
                {{--"sScrollX": "100%",--}}
                {{--"sScrollXInner": "110%",--}}
                {{--"bScrollCollapse": true,--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{{ url('cfadmin/getBasicObjectData') }}',--}}
                {{--columns: [--}}
                    {{--{ data: 'action', name: 'action', orderable: false, searchable: false},--}}
                    {{--{ data: 'id', name: 'id' },--}}
                    {{--{ data: 'status', name: 'status' },--}}
                    {{--{ data: 'voucher_prefix', name: 'voucher_prefix' },--}}
                    {{--{ data: 'voucher_no', name: 'voucher_no' },--}}
                    {{--{ data: 'created_at', name: 'created_at' },--}}
                    {{--{ data: 'updated_at', name: 'updated_at' }--}}
                {{--],--}}
                {{--"columnDefs": [--}}
                    {{--{ width: 300, targets: 0 },--}}
                {{--],--}}
                {{--order: [[ 1, "desc" ]],--}}
                {{--// initComplete: function () {--}}
                {{--//     this.api().columns().every(function () {--}}
                {{--//         var column = this;--}}
                {{--//         var input = document.createElement("input");--}}
                {{--//         $(input).appendTo($(column.header()).empty())--}}
                {{--//             .on('change', function () {--}}
                {{--//                 alert();--}}
                {{--//             });--}}
                {{--//     });--}}
                {{--// },--}}
                {{--// fnInitComplete: function(oSettings, json) {--}}
                {{--//     alert( 'DataTables has finished its initialisation.' );--}}
                {{--// }--}}
                {{--initComplete: function () {--}}
                    {{--// var r = $('#users-table tfoot tr');--}}
                    {{--//--}}
                    {{--// $('#users-table thead').append(r);--}}
                    {{--//--}}
                    {{--// this.api().columns().every(function () {--}}
                    {{--//     var column = this;--}}
                    {{--//     if(this[0][0] != 0){--}}
                    {{--//     var input = document.createElement("input");--}}
                    {{--//     $(input).appendTo($(column.header()).empty())--}}
                    {{--//         .on('change', function () {--}}
                    {{--//--}}
                    {{--//             column.search($(this).val(), false, false,true).draw();--}}
                    {{--//         });--}}
                    {{--//     }--}}
                    {{--//     else{--}}
                    {{--//         $(column.footer()).empty();--}}
                    {{--//     }--}}
                    {{--// });--}}

                {{--},--}}
                {{--drawCallback: function( settings ) {--}}
                    {{--$('.voucher_status_select2').on('change' , function () {--}}

                        {{--var select = $(this);--}}
                        {{--var status_value = $(this).val();--}}
                        {{--var formData = new FormData();--}}
                        {{--formData.append('voucher_id', $(this).data('voucherid'));--}}
                        {{--formData.append('status', $(this).val());--}}

                        {{--$.ajaxSetup({--}}
                            {{--headers: {--}}
                                {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                            {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                            {{--url: 'voucher/updatestatus',--}}
                            {{--method: 'POST',--}}
                            {{--processData: false,--}}
                            {{--contentType: false,--}}
                            {{--dataType: 'JSON',--}}
                            {{--cache: false,--}}
                            {{--data: formData,--}}
                            {{--success: function (result) {--}}

                                {{--if (result.success) {--}}

                                    {{--if(status_value == 4 || status_value == 6){--}}
                                        {{--select.remove();--}}
                                    {{--}--}}

                                    {{--var rowIdx = table.row('.selected').index();--}}
                                    {{--table.cell( rowIdx, 2 ).data(result.result).draw();--}}

                                {{--} else {--}}

                                {{--}--}}
                            {{--},--}}
                            {{--error: function (data) {--}}

                            {{--}--}}
                        {{--});--}}
                    {{--});--}}

                {{--}--}}

            {{--});--}}

//             // Apply the search
//             $.each($('.input-filter', table2.table().header()), function () {
//                 var column = table2.column($(this).index());
//
//                 $( 'input', this).on( 'keyup change', function () {
//                     if ( column.search() !== this.value ) {
//                         column
//                             .search( this.value )
//                             .draw();
//                     }
//                 } );
//             } );
//
// // Apply the search
//             $.each($('.select-filter', table2.table().header()), function () {
//                 var column = table2.column($(this).index());
//
//                 $( 'select', this).on( 'change', function () {
//                     if ( column.search() !== this.value ) {
//                         column
//                             .search( this.value )
//                             .draw();
//
//                     } else if ( column.search() !== "" ) {
//                         column
//                             .draw();
//                     }
//                 } );
//             } );

            var table = $('#example1').DataTable({
                orderCellsTop: true,
                "autoWidth": false,
                "sScrollX": "100%",
                "sScrollXInner": "110%",
                "bScrollCollapse": true,
                "pageLength": 25,
                "order": [[ 1, "desc" ]],
                "columnDefs": [
                    { width: 50, targets: 0 },
                    {
                        "targets": [ 1 ],
                        "visible": false
                    },
                    { "width": "50%", "targets": 2 },
                ],
                // drawCallback: function( settings ) {
                //     $('.voucher_status_select').on('change' , function () {
                //         var select = $(this);
                //         var status_value = $(this).val();
                //         var formData = new FormData();
                //         formData.append('voucher_id', $(this).data('voucherid'));
                //         formData.append('status', $(this).val());
                //
                //         $.ajaxSetup({
                //             headers: {
                //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //             }
                //         });
                //
                //         $.ajax({
                //             url: 'voucher/updatestatus',
                //             method: 'POST',
                //             processData: false,
                //             contentType: false,
                //             dataType: 'JSON',
                //             cache: false,
                //             data: formData,
                //             success: function (result) {
                //
                //                 if (result.success) {
                //
                //                     if(status_value == 4 || status_value == 6){
                //                         select.remove();
                //                     }
                //
                //                     var rowIdx = table.row('.selected').index();
                //                     table.cell( rowIdx, 2 ).data(result.result).redraw(false);
                //
                //                 } else {
                //
                //                 }
                //             },
                //             error: function (data) {
                //
                //             }
                //         });
                //
                //     });
                // }
            });

            $('#example1 tbody').on('change', '.voucher_status_select', function () {
                        var select = $(this);
                        var status_value = $(this).val();
                        var formData = new FormData();
                        formData.append('voucher_id', $(this).data('voucherid'));
                        formData.append('status', $(this).val());

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: 'voucher/updatestatus',
                            method: 'POST',
                            processData: false,
                            contentType: false,
                            dataType: 'JSON',
                            cache: false,
                            data: formData,
                            success: function (result) {

                                if (result.success) {

                                    if(status_value == 4 || status_value == 6){
                                        select.remove();
                                    }

                                    var rowIdx = table.row('.selected').index();
                                    table.cell( rowIdx, 2 ).data(result.result).redraw(false);

                                } else {

                                }
                            },
                            error: function (data) {

                            }
                        });
            } );

            // Apply the search
            table.columns().every(function (index) {
                $('#example1_wrapper thead tr:eq(1) th:eq(' + index + ') input').on('keyup change', function () {

                        table.column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();

                });
            });

            $('#example1 tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    //$(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#example1 tbody').on('dblclick', 'tr', function () {

                var data = table.row( this ).data();
                window.location = '{{ url('cfadmin/voucher/') }}/'+data[1];
//                alert( 'You clicked on '+data[0]+'\'s row' );
            } );

//            $('#button').click( function () {
//                table.row('.selected').remove().draw( false );
//            } );


            /* initialize the external events
             -----------------------------------------------------------------*/
            function ini_events(ele) {
                ele.each(function () {

                    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                    // it doesn't need to have a start or end
                    var eventObject = {
                        title: $.trim($(this).text()) // use the element's text as the event title
                    };

                    // store the Event Object in the DOM element so we can get to it later
                    $(this).data('eventObject', eventObject);

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true, // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });

                });
            }

            ini_events($('#external-events div.external-event'));

            /* initialize the calendar
             -----------------------------------------------------------------*/
            //Date for the calendar events (dummy data)
            var date = new Date();
            var d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear();
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
                //Random default events
                events: [
                    {
                        title: 'All Day Event',
                        start: new Date(y, m, 1),
                        backgroundColor: "#f56954", //red
                        borderColor: "#f56954" //red
                    },
                    {
                        title: 'Long Event',
                        start: new Date(y, m, d - 5),
                        end: new Date(y, m, d - 2),
                        backgroundColor: "#f39c12", //yellow
                        borderColor: "#f39c12" //yellow
                    },
                    {
                        title: 'Meeting',
                        start: new Date(y, m, d, 10, 30),
                        allDay: false,
                        backgroundColor: "#0073b7", //Blue
                        borderColor: "#0073b7" //Blue
                    },
                    {
                        title: 'Lunch',
                        start: new Date(y, m, d, 12, 0),
                        end: new Date(y, m, d, 14, 0),
                        allDay: false,
                        backgroundColor: "#00c0ef", //Info (aqua)
                        borderColor: "#00c0ef" //Info (aqua)
                    },
                    {
                        title: 'Birthday Party',
                        start: new Date(y, m, d + 1, 19, 0),
                        end: new Date(y, m, d + 1, 22, 30),
                        allDay: false,
                        backgroundColor: "#00a65a", //Success (green)
                        borderColor: "#00a65a" //Success (green)
                    },
                    {
                        title: 'Click for Google',
                        start: new Date(y, m, 28),
                        end: new Date(y, m, 29),
                        url: 'http://google.com/',
                        backgroundColor: "#3c8dbc", //Primary (light-blue)
                        borderColor: "#3c8dbc" //Primary (light-blue)
                    }
                ],
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.backgroundColor = $(this).css("background-color");
                    copiedEventObject.borderColor = $(this).css("border-color");

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                }
            });

            /* ADDING EVENTS */
            var currColor = "#3c8dbc"; //Red by default
            //Color chooser button
            var colorChooser = $("#color-chooser-btn");
            $("#color-chooser > li > a").click(function (e) {
                e.preventDefault();
                //Save color
                currColor = $(this).css("color");
                //Add color effect to button
                $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
            });
            $("#add-new-event").click(function (e) {
                e.preventDefault();
                //Get value and make sure it is not null
                var val = $("#new-event").val();
                if (val.length == 0) {
                    return;
                }

                //Create events
                var event = $("<div />");
                event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
                event.html(val);
                $('#external-events').prepend(event);

                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new-event").val("");
            });



        });
    </script>

@endsection
