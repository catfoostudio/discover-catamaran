<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('booking_number')->nullable();
            $table->string('voucher_prefix');
            $table->string('voucher_no');
            $table->integer('adult');
            $table->integer('child');
            $table->integer('infant');
            $table->text('product_note')->nullable();
            $table->decimal('total_price')->nullable();
            $table->decimal('discount_price')->nullable();
            $table->decimal('transfer_price')->nullable();
            $table->decimal('vat_price')->nullable();
            $table->decimal('fee_price')->nullable();
            $table->decimal('grandtotal_price')->nullable();
            $table->string('pickuptime_text')->nullable();
            $table->date('traveldate');
            $table->time('pickuptime')->nullable();
            $table->string('confirmation_no');
            $table->string('confirm_by');
            $table->string('transfer');
            $table->integer('zone_id')->unsigned()->nullable();
            $table->foreign('zone_id')->references('id')->on('zones');
            $table->string('zone')->nullable();
            $table->tinyInteger('status');
            $table->integer('invoice_id')->unsigned()->nullable();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->integer('tourcompany_id')->unsigned()->nullable();
            $table->foreign('tourcompany_id')->references('id')->on('tourcompanies');
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->foreign('hotel_id')->references('id')->on('hotels');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('stock_id')->unsigned()->nullable();
            $table->foreign('stock_id')->references('id')->on('stocks');
            $table->integer('code_id')->unsigned()->nullable();
            $table->foreign('code_id')->references('id')->on('codes');
            $table->text('remark');
            $table->string('pdf_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('vouchers');
    }
}
