<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Tourcompany;
use App\Models\TourcompanyProduct;
use App\Models\Zone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class PremiumdaytripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'premiumdaytrip.cfadmin.stock' : 'premiumdaytrip.index';
        $tourcompanywithproduct = Tourcompany::with('products')->get();
        $products = Product::where('premium_day_trip_flag', 1)->where('per_head',1)->where('status',1)->get();

//        dd($products[0]->units()->where('name','adult')->first());

        return view($view, [
            'tourcompanywithproduct' => $tourcompanywithproduct,
            'products' => $products,
        ]);
    }

    public function otherproduct()
    {
        //
        $view = ($this->isAdminRequest()) ? 'premiumdaytrip.cfadmin.stock' : 'premiumdaytrip.index';
        $tourcompanywithproduct = Tourcompany::with('products')->get();
        $products = Product::where('premium_day_trip_flag', 3)->where('status',1)->get();
        return view($view, [
            'tourcompanywithproduct' => $tourcompanywithproduct,
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        //dd($product->images->first());
        $products = Product::where('status',1)->get();
        $hotels = Hotel::where('status', 1)->get();

        $zones = Zone::whereHas('product', function ($query) use ($id) {
            $query->where('products.id', $id);
        })->get(['id', DB::raw('CONCAT(name, " ", DATE_FORMAT(pickuptime, "%h:%i %p")) as text')]);

        return view('premiumdaytrip.detail', [
            'product' => $product,
            'tourcompany' => $product->tourcompanies()->first(),
            'hotels' => $hotels,
            'zones' => $zones,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stock()
    {
        $view = ($this->isAdminRequest()) ? 'premiumdaytrip.cfadmin.stock' : 'home.index';

        $products = Product::where('premium_day_trip_flag', 1)->where('status', 1)->get();

        return view($view, [
            'products' => $products,
        ]);
    }

    public function updatestock(Request $request)
    {
        if($request->ajax()) {
            $input = $request->input();

            $tourproduct = Product::has('tourcompanies')->find($input['product'])->tourcompanies->find($input['tourcompany'])->pivot;

            $stock = $tourproduct->stocks()->where('reserved_date', $input['date'])->first();

            if (empty($stock)) {

                $stock = new Stock();
                $stock->tourcompany_product_id = $tourproduct->id;
                $stock->reserved_date = $input['date'];
                $stock->remaining_seat = 0;
                $stock->max_seat = $input['maxseat'];
                $stock->status = 1;
                $stock->remark = $input['remark'];
                $stock->save();

                return Response::json(array(
                    'success' => true,
                    'remainingseat' => $stock->remaining_seat,
                    'maxseat' => $stock->max_seat,
                    'initial' => true,
                    'remark' => $stock->remark,
                ));
            } else {
                if ($input['maxseat'] >= $stock->remaining_seat) {
                    $stock->max_seat = $input['maxseat'];
                    $stock->remark = $input['remark'];
                    $stock->status = 1;
                    $stock->save();

                    return Response::json(array(
                        'success' => true,
                        'remainingseat' => $stock->remaining_seat,
                        'maxseat' => $stock->max_seat,
                        'initial' => false,
                        'remark' => $stock->remark,
                    ));
                } else {
                    return Response::json(array(
                        'success' => false,
                        // 'data'   => $data
                    ));
                }

            }
        }

    }

    public function getvoucherlist(Request $request)
    {
//        $products = Product::where('premium_day_trip_flag' , 1)->get();
//        $tourproduct = $products->where('products.id' , 1)->first()->pivot;
//        $stock = $tourproduct->stocks()->where('reserved_date' , '2017-09-04')->first();

        $input = $request->input();

        $tourproduct = Product::has('tourcompanies')->find($input['product'])->tourcompanies->find($input['tourcompany'])->pivot;

        $stock = $tourproduct->stocks()->where('reserved_date' , $input['date'])->where('status', 1)->first();

//            $data  = [
//                'maxseat' => $stock->max_seat,
//                'remainingseat' => $stock->remaining_seat,
//                'date' => $stock->reserved_date,
//                'remark' => $stock->remark,
//            ];
//
//        return Response::json(array(
//            'success' => true,
//            'data'   => $data
//        ));
        return $stock->vouchers->toJson();
    }
}
