<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - DC Life Style</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    {{--<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">--}}
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body class="blog">
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=475854059211013&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <header class="intro">
        <div class="wrap">
            <h1>DC Life Style</h1>
        </div>
    </header>
    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- ThreeFourth -->
                <div class="full-width hentry">
                    <!-- Post Image -->
                    <div class="entry-featured">
                        <img src="{{ asset($dclifestyle->crop_image) }}" alt="destination" />
                    </div>
                    <!-- //Post Image -->

                    <!-- Post Content -->
                    <div>
                        <div class="box-white">
                            <h2>{{ $dclifestyle->name }}</h2>
                            <div class="meta">
                                <span>By <a href="#">{{ $dclifestyle->user->name }}</a></span>
                                <span>{{ date('d M Y', strtotime($dclifestyle->updated_at )) }}</span>
                                {{--<span>Categories: <a href="#">Events</a>, <a href="#">Luxury Yachts</a>, <a href="#">Photography</a></span>--}}
                                {{--<span><a href="#comments">2 Comments</a></span>--}}
                            </div>

                        </div>
                        <div class="my-container" style="padding: 30px;">
                            {!! $dclifestyle->content !!}
                        </div>
                        {{--<div class="box-white">--}}
                            {{--<div class="avatar left-pic">--}}
                                {{--<a href="#"><img src="{{ asset('images/dct-author.png')}}" alt="avatar" /></a>--}}
                            {{--</div>--}}
                            {{--<h6>About the author</h6>--}}
                            {{--<p>{{ $dclifestyle->user->remark }}</p>--}}
                        {{--</div>--}}
                    </div>
                    <!-- //Post Content -->

                    {{--<!-- Comments -->--}}
                    {{--<div class="comments box-white" id="comments">--}}
                        {{--<h6>Comments</h6>--}}
                        {{--<ul>--}}
                            {{--<li class="comment">--}}
                                {{--<div class="avatar left-pic"><a href="#"><img src="{{ asset('uploads/avatar1.jpg') }}" alt="avatar" width="80" /></a></div>--}}
                                {{--<div class="comment-author"><a href="#">Kimberly C.</a> said 1 month ago</div>--}}
                                {{--<div class="comment-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</div>--}}
                            {{--</li>--}}
                            {{--<li class="comment">--}}
                                {{--<div class="avatar left-pic"><a href="#"><img src="{{ asset('uploads/avatar1.jpg') }}" alt="avatar" width="80" /></a></div>--}}
                                {{--<div class="comment-author"><a href="#">Kimberly C.</a> said 1 month ago</div>--}}
                                {{--<div class="comment-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<h6>Leave a reply</h6>--}}
                        {{--<form>--}}
                            {{--<fieldset>--}}
                                {{--<div class="one-half">--}}
                                    {{--<label for="name">Your name</label>--}}
                                    {{--<input type="text" id="name"/>--}}
                                {{--</div>--}}

                                {{--<div class="one-half">--}}
                                    {{--<label for="email">Your e-mail</label>--}}
                                    {{--<input type="email" id="email"/>--}}
                                {{--</div>--}}

                                {{--<div class="full-width">--}}
                                    {{--<label>Your comment</label>--}}
                                    {{--<textarea></textarea>--}}
                                {{--</div>--}}

                                {{--<div class="full-width">--}}
                                    {{--<a href="#" title="Submit comment" class="button gold large">Submit comment</a>--}}
                                {{--</div>--}}
                            {{--</fieldset>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    {{--<!-- //Comments -->--}}

                    {{--<div class="pager box-white">--}}
                        {{--<a href="#" title="PREVIOUS POST" class="prev"><span class="icojam_symbol_backward"></span> PREVIOUS POST</a>--}}
                        {{--<a href="#" title="NEXT POST" class="next">NEXT POST <span class="icojam_symbol_foward"></span></a>--}}
                    {{--</div>--}}
                </div>
                <!-- //ThreeFourth -->

                <!-- OneFourth -->
                <!-- OneFourth -->

                <!-- //OneFourth -->
                <!-- //OneFourth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>

    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
    {{--<div class="wrap center">--}}
        {{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
        {{--<p>“Excellent - you found the right boat in the right place at the right time,<br /> and managed to change the dates for our convenience - brilliant!” </p>--}}
        {{--<p>- Johnatan Davidson</p>--}}
    {{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
{{--<script src="{{ asset('plugins/jquery-scoped-css/jquery.scoped.js') }}"></script>--}}
</body>
</html>