<?php

namespace App\Http\Controllers;

use App\Models\Discounttype;
use App\Models\Optional;
use App\Models\VoucherPrefix;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class DiscounttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'discounttype.cfadmin.index' : 'home.index';

        $discounttypes = Discounttype::all();

        return view($view, [
            'discounttypes' => $discounttypes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = ($this->isAdminRequest()) ? 'discounttype.cfadmin.create' : 'home.index';
        $discounttypes = Discounttype::all();
        $prefixes = VoucherPrefix::all();
        $optionals = Optional::all();
        return view($view, [
            'discounttypes' => $discounttypes,
            'prefixes' => $prefixes,
            'optionals' => $optionals,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->input();

        $discounttype = new Discounttype();
        $discounttype->name = $input['name'];
        $discounttype->value = $input['value'];
        $discounttype->type_value = $input['type_value'];
        $discounttype->min_purchase = $input['min_purchase'];
        $discounttype->max_discount = $input['max_discount'];
        $discounttype->adult_value = $input['adult_value'];
        $discounttype->child_value = $input['child_value'];
        $discounttype->private_value = $input['private_value'];
        $discounttype->user_id = Auth::user()->id;
        $discounttype->status = 1;
        $discounttype->save();

        if(!empty($input['optional'])){
            for ($i = 0; $i < count($input['optional']); $i++) {
                $optionals[$input['optional'][$i]] = ['user_id' => Auth::user()->id];
            }
            $discounttype->optionals()->sync($optionals);
        }

        $request->session()->flash('alert-success', 'Discount Type was successful created!');
        return redirect('cfadmin/discounttype/'.$discounttype->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($discounttype)
    {
        //
        $view = ($this->isAdminRequest()) ? 'discounttype.cfadmin.detail' : 'discounttype.detail';
        $optionals = Optional::all();
        //dd($dclifestyle);

        $discounttype = Discounttype::find($discounttype);
//        dd($discounttype->optionals()->pluck('optionals.id')->toArray());
        return view($view, [
            'discounttype' => $discounttype,
            'optionals' => $optionals,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->input();

        $discounttype = Discounttype::find($id);
        $discounttype->name = $input['name'];
        $discounttype->value = $input['value'];
        $discounttype->type_value = $input['type_value'];
        $discounttype->min_purchase = $input['min_purchase'];
        $discounttype->max_discount = $input['max_discount'];
        $discounttype->adult_value = $input['adult_value'];
        $discounttype->child_value = $input['child_value'];
        $discounttype->private_value = $input['private_value'];
        $discounttype->user_id = Auth::user()->id;

        for ($i = 0; $i < count($input['optional']); $i++) {
            $optionals[$input['optional'][$i]] = ['user_id' => Auth::user()->id];
        }
        $discounttype->save();
        $discounttype->optionals()->sync($optionals);


        $request->session()->flash('alert-success', 'Discount Type was successful updated!');
        return redirect('cfadmin/discounttype/'.$discounttype->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $discounttype = Discounttype::find($id);
        $codeNotExpire = $discounttype->codes()->where('status',1)->whereDate('startdate','<=',Carbon::now())->whereDate('enddate','>=',Carbon::now())->get();

        if(count($codeNotExpire)){
            session()->flash('alert-danger', 'Some Discount Code use this discount type! You can\'t delete before inactive that Discount Code.');
        }
        else{
            $discounttype->status = 0;
            $discounttype->save();
            session()->flash('alert-success', 'Discount Type was successful deleted!');
        }


        return redirect("cfadmin/discounttype");
    }

    public function active($id)
    {
        $discounttype = Discounttype::find($id);
        $discounttype->status = 1;
        $discounttype->save();
        session()->flash('alert-success', 'Discount Type was successful Actived!');
        return redirect("cfadmin/discounttype");
    }

    public function getdiscounttypesetting(Request $request)
    {
        if($request->ajax()){

            $input = $request->input();

            $discounttype = Discounttype::find($input['id']);
            $string = '';
            $first = true;
            foreach ($discounttype->optionals as $optional)
            {
                $string .=  '<input name="optional" type="radio" '.(($first) ? 'checked=checked' : '').' value="'.$optional->id.'"> <label for="inputEmail3">'.$optional->name.'</label><br>';
                $first = false;
            }

//            $data  = [
//                'maxseat' => $stock->max_seat,
//                'remainingseat' => $stock->remaining_seat,
//                'date' => $stock->reserved_date,
//                'remark' => $stock->remark,
//            ];

            return Response::json(array(
                'success' => true,
                'data'   => $discounttype,
                'optionals'   =>$string
            ));
        }
        else{

        }
    }
}
