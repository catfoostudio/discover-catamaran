<?php

namespace App\Http\Controllers;

use App\Helpers\Cashier;
use App\Helpers\Helper;
use App\Mail\OrderComplete;
use App\Models\Charge;
use App\Models\Code;
use App\Models\Country;
use App\Models\Customer;
use App\Models\Hotel;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\Tourcompany;
use App\Models\Voucher;
use App\Models\VoucherPrefix;
use App\Models\VoucherProductUnit;
use App\Models\Zone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('charge.complete');
    }

    /*
     * After confirm create temp voucher and call Omise API tp charge customer
     * Customer need to authorize himself with credit card
     * and callback to our website to update payment status
     */
    public function charge(Request $request)
    {

        DB::beginTransaction();
        try {

            $prefix = 'DCS';

            $product = Product::find(Session::get('product'));
            $tourcompany_id = Session::get('tourcompany');
            $adult = Session::get('adult');
            $child = Session::get('child');
            $infant = Session::get('infant');
            $travel_date = Session::get('travel_date');
            $paymentType = Session::get('payment'); //credit, bank, wechat
            $discountCode = Session::get('discount');

            //Sum all seat (Adult + child + infant)
            $totalunitamount = 0;
            $totalprice = 0;
            $adultProductUnit = null;
            $childProductUnit = null;
            $infantProductUnit = null;
            $fee = 3.5;
            $invoiceDays = null;
            $discountByType = 0;
            $discountOverall = 0;
            $codeId = null;

            if ($product->per_head != 0) {
                $totalunitamount = $adult + $child + $infant;
                $adultProductUnit = Product::has('units')->find($product->id)->units->find(1)->pivot;
                $adultprice = $adultProductUnit->price;

                $childProductUnit = Product::has('units')->find($product->id)->units->find(2)->pivot;
                $childprice = $childProductUnit->price;

                $infantProductUnit = Product::has('units')->find($product->id)->units->find(3)->pivot;
                $infantprice = $infantProductUnit->price;

                //Calculate totalprice before discount
                $totaladultprice = $adultprice * $adult;
                $totalchildprice = $childprice * $child;
                $totalinfant = $infantprice;
                $totalprice = $totaladultprice + $totalchildprice + $totalinfant;
            }

            //Retrieve discount price by Discount code
            $code = null;
            if (!empty($discountCode)) {
                $code = Code::where('code', $discountCode)->where('status', 1)->whereDate('startdate', '<=', Carbon::now())->whereDate('enddate', '>=', Carbon::now())->first();
                $codeId = $code->id;
                if (empty($code)) {
                    //If code is not available or not found redirect and return error to customer
                    return redirect('premiumdaytrip/' . $product->id)->withErrors(['discount' => 'Discount Code Incorrect or Expired'])->withInput();

                } else if ($totalprice < $code->min_purchase) {
                    //If promocode has minimum purchase require check first.
                    return redirect('premiumdaytrip/' . $product->id)->withErrors(['discount' => 'Your purchase not meet minimum purchase to use this discount code.'])->withInput();

                } else {
                    //If use promo code. Rrefix is change to prefix that promo code setting.
                    $prefix = $code->voucherprefix->name;
                    //If optional is set on is promo code get number of invoice days that promocode pick
                    if (!empty($code->optional)) {
                        $invoiceDays = $code->optional->number_value;
                    }

                    //Calculate DAY TRIP discount per type
                    $discountByType = ($code->adult_value * $adult) + ($code->child_value * $child);
                    $totalPriceAfterDiscountByType = $totalprice - $discountByType;

                    //Calculate overall discount
                    //This will discount the price after deduct price per type then will calculate this after
                    //get total price after discount per type already which if no discount per type is the same value as totalprice
                    $discountOverall = 0;
                    if ($code->discounttype->type_value == 'PERCENT') {
                        //Calculate in percent from total that above explain
                        $discountOverall = $totalPriceAfterDiscountByType * $code->value / 100;
                    } else {
                        //If just a value TH BAHT just grab that value
                        $discountOverall = $code->value;
                    }

                    //If setting max discount should check first before deduct from totalprice
                    //This check will apply only DISCOUNT VALUE. But discount per type is not check yet.
                    if ($code->max_discount != 0) {
                        $discountOverall = ($discountOverall > $code->max_discount) ? $code->max_discount : $discountOverall;
                    }

                    //Apply discount that we already calculate again to totalprice that we discoount by per type before this.
                    $totalafterdiscount = $totalPriceAfterDiscountByType - $discountOverall;

                }

            } else {
                //No discount just grab totalprice from base
                $totalafterdiscount = $totalprice;
            }

            if ($product->premium_day_trip_flag == 1) {

                $tourproduct = Product::has('tourcompanies')->find($product->id)->tourcompanies->find($tourcompany_id)->pivot;
                $stock = $tourproduct->stocks()->where('reserved_date', Carbon::createFromFormat('Y-m-d', $travel_date)->toDateString())->first();

                //Stock is initialized and available
                if (!empty($stock)) {

                    //seat that customer want to reserved (adult +child+ infant)
                    $seat = $totalunitamount;

                    //check stock seat is enough seat for new reservation
                    if ($stock->max_seat >= ($stock->remaining_seat + $seat)) {

                        //Reserved stock first prevent next voucher interfere this transaction.
                        $stock->remaining_seat += $seat;
                        $stock->save();

                        //Online Payment no member system always create new customer
                        $customer = new Customer();
                        $customer->name = Session::get('name');
                        $customer->tel = Session::get('tel');
                        $customer->email = Session::get('email');
                        $customer->line = Session::get('line');
                        $customer->wechat = Session::get('wechat');
                        $customer->customer_code = '0';
                        $customer->remark = '';
                        $customer->status = 1;
                        $customer->save();

                        //Get Voucher prefix then add 1 voucher to counter
                        $voucher_prefix = VoucherPrefix::where('name', $prefix)->first();
                        $voucher_prefix->amount += 1;
                        $voucher_prefix->save();

                        //Temp voucher created here
                        $voucher = new Voucher();
                        $voucher->adult = $adult;
                        $voucher->child = $child;
                        $voucher->infant = $infant;
                        $voucher->transfer = Session::get('transfer');
                        $voucher->traveldate = Carbon::createFromFormat('Y-m-d', $travel_date)->toDateString();

                        if (!empty(Session::get('hotel')) && Session::get('transfer') != 'notf') {
                            $voucher->hotel_id = Session::get('hotel');
                            $voucher->zone_id = Session::get('zone');
                        }

                        if (Session::get('transfer') == 'notf') {
                            $voucher->pickuptime_text = 'To be confirmed';
                            $voucher->transfer_price = 0;
                        } else {
                            //If customer choose with transfer setting detail and price here.
                            $zone = Zone::find(Session::get('zone'));
                            $pickuptime = $zone->name . ' ' . date("H:i", strtotime($zone->pickuptime));
                            $voucher->pickuptime_text = $pickuptime;
                            $voucher->transfer_price = $zone->price * ($adult + $child);
                        }

                        //Voucher detail
                        $voucher->product_id = $product->id;
                        $voucher->product_note = '';
                        $voucher->tourcompany_id = $tourcompany_id;
                        $voucher->confirmation_no = '';
                        $voucher->confirm_by = '';
                        $voucher->remark = '';
                        $voucher->customer_id = $customer->id;
                        $voucher->voucher_prefix = $voucher_prefix->name;
                        $voucher->voucher_no = $voucher_prefix->amount + 1;
                        $voucher->status = 7; //set Payment Pending status
                        $voucher->pdf_link = time() . '-' . Helper::generateRandomString();
                        $voucher->user_id = 6; //Default user for online booking
                        $voucher->stock_id = $stock->id; //link stock with voucher
                        $voucher->vat_price = 0;
                        $voucher->total_price = $totalprice; //Base total price with out discount, transfer, vat and fee
                        //save total discount value here
                        $voucher->discount_price = $discountByType + $discountOverall;
                        $voucher->code_id = $codeId;

                        //Add Transfer cost to totalprice after discount
                        $totalafterdiscount += $voucher->transfer_price;

                        $grandtotalprice = 0;
                        //Total price will add 3.5% for Omise If use credit card
                        if ($paymentType == 'credit') {
                            //Credit card use the totalprice after discount to calculate fee 3.5% for omise
                            $voucher->fee_price = ($totalafterdiscount * $fee / 100);
                            $grandtotalprice = $totalafterdiscount + $voucher->fee_price;

                        } else {
                            $voucher->fee_price = 0;
                            //Bank transfer and wechat pay no need to add omise fee price
                            $grandtotalprice = $totalafterdiscount;
                        }

                        //NOW WE FINALLY HAVE THE GRAND TOTAL HERER
                        //NOTE: DON'T HAVE A VAT CALCULATATION YET IN THIS VERSION

                        $invoice = $this->createInvoice($totalprice, $voucher->discount_price, $voucher->transfer_price, $voucher->fee_price, $grandtotalprice, $paymentType, $invoiceDays);

                        $voucher->grandtotal_price = $grandtotalprice;
                        $voucher->invoice_id = $invoice->id;
                        $voucher->save();

                        if (!empty($invoiceDays)) {
                            $invoice->remark = 'AGENT INVOICE: Code - '.$discountCode.', Discount ('.$code->value.' '.$code->discounttype->type_value.') - '.number_format($voucher->discount_price,2);
                            $invoice->save();
                        }

                        if ($product->per_head != 0) {
                            //Create product unit to kept amount of unit (Adult, Child and Infant)
                            //Adult
                            $adultUnit = new VoucherProductUnit();
                            $adultUnit->product_unit_id = $adultProductUnit->id;
                            $adultUnit->amount = $adult;
                            $adultUnit->status = 1;
                            $adultUnit->voucher_id = $voucher->id;
                            $adultUnit->save();

                            //Child
                            $childUnit = new VoucherProductUnit();
                            $childUnit->product_unit_id = $childProductUnit->id;
                            $childUnit->amount = $child;
                            $childUnit->status = 1;
                            $childUnit->voucher_id = $voucher->id;
                            $childUnit->save();

                            //Infant
                            $infantUnit = new VoucherProductUnit();
                            $infantUnit->product_unit_id = $infantProductUnit->id;
                            $infantUnit->amount = $infant;
                            $infantUnit->status = 1;
                            $infantUnit->voucher_id = $voucher->id;
                            $infantUnit->save();
                        }


                        if ($paymentType == 'credit') {
                            //OMISE CHARGE

                            if($grandtotalprice <= 20){
                                $invoice->remark = $invoice->remark. ', **CREDIT CARD BYPASS** price lower than 20 THB';
                                $invoice->save();
                                DB::commit();
                                return view('charge.waitconfirm');
                            }
                            else{
                                $omise_charge = $this->omiseCharge($voucher, $request->input('omiseToken'), $grandtotalprice,$request->ip());
                                DB::commit();
                                //If status pending (should be!) redirect to authorize uri to confirm authorize
                                if ($omise_charge['status'] == 'pending') {
                                    return redirect($omise_charge['authorize_uri']);
                                } elseif ($omise_charge['status'] == 'successful') {
                                    echo 'Please contact customer service. For update status.';
                                } else {
                                    echo 'Order cancelled. Please contact customer service.';
                                }
                            }

                        } else {

                            DB::commit();
                            //ถ้าไม่ใช่ agent invoice ที่ค้างเงิน เป็นแค่ส่วนลด หรือ bank transfer ก็จะส่งเมลเลยทันที
                            if (empty($invoiceDays)) {
                                Mail::to('lilybluecatz@gmail.com')->send(new \App\Mail\Invoice($voucher->toArray()));
                                return \redirect('payment/invoice/' . $voucher->voucher_prefix . '/' . $voucher->voucher_no);

                            } else {
                                return view('charge.waitconfirm');
                            }
                        }

                    } else {

                        //Clear session data
                        $request->session()->flush();
                        $request->session()->save();

                        return redirect('premiumdaytrip/' . $product->id)->withErrors(['stock full!'])->withInput();
                    }
                } else {
                    //Stock is not initialize yet reset data and return to daytrip page.
                    //Clear session data
                    $request->session()->flush();
                    $request->session()->save();

                    return redirect('premiumdaytrip/' . $product->id)->withErrors(['stock not initial!'])->withInput();
                }
            }


        } catch (\Exception $e) {
            DB::rollBack();
        }

    }

    public function success(Request $request, $id)
    {
        $voucher = Voucher::find($id);
        $omise_charge = Cashier::retrieve($voucher->charge->charge_id);

        if ($omise_charge['status'] == 'successful') {
            $charge = $voucher->charge;
            $charge->transaction_id = $omise_charge['transaction'];
            $charge->status = $omise_charge['status'];
            $charge->authorized = $omise_charge['authorized'];
            $charge->capture = $omise_charge['capture'];
            $charge->save();
            $voucher->status = 9;
            $voucher->save();
            $voucher->invoice->status = 3;
            $voucher->invoice->save();

            Mail::to('lilybluecatz@gmail.com')->send(new OrderComplete($voucher->toArray()));

            return view('charge.complete');
        } else {
            return 'status: ' . $omise_charge['status'];
        }
    }

    public function refund(Request $request, $id)
    {
//        $voucher = Voucher::find($id);
//        $omise_charge = Cashier::retrieve('chrg_test_5flou3lolu9xmy9pkzs');//$voucher->charge->charge_id);
//
//        $refund = $omise_charge->refunds()->create(array('amount' => $omise_charge['amount']));
//        $request->session()->flush();
//        $request->session()->save();
//        dd($request->session());
    }


    private function omiseCharge($voucher, $omiseToken, $amount, $ip)
    {
        $description = $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT). '|'.$voucher->traveldate.'|'.$voucher->product->name.'|'.$voucher->tourcompany->name.'|Adult:'.$voucher->adult .'/Child:'.$voucher->child.'/Infant:'.$voucher->infant .'|'.$voucher->customer->name.':'.$voucher->customer->tel.'|VoucherID:'.$voucher->id.'|InvoiceID:'.$voucher->invoice->id;
        //Charge Omise

        $omise_charge = Cashier::charge([
            'amount' => $amount * 100, //Convert to Omise currency unit *100 for decimal 2 digit
            'currency' => 'thb',
            'return_uri' => url('payment/success/' . $voucher->id),
            'card' => $omiseToken,
            'description' => $description,
            'ip' => $ip
        ]);

        //Create charge data row in our server
        $charge = new Charge();
        $charge->voucher_id = $voucher->id;
        $charge->charge_id = $omise_charge['id'];
        $charge->transaction_id = $omise_charge['transaction'];
        $charge->card_id = $omise_charge['card']['id'];
        $charge->customer_id = $omise_charge['customer'];
        $charge->amount = $omise_charge['amount'];
        $charge->currency = $omise_charge['currency'];
        $charge->status = $omise_charge['status'];
        $charge->authorized = $omise_charge['authorized'];
        $charge->capture = $omise_charge['capture'];
        $charge->save();

        return $omise_charge;
    }

    private function createInvoice($price, $discount, $transfer, $fee, $grandTotal, $paymentType, $invoiceDays)
    {
        //Create Invoice
        $invoice = new Invoice();
        $invoice->price = $price;
        $invoice->transfer = $transfer;
        $invoice->discount = $discount;
        $invoice->fee = $fee;
        $invoice->grandtotal = $grandTotal;
        $invoice->payment = $paymentType;


        //Invoice extend days from Promo code. If promocode not use or did'nt set invoice day. Just extend 36hours from now.
        if (empty($invoiceDays)) {
            $invoice->due_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addHours(36));
            $invoice->status = 1;
        } else {
            $invoice->due_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->addDays($invoiceDays));
            $invoice->status = 5;
        }
        $invoice->save();

        return $invoice;
    }

    public function showinvoice(Request $request, $prefix, $no)
    {
        $voucher = Voucher::where('voucher_prefix', $prefix)->where('voucher_no', $no)->first();

        if ($voucher->invoice->status === 5) {
            return abort(404);
        }
        return view('payment.invoice', [
            'voucher' => $voucher
        ]);
    }

    public function clearreservedsession()
    {
        //Clear session data
        Session::forget([
            'in-process',
            'travel_date',
            'name',
            'tel',
            'email',
            'adult',
            'child',
            'infant',
            'product',
            'tourcompany',
            'line',
            'wechat',
            'transfer',
            'zone',
            'hotel',
            'payment',
            'discount',
            'totalprice',
            'grandtotalprice',
        ]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

    }
}
