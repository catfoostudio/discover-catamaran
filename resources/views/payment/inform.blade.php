<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Confirm Reservation</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
        .ui-datepicker{
            background-color: white;
        }
        .error{
            border: orangered 1px solid !important;
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    {{--<header class="intro">--}}
        {{--<div class="wrap">--}}
            {{--<h1>Our Company</h1>--}}
            {{--<p>Discover Catamaran - The Original Premium Catamaran Excursion in Phuket</p>--}}
        {{--</div>--}}
    {{--</header>--}}
    <!-- //Intro -->
    <style>
        label{
            font-weight: bold;
        }
        .one-half{
            padding-bottom: 10px;
        }
        .one-third{
            padding-bottom: 10px;
        }
    </style>

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    {!! Form::open(['method' => 'POST' , 'action' => ['PaymentController@inform', $voucher], 'enctype' => 'multipart/form-data']) !!}
                    <div class="box-white" style="padding-top: 25px;">
                        <div class="row">
                            <div class="full-width" style="padding-bottom: 0px;">
                                <h1 style="padding-bottom: 2px;font-weight: bold">Inform Payment</h1><span style="color: orangered">(Required all information)</span>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one-third">
                                <label>Invoice no.</label>
                                <input type="text" name="no" value="{{ $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT) }}" {{ (($errors->has('no')) ? 'class=error' : '') }}>
                            </div>
                            <div class="one-third">
                                <label>Grand Total (Baht)</label>
                                <input type="text" name="amount" value="{{ $voucher->total_price }}" {{ (($errors->has('amount')) ? 'class=error' : '') }}>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one-third">
                                <label>Payment Type</label>
                                <select name="type">
                                    <option value="bank">Bank Transfer</option>
                                    <option value="wechat">Wechat Pay</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="one-third">
                                <label>Payment date (Y/m/d)</label>
                                <input type="text" id="paymentdate" name="date" {{ (($errors->has('date')) ? 'class=error' : '') }}>
                            </div>
                            <div class="one-third">
                                <label>Payment time (e.g. 17.58)</label>

                                <input type="text" name="time" {{ (($errors->has('time')) ? 'class=error' : '') }}>
                            </div>
                        </div>
                        <div class="row">

                            <div class="one-third">
                                <label>Attachment pay slip</label>
                                <input type="file" name="file" {{ (($errors->has('file')) ? 'class=error' : '') }}>
                            </div>
                        </div>
                        <div class="row">
                            <div class="full-width">
                                <button type="submit" class="button gold medium">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
{{--<div class="wrap center">--}}
{{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
{{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
{{--dates for our convenience - brilliant!” </p>--}}
{{--<p>- Johnatan Davidson</p>--}}
{{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $('#paymentdate').datepicker({ dateFormat: 'yy/mm/dd' });
</script>
</body>
</html>