<?php

namespace App\Exports;

use App\Models\Voucher;
use App\Models\Zone;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VouchersExport implements FromQuery, WithMapping, WithColumnFormatting, ShouldAutoSize, WithHeadings,WithEvents
{

    use Exportable;

//    public function __construct(int $id)
//    {

//    }

    public function query()
    {
        if(isset($this->year)){
            return Voucher::whereMonth('traveldate', $this->month)->whereYear('traveldate',$this->year)->where('user_id', $this->user_id)->where('product_id', $this->product_id);
        }
        else{
            $from = date($this->start);//date('2018-01-01');
            $to = date($this->end);
            return Voucher::whereBetween('traveldate', [$from, $to] )->where('user_id', $this->user_id)->where('user_id', $this->user_id)->where('product_id', $this->product_id);
        }
    }

    public function specific($month,$year,$user_id,$product_id)
    {
        $this->month = $month;
        $this->year = $year;
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        return $this;
    }

    public function period($start,$end,$user_id,$product_id)
    {
        $this->start = $start;
        $this->end = $end;
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        return $this;
    }


    public function map($voucher): array
    {
        return [
            $voucher->statusText,
            $voucher->traveldate,
            (!empty($voucher->product)) ? $voucher->product->name : '',
            $voucher->voucher_prefix.'-'.str_pad($voucher->voucher_no,4,"0",STR_PAD_LEFT),
            (!empty($voucher->customer)) ? $voucher->customer->name : '',
            (!empty($voucher->customer)) ? $voucher->customer->tel : '',
            $voucher->adult,
            $voucher->child,
            $voucher->infant,
            $voucher->pickuptime,
            (!empty($voucher->zone_id)) ? Zone::find($voucher->zone_id)->name.' '.Zone::find($voucher->zone_id)->pickuptime : $voucher->zone,
            (!empty($voucher->hotel)) ? $voucher->hotel->name : '',
            $voucher->transfer,
            $voucher->remark,
            $voucher->confirm_by,
            (!empty($voucher->tourcompany)) ? $voucher->tourcompany->name : '',
            $voucher->user->name,
            Date::dateTimeToExcel($voucher->created_at),
        ];
    }

    public function headings(): array
    {
        return [
            'Status',
            'Travel Date',
            'Product',
            'Voucher No.',
            'Customer Name',
            'Phone No.',
            'Adult',
            'Child',
            'Infant',
            'Pickup Time',
            'Zone',
            'Hotel',
            'Transfer',
            'Remark',
            'Confirmation By',
            'Tour Company',
            'User',
            'Created Date',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'P' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:Q1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
            },
        ];
    }

}
