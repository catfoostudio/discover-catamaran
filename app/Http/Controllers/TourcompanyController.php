<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Product;
use App\Models\Tourcompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TourcompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'tourcompany.cfadmin.index' : 'home.index';

        $tourcompanies = Tourcompany::all();
        return view($view, [
            'tourcompanies' => $tourcompanies,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $view = ($this->isAdminRequest()) ? 'tourcompany.cfadmin.create' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'countries' => $countries,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
//            'tel_code' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'address1' => 'required',
        ]);


        $input = $request->input();


        $tourcompany = new Tourcompany();
        $tourcompany->name = $input['name'];
        $tourcompany->country_id = $input['country'];
//        $tourcompany->tel_code = $input['tel_code'];
        $tourcompany->tel = $input['tel'];
        $tourcompany->email = $input['email'];
        $tourcompany->address1 = $input['address1'];


//        if (!empty($input['fax_code']))
//            $tourcompany->fax_code = $input['fax_code'];
//        if (!empty($input['fax']))
//            $tourcompany->fax = $input['fax'];
//        if (!empty($input['mobile_code']))
//            $tourcompany->mobile_code = $input['mobile_code'];
        if (!empty($input['mobile']))
            $tourcompany->mobile = $input['mobile'];
        if (!empty($input['facebook_url']))
            $tourcompany->facebook_url = $input['facebook_url'];
        if (!empty($input['line']))
            $tourcompany->line = $input['line'];
        if (!empty($input['other_contact']))
            $tourcompany->other_contact = $input['other_contact'];
        if (!empty($input['address2']))
            $tourcompany->address2 = $input['address2'];
        if (!empty($input['postcode']))
            $tourcompany->postcode = $input['postcode'];
        if (!empty($input['description']))
            $tourcompany->remark = $input['description'];
        $tourcompany->status = 1;
        $tourcompany->save();

        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/tourcompany");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($tourcompany)
    {
        //
        $view = ($this->isAdminRequest()) ? 'tourcompany.cfadmin.detail' : 'home.index';

        $countries = Country::all();

        return view($view, [
            'tourcompany' => $tourcompany,
            'countries' => $countries,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tourcompany)
    {
        //
        //
        $this->validate($request, [
            'name' => 'required|max:191',
//            'tel_code' => 'required',
            'tel' => 'required',
            'email' => 'required',
            'address1' => 'required',
        ]);

        $input = $request->input();
//dd($input);
        $tourcompany->name = $input['name'];
        $tourcompany->country_id = $input['country'];
//        $tourcompany->tel_code = $input['tel_code'];
        $tourcompany->tel = $input['tel'];
        $tourcompany->email = $input['email'];
        $tourcompany->address1 = $input['address1'];


//        if (!empty($input['fax_code']))
//            $tourcompany->fax_code = $input['fax_code'];
//        if (!empty($input['fax']))
//            $tourcompany->fax = $input['fax'];
//        if (!empty($input['mobile_code']))
//            $tourcompany->mobile_code = $input['mobile_code'];

            $tourcompany->mobile = $input['mobile'];

            $tourcompany->facebook_url = $input['facebook_url'];


            $tourcompany->line = $input['line'];

            $tourcompany->other_contact = $input['other_contact'];

            $tourcompany->address2 = $input['address2'];

            $tourcompany->postcode = $input['postcode'];
        if (!empty($input['description']))
            $tourcompany->description = $input['description'];
        else{
            $tourcompany->description = '';
        }
        $tourcompany->status = 1;
        $tourcompany->save();

        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/tourcompany");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getdata(Request $request)
    {
        if ($request->ajax()) {
            $input = $request->input();

            $tourcompany = Tourcompany::find($input['id']);
            if (empty($tourcompany)) {
                return 'null';
            } else {
                return $tourcompany->toJson();
            }


        }
    }

    public function gettourcompanyoption(Request $request)
    {

        $input = $request->input();

        if(!empty($input['id'])) {
            $tourcompanies = Tourcompany::whereHas('products', function ($query) use ($input) {
                $query->where('products.id', $input['id']);
            })->get(['id', 'name as text']);
        }
        else{
            $tourcompanies = Tourcompany::all(['id', 'name as text']);
        }

        return $tourcompanies->toJson();
    }
}
