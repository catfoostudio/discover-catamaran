<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $view = ($this->isAdminRequest()) ? 'payment.cfadmin.index' : 'home.index';

        $payments = Payment::all();
        return view($view, [
            'payments' => $payments,
        ]);
    }

    public function informwithno(Request $request, $prefix, $no)
    {
        $voucher = Voucher::where('voucher_prefix', $prefix)->where('voucher_no', $no)->first();

        if(empty($voucher) || $voucher->status == 9 || $voucher->invoice->status === 5){
            return abort(404);
        }

        return view('payment.inform', [
            'voucher' => $voucher,
        ]);
    }

    public function inform(Request $request, $voucher)
    {

        //validation
        $validate = [
            'no' => 'required',
            'amount' => 'required|numeric',
            'type' => 'required',
            'date' => 'required',
            'time' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ];

        $this->validate($request, $validate);

        $input = $request->input();

        $payment = new Payment();
        $payment->invoice_no = $input['no'];
        $payment->amount = $input['amount'];
        $payment->type = $input['type'];
        $payment->date = $input['date'];
        $payment->time = $input['time'];

        if(Input::file('file')){
            $time = time();
            $filename = $time . '_invoice_' . $voucher->invoice->id . '.jpg';
            $filepath = public_path('paymentslip') . '/' . $filename;
            Image::make(Input::file('file'))->save($filepath);
            $payment->file = $filename;
        }

        $payment->status = 1;
        $payment->voucher_id = $voucher->id;
        $payment->save();

        $voucher->status = 10;
        $voucher->invoice->status = 2;
        $voucher->invoice->save();
        $voucher->save();

        return view('payment.thankyou');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $payment = Payment::find($id);
        return view('payment.cfadmin.detail', [
            'payment' => $payment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
