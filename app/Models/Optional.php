<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Optional extends Model
{
    //

    public function discounttypes()
    {
        return $this->belongsToMany('App/Models/Discounttype');
    }
}
