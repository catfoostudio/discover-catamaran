<header class="header" role="banner">
    <div class="wrap">
        <!-- Logo -->
        <a href="{{ url('/') }}" title="Discover Catamaran" class="logo" style="padding-top: 0px;">
            <img src="{{ asset('images/2016_discover catamaranlogo-01.png') }}" style="display: inline;width:225px;" >
        </a>
        <!-- //Logo -->

        <!-- Primary menu -->
        <nav class="main-nav" role="navigation">
            <ul class="jetmenu" id="jetmenu">
                <li><a href="{{ url('hotdeals') }}" title="Book now">HOT DEALS</a></li>
                <li><a href="{{ url('premiumdaytrip') }}" title="Book now" >PREMIUM DAY TRIP</a></li>
                <li><a href="{{ url('privatecharter') }}" title="Book now" >PRIVATE CHARTER</a></li>
                <li><a href="{{ url('destination') }}" title="Book now" >DESTINATION</a></li>
                {{--<li><a href="{{ url('gallery') }}" title="Book now" >GALLERY</a></li>--}}
                <li><a href="{{ url('dclifestyle') }}" title="Book now" >DC LIFESTYLE</a></li>
                <li><a href="{{ url('otherproduct') }}" title="Book now" >OTHER PRODUCT</a></li>
                <li><a href="{{ url('contact') }}" title="Book now" >CONTACT</a></li>
                <li><a href="{{ url('en') }}" style="padding: 31px 0px">English </a>/<a href="{{ url('th') }}" style="padding: 31px 0px">ไทย </a>/<a href="{{ url('cn') }}" style="padding: 31px 0px">中文</a></li>
            </ul>
            {{--<div style="    position: absolute;right: 0px;top: 10px;font-size: 13px;">--}}
                    {{--<a href="{{ url('en') }}">English</a> /--}}
                    {{--<a href="{{ url('th') }}">ไทย</a> /--}}
                    {{--<a href="{{ url('cn') }}">中文</a>--}}
            {{--</div>--}}
            {{--<!-- Search -->--}}
            {{--<form class="advanced-search" action="charters.html">--}}
                {{--<div class="wrap">--}}
                    {{--<div>--}}
                        {{--<select>--}}
                            {{--<option selected>Select location</option>--}}
                            {{--<option>Caribbean</option>--}}
                            {{--<option>Mediterranean</option>--}}
                            {{--<option>Indian Ocean</option>--}}
                            {{--<option>South Pacific</option>--}}
                            {{--<option>South East Asia</option>--}}
                            {{--<option>South America</option>--}}
                            {{--<option>North America</option>--}}
                            {{--<option>Northern Europe</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--<input type="text" id="startDate" />--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--<select>--}}
                            {{--<option selected>Duration</option>--}}
                            {{--<option>1 week</option>--}}
                            {{--<option>2 weeks</option>--}}
                            {{--<option>3 weeks</option>--}}
                            {{--<option>4 weeks</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--<select>--}}
                            {{--<option selected>Cabins</option>--}}
                            {{--<option>3 or less</option>--}}
                            {{--<option>4 - 6</option>--}}
                            {{--<option>6 or more</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--<select>--}}
                            {{--<option selected>Yacht type</option>--}}
                            {{--<option>Motor yacht</option>--}}
                            {{--<option>Sailing yacht</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div><input type="submit" id="as-submit" class="button gold full" value="Search yachts" /></div>--}}
                    {{--<a href="javascript:void(0)" class="search-hide" title="Hide this box">Hide this box</a>--}}
                {{--</div>--}}
            {{--</form>--}}
            {{--<!-- //Search -->--}}
        </nav>
        <!-- //Primary menu -->
    </div>
</header>