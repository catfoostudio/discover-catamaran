<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discounttype extends Model
{
    //
    protected $table = 'discounttypes';

    public function optionals()
    {
        return $this->belongsToMany('App\Models\Optional');
    }

    public function codes()
    {
        return $this->hasMany('App\Models\Code');
    }
}
