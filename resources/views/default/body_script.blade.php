<script src="{{ asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{ asset('js/isotope.min.js')}}"></script>
<script src="{{ asset('js/ytplayer.min.js')}}"></script>
<script src="{{ asset('js/easypiechart.min.js')}}"></script>
<script src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/lightbox.min.js')}}"></script>
<script src="{{ asset('js/twitterfetcher.min.js')}}"></script>
<script src="{{ asset('js/smooth-scroll.min.js')}}"></script>
<script src="{{ asset('js/scrollreveal.min.js')}}"></script>
<script src="{{ asset('js/parallax.js')}}"></script>
<script src="{{ asset('js/scripts.js')}}"></script>
<script>

    var jump=function(e)
    {
        if (e){
            e.preventDefault();
            var target = $(this).attr("href");
        }else{
            var target = location.hash;
        }

        $('html,body').animate(
                {
                    scrollTop: $(target).offset().top
                },1000,function()
                {
                    location.hash = target;
                });

    }

    $('html, body').hide();

    $(document).ready(function()
    {
        $('a[href^=#]').bind("click", jump);

        if (location.hash){
            setTimeout(function(){
                $('html, body').scrollTop(0).show();
                jump();
            }, 0);
        }else{
            $('html, body').show();
        }
    });
</script>