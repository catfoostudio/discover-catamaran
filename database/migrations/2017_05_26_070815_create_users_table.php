<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('facebook_id')->default('');
            $table->string('facebook_url')->default('');
            $table->string('line_id')->default('');
            $table->string('tel_code')->default('');
            $table->string('tel')->default('');
            $table->string('mobile_code')->default('');
            $table->string('mobile')->default('');
            $table->string('fax_code')->default('');
            $table->string('fax')->default('');
            $table->text('remark')->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('avatar')->default('');
            $table->string('voucher_prefix')->default('');
            $table->integer('voucher_prefix_id')->unsigned()->nullable();
            $table->foreign('voucher_prefix_id')->references('id')->on('voucher_prefixes');
            $table->tinyInteger('status')->default(0);
            $table->timestamp('last_activity');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
