<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Discounttype;
use App\Models\Optional;
use App\Models\VoucherPrefix;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = ($this->isAdminRequest()) ? 'code.cfadmin.index' : 'home.index';

        $codes = Code::where('status',1)->get();


        return view($view, [
            'codes' => $codes,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'code.cfadmin.create' : 'home.index';
        $discounttypes = Discounttype::all();
        $prefixes = VoucherPrefix::all();
        $optionals = Optional::all();

        return view($view, [
            'discounttypes' => $discounttypes,
            'prefixes' => $prefixes,
            'optionals' => $optionals,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = [
            'code' => 'required',
            'start' => 'required',
            'end' => 'required|date|after:startdate',
            'discounttype' => 'required|min:1',
        ];

        $this->validate($request, $validate);

        $input = $request->input();

        $code = new Code();
        $code->code = $input['code'];
        $code->status = 1;
        $code->voucher_prefix_id = $input['prefix'];
        $code->startdate = Carbon::createFromFormat('m/d/Y', $input['start'])->toDateString();
        $code->enddate = Carbon::createFromFormat('m/d/Y', $input['end'])->toDateString();
        $code->discounttype_id = $input['discounttype'];
        if(!empty($input['optional'])){
            $code->optional_id = $input['optional'];
        }
        $code->user_id = Auth::user()->id;
        $code->remark = '';

        //Discount value
        $discounttype = Discounttype::find($code->discounttype_id);
        $code->value = (!empty($discounttype->value_flag)) ? $input['value'] : 0 ;
        $code->min_purchase = (!empty($discounttype->min_flag)) ? $input['min'] : 0 ;
        $code->max_discount = (!empty($discounttype->max_flag)) ? $input['max'] : 0 ;
        $code->adult_value = (!empty($discounttype->daytrip_flag)) ? $input['daytrip_adult'] : 0 ;
        $code->child_value = (!empty($discounttype->daytrip_flag)) ? $input['daytrip_child'] : 0 ;
        $code->private_value = (!empty($discounttype->private_flag)) ? $input['private'] : 0 ;
        $code->save();

        $request->session()->flash('alert-success', 'Code was successful created!');
        return redirect('cfadmin/code');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $view = ($this->isAdminRequest()) ? 'code.cfadmin.detail' : 'home.index';
        $discounttypes = Discounttype::all();
        $prefixes = VoucherPrefix::all();
        $optionals = Optional::all();
        return view($view, [
            'discounttypes' => $discounttypes,
            'prefixes' => $prefixes,
            'optionals' => $optionals,
            'code' => Code::find($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = [
            'code' => 'required',
            'start' => 'required',
            'end' => 'required|date|after:startdate',
            'discounttype' => 'required|min:1',
            'daytrip_adult' => 'integer|min:0',
            'daytrip_child' => 'integer|min:0'
        ];

        $this->validate($request, $validate);

        $input = $request->input();
        $code = Code::find($id);
        $code->code = $input['code'];
        $code->voucher_prefix_id = $input['prefix'];
        $code->startdate = Carbon::createFromFormat('m/d/Y', $input['start'])->toDateString();
        $code->enddate = Carbon::createFromFormat('m/d/Y', $input['end'])->toDateString();
        $code->discounttype_id = $input['discounttype'];
        if(!empty($input['optional'])){
            $code->optional_id = $input['optional'];
        }
        $discounttype = Discounttype::find($code->discounttype_id);
        $code->value = (!empty($discounttype->value_flag)) ? $input['value'] : 0 ;
        $code->min_purchase = (!empty($discounttype->min_flag)) ? $input['min'] : 0 ;
        $code->max_discount = (!empty($discounttype->max_flag)) ? $input['max'] : 0 ;
        $code->adult_value = (!empty($discounttype->daytrip_flag)) ? $input['daytrip_adult'] : 0 ;
        $code->child_value = (!empty($discounttype->daytrip_flag)) ? $input['daytrip_child'] : 0 ;
        $code->private_value = (!empty($discounttype->private_flag)) ? $input['private'] : 0 ;
        $code->user_id = Auth::user()->id;
        $code->remark = '';
        $code->save();

        $request->session()->flash('alert-success', 'Code was successful updated!');
        return redirect('cfadmin/code');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = Code::find($id);

//            1 'Await';
//            2 'Confirmed';
//            3 'Completed';
//            4 'Cancelled';
//            5 'No Show';
//            6 'No Launch';
//            7 'Payment Pending';
//            8 'Payment Pending Over Due';
//            9 'Payment Completed';
//            10 'Payment Review'
        $code->status = 0;
        $code->save();

        session()->flash('alert-success', 'Code was successful deleted!');
        return redirect("cfadmin/code");
    }
}
