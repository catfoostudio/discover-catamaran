<main class="main" role="main">
    <div id="rev_slider_1057_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="4kvideos" data-source="gallery" style="background-color:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_1057_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-2968" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 CLIP VIDEO YOUTUBE-->
                    <div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"
                         id="slide-2968-layer-1"
                         data-x="0"
                         data-y="0"
                         data-whitespace="nowrap"

                         data-type="video"
                         data-responsive_offset="on"

                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
                         data-ytid="cUtJfV3eb8c"
                         data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=50&hd=1&wmode=opaque&showinfo=0&rel=0;&start=08"
                         data-videorate="1" data-videowidth="100%" data-videoheight="100%"
                         data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"           data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-autoplay="off"
                         data-nextslideatend="true"
                         {{--data-videoposter="http://img.youtube.com/vi/6pxRHBw-k8M/maxresdefault.jpg"--}}
                         data-noposteronmobile="on"
                         data-volume="50"           data-allowfullscreenvideo="true"
                         data-videostartat="0:00"
                         style="z-index: 5;text-transform:left;border-width:0px;"> </div>

                    <!-- LAYER NR. 2 FOR SCROLLING WHEN MOUSE ON VIDEO-->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2968-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','on','on']"

                         data-type="shape"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2968-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>

                    <!-- LAYER NR. 4 PAUSE BUTTON-->
                    <div class="tp-caption VideoControls-Pause rev-btn "
                         id="slide-2968-layer-11"
                         data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','on','on']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2968-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-9","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 8; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>

                <!-- LAYER NR. 6 PLAY BUTTON-->
                    <div class="tp-caption VideoControls-Play rev-btn "
                         id="slide-2968-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','50','30','30']"
                         data-lineheight="['120','120','80','80']"
                         data-width="['120','120','80','80']"
                         data-height="['120','120','80','80']"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','on','on']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2968-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-12","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[7,7,7,7]"
                         data-lasttriggerstate="reset"
                         style="z-index: 10; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>

                    <!-- LAYER NR. 7 TITLE-->
                    <div class="tp-caption VideoPlayer-Title   tp-resizeme"
                         id="slide-2968-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['150','130','100','80']"
                         data-fontsize="['40','40','30','20']"
                         data-lineheight="['40','40','30','20']"
                         data-width="['none','none','none','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 11; white-space: nowrap;text-transform:left;">RACHA NOI TRIP</div>

                    <!-- LAYER NR. 8 SUBTITLE-->
                    <div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"
                         id="slide-2968-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['200','180','140','132']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 12; white-space: nowrap;text-transform:left;font-style:italic;">by Discover Catamaran</div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-2969" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube Two" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"
                         id="slide-2969-layer-1"
                         data-x="0"
                         data-y="0"
                         data-whitespace="nowrap"

                         data-type="video"
                         data-responsive_offset="on"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
                         data-ytid="12BuQfXYbEY" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"            data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-autoplay="off"
                         data-nextslideatend="true"
                         {{--data-videoposter="http://img.youtube.com/vi/gU10ALRQ0ww/maxresdefault.jpg"--}}
                         data-noposteronmobile="on"
                         data-volume="50"           data-allowfullscreenvideo="true"

                         style="z-index: 16;text-transform:left;border-width:0px;"> </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2969-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2969-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>

                    <!-- LAYER NR. 14 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2969-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 18;text-transform:left;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                    <!-- LAYER NR. 15 -->
                    <div class="tp-caption VideoControls-Pause rev-btn "
                         id="slide-2969-layer-11"
                         data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2969-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-9","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 19; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>

                    <!-- LAYER NR. 16 -->
                    {{--<div class="tp-caption VideoControls-Mute rev-btn "--}}
                         {{--id="slide-2969-layer-12"--}}
                         {{--data-x="['right','right','right','right']" data-hoffset="['72','72','72','72']"--}}
                         {{--data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"--}}
                         {{--data-width="50"--}}
                         {{--data-height="50"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="button"--}}
                         {{--data-actions='[{"event":"click","action":"toggle_mute_video","layer":"slide-2969-layer-1","delay":""}]'--}}
                         {{--data-basealign="slide"--}}
                         {{--data-responsive_offset="off"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'--}}
                         {{--data-textAlign="['center','center','center','center']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 20; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><div class="rs-untoggled-content"><i class="fa-icon-volume-up"></i> </div><div class="rs-toggled-content"><i class="fa-icon-volume-off"></i></div></div>--}}

                    <!-- LAYER NR. 17 -->
                    <div class="tp-caption VideoControls-Play rev-btn "
                         id="slide-2969-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','50','30','30']"
                         data-lineheight="['120','120','80','80']"
                         data-width="['120','120','80','80']"
                         data-height="['120','120','80','80']"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2969-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-12","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[7,7,7,7]"
                         data-lasttriggerstate="reset"
                         style="z-index: 21; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>

                    <!-- LAYER NR. 18 -->
                    <div class="tp-caption VideoPlayer-Title   tp-resizeme"
                         id="slide-2969-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['150','130','100','80']"
                         data-fontsize="['40','40','30','20']"
                         data-lineheight="['40','40','30','20']"
                         data-width="['none','none','none','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 22; white-space: nowrap;text-transform:left;">Maithon Trip Sailing Catamaran</div>

                    <!-- LAYER NR. 19 -->
                    <div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"
                         id="slide-2969-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['200','180','140','132']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 23; white-space: nowrap;text-transform:left;font-style:italic;">by Discover Catamaran</div>
                </li>
                {{--<!-- SLIDE  -->--}}
                {{--<li data-index="rs-2970" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube Three" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">--}}
                    {{--<!-- MAIN IMAGE -->--}}
                    {{--<img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>--}}
                    {{--<!-- LAYERS -->--}}

                    {{--<!-- LAYER NR. 23 -->--}}
                    {{--<div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"--}}
                         {{--id="slide-2970-layer-1"--}}
                         {{--data-x="0"--}}
                         {{--data-y="0"--}}
                         {{--data-whitespace="nowrap"--}}

                         {{--data-type="video"--}}
                         {{--data-responsive_offset="on"--}}

                         {{--data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'--}}
                         {{--data-ytid="nHXVc_cQqyI" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"            data-textAlign="['left','left','left','left']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-autoplay="off"--}}
                         {{--data-nextslideatend="true"--}}
                         {{--data-videoposter="http://img.youtube.com/vi/nHXVc_cQqyI/maxresdefault.jpg"--}}
                         {{--data-noposteronmobile="on"--}}
                         {{--data-volume="100"           data-allowfullscreenvideo="true"--}}

                         {{--style="z-index: 27;text-transform:left;border-width:0px;"> </div>--}}

                    {{--<!-- LAYER NR. 24 -->--}}
                    {{--<div class="tp-caption tp-shape tp-shapewrapper "--}}
                         {{--id="slide-2970-layer-4"--}}
                         {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                         {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                         {{--data-width="full"--}}
                         {{--data-height="full"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="shape"--}}
                         {{--data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2970-layer-1","delay":""}]'--}}
                         {{--data-basealign="slide"--}}
                         {{--data-responsive_offset="off"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                         {{--data-textAlign="['left','left','left','left']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}

                         {{--style="z-index: 28;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>--}}

                    {{--<!-- LAYER NR. 25 -->--}}
                    {{--<div class="tp-caption tp-shape tp-shapewrapper "--}}
                         {{--id="slide-2970-layer-2"--}}
                         {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                         {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                         {{--data-width="full"--}}
                         {{--data-height="full"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="shape"--}}
                         {{--data-basealign="slide"--}}
                         {{--data-responsive_offset="off"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                         {{--data-textAlign="['left','left','left','left']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 29;text-transform:left;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>--}}

                    {{--<!-- LAYER NR. 26 -->--}}
                    {{--<div class="tp-caption VideoControls-Pause rev-btn "--}}
                         {{--id="slide-2970-layer-11"--}}
                         {{--data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"--}}
                         {{--data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"--}}
                         {{--data-width="50"--}}
                         {{--data-height="50"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="button"--}}
                         {{--data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2970-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-9","delay":""}]'--}}
                         {{--data-basealign="slide"--}}
                         {{--data-responsive_offset="off"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'--}}
                         {{--data-textAlign="['center','center','center','center']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 30; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>--}}

                    {{--<!-- LAYER NR. 27 -->--}}
                    {{--<div class="tp-caption VideoControls-Mute rev-btn "--}}
                         {{--id="slide-2970-layer-12"--}}
                         {{--data-x="['right','right','right','right']" data-hoffset="['72','72','72','72']"--}}
                         {{--data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"--}}
                         {{--data-width="50"--}}
                         {{--data-height="50"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="button"--}}
                         {{--data-actions='[{"event":"click","action":"toggle_mute_video","layer":"slide-2970-layer-1","delay":""}]'--}}
                         {{--data-basealign="slide"--}}
                         {{--data-responsive_offset="off"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'--}}
                         {{--data-textAlign="['center','center','center','center']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 31; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><div class="rs-untoggled-content"><i class="fa-icon-volume-up"></i> </div><div class="rs-toggled-content"><i class="fa-icon-volume-off"></i></div></div>--}}

                    {{--<!-- LAYER NR. 28 -->--}}
                    {{--<div class="tp-caption VideoControls-Play rev-btn "--}}
                         {{--id="slide-2970-layer-3"--}}
                         {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                         {{--data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"--}}
                         {{--data-fontsize="['50','50','30','30']"--}}
                         {{--data-lineheight="['120','120','80','80']"--}}
                         {{--data-width="['120','120','80','80']"--}}
                         {{--data-height="['120','120','80','80']"--}}
                         {{--data-whitespace="nowrap"--}}
                         {{--data-visibility="['on','on','off','off']"--}}

                         {{--data-type="button"--}}
                         {{--data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2970-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-12","delay":""}]'--}}
                         {{--data-responsive_offset="on"--}}
                         {{--data-responsive="off"--}}
                         {{--data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'--}}
                         {{--data-textAlign="['center','center','center','center']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[7,7,7,7]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 32; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>--}}

                    {{--<!-- LAYER NR. 29 -->--}}
                    {{--<div class="tp-caption VideoPlayer-Title   tp-resizeme"--}}
                         {{--id="slide-2970-layer-5"--}}
                         {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                         {{--data-y="['top','top','top','top']" data-voffset="['112','91','70','80']"--}}
                         {{--data-fontsize="['40','40','30','20']"--}}
                         {{--data-lineheight="['40','40','30','20']"--}}
                         {{--data-width="['800','800','640','360']"--}}
                         {{--data-height="none"--}}
                         {{--data-whitespace="normal"--}}

                         {{--data-type="text"--}}
                         {{--data-responsive_offset="on"--}}

                         {{--data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                         {{--data-textAlign="['center','center','center','center']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 33; min-width: 800px; max-width: 800px; white-space: normal;text-transform:left;">AMERICAN SOUTHWEST IN 4K 60FPS </div>--}}

                    {{--<!-- LAYER NR. 30 -->--}}
                    {{--<div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"--}}
                         {{--id="slide-2970-layer-6"--}}
                         {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                         {{--data-y="['top','top','top','top']" data-voffset="['200','180','140','151']"--}}
                         {{--data-width="none"--}}
                         {{--data-height="none"--}}
                         {{--data-whitespace="nowrap"--}}

                         {{--data-type="text"--}}
                         {{--data-responsive_offset="on"--}}

                         {{--data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'--}}
                         {{--data-textAlign="['left','left','left','left']"--}}
                         {{--data-paddingtop="[0,0,0,0]"--}}
                         {{--data-paddingright="[0,0,0,0]"--}}
                         {{--data-paddingbottom="[0,0,0,0]"--}}
                         {{--data-paddingleft="[0,0,0,0]"--}}
                         {{--data-lasttriggerstate="reset"--}}
                         {{--style="z-index: 34; white-space: nowrap;text-transform:left;font-style:italic;">Jacob + Katie Schwarz </div>--}}

                    {{--<!-- LAYER NR. 31 -->--}}
                    {{--<a class="tp-caption VideoPlayer-Social   tp-resizeme"--}}
                       {{--href="https://www.youtube.com/user/jacobschwarz/featured" target="_blank"           id="slide-2970-layer-7"--}}
                       {{--data-x="['center','center','center','center']" data-hoffset="['-70','-70','-70','-70']"--}}
                       {{--data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"--}}
                       {{--data-width="none"--}}
                       {{--data-height="none"--}}
                       {{--data-whitespace="nowrap"--}}

                       {{--data-type="text"--}}
                       {{--data-actions=''--}}
                       {{--data-responsive_offset="on"--}}

                       {{--data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2000,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'--}}
                       {{--data-textAlign="['center','center','center','center']"--}}
                       {{--data-paddingtop="[0,0,0,0]"--}}
                       {{--data-paddingright="[0,0,0,0]"--}}
                       {{--data-paddingbottom="[0,0,0,0]"--}}
                       {{--data-paddingleft="[0,0,0,0]"--}}
                       {{--data-lasttriggerstate="reset"--}}
                       {{--style="z-index: 35; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-youtube"></i> </a>--}}

                    {{--<!-- LAYER NR. 32 -->--}}
                    {{--<a class="tp-caption VideoPlayer-Social   tp-resizeme"--}}
                       {{--href="https://twitter.com/jakeschwarz" target="_blank"          id="slide-2970-layer-8"--}}
                       {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                       {{--data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"--}}
                       {{--data-width="none"--}}
                       {{--data-height="none"--}}
                       {{--data-whitespace="nowrap"--}}

                       {{--data-type="text"--}}
                       {{--data-actions=''--}}
                       {{--data-responsive_offset="on"--}}

                       {{--data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2150,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'--}}
                       {{--data-textAlign="['center','center','center','center']"--}}
                       {{--data-paddingtop="[0,0,0,0]"--}}
                       {{--data-paddingright="[0,0,0,0]"--}}
                       {{--data-paddingbottom="[0,0,0,0]"--}}
                       {{--data-paddingleft="[0,0,0,0]"--}}
                       {{--data-lasttriggerstate="reset"--}}
                       {{--style="z-index: 36; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-twitter"></i> </a>--}}

                    {{--<!-- LAYER NR. 33 -->--}}
                    {{--<a class="tp-caption VideoPlayer-Social   tp-resizeme"--}}
                       {{--href="https://www.instagram.com/jacobschwarz/" target="_blank"          id="slide-2970-layer-9"--}}
                       {{--data-x="['center','center','center','center']" data-hoffset="['70','70','70','70']"--}}
                       {{--data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"--}}
                       {{--data-width="none"--}}
                       {{--data-height="none"--}}
                       {{--data-whitespace="nowrap"--}}

                       {{--data-type="text"--}}
                       {{--data-actions=''--}}
                       {{--data-responsive_offset="on"--}}

                       {{--data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2300,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'--}}
                       {{--data-textAlign="['center','center','center','center']"--}}
                       {{--data-paddingtop="[0,0,0,0]"--}}
                       {{--data-paddingright="[0,0,0,0]"--}}
                       {{--data-paddingbottom="[0,0,0,0]"--}}
                       {{--data-paddingleft="[0,0,0,0]"--}}
                       {{--data-lasttriggerstate="reset"--}}
                       {{--style="z-index: 37; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-camera-retro"></i> </a>--}}
                {{--</li>--}}

            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
    </div><!-- END REVOLUTION SLIDER -->
    <script type="text/javascript">
        var tpj=jQuery;

        var revapi1057;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_1057_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_1057_1");
            }else{
                revapi1057 = tpj("#rev_slider_1057_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"revolution/js/",
                    sliderLayout:"fullscreen",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        mouseScrollReverse:"default",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 50,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"uranus",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:778,
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            }
                        }
                    },
                    responsiveLevels:[1240,1024,778,480],
                    visibilityLevels:[1240,1024,778,480],
                    gridwidth:[1200,1024,778,480],
                    gridheight:[675,576,480,480],
                    lazyType:"none",
                    parallax: {
                        type:"scroll",
                        origo:"enterpoint",
                        speed:400,
                        levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
                        type:"scroll",
                    },
                    shadow:0,
                    spinner:"off",
                    stopLoop:"on",
                    stopAfterLoops:0,
                    stopAtSlide:1,
                    shuffle:"off",
                    autoHeight:"off",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "60px",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        }); /*ready*/
    </script>

    <!-- Tab navigation -->
    {{--<nav class="tabs four" role="navigation">--}}
        {{--<ul class="wrap">--}}
            {{--<li><a href="{{ url('hotdeals') }}" title="Tell us what you need. We will do the rest.">--}}
                    {{--<img src="images/handshake.png" alt="" /> Hot Deals--}}
                {{--</a></li>--}}
            {{--<li><a href="{{ url('premiumdaytrip') }}" title="Check our Early Season Mediterranean Deals.">--}}
                    {{--<img src="images/ico2.png" alt="" /> Premium Day Trip--}}
                {{--</a></li>--}}
            {{--<li><a href="{{ url('privatecharter') }}" title="New to Sailing? We‘ve got you covered.">--}}
                    {{--<img src="images/ico1.png" alt="" /> Private Charter--}}
                {{--</a></li>--}}
            {{--<li><a href="{{ url('destination') }}" class="anchor" title="Win a Sailing Holiday in Mediterranean!">--}}
                    {{--<img src="images/ico3.png" alt="" /> Destination--}}
                {{--</a></li>--}}
        {{--</ul>--}}
    {{--</nav>--}}
    <!-- //Tab navigation -->

    <!-- Call to action -->
    <section class="cta white">
        <div class="content">
            <!-- Wrapper -->
            <div class="wrap">
                <div class="row">
                    <!-- OneHalf -->
                    <div class="one-half">
                        <div class="box-white" style="padding: 0px;background-image: url({{ asset('images/home/01.jpg') }});background-size:cover;background-repeat:no-repeat;background-position: center center;">
                            <div style="background: rgba(74, 84, 17, 0.5);padding: 20px;height: 300px;">
                            <article>
                                <h1  style="visibility: visible;color: white;font-weight: bold">Premium Day Trip</h1>
                                <div style="padding: 0px 20px 20px 0px;font-size: 18px;color: white;height: 150px;">
                                    {{ __('home.premiumdaytrip') }}
                                </div>
                                <a href="{{ url('premiumdaytrip') }}" title="Begin your sailing adventure" class="anchor button white medium wow fadeInUp animated" style="visibility: visible;">Detail</a>
                            </article>
                            </div>
                        </div>
                    </div>
                    <!-- //OneHalf -->
                    <div class="one-half">
                        <div class="box-white" style="padding: 0px;background-image: url({{ asset('images/home/02.jpg') }});background-size:cover;background-repeat:no-repeat;background-position: center center; ">
                            <div style="background: rgba(0, 0, 0, 0.5);padding: 20px;height: 300px;">
                            <article>
                                <h1  style="visibility: visible;color: white;font-weight: bold">Private Chater</h1>
                                <div style="padding: 0px 20px 20px 0px;font-size: 18px;color: white;height: 150px;">
                                    {{ __('home.privatecharter') }}
                                </div>
                                <a href="{{ url('privatecharter') }}" title="Begin your sailing adventure" class="anchor button white medium wow fadeInUp animated" style="visibility: visible;">Detail</a>
                            </article>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="one-third">
                        <div class="box-white" style="padding: 0px;background-image: url({{ asset('images/home/03.jpg') }});background-size:cover;background-repeat:no-repeat;background-position: center center;">
                            <div style="background: rgba(66,0,0, 0.50);padding: 20px;height: 300px;">
                            <article>
                                <h1  style="visibility: visible;color: white;font-weight: bold">Hot Deals</h1>
                                <div style="padding: 0px 20px 20px 0px;font-size: 18px;color: white;height: 150px;">
                                    {{ __('home.hotdeals') }}
                                </div>
                                <a href="{{ url('hotdeals') }}" title="Begin your sailing adventure" class="anchor button white medium wow fadeInUp animated" style="visibility: visible;">Detail</a>
                            </article>
                            </div>
                        </div>
                    </div>
                    <div class="one-third">
                        <div class="box-white" style="padding: 0px;background-image: url({{ asset('images/home/04.jpg') }});background-size:cover;background-repeat:no-repeat;background-position: center center;">
                            <div style="background: rgba(181,77,7, 0.50);padding: 20px;height: 300px;">
                                <article>
                                    <h1  style="visibility: visible;color: white;font-weight: bold">Destination</h1>
                                    <div style="padding: 0px 20px 20px 0px;font-size: 18px;color: white;height: 150px;">
                                        {{ __('home.destination') }}
                                    </div>
                                    <a href="{{ url('destination') }}" title="Begin your sailing adventure" class="anchor button white medium wow fadeInUp animated" style="visibility: visible;">Detail</a>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="one-third">
                        <div class="box-white" style="padding: 0px;background-image: url({{ asset('images/home/05.jpg') }});background-size:cover;background-repeat:no-repeat;background-position: center center;">
                            <div style="background: rgba(31,92,8, 0.50);padding: 20px;height: 300px;">
                                <article>
                                    <h1  style="visibility: visible;color: white;font-weight: bold">DC Lifestyle</h1>
                                    <div style="padding: 0px 20px 20px 0px;font-size: 18px;color: white;height: 150px;">
                                        {{ __('home.dclifestyle') }}
                                    </div>
                                    <a href="{{ url('dclifestyle') }}" class="anchor button white medium wow fadeInUp animated" style="visibility: visible;">Detail</a>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Wrapper -->
        </div>
    </section>
    <!-- //Call to action -->

    {{--<!-- Deals -->--}}
    {{--<section class="content boxed grid2 noarrow">--}}
        {{--<ul id="lightSliderDeals">--}}
            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="yacht-single.html"><img src="uploads/img.jpg" alt="deal" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<header>Our Exclusive Deals</header>--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="yacht-single.html">Elan 1923 Impression</a></h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam aliquip ex ea commodoerat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</p>--}}

                            {{--<div class="details">--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_location_1"></span>--}}
                                    {{--<p>Base: Marina Kaštela</p>--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_friends"></span>--}}
                                    {{--<p>Berths: 10 (8+2)</p>--}}
                                {{--</div>--}}
                                {{--<div class="price">$ 5300,00</div>--}}
                                {{--<div><a href="yacht-single.html" title="Book now" class="button gold full medium solid">Book now</a> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="yacht-single.html"><img src="uploads/img.jpg" alt="deal" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<header>Our Exclusive Deals</header>--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="yacht-single.html">Elan 1923 Impression</a></h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam aliquip ex ea commodoerat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</p>--}}

                            {{--<div class="details">--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_location_1"></span>--}}
                                    {{--<p>Base: Marina Kaštela</p>--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_friends"></span>--}}
                                    {{--<p>Berths: 10 (8+2)</p>--}}
                                {{--</div>--}}
                                {{--<div class="price">$ 5300,00</div>--}}
                                {{--<div><a href="yacht-single.html" title="Book now" class="button gold full medium solid">Book now</a> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="yacht-single.html"><img src="uploads/img.jpg" alt="deal" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<header>Our Exclusive Deals</header>--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="yacht-single.html">Elan 1923 Impression</a></h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam aliquip ex ea commodoerat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper.</p>--}}

                            {{--<div class="details">--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_location_1"></span>--}}
                                    {{--<p>Base: Marina Kaštela</p>--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<span class="icojam_friends"></span>--}}
                                    {{--<p>Berths: 10 (8+2)</p>--}}
                                {{--</div>--}}
                                {{--<div class="price">$ 5300,00</div>--}}
                                {{--<div><a href="yacht-single.html" title="Book now" class="button gold full medium solid">Book now</a> </div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</section>--}}
    {{--<!-- //Deals -->--}}

    {{--<!-- Testimonials -->--}}
    {{--<section class="testimonials">--}}
        {{--<div class="wrap center">--}}
            {{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
            {{--<p>“Excellent - you found the right boat in the right place at the right time,<br /> and managed to change the dates for our convenience - brilliant!” </p>--}}
            {{--<p>- Johnatan Davidson</p>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- //Testimonials -->--}}

    {{--<!-- App -->--}}
    {{--<section class="white app">--}}
        {{--<div class="wrap center">--}}
            {{--<h2>Download our mobile booking app</h2>--}}
            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperut commodo consequat. </p>--}}
            {{--<ol class="custom triplets">--}}
                {{--<li class="wow fadeIn"><strong>Find a yacht</strong><br />Lorem ipsum dolor sit consectetuer adipiscing elit, sed diam nonummy nibh amet .</li>--}}
                {{--<li class="wow fadeIn" data-wow-delay=".2s"><strong>Make a booking</strong><br />Lorem ipsum dolor sit consectetuer adipiscing elit, sed diam nonummy nibh amet .</li>--}}
                {{--<li class="wow fadeIn" data-wow-delay=".4s"><strong>Brag to your friends</strong><br />Lorem ipsum dolor sit consectetuer adipiscing elit, sed diam nonummy nibh amet .</li>--}}
            {{--</ol>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- //App -->--}}

    {{--<!-- Photo -->--}}
    <section class="photo">
        <div class="wrap center">
            <h2>Find out more about what are we doing, what's new in Phuket, what not to miss, where are we travelling to, travel tips etc</h2>
            <p>Not only be a helpful friend in Phuket, we also love to share our travel experiences and tips of how to make it the best possible journey for others.</p>
            <a href="{{ url('dclifestyle') }}" title="Find out more" class="button white medium">Find out more</a>
        </div>
    </section>
    <!-- //Photo -->

    {{--<!-- Services -->--}}
    {{--<section class="white icons">--}}
        {{--<div class="wrap">--}}
            {{--<div class="row">--}}
                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_compas_2"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Our sailing destinations</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".2s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_map_3"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Where to sail guide</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".4s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_tags_1"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Our yachts and pricing</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".6s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_pupil_boy"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">New to sailing?</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_anchor"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Sail with a skipper</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".2s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_certificate"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Skipper training</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".4s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_holliday"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Beach club</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}

                {{--<!-- Item -->--}}
                {{--<div class="one-fourth wow fadeIn" data-wow-delay=".6s">--}}
                    {{--<a href="#" class="circle large border">--}}
                        {{--<span class="icojam_airdrop"></span>--}}
                    {{--</a>--}}
                    {{--<h4><a href="#">Flotilla sailing</a></h4>--}}
                    {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonumy nib.</p>--}}
                {{--</div>--}}
                {{--<!-- //Item -->--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- //Services -->--}}


    {{--<!-- Call to action -->--}}
    {{--<section class="cta grey">--}}
        {{--<div class="wrap">--}}
            {{--<a href="http://themeforest.net/user/themeenergy/portfolio" title="I am convinced" class="button white medium right">I am convinced</a>--}}
            {{--<h3>Another call to action section, just in case you have something to add.</h3>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- //Call to action -->--}}

    {{--<!-- Blog posts -->--}}
    {{--<section class="content boxed grid2">--}}
        {{--<ul id="lightSliderPosts">--}}
            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="blog-single.html"><img src="uploads/img.jpg" alt="post" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="blog-single.html">Win an All Inclusive Sailing Holiday in Mediterranean!</a></h3>--}}
                            {{--<div class="meta">--}}
                                {{--<span>By <a href="#">admin</a></span>--}}
                                {{--<span>May 23rd, 2016</span>--}}
                                {{--<span><a href="blog-single.html#comments">2 Comments</a></span>--}}
                            {{--</div>--}}
                            {{--<p>This year’s Taiwan International Boat Show was a huge success. Taken place from May 8 to 11, the event was attended by more than 70,000 visitors.</p>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in </p>--}}
                            {{--<a href="blog-single.html" class="button small gold" title="Read more">Read more</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="blog-single.html"><img src="uploads/img.jpg" alt="post" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="blog-single.html">Luxury Heart of Gold joins<br />our charter fleet</a></h3>--}}
                            {{--<div class="meta">--}}
                                {{--<span>By <a href="#">admin</a></span>--}}
                                {{--<span>May 23rd, 2016</span>--}}
                                {{--<span><a href="blog-single.html#comments">2 Comments</a></span>--}}
                            {{--</div>--}}
                            {{--<p>This year’s Taiwan International Boat Show was a huge success. Taken place from May 8 to 11, the event was attended by more than 70,000 visitors.</p>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in </p>--}}
                            {{--<a href="blog-single.html" class="button small gold" title="Read more">Read more</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}

            {{--<li>--}}
                {{--<!-- Item -->--}}
                {{--<article class="full-width hentry">--}}
                    {{--<figure class="one-half heightfix"><a href="blog-single.html"><img src="uploads/img.jpg" alt="post" /></a></figure>--}}
                    {{--<div class="one-half heightfix">--}}
                        {{--<div class="text">--}}
                            {{--<h3><a href="blog-single.html">Awarded Outstanding Sailing School and Outstanding Instructor Award</a></h3>--}}
                            {{--<div class="meta">--}}
                                {{--<span>By <a href="#">admin</a></span>--}}
                                {{--<span>May 23rd, 2016</span>--}}
                                {{--<span><a href="blog-single.html#comments">2 Comments</a></span>--}}
                            {{--</div>--}}
                            {{--<p>This year’s Taiwan International Boat Show was a huge success. Taken place from May 8 to 11, the event was attended by more than 70,000 visitors.</p>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in </p>--}}
                            {{--<a href="blog-single.html" class="button small gold" title="Read more">Read more</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</article>--}}
                {{--<!-- //Item -->--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</section>--}}
    {{--<!-- //Blog posts -->--}}

    {{--<!-- Call to action -->--}}
    {{--<section class="cta gold">--}}
        {{--<div class="wrap center">--}}
            {{--<h2>I am sold. This is outstanding.</h2>--}}
            {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorperut commodo consequat. </p>--}}
            {{--<a href="http://themeforest.net/user/themeenergy/portfolio" title="Buy this now" class="button white medium">Buy this now</a>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- //Call to action -->--}}

    {{--<!-- Yachts -->--}}
    {{--<div class="results">--}}
        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}

        {{--<!-- Item -->--}}
        {{--<figure class="one-fourth item">--}}
            {{--<img src="uploads/img.jpg" alt="" />--}}
            {{--<figcaption>--}}
                {{--<dl>--}}
                    {{--<dt>Elan 1923 Impression</dt>--}}
                    {{--<dd><span class="icojam_doublebed"></span> 10 berths</dd>--}}
                    {{--<dd><span class="icojam_toilet_paper"></span> 2 toilets</dd>--}}
                {{--</dl>--}}
                {{--<div class="price">Price from  <strong>50.000$</strong></div>--}}
                {{--<a href="yacht-single.html" title="Book now" class="button small gold">Book now</a>--}}
            {{--</figcaption>--}}
        {{--</figure>--}}
        {{--<!-- //Item -->--}}
    {{--</div>--}}
    {{--<!-- //Yachts -->--}}
</main>