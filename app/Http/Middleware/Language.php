<?php
/**
 * Created by PhpStorm.
 * User: catfoo
 * Date: 11/2/18
 * Time: 11:35 PM
 */

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\App;

class Language
{
    public function handle($request, Closure $next)
    {
        if(session()->has('locale'))
            App ::setLocale(session('locale'));
        else
            App::setLocale(config('app.locale'));

        return $next($request);
    }
}