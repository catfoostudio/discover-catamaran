<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Product;
use App\Models\Tourcompany;
use App\Models\TourcompanyProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $view = ($this->isAdminRequest()) ? 'stock.cfadmin.index' : 'home.index';
        $tourcompanywithproduct = Tourcompany::with('products')->get();

        return view($view, [
            'tourcompanywithproduct' => $tourcompanywithproduct,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->ajax()) {
            dd($request->input());

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getstockdata(Request $request)
    {
        $input = $request->input();

        //$tourcompany = Tourcompany::findOrFail(1);
        //$tourproduct = $tourcompany->products()->where('products.id' , $input['product'])->first()->pivot;

        $tourproduct = Product::has('tourcompanies')->find($input['product'])->tourcompanies->find($input['tourcompany'])->pivot;

        $stock = $tourproduct->stocks()->where('reserved_date' , $input['date'])->where('status', 1)->first();

        if(!empty($stock)){
            $data  = [
                'maxseat' => $stock->max_seat,
                'remainingseat' => $stock->remaining_seat,
                'date' => $stock->reserved_date,
                'remark' => $stock->remark,
            ];

            return Response::json(array(
                'success' => true,
                'data'   => $data
            ));
        }
        else{

            return Response::json(array(
                'success' => false,
                'message' => 'stock not initial',
            ));
        }
        //return $stock->vouchers->toJson();


    }

}
