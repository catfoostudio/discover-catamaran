<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Gallery</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>Gallery</h1>
            <p>Wild, exotic and remote; cosmopolitan and cutting-edge; untouched and tranquil, discover our incredible sailing destinations. <br />See the world. Differently.</p>
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content boxed grid4">

        @foreach($galleries as $gallery)
        <!-- Item -->
        <article class="one-half">
            <figure class="one-half heightfix"><a href="{{ url('gallery/'.$gallery->id) }}"><img src="{{ (count($gallery->images) != 0) ? asset($gallery->images[0]->path) : asset('images/image-soon.png') }}" alt="destination" /></a></figure>
            <div class="one-half heightfix">
                <div class="text">
                    <h3><a href="destinations-single.html">{{ $gallery->name }}</a></h3>
                    <p>{!! $gallery->description !!}</p>
                    <a href="{{ url('gallery/'.$gallery->id) }}" class="more" title="Read more">View more</a>
                </div>
            </div>
        </article>
        @endforeach
        <!-- //Item -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Bottom Sidebar -->
@include('template.bottom_sidebar')
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>