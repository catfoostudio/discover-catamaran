<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    public function images()
    {
        return $this->belongsToMany('App\Models\Image','image_gallery');
    }
}
