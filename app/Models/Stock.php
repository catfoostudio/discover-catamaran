<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //
    protected $table = 'stocks';

//    protected $fillable = [
//        'amount', 'price', 'package_id', 'hotel_id', 'remark'
//    ];
//
    public function tourcompanyproduct()
    {
        return $this->belongsTo('App\Models\TourcompanyProduct');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }
}
