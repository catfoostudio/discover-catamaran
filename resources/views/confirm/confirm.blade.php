<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Confirm Reservation</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese'
          rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child {
            width: auto
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    {{--<header class="intro">--}}
        {{--<div class="wrap">--}}
            {{--<h1>Our Company</h1>--}}
            {{--<p>Discover Catamaran - The Original Premium Catamaran Excursion in Phuket</p>--}}
        {{--</div>--}}
    {{--</header>--}}
    <!-- //Intro -->
    <style>
        label{
            font-weight: bold;
        }
        .one-half{
            padding-bottom: 10px;
        }
        .one-third{
            padding-bottom: 10px;
        }
    </style>

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- FullWidth -->
                <div class="full-width">
                    <div class="box-white" style="padding-top: 40px;">
                        <h1>Confirm Reservation</h1>

                        <h3 style="padding-bottom: 2px;">Information</h3>
                        <div class="row">
                        <div class="one-half">
                            <label>Product</label>
                            {{ \App\Models\Product::find(Session::get('product'))->name }}
                        </div>
                            <div class="one-half">
                                <label>Travel Date</label>
                                {{ \Carbon\Carbon::createFromFormat('Y-m-d', Session::get('travel_date'))->format('d F Y') }}
                            </div>
                            <div class="one-half">
                                <label>Name</label>
                                {{ Session::get('name') }}
                            </div>
                        <div class="one-half">
                            <label>Tel.</label>
                            {{ Session::get('tel') }}
                        </div>
                        <div class="one-half">
                            <label>E-mail</label>
                            {{ Session::get('email') }}
                        </div>
                        <div class="one-half">
                            <label>Line</label>
                            {{ (!empty(Session::get('line'))) ? Session::get('line') : '-' }}
                        </div>
                        <div class="one-half">
                            <label>Wechat ID</label>
                            {{ (!empty(Session::get('wechat'))) ? Session::get('wechat') : '-' }}
                        </div>
                        </div>

                        <h3 style="padding-bottom: 2px;">Traveler</h3>
                        <div class="row">
                        <div class="one-third">
                            <label>Adult</label>
                            {{ Session::get('adult') }}
                        </div>
                        <div class="one-third">
                            <label>Child 4-11 yrs</label>
                            {{ Session::get('child') }}
                        </div>
                        <div class="one-third">
                            <label>Infant 1-3 yrs</label>
                            {{ Session::get('infant') }}
                        </div>
                        </div>
                        <h3 style="padding-bottom: 2px;">Transfer</h3>
                        <div class="row">
                        <div class="one-third">
                            <label>Transfer</label>
                            {{ (Session::get('transfer') == 'wt') ? 'With Transfer' : 'Without Transfer' }}
                        </div>
                        @if(Session::get('transfer') == 'wt')
                            <div class="one-third">
                                <label>Zone</label>
                                {{ \App\Models\Zone::find(Session::get('zone'))->name.' '.date("H:i", strtotime(\App\Models\Zone::find(Session::get('zone'))->pickuptime)) }}
                            </div>
                            <div class="one-third">
                                <label>Hotel</label>
                                {{ \App\Models\Hotel::find(Session::get('hotel'))->name }}
                            </div>
                        @endif
                        </div>
                        <h3 style="padding-bottom: 2px;">Payment</h3>
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="one-half">
                                <label>Total Price</label>
                                {{ (!empty(Session::get('grandtotalprice'))) ? number_format(Session::get('grandtotalprice'),2).' Baht' : '0 Baht' }}
                            </div>
                        <div class="one-half">
                            <label> Payment type</label>
                            @if(Session::get('payment') == 'credit')
                                Credit Card
                            @elseif(Session::get('payment') == 'bank')
                                Bank Transfer
                            @elseif(Session::get('payment') == 'wechat')
                                Wechat Pay
                            @endif
                        </div>
                        <div class="one-half">
                            <label>Discount code</label>
                            {{ (!empty(Session::get('discount'))) ? Session::get('discount') : '-' }}
                        </div>
                        </div>
                        @if(Session::get('payment') == 'credit' && Session::get('grandtotalprice') <= 20)
                            <form name="checkoutForm" method="POST" action="{{ action('ChargeController@charge') }}">
                                {{ csrf_field() }}
                                <button title="SUBMIT" type="submit" class="button gold medium right">Confirm</button>
                                <a href="{{ url('premiumdaytrip/'.(\App\Models\Product::find(Session::get('product'))->id)) }}"><button id="btn-cancel-order" type="button" class="button gold medium left btn-cancel">Cancel</button></a>
                            </form>
                        @elseif(Session::get('payment') == 'credit')
                        <form name="checkoutForm" method="POST" action="{{ action('ChargeController@charge') }}" style="text-align: right">
                            {{ csrf_field() }}
                            <script type="text/javascript" src="https://cdn.omise.co/omise.js"
                                    data-key="pkey_test_5fjechtz8nqp0kvg050"
                                    data-image="{{ asset('dct-0013.png') }}"
                                    data-frame-label="Discover Catamaran"
                                    data-button-label="Confirm"
                                        data-submit-label="Submit"
                                    data-location="no"
                                    data-amount="{{ (!empty(Session::get('grandtotalprice'))) ? Session::get('grandtotalprice')*100 : 0}}"
                                    data-currency="thb"
                            >
                            </script>
                            <!--the script will render <input type="hidden" name="omiseToken"> for you automatically-->
                            <a href="{{ url('premiumdaytrip/'.(\App\Models\Product::find(Session::get('product'))->id)) }}"><button id="btn-cancel-order" type="button" class="button medium left btn-cancel">Cancel</button></a>
                        </form>
                        <style>
                            .omise-checkout-button{
                                display: inline-block;
                                border-style: solid;
                                border-width: 2px;
                                text-transform: uppercase;
                                font-weight: bold;
                                padding: 0 20px;
                                max-width: 100%;
                                white-space: nowrap;
                                height: 50px;
                                line-height: 46px;
                                min-width: 200px;
                                border-color: #ffffff;
                                color: #ffffff;
                                background-color: deepskyblue;
                            }
                            .omise-checkout-button:hover,
                            .omise-checkout-button:focus 		{background: #0094c3;color:#fff;}
                            .btn-cancel{
                                border-color: white;
                                color: white;
                                background-color: #ff2727;
                            }
                            .btn-cancel:hover,
                            .btn-cancel:focus 		{background-color:#DF2020;color:#fff;}
                        </style>
                        @elseif(Session::get('payment') == 'bank' || Session::get('payment') == 'wechat')
                            <form name="checkoutForm" method="POST" action="{{ action('ChargeController@charge') }}">
                                {{ csrf_field() }}
                                <button title="SUBMIT" type="submit" class="button gold medium right">Confirm</button>
                                <a href="{{ url('premiumdaytrip/'.(\App\Models\Product::find(Session::get('product'))->id)) }}"><button id="btn-cancel-order" type="button" class="button gold medium left btn-cancel">Cancel</button></a>
                            </form>
                        @endif
                    </div>
                </div>
                <!-- //FullWidth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
{{--<div class="wrap center">--}}
{{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
{{--<p>“Excellent - you found the right boat in the right place at the right time,<br/> and managed to change the--}}
{{--dates for our convenience - brilliant!” </p>--}}
{{--<p>- Johnatan Davidson</p>--}}
{{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('js/scripts.js') }}"></script>
<script>
    $('#btn-cancel-order').click(function () {
        window.location.replace("");
    });

    $("#checkoutForm").submit(function () {

        var form = $(this);
        // Disable the submit button to avoid repeated click.
        form.find("input[type=submit]").prop("disabled", true);

    });
</script>
</body>
</html>