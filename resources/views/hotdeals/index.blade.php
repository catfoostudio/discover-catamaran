<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="author" content="themeenergy.com">--}}

    <title>Discover Catamaran - Hot Deals</title>

    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/fonts.css" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="images/favicon.ico" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .img-destination{
            background-size:     cover;
            background-repeat:   no-repeat;
            background-position: center center;
            min-height: 100%;

        }
        .item-destination{
            border: 1px #0d0c0b6b solid;
            margin-bottom: 50px;
            padding: 0px
        }

        @media only screen and (max-width: 840px) {
            .img-destination{
                min-height: 300px;

            }
        }
    </style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>Hot Deals</h1>
            {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet risus dui. Quisque et malesuada mi, sed accumsan sem. Proin suscipit, sem at blandit ultricies, nisl ante interdum ex, vel placerat eros lorem vel libero. Vivamus dui dui, euismod id nibh at, suscipit accumsan nulla. Nunc feugiat consectetur euismod. Praesent in ex nulla. Nunc sed arcu et odio vehicula pulvinar. </p>--}}
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content" style="padding: 50px;">
        <!-- Item -->
        <article class="full-width item-destination">
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/maxresdefault.jpg');"></div>
            </div>
            <div class="one-half heightfix" style="padding: 0px;overflow: hidden;">
                <div class="text" >
                    <h3><a href="destinations-micro.html">Phi Phi islands</a></h3>
                    <p>The Phi Phi islands are some of the loveliest in Southeast Asia. Just a 45-minute speedboat jaunt and a 90-minute ferryboat ride from Phuket, these picture postcard islands offer the ultimate tropical getaway.</p>
                    <p>Classic beaches, stunning rock formations, and vivid turquoise waters teeming with colourful marine life - it's paradise perfected.</p>
                    <p>There are two islands, Phi Phi Don and Phi Phi Leh. The larger and inhabited, Phi Phi Don attracts hundreds of visitors to stay on its lovely shores.</p>
                    <p>There are two islands, Phi Phi Don and Phi Phi Leh. The larger and inhabited, Phi Phi Don attracts hundreds of visitors to stay on its lovely shores.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/Pro_A_6-1-17_170112_0045.jpg');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/CR-38.JPG');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/TH_-60351.JPG');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/aleenta-Phang_Nga_Bay_2.jpg');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/bond_island.jpg');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
        <!-- Item -->
        <article class="full-width item-destination">
            <figure class="one-half heightfix" style="padding: 0px;">
                <div class="img-destination" style="background-image: url('destinations/640x390_669992_1444827137_(2).jpg');"></div>
            </figure>
            <div class="one-half heightfix" style="padding: 0px;">
                <div class="text">
                    <h3><a href="destinations-micro.html">Racha island</a></h3>
                    <p>The Racha (or Raya) islands, some 12 km south of Phuket, are best known as excellent diving and snorkelling daytrip destinations. Racha Yai, however, is quickly developing into a place to stay on for a while with a number of bungalow and resort operations springing up in recent years.</p>
                    <p>Racha Yai reveals itself in splendid fashion, with most arrivals landing onto a strip of fine white sand tucked deep into the long, U-shaped main bay, called Ao Tawan Tok or Ao Bungalow. The water here is clear and perfect for snorkelling, though the bay gets quite busy with visiting boats in the afternoons. The other large bay, Ao Siam, is a pretty place where lonely strolls in solitude are possible.</p>
                    <p>Racha Noi is uninhabited and has no services or accommodation, but there's some great diving in the area.</p>
                    {{--<p>Nestled in the Bay of Naples near the exclusive islands of Capri and Ischia, this secret gem is relatively undiscovered and untouched. </p>--}}
                    {{----}}
                    {{--<p>Perfectly located for access to the stunning Amalfi Coast and Pontine Islands, the sheltered short passages and warm breezes make this area ideal for a relaxing charter. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed. </p>--}}
                    {{--<a href="destinations-micro.html" class="more" title="Read more">Read more</a>--}}
                    {{--<div class="highlight">--}}
                    {{--<h4><span class="icojam_heart"></span>Highlights of Phi Phi islands</h4>--}}
                    {{--<ul class="nobullet twins">--}}
                    {{--<li>Lorem ipsum dolor sit amet, adipiscing elit.</li>--}}
                    {{--<li>Donec quis sagittis mauris elementum.</li>--}}
                    {{--<li>Fusce ut nunc ,ligula at, venenatis enim.</li>--}}
                    {{--<li>Vestibulum et nisi sed sit amet lacus.</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<p>Base open dates: Open all year round.</p>--}}
                </div>
            </div>
        </article>
        <!-- //Item -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>