<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscounttypeOptionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounttype_optional', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discounttype_id')->unsigned()->nullable();
            $table->foreign('discounttype_id')->references('id')->on('discounttypes');
            $table->integer('optional_id')->unsigned()->nullable();
            $table->foreign('optional_id')->references('id')->on('optionals');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounttype_optional');
    }
}
