@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Promo Code#{{ $code->id }} - {{ $code->code }}
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/code') }}">Promo Codes</a></li>
                <li class="active">Promo Code#{{ $code->id }} - {{ $code->code }}</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['method' => 'PUT' , 'action' => ['CodeController@update', $code]]) !!}
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Promo Code Info.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Promo Code</label>
                                        <input type="text" class="form-control" name="code" value="{{ old('code', $code->code) }}" >
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">PREFIX</label>
                                        <select class="form-control" name="prefix">
                                            @foreach($prefixes as $prefix)
                                                <option value="{{$prefix->id}}" {{ old('prefix',$code->voucher_prefix_id) == $prefix->id ? 'selected=selected' : '' }}>{{$prefix->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Start Date</label>
                                        <input type="text" class="form-control datepicker" name="start" autocomplete="off" value="{{ old('start', date('m/d/Y' , strtotime($code->startdate))) }}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">End Date</label>
                                        <input type="text" class="form-control datepicker" name="end" autocomplete="off" value="{{ old('end', date('m/d/Y' , strtotime($code->enddate))) }}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-header with-border">
                            <h3 class="box-title">Discount Type.</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Discount Type</label>
                                        <select class="form-control" name="discounttype">
                                            <option value="">Select Discount Type</option>
                                            @foreach($discounttypes as $discounttype)
                                                <option value="{{$discounttype->id}}" {{ $code->discounttype_id == $discounttype->id ? 'selected=selected' : '' }}>{{$discounttype->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="discounttype_value" {{ $code->discounttype->value_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">Value <span id="type_value"></span></label>
                                            <input type="text" name="value" class="form-control" value="{{ $code->value }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="discounttype_minpurchase" {{ $code->discounttype->min_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">MIN Purchase</label>
                                            <input type="text" name="min" class="form-control"  value="{{ $code->min_purchase }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="discounttype_maxdiscount" {{ $code->discounttype->max_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">MAX Discount</label>
                                            <input type="text" name="max" class="form-control"  value="{{ $code->max_discount }}" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="discounttype_adult" {{ $code->discounttype->daytrip_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">Day trip (Adult) <span class="per_unit_type_value"></span></label>
                                            <input name="daytrip_adult" type="number" class="form-control" value="{{ $code->adult_value }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="discounttype_child" {{ $code->discounttype->daytrip_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">Day trip (Child) <span class="per_unit_type_value"></span></label>
                                            <input name="daytrip_child" type="number" class="form-control" value="{{ $code->child_value }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div id="discounttype_private" {{ $code->discounttype->private_flag == 0 ? 'style=display:none;' : '' }}>
                                            <label for="inputEmail3">Private Charter <span class="per_unit_type_value"></span></label>
                                            <input name="private" type="text" class="form-control"  value="{{ $code->private_value }}" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="optional">
                            </div>

                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Optional</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12" id="optionals">
                                        @foreach($code->discounttype->optionals as $optional)
                                            <input name="optional" type="radio" {{ (($code->optional_id == $optional->id) ? 'checked=checked' : '') }} value="{{$optional->id}}"> <label for="inputEmail3">{{$optional->name}}</label><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Save</button>
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>

        {!! Form::close() !!}
        <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2({});
            $('#lfm').filemanager('image');
            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                minuteStep: 1,
            });

            $("select[name='discounttype']").change(function() {

                $.ajax({
                    url: '/cfadmin/api/getdiscounttypesetting',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: $(this).val()},

                    success: function (data) {

                        $('#optionals').html(data.optionals);

                        if(data.data.value_flag == 0){
                            $('#discounttype_value').css('display','none');
                            $('#type_value').text('')
                        }
                        else{
                            $('#discounttype_value').css('display','block');
                            $('#type_value').text('(' + data.data.type_value + ')')
                            // $('#discounttype_value input').val(data.data.value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                        }

                        if(data.data.min_flag == 0){
                            $('#discounttype_minpurchase').css('display','none');
                        }
                        else{
                            $('#discounttype_minpurchase').css('display','block');
                            // $('#discounttype_minpurchase input').val(data.data.min_purchase + ' THB');
                        }

                        if(data.data.max_flag == 0){
                            $('#discounttype_maxdiscount').css('display','none');
                        }
                        else{
                            $('#discounttype_maxdiscount').css('display','block');
                            // $('#discounttype_maxdiscount input').val(data.data.max_discount + ' THB');
                        }

                        // if(data.data.adult_value == 0){
                        //     $('#discounttype_adult').css('display','none');
                        // }
                        // else{
                        //     $('#discounttype_adult').css('display','block');
                        //     $('#discounttype_adult input').val(data.data.adult_value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                        // }
                        //
                        // if(data.data.child_value == 0){
                        //     $('#discounttype_child').css('display','none');
                        // }
                        // else{
                        //     $('#discounttype_child').css('display','block');
                        //     $('#discounttype_child input').val(data.data.child_value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                        // }

                        if(data.data.daytrip_flag == 0){
                            $('#discounttype_adult').css('display','none');
                            $('#discounttype_child').css('display','none');
                            $('.per_unit_type_value').text('')
                        }
                        else{
                            $('#discounttype_adult').css('display','block');
                            // $('#discounttype_adult input').val(data.data.adult_value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                            $('#discounttype_child').css('display','block');
                            // $('#discounttype_child input').val(data.data.child_value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                            $('.per_unit_type_value').text('(' + data.data.type_value + ')')
                        }

                        if(data.data.private_flag == 0){
                            $('#discounttype_private').css('display','none');
                            $('.per_unit_type_value').text('')
                        }
                        else{
                            $('#discounttype_private').css('display','block');
                            $('.per_unit_type_value').text('(' + data.data.type_value + ')')
                            // $('#discounttype_private input').val(data.data.private_value + ' ' + ((data.data.type_value == 'PERCENT') ? '%' : data.data.type_value) );
                        }

                    },
                    error: function (data) {

                    }
                });
            });
        });
    </script>
@endsection
