<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Integrated and modified by  <a href="http://www.catfoostudio.com">Catfoostudio.com</a></b>
    </div>
    <strong>All rights reserved. | Copyright © 2017 <a href="{{ url('/') }}">discover-catamaran.com</a> Created by <a href="http://www.catfoostudio.com">Catfoostudio.com</a></strong>
</footer>