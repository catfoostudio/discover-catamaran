<!DOCTYPE html>
<html lang="en">
<head>

    @include('template.head')
    <style>
        .fc-title {
            color: white
        }

        .fc-title:hover {
            color: #ffe978
        }

        .fc-scroller {
            height: auto !important;
        }

        .fc-head .fc-widget-header {
            margin-right: 0 !important;
        }

        .fc-scroller {
            overflow: visible !important;
        }
        .ui-datepicker{
            background-color: white;
        }
    </style>


    {{--<!-- LOAD JQUERY LIBRARY -->--}}
    {{--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>--}}

<!-- LOADING FONTS AND ICONS -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"
          property="stylesheet" type="text/css" media="all"/>

    <link rel="stylesheet" type="text/css"
          href="/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css"
          href="/plugins/revolution/revolution/fonts/font-awesome/css/font-awesome.min.css">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/layers.css">

    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/navigation.css">

    {{--<link rel="stylesheet" type="text/css" href="/plugins/revolution/assets/css/noneed.css">--}}
<!-- REVOLUTION JS FILES -->
    <script type="text/javascript"
            src="/plugins/revolution/revolution/js/source/jquery.themepunch.tools.min.js"></script>
    {{--<script type="text/javascript" src="/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>--}}
    <script type="text/javascript"
            src="/plugins/revolution/revolution/js/source/jquery.themepunch.revolution.js"></script>

    {{--<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
    {{--src="/plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>--}}

</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap" style="padding-bottom:20px;">
            <h1>{{ $product->name }}</h1>
            {!! $product->short_description  !!}
        </div>
    </header>
    <!-- //Intro -->
    <!-- SLIDER EXAMPLE -->
    <section class="example">
        <article class="content">
            <div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
                 data-alias="facebook-feed8"
                 style="margin:0px auto;background-color:#dddddd;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
                <div id="rev_slider_8_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                    <ul>
                        <!-- SLIDE  -->
                        @foreach($product->images()->orderBy('order')->get() as $image)
                            <li data-index="rs-28" data-transition="scaledownfromleft" data-slotamount="default"
                                data-easein="default" data-easeout="default" data-masterspeed="1500"
                                data-thumb="{{ asset(explode('.',$image->path)[0].'_100x50.jpg')}}" data-rotate="0"
                                data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                                data-saveperformance="off" data-title="Umbrella" data-param1="September 7, 2015"
                                data-param2="Alfon Much, The Precious Stones" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="/plugins/revolution/assets/images/dummy.png" alt="" width="1920" height="1080"
                                     data-lazyload="{{ asset($image->path)}}"
                                     data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->
                            </li>
                        @endforeach
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->


        </article>
    </section>
    <!-- Gallery -->
{{--<div class="gallery" id="gallery">--}}
{{--@foreach($product->images()->orderBy('order')->get() as $image)--}}
{{--<figure class="one-fourth" data-src="{{ asset($image->path)}}" style="overflow: hidden;">--}}
{{--<img src="{{ asset($image->path)}}" alt=""/>--}}
{{--<figcaption>--}}
{{--<span class="icojam_zoom_in"></span>--}}
{{--<div>--}}
{{--<h5>BOL GOLDEN CAPE</h5>--}}
{{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>--}}
{{--</div>--}}
{{--</figcaption>--}}
{{--</figure>--}}
{{--@endforeach--}}

{{--<!-- //Item -->--}}
{{--</div>--}}
<!-- //Gallery -->

    <!-- Content -->
    <div class="content">
        <!-- Tab navigation -->
        <nav class="tabs six" role="navigation" id="tab-navigation">
            <ul class="wrap">
                <li><a href="#tab1" title="Description">
                        <span class="icojam_info_3"></span> Description
                    </a></li>
                {{--<li><a href="#tab2" title="Specifications">--}}
                {{--<span class="icojam_document"></span> Specifications--}}
                {{--</a></li>--}}
                {{--<li><a href="#tab3" title="Equipment">--}}
                {{--<span class="icojam_anchor"></span> Equipment--}}
                {{--</a></li>--}}
                {{--<li><a href="#tab4" title="Availability" id="calendareventlink">--}}
                        {{--<span class="icojam_calendar_1"></span> Availability--}}
                    {{--</a></li>--}}
                {{--<li><a href="#tab5" title="Contact Broker">--}}
                        {{--<span class="icojam_target"></span> Contact directly--}}
                    {{--</a></li>--}}
                {{--<li><a href="#tab6" title="Get Brochure">--}}
                        {{--<span class="icojam_inbox_receive"></span> Get Brochure--}}
                    {{--</a></li>--}}
            </ul>
        </nav>
        <!-- //Tab navigation -->

        <!-- Wrapper -->
        <div class="wrap">
            <!-- Tab Content-->
            <article class="tab-content" id="tab1">
                <div class="row">

                    <!-- OneHalf -->
                    <div class="one-half">
                        {!! $product->description !!}
                    </div>
                    <!-- //OneHalf -->

                    <!-- OneHalf -->

                    {{--<div class="one-half">--}}
                        {{--<div id="calendar" style="background-color: white;padding: 15px;"></div>--}}
                        {{--<a href="#bookingSteps" title="Proceed with booking" class="button gold large full">Proceed--}}
                        {{--with booking</a>--}}
                        {{--<img src="{{ asset('uploads/content1.png') }}" alt="Elan 1923 Impression" />--}}
                    {{--</div>--}}

                    {{--<!-- Booking Steps -->--}}
                    {{--<div class="booking bookingSteps one-half">--}}
                        {{--<!-- TwoThird -->--}}
                        {{--<div class="full-width">--}}
                            {{--<dl class="accordion">--}}
                                {{--<!-- Item -->--}}
                                {{--<dt>Client information</dt>--}}
                                {{--<dd>--}}
                                    {{--{!! Form::open(['method' => 'POST' , 'action' => ['EnquireController@store'] , 'class' => 'row']) !!}--}}
                                    {{--{!! Form::hidden('product' , $product->id) !!}--}}
                                    {{--{!! Form::hidden('tourcompany' , $product->tourcompanies->first()->id) !!}--}}
                                    {{--<p>Please enter your details to complete an order. All fields are required. </p>--}}
                                    {{--@if (count($errors) > 0)--}}
                                        {{--<div class="alert alert-danger">--}}
                                            {{--<strong>Sorry!</strong><br><br>--}}
                                            {{--<ul>--}}
                                                {{--@foreach ($errors->all() as $error)--}}
                                                    {{--<li>{{ $error }}</li>--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="full-width">--}}

                                            {{--<fieldset>--}}
                                                {{--<h3>Information</h3>--}}
                                                {{--<div class="one-half">--}}
                                                    {{--<label for="Depart Date">Depart Date <span style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="text" id="departdate" autocomplete="off" name="travel_date" readonly/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-half">--}}
                                                {{--<label for="Arrival Date">Arrival Date <span--}}
                                                {{--style="color: red;">(required)</span></label>--}}
                                                {{--<input type="text" id="arrivaldate"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-half">--}}
                                                    {{--<label >Name <span style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="text" name="name"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-half">--}}
                                                    {{--<label >Tel. <span style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="number" id="phone" name="tel"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-half">--}}
                                                    {{--<label >E-mail <span--}}
                                                                {{--style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="email" id="email" name="email"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-half">--}}
                                                    {{--<label >Line</label>--}}
                                                    {{--<input type="text" id="line" name="line"/>--}}
                                                {{--</div>--}}
                                                {{--<div class="one-half">--}}
                                                    {{--<label >Wechat ID</label>--}}
                                                    {{--<input type="text" id="wechat" name="wechat"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="full-width">--}}
                                                    {{--<label>ENQUIRE</label>--}}
                                                    {{--<textarea name="enquire"></textarea>--}}
                                                {{--</div>--}}
                                            {{--</fieldset>--}}
                                            {{--<fieldset>--}}
                                                {{--<h3>Traveler</h3>--}}
                                                {{--<div class="one-third">--}}
                                                    {{--<label >Adult <span style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="number" name="adult" value="1"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-third">--}}
                                                    {{--<label >Child 4-11 yrs</label>--}}
                                                    {{--<input type="number" name="child" value="0"/>--}}
                                                {{--</div>--}}

                                                {{--<div class="one-third">--}}
                                                    {{--<label>Infant 1-3 yrs</label>--}}
                                                    {{--<input type="number" name="infant" value="0"/>--}}
                                                {{--</div>--}}

                                            {{--</fieldset>--}}
                                            {{--<fieldset>--}}
                                                {{--<h3>Transfer</h3>--}}
                                                {{--<div class="one-third">--}}
                                                    {{--<label for="transfer">Transfer <span--}}
                                                                {{--style="color: red;">(required)</span></label>--}}
                                                    {{--<select name="transfer" id="transfer">--}}
                                                        {{--<option value="wt">with transfer</option>--}}
                                                        {{--<option value="wot">without transfer</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                                {{--<div class="one-third transfer-info">--}}
                                                    {{--<label for="zone">Zone <span--}}
                                                                {{--style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="text" name="zone"/>--}}
                                                {{--</div>--}}
                                                {{--<div class="one-third transfer-info">--}}
                                                    {{--<label for="hotel">Hotel <span style="color: red;">(required)</span></label>--}}
                                                    {{--<input type="text" name="hotel"/>--}}
                                                {{--</div>--}}

                                            {{--</fieldset>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<h5>Promo code and payment type</h5>--}}
                                    {{--<p>Please enter your promotion code, if you have received it.</p>--}}

                                    {{--<div class="row">--}}
                                        {{--<div class="one-half">--}}
                                            {{--<label for="discount">Discount code</label>--}}
                                            {{--<input type="text" id="discount" name="discount"/>--}}
                                        {{--</div>--}}

                                        {{--<div class="one-half">--}}
                                            {{--<label for="payment">Payment type</label>--}}
                                            {{--<select id="payment" name="payment">--}}
                                                {{--<option selected>Payment type</option>--}}
                                                {{--<option>Credit card</option>--}}
                                                {{--<option>Paypal</option>--}}
                                                {{--<option>Direct bank transfer</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}

                                        {{--<div class="full-width checkbox">--}}
                                            {{--<input type="checkbox" id="terms" name="terms"/>--}}
                                            {{--<label for="terms">By clicking this box you accept general terms and--}}
                                                {{--conditions.</label>--}}

                                        {{--</div>--}}
                                        {{--<div class="full-width" id="recaptcha">--}}
                                            {{--@if ($errors->has('g-recaptcha-response'))--}}
                                                {{--<span class="help-block"><strong>{{ $errors->first('g-recaptcha-response') }}</strong></span>--}}
                                            {{--@endif--}}
                                            {{--{!! NoCaptcha::display() !!}--}}
                                        {{--</div>--}}
                                        {{--<div class="full-width">--}}
                                            {{--<button title="SUBMIT" type="submit" class="button gold large">SUBMIT--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</dd>--}}
                                {{--<!-- //Item -->--}}


                            {{--</dl>--}}
                            {{--<div class="thank-you-note">--}}
                                {{--<div class="box-white">--}}
                                    {{--<h2>Thank you</h2>--}}
                                    {{--<p>We will get back you with regards your reservation within 24 hours</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- TwoThird -->--}}

                    {{--</div>--}}
                    <!-- //Booking Steps -->
                    <!-- //OneHalf -->
                </div>
            </article>
            <!-- //Tab Content-->

        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
{{--<script src="{{ asset('js/jquery.min.js')}}"></script>--}}

<script src="{{ asset('js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('js/jetmenu.js')}}"></script>
<script src="{{ asset('js/jquery.uniform.min.js')}}"></script>
<script src="{{ asset('js/lightGallery.min.js')}}"></script>
<script src="{{ asset('js/scripts.js')}}"></script>


{!! NoCaptcha::renderJs() !!}
<!-- Page specific script -->
<script>
    $(function () {

        $('#departdate').datepicker({ dateFormat: 'yy-mm-dd' });

        $('#transfer').change(function () {
           if($(this).val() == 'wt'){
                $('.transfer-info').show();
           }
           else {
               $('.transfer-info').hide();
           }
        });
        // Initialize Select2 Elements
        $('.accordion dt:first-of-type').addClass('expanded');
        $('.accordion dd:first-of-type').show();
        //
        // function pad(str, max) {
        //     str = str.toString();
        //     return str.length < max ? pad("0" + str, max) : str;
        // }
        //
        //
        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }

        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();

        var currentEventDate = new Date();

        var allevents = [
                @foreach($tourcompany->pivot->stocks as $s)
            {

                title: '{{ $s->max_seat - $s->remaining_seat }}',
                start: '{!! date('Y-m-d' , strtotime($s->reserved_date)) !!}',
                allDay: true,
                backgroundColor: "#dd4b39", //red
                borderColor: "#dd4b39", //red
            },
            @endforeach
        ];

        function calendar(){
            $('#calendar').fullCalendar({
                // timezone: 'Asia/Bangkok',
                selectable: true,
                dayClick: function (date, jsEvent, view) {
                    $('#departdate').val(date.format("YYYY-MM-DD"));
                },
                eventClick: function (calEvent, jsEvent, view) {
                    // Get the case number in the row
                    $('#departdate').val(calEvent.start.format("YYYY-MM-DD"));
                },
                {{--// currentEvent = calEvent;--}}
                        {{--//var date = calEvent.start;--}}
                        {{--//console.log($(event.target).closest('.content').prev().find('h1').text());--}}
                        {{--currentEventDate = date;--}}

                        {{--//$('#stockLeftLabel').text(date.format('MMMM Do YYYY') + ' : ' + $(event.target).closest('.content').prev().find('h1').text());--}}
                        {{--$('input[name="date"]').val(date.format('YYYY/MM/DD'));--}}
                        {{--$('input[name="product"]').val($(event.target).closest('.content').prev().data('product'));--}}
                        {{--//$('#stockLeft').modal('show');--}}

                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}


                        {{--var events = $('#calendar_{{$product->id}}_{{ $tourcompany->id }}').fullCalendar('clientEvents');--}}
                        {{--console.log(events.length);--}}
                        {{--for (var i = 0; i < events.length; i++) {--}}
                        {{--if (moment(date).isSame(moment(events[i].start))) {--}}
                        {{--//ShowEditScreen(events[i].id);--}}
                        {{--currentEvent = events[i];--}}

                        {{--//alert(moment(moment(events[i].start)));--}}
                        {{--break;--}}
                        {{--}--}}
                        {{--else if (i == events.length - 1) {--}}
                        {{--//ShowCreateNew(date);--}}
                        {{--//alert(2);--}}
                        {{--}--}}
                        {{--}--}}

                        {{--var formData = new FormData();--}}
                        {{--formData.append('product', '{{ $product->id }}');--}}
                        {{--formData.append('tourcompany', '{{ $tourcompany->id }}');--}}
                        {{--formData.append('maxseat', $("input[name='maxseat_{{$product->id}}_{{ $tourcompany->id }}']").val());--}}

                        {{--formData.append('date', $("input[name='date']").val());--}}
                        {{--formData.append('remark', $("textarea[name='remark_{{$product->id}}_{{ $tourcompany->id }}']").val());--}}

                        {{--$.ajaxSetup({--}}
                        {{--headers: {--}}
                        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                        {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                        {{--url: '/cfadmin/api/getstockdata',--}}
                        {{--method: 'POST',--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--dataType: 'JSON',--}}
                        {{--cache: false,--}}
                        {{--data: formData,--}}
                        {{--success: function (result) {--}}
                        {{--$('#title_{{ $product->id }}_{{ $tourcompany->id }}').text('Info. : ' + $('input[name="date"]').val());--}}
                        {{--if (result.success) {--}}
                        {{--/*var newEvent = new Object();--}}
                        {{--var remaining = data.remaining;--}}

                        {{--$('#stockLeft').modal('hide');*/--}}
                        {{--//alert(data.data.maxseat);--}}

                        {{--$("input[name='maxseat_{{$product->id}}_{{ $tourcompany->id }}']").val(result.data.maxseat);--}}


                        {{--$("textarea[name='remark_{{$product->id}}_{{ $tourcompany->id }}']").val(result.data.remark);--}}
                        {{--$("#title_booking_date_{{ $product->id }}_{{ $tourcompany->id }}").text($('input[name="date"]').val());--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideDown("slow");--}}
                        {{--} else {--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideDown("slow");--}}
                        {{--//                                $('#stockLeft').modal('hide');--}}
                        {{--//alert(result.message);--}}
                        {{--}--}}
                        {{--},--}}
                        {{--error: function (data) {--}}
                        {{--//                            $('#stockLeft').modal('hide');--}}
                        {{--//alert(data.message);--}}
                        {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                        {{--url: 'getvoucherlist',--}}
                        {{--method: 'POST',--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--dataType: 'JSON',--}}
                        {{--cache: false,--}}
                        {{--data: formData,--}}
                        {{--success: function (data) {--}}
                        {{--//                            if(data.success) {--}}
                        {{--//--}}
                        {{--//                            } else {--}}
                        {{--//--}}
                        {{--//                            }--}}

                        {{--$('#booking_table_{{ $product->id }}_{{ $tourcompany->id }}').html('<tr>\n' +--}}
                        {{--'                                                        <th>No.</th>\n' +--}}
                        {{--'                                                        <th>Name</th>\n' +--}}
                        {{--'                                                        <th>Units</th>\n' +--}}
                        {{--'                                                        <th>Hotel</th>\n' +--}}
                        {{--'                                                    </tr>');--}}

                        {{--$.each(data, function (i, item) {--}}

                        {{--$('#booking_table_{{ $product->id }}_{{ $tourcompany->id }}').append('<tr>' +--}}
                        {{--'<td>' + data[i].voucher_prefix + '-' + pad(data[i].voucher_no, 4) + '</td>' +--}}
                        {{--'<td>' + data[i].customername + '</td>' +--}}
                        {{--'<td>' + data[i].units + '</td>' +--}}
                        {{--'<td>' + data[i].hotelname + '</td>' +--}}
                        {{--'</tr>');--}}
                        {{--})--}}


                        {{--//alert(data[0].id);--}}
                        {{--},--}}
                        {{--error: function (data) {--}}

                        {{--}--}}
                        {{--});--}}

                        {{--// change the day's background color just for fun--}}
                        {{--//  $(this).css('background-color', 'red');--}}

                        {{--},--}}
                        {{--eventClick: function (calEvent, jsEvent, view) {--}}
                        {{--// Get the case number in the row--}}
                        {{--// pos X clicked on the row / total width of the row * 100 (to percent) / (percent of 7 days by week)--}}

                        {{--var dateClicked;--}}
                        {{--var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));--}}
                        {{--// Get the table--}}
                        {{--var table = $(this).parent().parent().parent().parent().children();--}}
                        {{--$(table).each(function () {--}}
                        {{--// Get the thead--}}
                        {{--if ($(this).is('thead')) {--}}
                        {{--var tds = $(this).children().children();--}}
                        {{--dateClicked = $(tds[caseNumber]).attr("data-date");--}}
                        {{--//alert(dateClicked);--}}
                        {{--}--}}
                        {{--});--}}
                        {{--//                    alert(dateClicked);--}}
                        {{--//                    alert(moment(dateClicked));--}}
                        {{--//                    alert(moment(dateClicked).format());--}}
                        {{--//alert(new Date(dateClicked));--}}
                        {{--var date = moment(dateClicked);--}}
                        {{--//alert(date);--}}
                        {{--currentEventDate = dateClicked;--}}
                        {{--//$('#stockLeftLabel').text(date.format('MMMM Do YYYY') + ' : ' + $(event.target).closest('.content').prev().find('h1').text());--}}
                        {{--$('input[name="date"]').val(date.format('YYYY/MM/DD'));--}}
                        {{--$('input[name="product"]').val($(event.target).closest('.content').prev().data('product'));--}}


                        {{--//$('#stockLeft').modal('show');--}}

                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}

                        {{--var events = $('#calendar_{{$product->id}}_{{ $tourcompany->id }}').fullCalendar('clientEvents');--}}

                        {{--//console.log(events.[i].start);--}}
                        {{--for (var i = 0; i < events.length; i++) {--}}

                        {{--if (date.isSame(moment(events[i].start).format())) {--}}
                        {{--//ShowEditScreen(events[i].id);--}}
                        {{--currentEvent = events[i];--}}
                        {{--//alert(1);--}}
                        {{--break;--}}
                        {{--}--}}
                        {{--else if (i == events.length - 1) {--}}
                        {{--//ShowCreateNew(date);--}}
                        {{--// alert(moment(date));--}}
                        {{--}--}}
                        {{--}--}}

                        {{--var formData = new FormData();--}}
                        {{--formData.append('product', '{{ $product->id }}');--}}
                        {{--formData.append('tourcompany', '{{ $tourcompany->id }}');--}}
                        {{--formData.append('maxseat', $("input[name='maxseat_{{$product->id}}_{{ $tourcompany->id }}']").val());--}}

                        {{--formData.append('date', $("input[name='date']").val());--}}
                        {{--formData.append('remark', $("textarea[name='remark_{{$product->id}}_{{ $tourcompany->id }}']").val());--}}

                        {{--$.ajaxSetup({--}}
                        {{--headers: {--}}
                        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                        {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                        {{--url: '/cfadmin/api/getstockdata',--}}
                        {{--method: 'POST',--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--dataType: 'JSON',--}}
                        {{--cache: false,--}}
                        {{--data: formData,--}}
                        {{--success: function (result) {--}}
                        {{--$('#title_{{ $product->id }}_{{ $tourcompany->id }}').text('Info. : ' + $('input[name="date"]').val());--}}
                        {{--if (result.success) {--}}
                        {{--/*var newEvent = new Object();--}}
                        {{--var remaining = data.remaining;--}}

                        {{--$('#stockLeft').modal('hide');*/--}}
                        {{--//alert(data.data.maxseat);--}}

                        {{--$("input[name='maxseat_{{$product->id}}_{{ $tourcompany->id }}']").val(result.data.maxseat);--}}


                        {{--$("textarea[name='remark_{{$product->id}}_{{ $tourcompany->id }}']").val(result.data.remark);--}}
                        {{--$("#title_booking_date_{{ $product->id }}_{{ $tourcompany->id }}").text($('input[name="date"]').val());--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideDown("slow");--}}
                        {{--} else {--}}
                        {{--$('#detail_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}_{{ $tourcompany->id }}').slideDown("slow");--}}
                        {{--//                                $('#stockLeft').modal('hide');--}}
                        {{--//alert(result.message);--}}
                        {{--}--}}
                        {{--},--}}
                        {{--error: function (data) {--}}
                        {{--//                            $('#stockLeft').modal('hide');--}}
                        {{--//alert(data.message);--}}
                        {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                        {{--url: 'getvoucherlist',--}}
                        {{--method: 'POST',--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--dataType: 'JSON',--}}
                        {{--cache: false,--}}
                        {{--data: formData,--}}
                        {{--success: function (data) {--}}
                        {{--//                            if(data.success) {--}}
                        {{--//--}}
                        {{--//                            } else {--}}
                        {{--//--}}
                        {{--//                            }--}}

                        {{--$('#booking_table_{{ $product->id }}_{{ $tourcompany->id }}').html('<tr>\n' +--}}
                        {{--'                                                        <th>No.</th>\n' +--}}
                        {{--'                                                        <th>Name</th>\n' +--}}
                        {{--'                                                        <th>Units</th>\n' +--}}
                        {{--'                                                        <th>Hotel</th>\n' +--}}
                        {{--'                                                    </tr>');--}}

                        {{--$.each(data, function (i, item) {--}}

                        {{--$('#booking_table_{{ $product->id }}_{{ $tourcompany->id }}').append('<tr>' +--}}
                        {{--'<td>' + data[i].voucher_prefix + '-' + pad(data[i].voucher_no, 4) + '</td>' +--}}
                        {{--'<td>' + data[i].customername + '</td>' +--}}
                        {{--'<td>' + data[i].units + '</td>' +--}}
                        {{--'<td>' + data[i].hotelname + '</td>' +--}}
                        {{--'</tr>');--}}
                        {{--})--}}


                        {{--//alert(data[0].id);--}}
                        {{--},--}}
                        {{--error: function (data) {--}}

                        {{--}--}}
                        {{--});--}}

                        {{--// alert(2);--}}
                        {{--//$('#calendar{{$product->id}}').fullCalendar('select', calEvent.start.format('YYYY-MM-DD'));--}}
                        {{--/*currentEvent = calEvent;--}}
                        {{--var date = calEvent.start;--}}
                        {{--console.log($(event.target).closest('.content').prev().find('h1').text());--}}
                        {{--currentEventDate = calEvent.start;--}}
                        {{--//$('#stockLeftLabel').text(date.format('MMMM Do YYYY') + ' : ' + $(event.target).closest('.content').prev().find('h1').text());--}}
                        {{--$('input[name="date"]').val(date.format('YYYY/MM/DD'));--}}
                        {{--$('input[name="product"]').val($(event.target).closest('.content').prev().data('product'));--}}
                        {{--//$('#stockLeft').modal('show');--}}

                        {{--$('#detail{{ $product->id }}').slideUp("slow");--}}


                        {{--var formData = new FormData();--}}
                        {{--formData.append('product', $('input[name="product"]').val());--}}
                        {{--formData.append('maxseat', $("input[name='maxseat_{{$product->id}}']").val());--}}
                        {{--formData.append('date', $("input[name='date']").val());--}}
                        {{--formData.append('remark', $("textarea[name='remark_{{$product->id}}']").val());--}}

                        {{--$.ajaxSetup({--}}
                        {{--headers: {--}}
                        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                        {{--}--}}
                        {{--});--}}

                        {{--$.ajax({--}}
                        {{--url: '/cfadmin/api/getstockdata',--}}
                        {{--method: 'POST',--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--dataType: 'JSON',--}}
                        {{--cache: false,--}}
                        {{--data: formData,--}}
                        {{--success: function (result) {--}}
                        {{--$('#title_{{ $product->id }}').text('Info. : ' + $('input[name="date"]').val());--}}
                        {{--if (result.success) {--}}

                        {{--//alert(data.data.maxseat);--}}

                        {{--$("input[name='maxseat_{{$product->id}}']").val(result.data.maxseat);--}}
                        {{--$("textarea[name='remark_{{$product->id}}']").val(result.data.remark);--}}
                        {{--$("#title_booking_date_{{ $product->id }}").text($('input[name="date"]').val());--}}
                        {{--$('#detail_initial_{{ $product->id }}').slideUp("slow");--}}
                        {{--$('#detail{{ $product->id }}').slideUp("slow");--}}
                        {{--$('#detail{{ $product->id }}').slideDown("slow");--}}
                        {{--} else {--}}
                        {{--$('#detail{{ $product->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}').slideUp("slow");--}}
                        {{--$('#detail_initial_{{ $product->id }}').slideDown("slow");--}}
                        {{--//                                $('#stockLeft').modal('hide');--}}
                        {{--//alert(result.message);--}}
                        {{--}--}}
                        {{--},--}}
                        {{--error: function (data) {--}}
                        {{--//                            $('#stockLeft').modal('hide');--}}
                        {{--//alert(data.message);--}}
                        {{--}--}}
                        {{--});*/--}}
                        {{--},--}}
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
                //Random default events
                events: allevents,
            });
        }

        function slider(){
            $("#rev_slider_8_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "/plugins/revolution/revolution/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 3000,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    thumbnails: {
                        style: 'hesperiden',
                        enable: true,
                        width: 100,
                        height: 50,
                        min_width: 100,
                        wrapper_padding: 5,
                        wrapper_color: 'transparent',
                        wrapper_opacity: '1',
                        visibleAmount: 5,
                        hide_onmobile: false,
                        hide_onleave: false,
                        direction: 'vertical',
                        span: false,
                        position: 'inner',
                        space: 10,
                        h_align: 'right',
                        v_align: 'bottom',
                        h_offset: 0,
                        v_offset: 20
                    }
                },
                gridwidth: 800,
                gridheight: 600,
                lazyType: "single",
                shadow: 0,
                spinner: "off",

                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }

        $.when(calendar() ).done(function() {
            slider();
        })

        //$("#calendareventlink").click(function () {

        // });


        {{--$('.btn_update_stock_{{ $product->id }}_{{ $tourcompany->id }}').on('click', function () {--}}
        {{--var formData = new FormData();--}}
        {{--formData.append('maxseat', $('input[name="maxseat_{{ $product->id }}_{{ $tourcompany->id }}"]').val());--}}
        {{--formData.append('initial_maxseat', $("input[name='initial_maxseat_{{$product->id}}_{{ $tourcompany->id }}']").val());--}}
        {{--formData.append('remark', $("textarea[name='remark_{{ $product->id }}_{{ $tourcompany->id }}']").val());--}}
        {{--formData.append('initial_remark', $("textarea[name='initial_remark_{{ $product->id }}_{{ $tourcompany->id }}']").val());--}}
        {{--formData.append('product', '{{ $product->id }}');--}}
        {{--formData.append('tourcompany', '{{ $tourcompany->id }}');--}}
        {{--formData.append('date', $("input[name='date']").val());--}}

        {{--$.ajaxSetup({--}}
        {{--headers: {--}}
        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--}--}}
        {{--});--}}

        {{--$.ajax({--}}
        {{--url: 'updatestock',--}}
        {{--method: 'POST',--}}
        {{--processData: false,--}}
        {{--contentType: false,--}}
        {{--dataType: 'JSON',--}}
        {{--cache: false,--}}
        {{--data: formData,--}}
        {{--success: function (result) {--}}
        {{--if (result.success) {--}}
        {{--console.log(result);--}}
        {{--if (result.initial) {--}}
        {{--var newEvent = new Object();--}}
        {{--var remaining = result.remainingseat;--}}
        {{--var maxseat = result.maxseat;--}}
        {{--newEvent.start = currentEventDate;--}}
        {{--newEvent.allDay = true;--}}
        {{--newEvent.backgroundColor = '#dd4b39';--}}
        {{--newEvent.borderColor = '#dd4b39';--}}

        {{--newEvent.title = remaining + '/' + maxseat;--}}

        {{--$('#calendar_{{ $product->id }}_{{ $tourcompany->id }}').fullCalendar('renderEvent', newEvent);--}}
        {{--}--}}
        {{--else {--}}
        {{--currentEvent.title = result.remainingseat + '/' + result.maxseat;--}}
        {{--$('#calendar_{{ $product->id }}_{{ $tourcompany->id }}').fullCalendar('updateEvent', currentEvent);--}}
        {{--}--}}

        {{--//$('#stockLeft').modal('hide');--}}
        {{--} else {--}}
        {{--//$('#stockLeft').modal('hide');--}}
        {{--}--}}
        {{--},--}}
        {{--error: function (data) {--}}
        {{--//                        $('#stockLeft').modal('hide');--}}
        {{--}--}}
        {{--});--}}
        {{--});--}}


        // /* ADDING EVENTS */
        // var currColor = "#3c8dbc"; //Red by default
        // //Color chooser button
        // var colorChooser = $("#color-chooser-btn");
        // $("#color-chooser > li > a").click(function (e) {
        //     e.preventDefault();
        //     //Save color
        //     currColor = $(this).css("color");
        //     //Add color effect to button
        //     $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        // });
        // $("#add-new-event").click(function (e) {
        //     e.preventDefault();
        //     //Get value and make sure it is not null
        //     var val = $("#new-event").val();
        //     if (val.length == 0) {
        //         return;
        //     }
        //
        //     //Create events
        //     var event = $("<div />");
        //     event.css({
        //         "background-color": currColor,
        //         "border-color": currColor,
        //         "color": "#fff"
        //     }).addClass("external-event");
        //     event.html(val);
        //     $('#external-events').prepend(event);
        //
        //     //Add draggable funtionality
        //     ini_events(event);
        //
        //     //Remove event from text input
        //     $("#new-event").val("");
        // });
        //
        // $('#calendar').fullCalendar({
        //     defaultView: 'month',
        //     header: {
        //         left: 'prev,next',
        //         center: 'title',
        //         right: 'month,agendaWeek,agendaDay'
        //     },
        //     selectable: true,
        //     select: function(startDate, endDate) {
        //         alert('selected ' + startDate.format() + ' to ' + endDate.format());
        //     }
        //
        // })
    });
</script>

</body>
</html>