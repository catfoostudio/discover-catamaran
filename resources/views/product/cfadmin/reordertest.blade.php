<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <style>
        body {
            background: #FFFFFF;
            margin: 0px;
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            color: #4f5252;
            font-weight: 400;
        }
        .container{
            margin-top:50px;
            padding: 10px;
        }
        ul, ol, li {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        .reorder_link {
            color: #3675B4;
            border: solid 2px #3675B4;
            border-radius: 3px;
            text-transform: uppercase;
            background: #fff;
            font-size: 18px;
            padding: 10px 20px;
            margin: 15px 15px 15px 0px;
            font-weight: bold;
            text-decoration: none;
            transition: all 0.35s;
            -moz-transition: all 0.35s;
            -webkit-transition: all 0.35s;
            -o-transition: all 0.35s;
            white-space: nowrap;
        }
        .reorder_link:hover {
            color: #fff;
            border: solid 2px #3675B4;
            background: #3675B4;
            box-shadow: none;
        }
        #reorder-helper{
            margin: 18px 10px;
            padding: 10px;
        }
        .light_box {
            background: #efefef;
            padding: 20px;
            margin: 15px 0;
            text-align: center;
            font-size: 1.2em;
        }

        /* image gallery */
        .gallery{ width:100%; float:left; margin-top:100px;}
        .gallery ul{ margin:0; padding:0; list-style-type:none;}
        .gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
        .gallery img{ width:250px;}

        /* notice box */
        .notice, .notice a{ color: #fff !important; }
        .notice { z-index: 8888;padding: 10px;margin-top: 20px; }
        .notice a { font-weight: bold; }
        .notice_error { background: #E46360; }
        .notice_success { background: #657E3F; }
    </style>
</head>
<body>
<div class="container">
    <a href="javascript:void(0);" class="reorder_link" id="saveReorder">reorder photos</a>
    <button id="add_photo">Add Photo</button>
    <div id="reorderHelper" class="light_box" style="display:none;">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
    <div class="gallery">
        <ul class="reorder_ul reorder-photos-list">
            <?php
            // Include and create instance of db class
            //require_once 'DB.class.php';
            //$db = new DB();

            // Fetch all images from database

            //$images = \App\Models\Image::all();
            ?>
            @if(!empty($product->images))
                @foreach($product->images()->orderBy('order')->get() as $image)
                <li id="image_li_{{ $image->id }}" class="ui-sortable-handle">
                    <img class="btn-delete-image" data-id="{{ $image->id }}" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer" />
                    <a href="javascript:void(0);" style="float:none;" class="image_link">
                        <img src="{{ asset($image->path) }}" alt="" style="width: 200px;height: 150px;object-fit:cover;object-position: 50% 50%">
                    </a>
                </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.reorder_link').on('click',function(){
            $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
            $('.reorder_link').html('save reordering');
            $('.reorder_link').attr("id","saveReorder");
            $('#reorderHelper').slideDown('slow');
            $('.image_link').attr("href","javascript:void(0);");
            $('.image_link').css("cursor","move");
            $("#saveReorder").click(function( e ){
                if( !$("#saveReorder i").length ){
                    $(this).html('').prepend('<img src="images/refresh-animated.gif"/>');
                    $("ul.reorder-photos-list").sortable('destroy');
                    $("#reorderHelper").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');

                    var h = [];
                    $("ul.reorder-photos-list li").each(function() {  h.push($(this).attr('id').substr(9));  });

                    $.ajax({
                        type: "POST",
                        url: "{{ url('cfadmin/orderprocess') }}",
                        data: {ids: " " + h + ""},
                        headers:
                            {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        success: function(){
                            //window.location.reload();
                        }
                    });
                    return false;
                }
                e.preventDefault();
            });
        });

        var lfm = function(options, cb) {

            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = cb;
        }


        $('.btn-delete-image').click(function () {

            var imageObject = $(this).parent();

            $.ajax({
                type: "POST",
                url: "{{ url('cfadmin/removeproductimage') }}",
                data: {id: $(this).data('id'), productid: '{{ $product->id }}'},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function(result){
                    imageObject.remove();
                }
            });

        });



        $('#add_photo').click( function (e) {
            lfm({type: 'image', prefix: ''}, function(url, path) {
                //alert(url + ' ' + path);

                $.ajax({
                    type: "POST",
                    url: "{{ url('cfadmin/addproductimage') }}",
                    data: {path: path,id: '{{ $product->id }}'},
                    headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    success: function(result){
                        $('.reorder-photos-list').append('<li id="image_li_'+result+'" class="ui-sortable-handle">\n' +
                            '                    <img class="btn-delete-image" data-id="'+result+'" src="{{ asset('images/red-cross.png') }}" style="position: absolute;width: 20px;height: 20px;cursor: pointer" />\n' +
                            '                    <a href="javascript:void(0);" style="float:none;" class="image_link">\n' +
                            '                        <img src="'+url+'" alt="" style="width: 150px;height: 150px;">\n' +
                            '                    </a>\n' +
                            '                </li>');
                    }
                });


            });
        });
    });
</script>
</body>
</html>
