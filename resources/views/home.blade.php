<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Home</title>



<!-- LOAD JQUERY LIBRARY -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>

    <!-- LOADING FONTS AND ICONS -->
    <link href="http://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Lato%3A900" rel="stylesheet" property="stylesheet" type="text/css" media="all" />


    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/fonts/font-awesome/css/font-awesome.css">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->


    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/layers.css">

    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/navigation.css">

    <!-- FONT AND STYLE FOR BASIC DOCUMENTS, NO NEED FOR FURTHER USAGE IN YOUR PROJECTS-->
    {{--<link href="http://fonts.googleapis.com/css?family=Roboto%3A700%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all" />--}}
    {{--<link rel="stylesheet" type="text/css" href="/plugins/revolution/assets/css/noneed.css">--}}
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="/plugins/revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
    .tab-content table td:first-child{width:auto}
    </style>
</head>
<body class="home">
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=475854059211013&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- Header -->
@include('template.header')
<!-- //Header -->

<!-- Main -->
@include('template.main')
<!-- //Main -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
{{--<script src="js/jquery.min.js"></script>--}}
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jetmenu.js') }}"></script>
<script src="{{ asset('js/jquery.uniform.min.js') }}"></script>
{{--<script src="{{ asset('js/jquery.lightSlider.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/wow.min.js') }}"></script>--}}
<script src="{{ asset('js/scripts.js') }}"></script>

<script>
    // $(document).ready(function() {
    //     $("#lightSliderPosts").lightSlider({
    //         item:1,
    //         keyPress:true,
    //         gallery:false,
    //         pager:false,
    //         prevHtml: 'PREVIOUS',
    //         nextHtml: 'NEXT'
    //     });
    //
    //     $("#lightSliderDeals").lightSlider({
    //         item:1,
    //         keyPress:true,
    //         gallery:false,
    //         pager:false,
    //         prevHtml: 'PREVIOUS',
    //         nextHtml: 'NEXT'
    //     });
    //
    //     new WOW().init();
    // });
</script>
<div style="font-size: 8px;background-color: #2b3a42;">Icons made by <a  href="https://www.flaticon.com/authors/tomas-knop" title="Tomas Knop" style="color: #2b3a42">Tomas Knop</a> from <a href="https://www.flaticon.com/" title="Flaticon" style="color: #2b3a42">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank" style="color: #2b3a42"  >CC 3.0 BY</a></div>
</body>
</html>