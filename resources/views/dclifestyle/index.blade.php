<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - DC Life Style</title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/lightGallery.min.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <style>
        .tab-content table td:first-child{width:auto}
    </style>
</head>
<body class="blog">
<!-- Preloader -->
<div class="preloader">
    <div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //Preloader -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=475854059211013&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<!-- Header -->
@include('template.header')
<!-- //Header -->


<!-- Main -->
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap">
            <h1>DC Life Style</h1>
            {{--<p>Wild, exotic and remote; cosmopolitan and cutting-edge; untouched and tranquil, discover our incredible sailing destinations. <br />See the world. Differently.</p>--}}
        </div>
    </header>
    <!-- //Intro -->

    <!-- Content -->
    <div class="content static">
        <!-- Wrapper -->
        <div class="wrap">
            <div class="row">
                <!-- ThreeFourth -->

                        @foreach($dclifestyles as $dclifestyle)
                        <!-- Item -->
                        <article class="one-third hentry">
                            <figure><a href="{{ url('dclifestyle/'.$dclifestyle->id) }}"><img src="{{ asset($dclifestyle->crop_image) }}" alt="image" /></a></figure>
                            <div>
                                <div class="text">
                                    <h3><a href="{{ url('dclifestyle/'.$dclifestyle->id) }}">{{ $dclifestyle->name }}</a></h3>
                                    <div class="meta">
                                        <span>By {{ $dclifestyle->user->name }}</span>
                                        <span>{{ $dclifestyle->updated_at->format('d M Y') }}</span>
                                        {{--<span><a href="{{ url('dclifestyle/'.$dclifestyle->id) }}#comments">2 Comments</a></span>--}}
                                    </div>
                                    <p style="height: 65px;overflow: hidden;">{{ strip_tags($dclifestyle->content) }} </p>
                                    <a href="{{ url('dclifestyle/'.$dclifestyle->id) }}" class="more" title="Read more">Read more</a>
                                </div>
                            </div>
                        </article>
                        <!-- //Item -->
                        @endforeach

                        {{--<div class="pager2 full-width">--}}
                            {{--<a href="#">1</a>--}}
                            {{--<span>2</span>--}}
                            {{--<a href="#">3</a>--}}
                            {{--<a href="#">4</a>--}}
                            {{--<a href="#">5</a>--}}
                        {{--</div>--}}

                <!-- //ThreeFourth -->

                <!-- OneFourth -->
                {{--<aside class="one-fourth sidebar sidebar-right">--}}
                    {{--<div class="widget">--}}
                        {{--<h3>Search the blog</h3>--}}
                        {{--<div class="search">--}}
                            {{--<input type="search" id="search" placeholder="Enter your search term" />--}}
                            {{--<a href="#" class="searchsubmit" title="Search"><span class="icojam_search"></span></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="widget">--}}
                        {{--<h3>Categories</h3>--}}
                        {{--<ul class="nobullet">--}}
                            {{--<li><a href="#" title="Events">Events</a></li>--}}
                            {{--<li><a href="#" title="Luxury yachts">Luxury yachts</a></li>--}}
                            {{--<li><a href="#" title="Charter yachts">Charter yachts</a></li>--}}
                            {{--<li><a href="#" title="Sales">Sales</a></li>--}}
                            {{--<li><a href="#" title="Management">Management</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    {{--<div class="widget">--}}
                        {{--<h3>DC Life Style</h3>--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation  commodo Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed </p>--}}
                    {{--</div>--}}


                    {{--<div class="widget">--}}
                        {{--<h3>Latest posts</h3>--}}
                        {{--<ul class="latest-posts">--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<img src="uploads/img.jpg" alt="" width="90" />--}}
                                    {{--<h4>Lorem ipsum dolor Sit amet</h4>--}}
                                    {{--<span class="date">June 13, 2014</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<img src="uploads/img.jpg" alt="" width="90" />--}}
                                    {{--<h4>Lorem ipsum dolor Sit amet</h4>--}}
                                    {{--<span class="date">June 13, 2014</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<img src="uploads/img.jpg" alt="" width="90" />--}}
                                    {{--<h4>Lorem ipsum dolor Sit amet</h4>--}}
                                    {{--<span class="date">June 13, 2014</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<img src="uploads/img.jpg" alt="" width="90" />--}}
                                    {{--<h4>Lorem ipsum dolor Sit amet</h4>--}}
                                    {{--<span class="date">June 13, 2014</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<img src="uploads/img.jpg" alt="" width="90" />--}}
                                    {{--<h4>Lorem ipsum dolor Sit amet</h4>--}}
                                    {{--<span class="date">June 13, 2014</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}


                    {{--<div class="widget">--}}
                        {{--<h3>Find us on Facebook</h3>--}}
                        {{--<div class="fb-like-box" data-href="https://www.facebook.com/discover-catamaran" data-width="270" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>--}}
                    {{--</div>--}}

                    {{--<div class="widget">--}}
                        {{--<h3>Advertisment</h3>--}}
                        {{--<p><a href="#"><img src="uploads/broker.jpg" alt="" /></a></p>--}}
                    {{--</div>--}}
                {{--</aside>--}}
                <!-- //OneFourth -->
            </div>
        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
<!-- //Main -->

<!-- Testimonials -->
{{--<section class="testimonials">--}}
    {{--<div class="wrap center">--}}
        {{--<h6>WOW – This is the Best Theme I have ever seen.</h6>--}}
        {{--<p>“Excellent - you found the right boat in the right place at the right time,<br /> and managed to change the dates for our convenience - brilliant!” </p>--}}
        {{--<p>- Johnatan Davidson</p>--}}
    {{--</div>--}}
{{--</section>--}}
<!-- //Testimonials -->

<!-- Bottom Sidebar -->
{{--@include('template.bottom_sidebar')--}}
<!-- //Bottom Sidebar -->

<!-- Footer -->
@include('template.footer')
<!-- //Footer -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jetmenu.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>