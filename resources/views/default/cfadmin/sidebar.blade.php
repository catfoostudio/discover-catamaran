<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/boxed-bg.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="{{ url('cfadmin/voucher') }}"><i class="fa fa-file"></i> <span>Vouchers</span></a></li>
            <li><a href="{{ url('cfadmin/customer') }}"><i class="fa fa-user"></i> <span>Customers</span></a></li>
            <li><a href="{{ url('cfadmin/hotel') }}"><i class="fa fa-hotel"></i> <span>Hotels</span></a></li>
            <li><a href="{{ url('cfadmin/tourcompany') }}"><i class="fa fa-globe"></i> <span>Tour Companies</span></a></li>
            <li><a href="{{ url('cfadmin/product') }}"><i class="fa fa-ship"></i> <span>Products</span></a></li>
            <li><a href="{{ url('cfadmin/unit') }}"><i class="fa fa-dollar"></i> <span>Units</span></a></li>
{{--            <li><a href="{{ url('cfadmin/zone') }}"><i class="fa fa-user"></i> <span>Zones</span></a></li>--}}
            <li>
                <a href="#">
                    <i class="fa fa-calendar"></i> <span>Premium Day Trip</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu menu-open" style="display: block;">
                    {{--<li><a href="{{ url('cfadmin/premiumdaytrip/stock') }}"><i class="fa fa-circle-o"></i> Stock</a></li>--}}
                    <li><a href="{{ url('cfadmin/premiumdaytrip/stock') }}"><i class="fa fa-circle-o"></i> Stock</a></li>
                </ul>
            </li>
            <li><a href="{{ url('cfadmin/gallery') }}"><i class="fa fa-image"></i> <span>Galleries</span></a></li>
            <li><a href="{{ url('cfadmin/dclifestyle') }}"><i class="fa fa-blog"></i> <span>DC Life Style</span></a></li>
            <li><a href="{{ url('cfadmin/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
            <li><a href="{{ url('cfadmin/invoice') }}"><i class="fa fa-dollar"></i><span>Invoices</span></a></li>
            <li><a href="{{ url('cfadmin/payment') }}"><i class="fa fa-dollar"></i><span>Payment</span></a></li>
            <li><a href="{{ url('cfadmin/code') }}"><i class="glyphicon glyphicon-font"></i> <span>Promo code</span></a></li>
{{--            <li><a href="{{ url('cfadmin/discounttype') }}"><i class="glyphicon glyphicon-font"></i> <span>Discount Types</span></a></li>--}}
            <li><a href="{{ url('cfadmin/voucherprefix') }}"><i class="glyphicon glyphicon-font"></i> <span>Voucher Prefixes</span></a></li>
            <li><a href="{{ url('cfadmin/report') }}"><i class="fa fa-area-chart"></i><span>Report</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>