<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'unit.cfadmin.index' : 'home.index';

        $units = Unit::all();
        return view($view, [
            'units' => $units,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'unit.cfadmin.create' : 'home.index';


        return view($view, [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //// //
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);


        $input = $request->input();

        $units = new Unit();
        $units->name = $input['name'];

        if (!empty($input['remark']))
            $units->remark = $input['remark'];

        $units->status = 1;
        $units->save();
        $request->session()->flash('alert-success', 'Unit was successful updated!');
        return redirect("cfadmin/unit");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($unit)
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'unit.cfadmin.detail' : 'home.index';

        //$voucherprefix = VoucherPrefix::all();

//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'unit' => $unit,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $unit)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);

        $input = $request->input();

        $unit->name = $input['name'];
        //$voucherprefix->logo = $input['logo'];

        if (!empty($input['remark']))
            $unit->remark = $input['remark'];

        $unit->status = 1;
        $unit->save();
        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/unit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getunithtml(Request $request){

        $input = $request->input();
        $html = '';

        $product = Product::find($input['id']);
        if($product->per_head == 0) {
            $units = Unit::whereHas('products', function ($query) use ($input) {
                $query->where('products.id', $input['id']);
            })->get();

            foreach ($units as $unit) {
                $html .= '<div class="row"><div class="col-md-12" ><label>' . $unit->name . '</label><span class="text-red"><b>*</b></span><input class="form-control" type="hidden" name="unit_id[]" value="' . $unit->id . '"><input class="form-control" type="text" name="unit_amount[]"></div></div>';
            }
        }
        else{

        }

        return $html;
    }
}
