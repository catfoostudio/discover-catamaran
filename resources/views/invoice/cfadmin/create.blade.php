@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Invoice
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="{{ url('cfadmin/invoice') }}">Invoices</a></li>
                <li class="active">Add Invoice</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        {!! Form::open(['method' => 'POST' , 'action' => ['InvoiceController@store']]) !!}
                        <div class="box-header with-border">
                            <h3 class="box-title">Invoice Info.</h3>
                            {{--<input value="THB" type="radio" style="margin-left: 10px;" name="type_value" checked> <label>THB</label> <input value="PERCENT" type="radio" name="type_value" style="margin-left: 10px;"> <label>Percent %</label>--}}
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Name</label>
                                        <input type="text" name="name" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <div >
                                            <label for="inputEmail3">Price</label>
                                            <input type="text" class="form-control" name="price" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Payment Due</label>
                                        <input type="text" name="payment_due" id="payment_due" class="form-control">
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Customer Detail:</h3>
                            <label>
                                <input type="radio" name="customer_type" class="minimal" value="1" {{ (old('customer_type' , 2) == 1) ? 'checked' : ''
                                }} >
                                New Customer
                            </label>
                            <label>
                                <input type="radio" name="customer_type" class="minimal" value="2" {{ (old('customer_type',2) == 2) ? 'checked' : ''
                                }}>
                                Regular Customer
                            </label>
                        </div>
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div id="new_cust" {{ old('customer_type', 2) == 2 ? 'style=display:none' : '' }}>
                                                    <label for="inputEmail3" >Name</label> <span class="text-red"><b>*</b></span>
                                                    <input type="text" class="form-control" name="cust_name"
                                                           value="{{ old('cust_name') }}">
                                                </div>
                                                <div id="regular_cust" {{ old('customer_type',2) == 1 ? 'style=display:none' : '' }}>
                                                    <label for="inputEmail3">Regular customer</label>
                                                    <select class="form-control select2" style="width: 100%;"
                                                            name="customer">
                                                        @foreach($customers as $customer)
                                                            <option value="{{ $customer->id }}">{{ $customer->name.' : '.$customer->tel }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Tel.</label>
                                                <input type="text" class="form-control" name="cust_tel"
                                                       value="{{ old('cust_tel') }}" {{ old('customer_type',2) == 2 ? 'disabled' : '' }}>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Email</label>
                                                <input type="text" class="form-control" name="cust_email"
                                                       value="{{ old('cust_email') }}" {{ old('customer_type',2) == 2 ? 'disabled' : '' }}>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="inputEmail3">Address</label>
                                                <textarea class="form-control" name="cust_address" {{ old('customer_type',2) == 2 ? 'disabled' : '' }}></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info">Add</button>
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $(function () {
            $('#payment_due').datepicker({
                format: 'dd/mm/yyyy',
                autoclose:true,
            });

            $('.select2').select2();

            $('input[name="customer_type"]').on('ifChecked', function (event) {
                if ($(this).val() == 1) {
                    $('#new_cust').show();
                    $('#regular_cust').hide();
                    $('input[name="cust_tel"]').prop('disabled', false);
                    $('input[name="cust_email"]').prop('disabled', false);
                    $('textarea[name="cust_address"]').prop('disabled', false);
                }
                else {
                    $('#new_cust').hide();
                    $('#regular_cust').show();
                    $('input[name="cust_name"]').val('');
                    $('input[name="cust_tel"]').val('');
                    $('input[name="cust_email"]').val('');
                    $('textarea[name="cust_address"]').val('');
                    $('input[name="cust_tel"]').prop('disabled', true);
                    $('input[name="cust_email"]').prop('disabled', true);
                    $('textarea[name="cust_address"]').prop('disabled', true);
                }
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    </script>
@endsection
