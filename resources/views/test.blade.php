
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<meta name="keywords" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    {{--<meta name="description" content="Sailor - Yacht Charter Booking HTML Template" />--}}
    <title>Discover Catamaran - Premium day trip: </title>


    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700|Raleway:400,500,600,700&amp;subset=latin,greek,cyrillic,vietnamese' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- box-navy: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.js') }}"></script>


    <style>
        .tab-content table td:first-child{width:auto}
    </style>

    <style>
        .fc-title {
            color: white
        }

        .fc-title:hover {
            color: #ffe978
        }

        .fc-scroller {
            height: auto !important;
        }

        .fc-head .fc-widget-header {
            margin-right: 0 !important;
        }

        .fc-scroller {
            overflow: visible !important;
        }
    </style>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>


    <!-- LOADING FONTS AND ICONS -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"
          property="stylesheet" type="text/css" media="all"/>

    <link rel="stylesheet" type="text/css"
          href="/plugins/revolution/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" type="text/css"
          href="/plugins/revolution/revolution/fonts/font-awesome/css/font-awesome.min.css">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/layers.css">

    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/navigation.css">

{{--<link rel="stylesheet" type="text/css" href="/plugins/revolution/assets/css/noneed.css">--}}
<!-- REVOLUTION JS FILES -->
    <script type="text/javascript"
            src="/plugins/revolution/revolution/js/source/jquery.themepunch.tools.min.js"></script>
    {{--<script type="text/javascript" src="/plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>--}}
    <script type="text/javascript"
            src="/plugins/revolution/revolution/js/source/jquery.themepunch.revolution.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="/plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js"></script>

    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

    <script src="{{ asset('plugins/fullcalendar/fullcalendar.js') }}"></script>






    {{--<script src='/assets/demo-to-codepen.js'></script>--}}


    <style>

        html, body {
            margin: 0;
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 40px auto;
        }

    </style>


    <script>

        // $(function() {
        //
        //     $("#rev_slider_8_1").show().revolution({
        //         sliderType: "standard",
        //         jsFileLocation: "/plugins/revolution/revolution/js/",
        //         sliderLayout: "fullwidth",
        //         dottedOverlay: "none",
        //         delay: 3000,
        //         navigation: {
        //             keyboardNavigation: "on",
        //             keyboard_direction: "horizontal",
        //             mouseScrollNavigation: "off",
        //             onHoverStop: "off",
        //             touch: {
        //                 touchenabled: "on",
        //                 swipe_threshold: 75,
        //                 swipe_min_touches: 1,
        //                 swipe_direction: "horizontal",
        //                 drag_block_vertical: false
        //             },
        //             thumbnails: {
        //                 style: 'hesperiden',
        //                 enable: true,
        //                 width: 100,
        //                 height: 50,
        //                 min_width: 100,
        //                 wrapper_padding: 5,
        //                 wrapper_color: 'transparent',
        //                 wrapper_opacity: '1',
        //                 visibleAmount: 5,
        //                 hide_onmobile: false,
        //                 hide_onleave: false,
        //                 direction: 'vertical',
        //                 span: false,
        //                 position: 'inner',
        //                 space: 10,
        //                 h_align: 'right',
        //                 v_align: 'bottom',
        //                 h_offset: 0,
        //                 v_offset: 20
        //             }
        //         },
        //         gridwidth: 800,
        //         gridheight: 600,
        //         lazyType: "single",
        //         shadow: 0,
        //         spinner: "off",
        //
        //         shuffle: "off",
        //         autoHeight: "off",
        //         disableProgressBar: "on",
        //         hideThumbsOnMobile: "off",
        //         hideSliderAtLimit: 0,
        //         hideCaptionAtLimit: 0,
        //         hideAllCaptionAtLilmit: 0,
        //         debugMode: false,
        //         fallbacks: {
        //             simplifyAll: "off",
        //             nextSlideOnWindowFocus: "off",
        //             disableFocusListener: false,
        //         }
        //     });
        //
        //     $('#calendar').fullCalendar({
        //         selectable: true,
        //         header: {
        //             left: 'prev,next today',
        //             center: 'title',
        //             right: 'month,agendaWeek,agendaDay'
        //         },
        //         dayClick: function(date) {
        //             alert('clicked ' + date.format());
        //         },
        //         select: function(startDate, endDate) {
        //             alert('selected ' + startDate.format() + ' to ' + endDate.format());
        //         }
        //     });
        //
        // });

    </script>

</head>
<body>
<main class="main" role="main">
    <!-- Intro -->
    <header class="intro">
        <div class="wrap" style="padding-bottom:20px;">
            <h1>test</h1>
            test
        </div>
    </header>

    <div id="rev_slider_1057_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="4kvideos" data-source="gallery" style="background-color:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_1057_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
            <ul>    <!-- SLIDE  -->
                <li data-index="rs-2968" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"
                         id="slide-2968-layer-1"
                         data-x="0"
                         data-y="0"
                         data-whitespace="nowrap"

                         data-type="video"
                         data-responsive_offset="on"

                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
                         data-ytid="6pxRHBw-k8M" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;&start=08" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"           data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-autoplay="off"
                         data-nextslideatend="true"
                         data-videoposter="http://img.youtube.com/vi/6pxRHBw-k8M/maxresdefault.jpg"
                         data-noposteronmobile="on"
                         data-volume="100"           data-allowfullscreenvideo="true"
                         data-videostartat="0:08"
                         style="z-index: 5;text-transform:left;border-width:0px;"> </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2968-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2968-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2968-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 7;text-transform:left;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption VideoControls-Pause rev-btn "
                         id="slide-2968-layer-11"
                         data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2968-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-9","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 8; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption VideoControls-Mute rev-btn "
                         id="slide-2968-layer-12"
                         data-x="['right','right','right','right']" data-hoffset="['72','72','72','72']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"toggle_mute_video","layer":"slide-2968-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 9; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><div class="rs-untoggled-content"><i class="fa-icon-volume-up"></i> </div><div class="rs-toggled-content"><i class="fa-icon-volume-off"></i></div></div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption VideoControls-Play rev-btn "
                         id="slide-2968-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','50','30','30']"
                         data-lineheight="['120','120','80','80']"
                         data-width="['120','120','80','80']"
                         data-height="['120','120','80','80']"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2968-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2968-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2968-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2968-layer-12","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[7,7,7,7]"
                         data-lasttriggerstate="reset"
                         style="z-index: 10; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption VideoPlayer-Title   tp-resizeme"
                         id="slide-2968-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['150','130','100','80']"
                         data-fontsize="['40','40','30','20']"
                         data-lineheight="['40','40','30','20']"
                         data-width="['none','none','none','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 11; white-space: nowrap;text-transform:left;">COSTA RICA IN 4K 60FPS </div>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"
                         id="slide-2968-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['200','180','140','132']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 12; white-space: nowrap;text-transform:left;font-style:italic;">Jacob + Katie Schwarz </div>

                    <!-- LAYER NR. 9 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.youtube.com/user/jacobschwarz/featured" target="_blank"           id="slide-2968-layer-7"
                       data-x="['center','center','center','center']" data-hoffset="['-70','-70','-70','-70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2000,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 13; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-youtube"></i> </a>

                    <!-- LAYER NR. 10 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://twitter.com/jakeschwarz" target="_blank"          id="slide-2968-layer-8"
                       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2150,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 14; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-twitter"></i> </a>

                    <!-- LAYER NR. 11 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.instagram.com/jacobschwarz/" target="_blank"          id="slide-2968-layer-9"
                       data-x="['center','center','center','center']" data-hoffset="['70','70','70','70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2300,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 15; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-camera-retro"></i> </a>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-2969" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube Two" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"
                         id="slide-2969-layer-1"
                         data-x="0"
                         data-y="0"
                         data-whitespace="nowrap"

                         data-type="video"
                         data-responsive_offset="on"

                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
                         data-ytid="gU10ALRQ0ww" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"            data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-autoplay="off"
                         data-nextslideatend="true"
                         data-videoposter="http://img.youtube.com/vi/gU10ALRQ0ww/maxresdefault.jpg"
                         data-noposteronmobile="on"
                         data-volume="100"           data-allowfullscreenvideo="true"

                         style="z-index: 16;text-transform:left;border-width:0px;"> </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2969-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2969-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>

                    <!-- LAYER NR. 14 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2969-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 18;text-transform:left;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                    <!-- LAYER NR. 15 -->
                    <div class="tp-caption VideoControls-Pause rev-btn "
                         id="slide-2969-layer-11"
                         data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2969-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-9","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 19; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>

                    <!-- LAYER NR. 16 -->
                    <div class="tp-caption VideoControls-Mute rev-btn "
                         id="slide-2969-layer-12"
                         data-x="['right','right','right','right']" data-hoffset="['72','72','72','72']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"toggle_mute_video","layer":"slide-2969-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 20; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><div class="rs-untoggled-content"><i class="fa-icon-volume-up"></i> </div><div class="rs-toggled-content"><i class="fa-icon-volume-off"></i></div></div>

                    <!-- LAYER NR. 17 -->
                    <div class="tp-caption VideoControls-Play rev-btn "
                         id="slide-2969-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','50','30','30']"
                         data-lineheight="['120','120','80','80']"
                         data-width="['120','120','80','80']"
                         data-height="['120','120','80','80']"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2969-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2969-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2969-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2969-layer-12","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[7,7,7,7]"
                         data-lasttriggerstate="reset"
                         style="z-index: 21; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>

                    <!-- LAYER NR. 18 -->
                    <div class="tp-caption VideoPlayer-Title   tp-resizeme"
                         id="slide-2969-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['150','130','100','80']"
                         data-fontsize="['40','40','30','20']"
                         data-lineheight="['40','40','30','20']"
                         data-width="['none','none','none','360']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 22; white-space: nowrap;text-transform:left;">TIMELAPSE IN 4K </div>

                    <!-- LAYER NR. 19 -->
                    <div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"
                         id="slide-2969-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['200','180','140','132']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 23; white-space: nowrap;text-transform:left;font-style:italic;">Jacob + Katie Schwarz </div>

                    <!-- LAYER NR. 20 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.youtube.com/user/jacobschwarz/featured" target="_blank"           id="slide-2969-layer-7"
                       data-x="['center','center','center','center']" data-hoffset="['-70','-70','-70','-70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2000,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 24; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-youtube"></i> </a>

                    <!-- LAYER NR. 21 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://twitter.com/jakeschwarz" target="_blank"          id="slide-2969-layer-8"
                       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2150,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 25; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-twitter"></i> </a>

                    <!-- LAYER NR. 22 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.instagram.com/jacobschwarz/" target="_blank"          id="slide-2969-layer-9"
                       data-x="['center','center','center','center']" data-hoffset="['70','70','70','70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2300,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 26; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-camera-retro"></i> </a>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-2970" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power1.easeInOut" data-easeout="Power1.easeInOut" data-masterspeed="500"  data-thumb="http://revolution.themepunch.com/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Youtube Three" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/plugins/revolution/assets/images/transparent.png"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 23 -->
                    <div class="tp-caption   tp-resizeme fullscreenvideo rs-parallaxlevel-5 tp-videolayer"
                         id="slide-2970-layer-1"
                         data-x="0"
                         data-y="0"
                         data-whitespace="nowrap"

                         data-type="video"
                         data-responsive_offset="on"

                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"nothing"}]'
                         data-ytid="nHXVc_cQqyI" data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;volume=100&hd=1&wmode=opaque&showinfo=0&rel=0;" data-videorate="1" data-videowidth="100%" data-videoheight="100%" data-videocontrols="none" data-videoloop="none" data-forceCover="1" data-aspectratio="16:9"            data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-autoplay="off"
                         data-nextslideatend="true"
                         data-videoposter="http://img.youtube.com/vi/nHXVc_cQqyI/maxresdefault.jpg"
                         data-noposteronmobile="on"
                         data-volume="100"           data-allowfullscreenvideo="true"

                         style="z-index: 27;text-transform:left;border-width:0px;"> </div>

                    <!-- LAYER NR. 24 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2970-layer-4"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"stopvideo","layer":"slide-2970-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 28;text-transform:left;background-color:rgba(0, 0, 0, 0);border-color:rgba(0, 0, 0, 0);border-width:0px;cursor:pointer;"> </div>

                    <!-- LAYER NR. 25 -->
                    <div class="tp-caption tp-shape tp-shapewrapper "
                         id="slide-2970-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="shape"
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":1000,"to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 29;text-transform:left;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"> </div>

                    <!-- LAYER NR. 26 -->
                    <div class="tp-caption VideoControls-Pause rev-btn "
                         id="slide-2970-layer-11"
                         data-x="['right','right','right','right']" data-hoffset="['20','20','20','20']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stopvideo","layer":"slide-2970-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-12","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-9","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 30; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-pause"></i> </div>

                    <!-- LAYER NR. 27 -->
                    <div class="tp-caption VideoControls-Mute rev-btn "
                         id="slide-2970-layer-12"
                         data-x="['right','right','right','right']" data-hoffset="['72','72','72','72']"
                         data-y="['bottom','bottom','bottom','bottom']" data-voffset="['20','20','20','20']"
                         data-width="50"
                         data-height="50"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"toggle_mute_video","layer":"slide-2970-layer-1","delay":""}]'
                         data-basealign="slide"
                         data-responsive_offset="off"
                         data-responsive="off"
                         data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;sX:0.85;sY:0.85;","delay":"bytrigger","ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":500,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 31; min-width: 50px; max-width: 50px; max-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><div class="rs-untoggled-content"><i class="fa-icon-volume-up"></i> </div><div class="rs-toggled-content"><i class="fa-icon-volume-off"></i></div></div>

                    <!-- LAYER NR. 28 -->
                    <div class="tp-caption VideoControls-Play rev-btn "
                         id="slide-2970-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                         data-fontsize="['50','50','30','30']"
                         data-lineheight="['120','120','80','80']"
                         data-width="['120','120','80','80']"
                         data-height="['120','120','80','80']"
                         data-whitespace="nowrap"
                         data-visibility="['on','on','off','off']"

                         data-type="button"
                         data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2970-layer-3","delay":""},{"event":"click","action":"playvideo","layer":"slide-2970-layer-1","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-2","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-5","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-6","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2970-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-11","delay":""},{"event":"click","action":"startlayer","layer":"slide-2970-layer-12","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"from":"sX:0.5;sY:0.5;opacity:0;","speed":1000,"to":"o:1;sX:0.85;sY:0.85;","delay":1000,"ease":"Power1.easeInOut"},{"delay":"bytrigger","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bs:solid;"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[7,7,7,7]"
                         data-lasttriggerstate="reset"
                         style="z-index: 32; min-width: 120px; max-width: 120px; max-width: 120px; max-width: 120px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><i class="fa-icon-play"></i> </div>

                    <!-- LAYER NR. 29 -->
                    <div class="tp-caption VideoPlayer-Title   tp-resizeme"
                         id="slide-2970-layer-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['112','91','70','80']"
                         data-fontsize="['40','40','30','20']"
                         data-lineheight="['40','40','30','20']"
                         data-width="['800','800','640','360']"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 33; min-width: 800px; max-width: 800px; white-space: normal;text-transform:left;">AMERICAN SOUTHWEST IN 4K 60FPS </div>

                    <!-- LAYER NR. 30 -->
                    <div class="tp-caption VideoPlayer-SubTitle   tp-resizeme"
                         id="slide-2970-layer-6"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['200','180','140','151']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1500,"ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":700,"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-lasttriggerstate="reset"
                         style="z-index: 34; white-space: nowrap;text-transform:left;font-style:italic;">Jacob + Katie Schwarz </div>

                    <!-- LAYER NR. 31 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.youtube.com/user/jacobschwarz/featured" target="_blank"           id="slide-2970-layer-7"
                       data-x="['center','center','center','center']" data-hoffset="['-70','-70','-70','-70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2000,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 35; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-youtube"></i> </a>

                    <!-- LAYER NR. 32 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://twitter.com/jakeschwarz" target="_blank"          id="slide-2970-layer-8"
                       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2150,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 36; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-twitter"></i> </a>

                    <!-- LAYER NR. 33 -->
                    <a class="tp-caption VideoPlayer-Social   tp-resizeme"
                       href="https://www.instagram.com/jacobschwarz/" target="_blank"          id="slide-2970-layer-9"
                       data-x="['center','center','center','center']" data-hoffset="['70','70','70','70']"
                       data-y="['top','top','top','top']" data-voffset="['450','400','320','320']"
                       data-width="none"
                       data-height="none"
                       data-whitespace="nowrap"

                       data-type="text"
                       data-actions=''
                       data-responsive_offset="on"

                       data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0;sY:0;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;sX:0.5;sY:0.5;","delay":2300,"ease":"Elastic.easeOut"},{"delay":"bytrigger","speed":700,"to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;sX:1.01;sY:1.01;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);br:0px 0px 0px 0px;"}]'
                       data-textAlign="['center','center','center','center']"
                       data-paddingtop="[0,0,0,0]"
                       data-paddingright="[0,0,0,0]"
                       data-paddingbottom="[0,0,0,0]"
                       data-paddingleft="[0,0,0,0]"
                       data-lasttriggerstate="reset"
                       style="z-index: 37; white-space: nowrap;text-transform:left;cursor:pointer;"><i class="fa-icon-camera-retro"></i> </a>
                </li>
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
    </div><!-- END REVOLUTION SLIDER -->
    <script type="text/javascript">
        var tpj=jQuery;

        var revapi1057;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_1057_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_1057_1");
            }else{
                revapi1057 = tpj("#rev_slider_1057_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"revolution/js/",
                    sliderLayout:"fullscreen",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        mouseScrollReverse:"default",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 50,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"uranus",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:778,
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            }
                        }
                    },
                    responsiveLevels:[1240,1024,778,480],
                    visibilityLevels:[1240,1024,778,480],
                    gridwidth:[1200,1024,778,480],
                    gridheight:[675,576,480,480],
                    lazyType:"none",
                    parallax: {
                        type:"scroll",
                        origo:"enterpoint",
                        speed:400,
                        levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
                        type:"scroll",
                    },
                    shadow:0,
                    spinner:"off",
                    stopLoop:"on",
                    stopAfterLoops:0,
                    stopAtSlide:1,
                    shuffle:"off",
                    autoHeight:"off",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "60px",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        }); /*ready*/
    </script>

    <!-- //Intro -->
    <!-- SLIDER EXAMPLE -->
    {{--<section class="example">--}}
        {{--<article class="content">--}}
            {{--<div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"--}}
                 {{--data-alias="facebook-feed8"--}}
                 {{--style="margin:0px auto;background-color:#dddddd;padding:0px;margin-top:0px;margin-bottom:0px;">--}}
                {{--<!-- START REVOLUTION SLIDER 5.0.7 auto mode -->--}}
                {{--<div id="rev_slider_8_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">--}}
                    {{--<ul>--}}
                        {{--<!-- SLIDE  -->--}}

                        {{--<li data-index="rs-28" data-transition="scaledownfromleft" data-slotamount="default"--}}
                            {{--data-easein="default" data-easeout="default" data-masterspeed="1500"--}}
                            {{--data-thumb="http://discover-catamaran.local/crop_images/1539158206_1_100x50.jpg" data-rotate="0"--}}
                            {{--data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"--}}
                            {{--data-saveperformance="off" data-title="Umbrella" data-param1="September 7, 2015"--}}
                            {{--data-param2="Alfon Much, The Precious Stones" data-description="">--}}
                            {{--<!-- MAIN IMAGE -->--}}
                            {{--<img src="/plugins/revolution/assets/images/dummy.png" alt="" width="1920" height="1080"--}}
                                 {{--data-lazyload="http://discover-catamaran.local/crop_images/1539158206_1.jpg"--}}
                                 {{--data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"--}}
                                 {{--class="rev-slidebg" data-no-retina>--}}
                            {{--<!-- LAYERS -->--}}
                        {{--</li>--}}

                    {{--</ul>--}}
                    {{--<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- END REVOLUTION SLIDER -->--}}


        {{--</article>--}}
    {{--</section>--}}
    <!-- Gallery -->
{{--<div class="gallery" id="gallery">--}}
{{--@foreach($product->images()->orderBy('order')->get() as $image)--}}
{{--<figure class="one-fourth" data-src="{{ asset($image->path)}}" style="overflow: hidden;">--}}
{{--<img src="{{ asset($image->path)}}" alt=""/>--}}
{{--<figcaption>--}}
{{--<span class="icojam_zoom_in"></span>--}}
{{--<div>--}}
{{--<h5>BOL GOLDEN CAPE</h5>--}}
{{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</p>--}}
{{--</div>--}}
{{--</figcaption>--}}
{{--</figure>--}}
{{--@endforeach--}}

{{--<!-- //Item -->--}}
{{--</div>--}}
<!-- //Gallery -->

    <!-- Content -->
    <div class="content">
        <!-- Tab navigation -->
        <nav class="tabs six" role="navigation" id="tab-navigation">
            <ul class="wrap">
                <li><a href="#tab1" title="Description">
                        <span class="icojam_info_3"></span> Description
                    </a></li>
                {{--<li><a href="#tab2" title="Specifications">--}}
                {{--<span class="icojam_document"></span> Specifications--}}
                {{--</a></li>--}}
                {{--<li><a href="#tab3" title="Equipment">--}}
                {{--<span class="icojam_anchor"></span> Equipment--}}
                {{--</a></li>--}}
                <li><a href="#tab4" title="Availability" id="calendareventlink">
                        <span class="icojam_calendar_1"></span> Availability
                    </a></li>
                <li><a href="#tab5" title="Contact Broker">
                        <span class="icojam_target"></span> Contact directly
                    </a></li>
                <li><a href="#tab6" title="Get Brochure">
                        <span class="icojam_inbox_receive"></span> Get Brochure
                    </a></li>
            </ul>
        </nav>
        <!-- //Tab navigation -->

        <!-- Wrapper -->
        <div class="wrap">
            <!-- Tab Content-->
            <article class="tab-content" id="tab1">
                <div class="row">

                    <!-- OneHalf -->
                    <div class="one-half">
                        test
                    </div>
                    <!-- //OneHalf -->

                    <!-- OneHalf -->

                    <div class="one-half">
                        <div id="calendar"></div>
                        {{--<a href="#bookingSteps" title="Proceed with booking" class="button gold large full">Proceed--}}
                        {{--with booking</a>--}}
                        {{--<img src="{{ asset('uploads/content1.png') }}" alt="Elan 1923 Impression" />--}}
                    </div>

                    <!-- Booking Steps -->
                    <div class="booking bookingSteps one-half">
                        <!-- TwoThird -->
                        <div class="full-width">
                            <dl class="accordion">
                                <!-- Item -->
                                <dt>Client information</dt>
                                <dd>
                                    {!! Form::open(['method' => 'POST' , 'action' => ['EnquireController@store'] , 'class' => 'row']) !!}
                                    <p>Please enter your details to complete an order. All fields are required. </p>

                                    <div class="row">
                                        <div class="full-width">

                                            <fieldset>
                                                <h3>Information</h3>
                                                <div class="one-half">
                                                    <label for="Depart Date">Depart Date <span style="color: red;">(required)</span></label>
                                                    <input type="text" id="departdate" autocomplete="off"/>
                                                </div>

                                                {{--<div class="one-half">--}}
                                                {{--<label for="Arrival Date">Arrival Date <span--}}
                                                {{--style="color: red;">(required)</span></label>--}}
                                                {{--<input type="text" id="arrivaldate"/>--}}
                                                {{--</div>--}}

                                                <div class="one-half">
                                                    <label for="phone">Name <span style="color: red;">(required)</span></label>
                                                    <input type="number" id="phone"/>
                                                </div>

                                                <div class="one-half">
                                                    <label for="email">Tel. <span style="color: red;">(required)</span></label>
                                                    <input type="email" id="email"/>
                                                </div>

                                                <div class="one-half">
                                                    <label for="phone">E-mail <span
                                                                style="color: red;">(required)</span></label>
                                                    <input type="number" id="phone"/>
                                                </div>

                                                <div class="one-half">
                                                    <label for="email">Line</label>
                                                    <input type="email" id="email"/>
                                                </div>
                                                <div class="one-half">
                                                    <label for="email">Wechat ID</label>
                                                    <input type="email" id="email"/>
                                                </div>

                                                <div class="full-width">
                                                    <label>ENQUIRE</label>
                                                    <textarea></textarea>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <h3>Traveler</h3>
                                                <div class="one-third">
                                                    <label for="name">Adult <span style="color: red;">(required)</span></label>
                                                    <input type="text" id="name"/>
                                                </div>

                                                <div class="one-third">
                                                    <label for="surname">Child 4-11 yrs</label>
                                                    <input type="text" id="surname"/>
                                                </div>

                                                <div class="one-third">
                                                    <label for="phone">Infant 1-3 yrs</label>
                                                    <input type="number" id="phone"/>
                                                </div>

                                            </fieldset>
                                            <fieldset>
                                                <h3>Transfer</h3>
                                                <div class="one-third">
                                                    <label for="transfer">Transfer <span
                                                                style="color: red;">(required)</span></label>
                                                    <select name="transfer" id="transfer">
                                                        <option value="wt">with transfer</option>
                                                        <option value="wot">without transfer</option>
                                                    </select>
                                                </div>
                                                <div class="one-third transfer-info">
                                                    <label for="zone">Zone <span
                                                                style="color: red;">(required)</span></label>
                                                    <input type="text" name="zone"/>
                                                </div>
                                                <div class="one-third transfer-info">
                                                    <label for="hotel">Hotel <span style="color: red;">(required)</span></label>
                                                    <input type="text" name="hotel"/>
                                                </div>

                                            </fieldset>

                                        </div>
                                    </div>

                                    <h5>Promo code and payment type</h5>
                                    <p>Please enter your promotion code, if you have received it.</p>

                                    <div class="row">
                                        <div class="one-half">
                                            <label for="discount">Discount code</label>
                                            <input type="text" id="discount"/>
                                        </div>

                                        <div class="one-half">
                                            <label for="payment">Payment type</label>
                                            <select id="payment">
                                                <option selected>Payment type</option>
                                                <option>Credit card</option>
                                                <option>Paypal</option>
                                                <option>Direct bank transfer</option>
                                            </select>
                                        </div>

                                        <div class="full-width checkbox">
                                            <input type="checkbox" id="terms"/>
                                            <label for="terms">By clicking this box you accept general terms and
                                                conditions.</label>

                                        </div>
                                        <div class="full-width" id="recaptcha">
                                            @if ($errors->has('g-recaptcha-response'))
                                                <span class="help-block"><strong>{{ $errors->first('g-recaptcha-response') }}</strong></span>
                                            @endif
                                            {!! NoCaptcha::display() !!}
                                        </div>
                                        <div class="full-width">
                                            <button title="SUBMIT" type="submit" class="button gold large">SUBMIT
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </dd>
                                <!-- //Item -->


                            </dl>
                            <div class="thank-you-note">
                                <div class="box-white">
                                    <h2>Thank you</h2>
                                    <p>We will get back you with regards your reservation within 24 hours</p>
                                </div>
                            </div>
                        </div>
                        <!-- TwoThird -->

                    </div>
                    <!-- //Booking Steps -->
                    <!-- //OneHalf -->
                </div>
            </article>
            <!-- //Tab Content-->

        </div>
        <!-- //Wrapper -->
    </div>
    <!-- //Content-->
</main>
</body>

</html>
