<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscounttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type_value'); //percent or baht
            $table->tinyInteger('value_flag')->default(0);
            $table->tinyInteger('min_flag')->default(0);
            $table->tinyInteger('max_flag')->default(0);
            $table->tinyInteger('daytrip_flag')->default(0);
            $table->tinyInteger('private_flag')->default(0);
            $table->tinyInteger('optional_flag')->default(0);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounttypes');
    }
}
