<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Tourcompany;
use Illuminate\Http\Request;

class PrivateCharterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $view = ($this->isAdminRequest()) ? 'privatecharter.cfadmin.index' : 'privatecharter.index';
//
//        return view($view, [
//
//        ]);

        //
        $view = ($this->isAdminRequest()) ? 'privatecharter.cfadmin.stock' : 'privatecharter.index';
        $tourcompanywithproduct = Tourcompany::with('products')->get();
        $products = Product::where('premium_day_trip_flag', 2)->where('status',1)->get();
        return view($view, [
            'tourcompanywithproduct' => $tourcompanywithproduct,
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = Product::find($id);

        return view('privatecharter.detail', [
            'product' => $product,
            'tourcompany' => $product->tourcompanies()->first(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
