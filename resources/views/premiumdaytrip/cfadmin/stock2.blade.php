@extends('cfadmin')
@section('content')
    <style type="text/css">
        .fc-resource-area {
            /*font-weight: bold;*/
        }

        .fc-highlight {
            background: #538CB8;
            opacity: 0.5;
        }
        .fc-highlight-container{
            z-index: 2;
        }


    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Premium Day Trip
            </h1>
            <small>Stock</small>
            <ol class="breadcrumb">
                <li><a href="{{ url('cfadmin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('cfadmin/premiumdaytrip/stock') }}">Premium Day Trip</a></li>
                <li class="active"><a href="{{ url('cfadmin/premiumdaytrip/stock') }}">Stock</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="input-group date" style="width: 200px;float: left;margin-right: 10px;">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="travel_date" autocomplete="off"
                               class="form-control pull-right datepicker"
                               value="{{ old('travel_date') }}" id="gotodate">

                    </div>
                    <button type="button" class="btn btn-block btn-primary" id="btn-goto-date"  style="width: 80px;">Go</button>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">

                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body no-padding">
                            <div id="calendar"></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Booking Detail</h3>

                            {{--<div class="box-tools">--}}
                                {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                    {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                                    {{--<div class="input-group-btn">--}}
                                        {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover" >
                               <tbody id="booking_table"></tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
            </div>
        </section>
    </div>
    {{--@forelse($products as $p)--}}
    {{--@foreach($p->tourcompanies as $t)--}}
    {{--<!-- Content Wrapper. Contains page content -->--}}
    {{--<div class="content-wrapper">--}}
    {{--<section class="content">--}}
    {{--<div class="row">--}}

    {{--<div class="col-md-12">--}}
    {{--<div id="calendar"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    {{--<!-- Content Header (Page header) -->--}}
    {{--<section class="content-header" data-product="{{ $p->id }}">--}}
    {{--<h1>--}}
    {{--{{ $p->name }}--}}
    {{--</h1>--}}
    {{--<small>{{ $t->name }}</small>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li class="active">Calendar</li>--}}
    {{--</ol>--}}
    {{--</section>--}}
    {{--<!-- Main content -->--}}
    {{--<section class="content">--}}
    {{--<div class="row">--}}

    {{--<div class="col-md-12">--}}


    {{--<div class="box box-primary">--}}
    {{--<div class="box-body no-padding">--}}
    {{--<!-- THE CALENDAR -->--}}
    {{--<div id="calendar_{{ $p->id }}_{{ $t->id }}"></div>--}}
    {{--</div>--}}
    {{--<div class="box-body form">--}}
    {{--<h4 id="title_{{ $p->id }}"></h4>--}}
    {{--<div id="detail_{{ $p->id }}_{{ $t->id }}" style="display: none;">--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<h4>Max Stock</h4>--}}
    {{--{{ Form::number('maxseat_'.$p->id.'_'.$t->id, 0, ['class'=> 'form-control','min' => 0,  'onkeypress' => "return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"]) }}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<h4>Remark</h4>--}}
    {{--{{ Form::textarea('remark_'.$p->id.'_'.$t->id, '', ['class'=> 'form-control', 'size' => '30x3']) }}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<input type="button" value="Save"--}}
    {{--class="btn btn-primary btn_update_stock_{{ $p->id }}_{{ $t->id }}">--}}
    {{--<input type="button" value="Cancel" class="btn btn-default pull-right">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<h4>Booking on <span--}}
    {{--id="title_booking_date_{{ $p->id }}_{{ $t->id }}"></span>--}}
    {{--</h4>--}}
    {{--<table class="table table-bordered"--}}
    {{--id="booking_table_{{ $p->id }}_{{ $t->id }}">--}}

    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div id="detail_initial_{{ $p->id }}_{{ $t->id }}" style="display: none;">--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-4">--}}
    {{--<h4>Max Stock</h4>--}}
    {{--{{ Form::number('initial_maxseat_'.$p->id.'_'.$t->id , 0, ['class'=> 'form-control','min' => 0,  'onkeypress' => "return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"]) }}--}}
    {{--</div>--}}
    {{--<div class="col-md-8">--}}
    {{--<h4>&nbsp</h4>--}}
    {{--<input class="btn btn-primary btn_update_stock_{{ $p->id }}_{{ $t->id }}"--}}
    {{--value="Initial stock">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<h4>Remark</h4>--}}
    {{--{{ Form::textarea('initial_remark_'.$p->id.'_'.$t->id, '', ['class'=> 'form-control', 'size' => '30x3']) }}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- /.box-body -->--}}
    {{--</div>--}}
    {{--<!-- /. box -->--}}
    {{--</div>--}}
    {{--<!-- /.col -->--}}
    {{--</div>--}}
    {{--<!-- /.row -->--}}
    {{--</section>--}}
    {{--<!-- /.content -->--}}
    {{--</div>--}}
    {{--<!-- /.content-wrapper -->--}}
    {{--@endforeach--}}
    {{--@empty--}}
    {{--<div class="content-wrapper">--}}
    {{--<!-- Content Header (Page header) -->--}}
    {{--<section class="content-header">--}}
    {{--<h1>--}}
    {{--Stock Premium Day Trip not available!--}}
    {{--<small>Discover Catamaran</small>--}}
    {{--</h1>--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
    {{--<li><a href="#">Premium Day Trip</a></li>--}}
    {{--<li class="active">Stock</li>--}}
    {{--</ol>--}}
    {{--</section>--}}

    {{--<!-- Main content -->--}}
    {{--<section class="content">--}}
    {{--<div class="row">--}}

    {{--<div class="col-md-12">--}}
    {{--<div class="box box-primary">--}}
    {{--<div class="box-body no-padding">--}}
    {{--<!-- THE CALENDAR -->--}}

    {{--</div>--}}
    {{--<!-- /.box-body -->--}}
    {{--</div>--}}
    {{--<!-- /. box -->--}}
    {{--</div>--}}
    {{--<!-- /.col -->--}}
    {{--</div>--}}
    {{--<!-- /.row -->--}}
    {{--</section>--}}
    {{--<!-- /.content -->--}}
    {{--</div>--}}
    {{--<!-- /.content-wrapper -->--}}
    {{--@endforelse--}}

    <!-- Modal -->
    {{--<div class="modal fade" id="stockLeft" role="dialog" aria-labelledby="myModalLabel">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
                                {{--aria-hidden="true">&times;</span></button>--}}
                    {{--<h4 class="modal-title" id="stockLeftLabel">Modal title</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--{!! Form::hidden('product', '') !!}--}}
                    {{--{!! Form::hidden('date', '') !!}--}}
                    {{--<h4>Max Stock</h4>--}}
                    {{--{{ Form::number('maxseat', 0, ['class'=> 'form-control','min' => 0,  'onkeypress' => "return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"]) }}--}}
                    {{--<h4>Max Stock</h4>--}}
                    {{--<textarea class="form-control" name="remark" placeholder="หมายเหตุ"></textarea>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--<button type="button" class="btn btn-primary" id="btnSaveStock">Save changes</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('product', '' , [ 'id' => 'product' ]) !!}
                    {!! Form::hidden('tourcompany', '', [ 'id' => 'tourcompany' ]) !!}
                    {!! Form::hidden('date', '' , [ 'id' => 'date' ]) !!}
                    <h4>Max Stock</h4>
                    {{ Form::number('maxseat', 0, ['id' => 'maxseat', 'class'=> 'form-control','min' => 0,  'onkeypress' => "return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"]) }}
                    <h4>Remark</h4>
                    <textarea class="form-control" name="remark" placeholder="หมายเหตุ" id="remark"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" id="btn-update-cancel">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-update-stock">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    {{--    <script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>--}}
    <!-- jQuery 3.1.0 -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/select2.full.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/fullcalendar/fullcalendar.js') }}"></script>
    <script src="{{ asset('js/scheduler.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();

            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }



            $('.datepicker').datepicker({
                autoclose: true
            });

            /*
             * NEW STOCK CALENDAR
             * updated 2018/06/21 12:03
             */

            $('#calendar').fullCalendar({
                schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                selectable: true,
                resourceAreaWidth: 230,
                now: moment().format('YYYY/MM/DD'),//'2015-08-07',
                firstDay: moment().format('YYYY/MM/DD'),//'2015-08-07',
                aspectRatio: 1.5,
                scrollTime: '00:00',
                height: 480,
                html: true,
                header: {
                    left: 'promptResource today prev,next',
                    center: 'title',
                    right: ''
                },
//                customButtons: {
//                    promptResource: {
//                        text: '+ room',
//                        click: function () {
//                            var title = prompt('Room name');
//                            if (title) {
//                                $('#calendar').fullCalendar(
//                                    'addResource',
//                                    {title: title},
//                                    true // scroll to the new resource?
//                                );
//                            }
//                        }
//                    }
//                },
                defaultView: 'stock',
                views: {
                    stock: {
                        type: 'timeline',
                        duration: {weeks: 1},
                        slotDuration: {days: 1},
                        buttonText: 'Stock'
                    }
                },

                resourceLabelText: 'Product : Tour Company',
                resources: [

                        @foreach($products as $p)
                        @foreach($p->tourcompanies as $t)
                    {
                        id: '{{ $p->id.'_'.$t->id}}', title: '{{$p->name.': ' }}{{ $t->name}}'
                    },
                    @endforeach
                    @endforeach

                ],
                events: [
                        @foreach($products as $p)
                        @foreach($p->tourcompanies as $t)
                        @foreach($t->pivot->stocks()->where('status',1)->get() as $s)
                    {
                        resourceId: '{{ $p->id.'_'.$t->id}}',
                        start: '{!! date('Y/m/d' , strtotime($s->reserved_date)) !!}',
                        title: '{{ $s->remaining_seat.'/'.$s->max_seat }}',
                        allDay: true,
                        backgroundColor: "#dd4b39",
                        borderColor: "#dd4b39",
                        remark: {!! json_encode(nl2br($s->remark))  !!}
                    },
                    @endforeach
                    @endforeach
                    @endforeach
                ],
                eventAfterRender: function(event, element, view) {
                    $(element).css('width','auto');
                    $(element).css('height','28px');
                    $(element).css('text-align','center');
                    $(element).css('font-size','20px');
                    $(element).css('padding-top','5px');
                },
                resourceRender: function (resourceObj, labelTds, bodyTds) {
                    bodyTds.on('click', function () {
                        //console.log(bodyTds);
                    });
                },
                eventClick: function (calEvent, jsEvent, view) {
                    //console.log(calEvent);
                    var product_tour = calEvent.resourceId.split('_');
                    $('#product').val(product_tour[0]);
                    $('#tourcompany').val(product_tour[1]);
                    $('#date').val(moment(calEvent.start).format('YYYY/MM/DD'));
                    $("#modal-default").modal();


                    var formData = new FormData();
                    formData.append('product', product_tour[0]);
                    formData.append('tourcompany', product_tour[1]);
                    formData.append('date', moment(calEvent.start).format('YYYY/MM/DD'));

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: 'getvoucherlist',
                        method: 'POST',
                        dataType: 'JSON',
                        data: {
                            maxseat: $('input[id="maxseat"]').val(),
                            remark: $("textarea[id='remark']").val(),
                            product: $('input[id="product"]').val(),
                            tourcompany: $('input[id="tourcompany"]').val(),
                            date: $("input[id='date']").val()
                        },
                        success: function (data) {
                            //                            if(data.success) {
                            //
                            //                            } else {
                            //
                            //                            }

                            $('#booking_table').html('<tr>\n' +
                                '                                                        <th>Status.</th>\n' +
                                '                                                        <th>Voucher No.</th>\n' +
                                '                                                        <th>Name</th>\n' +
                                '                                                        <th>Adult</th>\n' +
                                '                                                        <th>Child</th>\n' +
                                '                                                        <th>Infant</th>\n' +
                                '                                                        <th>Transfer</th>\n' +
                                '                                                        <th>Pickup Time</th>\n' +
                                '                                                        <th>Hotel</th>\n' +
                                '                                                    </tr>');

                            $.each(data, function (i, item) {

                                $('#booking_table').append('<tr class="clickable-row" data-href="{{ url('cfadmin/voucher') }}/'+data[i].id+'">' +
                                    '<td>' + data[i].statustext + '</td>' +
                                    '<td><a href="{{ url('cfadmin/voucher') }}/'+data[i].id+'">' + data[i].voucher_prefix + '-' + pad(data[i].voucher_no, 4) + '</a></td>' +
                                    '<td>' + data[i].customername + '</td>' +
                                    '<td>' + data[i].adult + '</td>' +
                                    '<td>' + data[i].child + '</td>' +
                                    '<td>' + data[i].infant + '</td>' +
                                    '<td>' + data[i].transfer + '</td>' +
                                    '<td>' + data[i].pickuptime_text + '</td>' +
                                    '<td>' + data[i].hotelname + '</td>' +
                                    '</tr>');
                            })


                            //alert(data[0].id);
                        },
                        error: function (data) {

                        }
                    });


                    $.ajax({
                        url: '/cfadmin/api/getstockdata',
                        method: 'POST',
                        processData: false,
                        contentType: false,
                        dataType: 'JSON',
                        cache: false,
                        data: formData,
                        success: function (result) {
                            //$('#title_{{ $p->id }}_{{ $t->id }}').text('Info. : ' + $('input[name="date"]').val());
                            if (result.success) {
                                $('#maxseat').val(result.data.maxseat);
                                $('#remark').val(result.data.remark);
                                /*var newEvent = new Object();
                                var remaining = data.remaining;

                                $('#stockLeft').modal('hide');*/
                                //alert(data.data.maxseat);

                                {{--$("input[name='maxseat_{{$p->id}}_{{ $t->id }}']").val(result.data.maxseat);--}}


                                {{--$("textarea[name='remark_{{$p->id}}_{{ $t->id }}']").val(result.data.remark);--}}
                                {{--$("#title_booking_date_{{ $p->id }}_{{ $t->id }}").text($('input[name="date"]').val());--}}
                                {{--$('#detail_initial_{{ $p->id }}_{{ $t->id }}').slideUp("slow");--}}
                                {{--$('#detail_{{ $p->id }}_{{ $t->id }}').slideUp("slow");--}}
                                {{--$('#detail_{{ $p->id }}_{{ $t->id }}').slideDown("slow");--}}
                            } else {
                                {{--$('#detail_{{ $p->id }}_{{ $t->id }}').slideUp("slow");--}}
                                {{--$('#detail_initial_{{ $p->id }}_{{ $t->id }}').slideUp("slow");--}}
                                {{--$('#detail_initial_{{ $p->id }}_{{ $t->id }}').slideDown("slow");--}}
                                //                                $('#stockLeft').modal('hide');
                                //alert(result.message);
                            }
                        },
                        error: function (data) {
//                            $('#stockLeft').modal('hide');
                            //alert(data.message);
                        }
                    });

//                    console.log(calEvent.resourceId);
//                    console.log(moment(calEvent.start).format('YYYY/MM/DD'));
//                        alert('Event: ' + calEvent.title);
//                        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//                        alert('View: ' + view.name);
//
//                        // change the border color just for fun
//                        $(this).css('border-color', 'red');

                },
                dayClick: function (date, jsEvent, view, resourceObj) {
                    if (!IsDateHasEvent(date, resourceObj)) {
                        //console.log(resourceObj.id);
                        //var event={id:1 , title: 'New event', start:  new Date()};
                        var product_tour = resourceObj.id.split('_');
                        //alert('product: '+ product_tour[0] + '| tour: '+ product_tour[1] + ' | date: ' + moment(date).format('YYYY/MM/DD'));
                        $('#product').val(product_tour[0]);
                        $('#tourcompany').val(product_tour[1]);
                        $('#date').val(moment(date).format('YYYY/MM/DD'));
                        $('#maxseat').val(0);
                        $('#remark').val("");

                        $("#modal-default").modal();




                        //                        $(".fc-state-highlight").removeClass("fc-state-highlight");
//                        $(jsEvent.target).addClass("fc-state-highlight");
//                        var events = $('#calendar').fullCalendar('clientEvents');
//
//                        for (var i = 0; i < events.length; i++) {
//
//                            var p_t = events[i].resourceId.split('_');
//
//                            if (moment(date).diff(moment(events[i].start)) == 0 && p_t[0] == product_tour[0] &&  p_t[1] == product_tour[1]){//date.isSame(moment(events[i].start).format('YYYY-MM-DD'))) {
//                                currentEvent = events[i];
//                                break;
//                            }
//                            else if (i == events.length - 1) {
//                                //ShowCreateNew(date);
//                                //alert(moment(date));
//                            }
//                        }


                    }
//                        alert('Clicked on: ' + date.format());
//
//                        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//
//                        alert('Current view: ' + view.name);
//
//                        // change the day's background color just for fun
//                        $(this).css('background-color', 'red');

                },
                eventMouseover: function (calEvent, jsEvent) {

                    var content = '';
                    if (calEvent.remark.trim() === "")
                        content = 'display:none;';
                    else
                        content = 'padding: 5px;width:auto;height:auto;';

                    var tooltip = '<div class="tooltipevent" style="font-weight:bold;background:yellow;position:absolute;z-index:10001;' + content + '">' + calEvent.remark + '</div>';
                    var $tooltip = $(tooltip).appendTo('body');

                    $(this).mouseover(function (e) {
                        $(this).css('z-index', 10000);
                        $tooltip.fadeIn('500');
                        $tooltip.fadeTo('10', 1.9);
                    }).mousemove(function (e) {
                        $tooltip.css('top', e.pageY + 10);
                        $tooltip.css('left', e.pageX + 20);
                    });
                },

                eventMouseout: function (calEvent, jsEvent) {
                    $(this).css('z-index', 8);
                    $('.tooltipevent').remove();
                },

            });


            function IsDateHasEvent(date, resourceObj) {
                var allEvents = [];
                allEvents = $('#calendar').fullCalendar('clientEvents');

                //console.log(allEvents);
                var event = $.grep(allEvents, function (v) {
                    return (+v.start === +date && v.resourceId === resourceObj.id);
                });
                return event.length > 0;
            }

            function addEvent() {
                var event2 = {resourceId: resourceObj.id, start: date, end: date, title: 'event 1'};
                $('#calendar').fullCalendar('renderEvent', event2, true);
            }


        });

//        $(document).on('click touchstart', '.clickable-row', function () {
//                window.location = $(this).data("href");
//
//        });

        $(document).on('click touchstart', '#btn-goto-date', function () {
            $('#calendar').fullCalendar( 'gotoDate', moment($('input[id="gotodate"]').val()));

        });

        $(document).on('click touchstart', '#btn-update-cancel', function () {

            var resource_id = $('input[id="product"]').val() + '_' + $('input[id="tourcompany"]').val();

            $('#calendar').fullCalendar('select', moment($("input[id='date']").val()), null, resource_id);
            $('.fc-highlight-container').css('z-index', 2);
            //console.log($("input[id='date']").val());

            $("#modal-default").modal("hide");

        });


        $(document).on('click touchstart', '#btn-update-stock', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var resource_id = $('input[id="product"]').val() + '_' + $('input[id="tourcompany"]').val();

            $('#btn-update-stock').attr('disabled',true);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: 'updatestock',
                method: 'POST',
                dataType: 'JSON',
                cache: false,
                data: {
                    maxseat: $('input[id="maxseat"]').val(),
                    remark: $("textarea[id='remark']").val(),
                    product: $('input[id="product"]').val(),
                    tourcompany: $('input[id="tourcompany"]').val(),
                    date: $("input[id='date']").val()
                },
                success: function (result) {
                    if (result.success) {
                        //console.log(result);

                        if (result.initial) {
                            var event2 = {
                                remark: result.remark,
                                resourceId: resource_id,
                                start: $("input[id='date']").val(),
                                title: result.remainingseat + '/' + result.maxseat,
                                allDay: true,
                                backgroundColor: "#dd4b39",
                                borderColor: "#dd4b39"
                            };
                            $('#calendar').fullCalendar('renderEvent', event2, true);

                        }
                        else {

                            var events = $('#calendar').fullCalendar('clientEvents');

                            for (var i = 0; i < events.length; i++) {

                                var p_t = events[i].resourceId.split('_');

                                if (moment($("input[id='date']").val()).diff(moment(events[i].start)) == 0 && p_t[0] == $('input[id="product"]').val() && p_t[1] == $('input[id="tourcompany"]').val()) {//date.isSame(moment(events[i].start).format('YYYY-MM-DD'))) {
                                    currentEvent = events[i];
                                    break;
                                }
                                else if (i == events.length - 1) {
                                    //ShowCreateNew(date);
                                    //alert(moment(date));
                                }
                            }

                            currentEvent.title = result.remainingseat + '/' + result.maxseat;
                            currentEvent.remark = result.remark;
                            $('#calendar').fullCalendar('updateEvent', currentEvent);
                        }
                        {{--var newEvent = new Object();--}}
                        {{--var remaining = result.remainingseat;--}}
                        {{--var maxseat = result.maxseat;--}}
                        {{--newEvent.start = currentEventDate;--}}
                        {{--newEvent.allDay = true;--}}
                        {{--newEvent.backgroundColor = '#dd4b39';--}}
                        {{--newEvent.borderColor = '#dd4b39';--}}

                        {{--newEvent.title = remaining + '/' + maxseat;--}}

                        {{--$('#calendar_{{ $p->id }}_{{ $t->id }}').fullCalendar('renderEvent', newEvent);--}}
                        {{--}--}}
                        {{--else {--}}

                        $("#modal-default").modal("hide");
                        $('#btn-update-stock').attr('disabled',false);

                        //$('#stockLeft').modal('hide');
                    } else {

                        //$('#stockLeft').modal('hide');
                    }
                },
                error: function (data) {

                    $("#modal-default").modal("hide");
                    $('#btn-update-stock').attr('disabled',false);
                }
            });
        });

    </script>

@endsection
