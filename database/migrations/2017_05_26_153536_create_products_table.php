<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('premium_day_trip_flag')->default(0);
            $table->boolean('per_head')->default(0);
            $table->text('description')->nullable();
            $table->decimal('price', 10 ,2)->nullable();
            $table->text('remark')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->text('short_description')->nullable();
            $table->string('destination');
            $table->integer('max_pax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('products');
    }
}
