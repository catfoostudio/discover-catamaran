@extends('cfadmin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Voucher
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="#">Voucher</a></li>
                <li class="active">Add Voucher</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['method' => 'POST' , 'action' => ['VoucherController@store']]) !!}

            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Customer Detail:</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div id="new_cust">
                                                    <label for="inputEmail3" >Name</label><br>
                                                    {{ $voucher->customer->name }}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Tel.</label><br>
                                                {{ $voucher->customer->tel }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Voucher No.</label>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select class="form-control select2" style="width: 100%;"
                                                        name="voucher_prefix">
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role->code == 'admin')
                                                        @foreach($voucherprefixes as $voucherprefix)
                                                            <option value="{{ $voucherprefix->id }}">{{ $voucherprefix->name }}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="{{ $voucherprefixes->id }}">{{ $voucherprefixes->name }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="voucher_no"
                                                       value="-XXXX (auto generate no.)" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{--{{ $voucher->unit }}--}}
                                        <?php $string = ''; ?>
                                        <label for="inputEmail3">Units</label><br>
                                        @foreach($voucherproductunits as $vpu)
                                            <?php

                                                $pu = \App\Models\ProductUnit::find($vpu->product_unit_id);
                                                $string .= $pu->unit->name.': '.$vpu->amount.' , ';
                                            ?>
                                        @endforeach
                                        {{ $string }}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Transfer</label><br>
                                        <label>
                                            <input type="radio" name="transfer" value="2w" class="minimal"
                                                   checked {{ old('transfer' , 'checked') == '2w' ? 'checked' : '' }}>
                                            2/W
                                        </label>
                                        <label>
                                            <input type="radio" name="transfer" value="1win"
                                                   class="minimal" {{ old('transfer') == '1win' ? 'checked' : '' }}>
                                            1/W in
                                        </label>
                                        <label>
                                            <input type="radio" name="transfer" value="1wout"
                                                   class="minimal" {{ old('transfer') == '1wout' ? 'checked' : '' }}>
                                            1/W out
                                        </label>
                                        <label>
                                            <input type="radio" name="transfer" value="notf"
                                                   class="minimal" {{ old('transfer') == 'notf' ? 'checked' : '' }}>
                                            NO T/F
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">

                                        <label for="inputEmail3">Hotel</label><br>
                                       {{ !empty($voucher->hotel) ? $voucher->hotel->name : '-' }}
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Pickuptime</label><br>
                                        {{ date("H : i", strtotime($voucher->pickuptime)) }}
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Detail</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                           <b style="font-size: 40px;">{{ $voucher->traveldate }}</b>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="inputEmail3">Product</label>
                                        <select class="form-control select2" style="width: 100%;" name="product">
                                            <option></option>
                                            @foreach($products as $product)
                                                <option {{ old('product') == $product->id ? 'selected=selected' : '' }} value="{{ $product->id }}">{{ $product->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="inputEmail3">Product Note</label>
                                <textarea class="form-control" rows="3"
                                          name="product_note">{{ old('product_note') }}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Unit of product</h3>
                                    </div>
                                    <div class="col-md-12" id="product_unit_section">
                                        @for($i = 0; $i < count(old('unit_id')); $i++)
                                            <div class="row"><div class="col-md-12" ><label>{{ \App\Models\Unit::find(old('unit_id.'.$i))->name }}</label><input class="form-control" type="hidden" name="unit_id[]" value="{{ old('unit_id.'.$i) }}"><input class="form-control" type="text" name="unit_amount[]" value="{{ old('unit_amount.'.$i) }}"></div></div>
                                        @endfor
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row" id="transfersection">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Transfer</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Hotel</label>
                                        <select class="form-control select2" style="width: 100%;" name="hotel">
                                            @foreach($hotels as $hotel)
                                                <option value="{{ $hotel->id }}">{{ $hotel->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Zone</label>
                                        <select class="form-control select2" style="width: 100%;" name="zone">
                                            @if(!empty($zones))
                                                @foreach($zones as $zone)
                                                    <option value="{{ $zone->id }}">{{ $zone->text }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Pickup Time</label>
                                        <input type="text" class="form-control" name="pickuptime"
                                               value="{{ old('pickuptime') }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="other_zone_flag">
                                        <label for="inputEmail3">Other Zone</label>
                                        <input type="text" class="form-control" name="other_zone"
                                               value="{{ old('other_zone') }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Booking Ref</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Issue to</label><span class="text-red"><b>*</b></span>
                                        <select class="form-control select2" style="width: 100%;" name="tourcompany">
                                            <option></option>
                                            @foreach($tourcompanies as $tourcompany)
                                                <option value="{{ $tourcompany->id }}">{{ $tourcompany->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail3">Issue by</label>
                                        <input type="text" name="travel_date" class="form-control pull-right"
                                               style="font-style: italic;font-weight: bold"
                                               value="ID {{ Auth::user()->id.' : '.Auth::user()->name }}"
                                               disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Tel.</label>
                                                <input type="text" id="tourcompany_tel" class="form-control"
                                                       disabled="disabled">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Fax.</label>
                                                <input type="text" id="tourcompany_fax" class="form-control"
                                                       disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Confirmed by.</label>
                                                <input type="text" class="form-control" name="confirmation_no"
                                                       value="{{ old('confirmation_no') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Confirmation No.</label>
                                                <input type="text" class="form-control" name="confirm_by"
                                                       value="{{ old('confirm_by') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Tel.</label>
                                                <input type="text" class="form-control"
                                                       value="{{ Auth::user()->telfull }}" disabled="disabled">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Fax.</label>
                                                <input type="text" class="form-control"
                                                       value="{{ Auth::user()->faxfull }}" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Hotel Info</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body form">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="hotel_info"></span>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Remark</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group">

                                <textarea class="form-control" rows="3" name="remark"
                                >{{ old('remark') }}</textarea>
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Sumbit</button>
                            {{--<button type="submit" class="btn btn-info">Sumbit and print</button>--}}
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')

    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="../../plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="../../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>

    <script>
        $(document).ready(function () {
            //Initialize Select2 Elements
            var select2tour = $('select[name="tourcompany"]');
            var select2product = $('select[name="product"]');
            var select2zone = $('select[name="zone"]');

            $('.select2').select2();

            select2tour.select2({
                placeholder: 'Please select Tour Company',
                allowClear: true,
            });

            select2product.select2({
                placeholder: 'Please select Product',
                allowClear: true,
            });

            select2zone.select2({
                placeholder: 'Please select Zone',
                allowClear: true,
            });



            select2product.on("change", function (e) {

                select2tour.select2().empty();

                $.ajax({
                    url: '/cfadmin/api/gettourcompanyoption',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: select2product.val()},

                    success: function (data) {

                        select2tour.prepend('<option></option>').select2({
                            allowClear: true,
                            placeholder: 'Please select Tour Company',
                            data: data
                        });

                    },
                    error: function (data) {

                    }
                });

                //select2tour.val('').trigger('change')

                select2tour.val(null).trigger('change.select2');


                select2zone.select2().empty();

                $.ajax({
                    url: '/cfadmin/api/getzoneoption',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: select2product.val()},

                    success: function (data) {

                        select2zone.prepend('<option></option>').select2({
                            allowClear: true,
                            placeholder: 'Please select Tour Company',
                            data: data
                        });

                    },
                    error: function (data) {

                    }
                });

                //select2tour.val('').trigger('change')

                select2zone.val(null).trigger('change.select2');


                $.ajax({
                    url: '/cfadmin/api/getunithtml',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    //dataType: 'json',
                    //cache: false,
                    data: {id: select2product.val()},

                    success: function (data) {

                        $('#product_unit_section').html('');
                        $('#product_unit_section').append(data);

                    },
                    error: function (data) {

                    }
                });



            });

            select2zone.on("change", function (e) {

                $('input[name="pickuptime"]').val($(this).select2('data')[0].text);
            });
            /*
                        select2tour.on("change", function (e) {
                            if(select2product.find('option:selected').val() == '') {
                                select2product.select2().empty();
                                $.ajax({
                                    url: '/cfadmin/api/getproductoption',
                                    method: 'GET',
            //                    processData: false,
                                    contentType: false,
                                    dataType: 'json',
                                    //cache: false,
                                    data: {id: select2tour.val()},

                                    success: function (data) {

                                        select2product.prepend('<option></option>').select2({
                                            allowClear: true,
                                            placeholder: 'Please select Product',
                                            data: data
                                        });

                                    },
                                    error: function (data) {

                                    }
                                });
                                select2product.val(null).trigger('change.select2');
                            }
                        });
            */
            /*  select2tour.add(select2product).on('select2:unselecting', function (e) {
                  clearbothselect2();
              });
  */

            $('input[name="other_zone_flag"]').on("change", function () {
                if(this.checked){
                    $('input[name="pickuptime"]').val('');
                    $('input[name="pickuptime"]').prop('disabled' , true);
                    $('select[name="zone"]').prop("disabled", true);

                }
                else{
                    $('input[name="pickuptime"]').val('');
                    $('input[name="pickuptime"]').prop('disabled' , false);
                    $('select[name="zone"]').prop("disabled", false);
                }

            });

            select2product.on('select2:unselecting', function (e) {
                clearbothselect2();
            });

            function clearbothselect2() {
                select2product.select2().empty();
                $.ajax({
                    url: '/cfadmin/api/getproductoption',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: 0},

                    success: function (data) {

                        select2product.prepend('<option></option>').select2({
                            allowClear: true,
                            placeholder: 'Please select Product',
                            data: data
                        });

                    },
                    error: function (data) {

                    }
                });
                select2tour.select2().empty();
                $.ajax({
                    url: '/cfadmin/api/gettourcompanyoption',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: 0},

                    success: function (data) {

                        select2tour.prepend('<option></option>').select2({
                            allowClear: true,
                            placeholder: 'Please select Product',
                            data: data
                        });

                    },
                    error: function (data) {

                    }
                });

            }

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('.datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                minuteStep: 1,
            });

            $('input[name="customer_type"]').on('ifChecked', function (event) {
                if ($(this).val() == 1) {
                    $('#new_cust').show();
                    $('#regular_cust').hide();
                    $('input[name="cust_tel"]').prop('disabled', false);
                }
                else {
                    $('#new_cust').hide();
                    $('#regular_cust').show();
                    $('input[name="cust_tel"]').val('');
                    $('input[name="cust_tel"]').prop('disabled', true);
                }
            });


            $('select[name="tourcompany"]').on("change", function (e) {
                flushtourcompanydata();
            });

            flushtourcompanydata();

            $('input[name="transfer"]').on('ifChecked', function(event){
                if($(this).val() == 'notf') {
                    $('#transfersection').hide();
                }
                else{
                    $('#transfersection').show();
                }
            });

            if(select2product.val() != ''){
                select2zone.select2().empty();

                $.ajax({
                    url: '/cfadmin/api/getzoneoption',
                    method: 'GET',
//                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    //cache: false,
                    data: {id: select2product.val()},

                    success: function (data) {

                        select2zone.prepend('<option></option>').select2({
                            allowClear: true,
                            placeholder: 'Please select Tour Company',
                            data: data
                        });

                        select2zone.val({{ old('zone') }}).trigger('change.select2');

                    },
                    error: function (data) {

                    }
                });

                //select2tour.val('').trigger('change')

                select2zone.val(null).trigger('change.select2');


            }



        });

        function flushtourcompanydata() {
            $.ajax({
                url: '/cfadmin/api/tourcompanydata',
                method: 'GET',
//                    processData: false,
                contentType: false,
                //cache: false,
                data: {id: $('select[name="tourcompany"]').val()},

                success: function (data) {

                    if (data == 'null') {
                        $('#tourcompany_tel').val('-');
                        $('#tourcompany_fax').val('-');
                    }
                    else {
                        var tourcompany = $.parseJSON(data);

                        $('#tourcompany_tel').val(tourcompany.telfull);
                        $('#tourcompany_fax').val(tourcompany.faxfull);
                    }

                    /*if(data.success) {
                     console.log(data);
                     // window.location = '';//Desired location for redirect
                     } else {

                     // If the signin wasn't successful.
                     }*/
                },
                error: function (data) {
                    // If you got an error code.

                }
            });
        }


    </script>
@endsection
