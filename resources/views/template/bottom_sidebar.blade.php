<aside class="sidebar bottom white" role="complementary">
    <!-- Wrapper -->
    <div class="wrap">
        <div class="row">
            <h2>Have questions? Get in touch. </h2>
            <!-- OneFourth -->

            <!-- //OneFourth -->

            <!-- OneFourth -->
            <div class="one-third">
                <h5>Tel. Fax.</h5>
                <p><span class="circle small"><i class="fa fa-phone"></i></span>66- (0)94-597-6111 (EN)</p>
                <p><span class="circle small"><i class="fa fa-phone"></i></span>66- (0)94-597-6222 (TH)</p>
                <p><span class="circle small"><i class="fa fa-phone"></i></span>66- (0)87-461-9555 (CHI)</p>
                <p><span class="circle small"><i class="fa fa-fax"></i></span>66- (0)76-606646 (FAX.)</p>
            </div>
            <!-- //OneFourth -->

            <!-- OneFourth -->
            <div class="one-third">
                <h5>Email</h5>
                <p><span class="circle small"><i class="fa fa-envelope"></i></span> <a href="mailto:discovercatamaran@gmail.com">discovercatamaran@gmail.com (EN)</a></p>
                <p><span class="circle small"><i class="fa fa-envelope"></i></span> <a href="mailto:discovercatamaran.1@gmail.com">discovercatamaran.1@gmail.com (TH)</a></p>
                <p><span class="circle small"><i class="fa fa-envelope"></i></span> <a href="mailto:allatphuket@gmail.com">allatphuket@gmail.com (CH)</a></p>
            </div>
            <!-- //OneFourth -->
            <div class="one-third">
                <h5>Chat online</h5>
                <p><span class="circle small"><i class="fa fa-comment"></i></span>Catamaran001, Catamaran002</p>
                <p><span class="circle small"><i class="fa fa-weixin"></i></span>普吉岛双体帆船</p>
            </div>

        </div>
    </div>
    <!-- //Wrapper -->
</aside>