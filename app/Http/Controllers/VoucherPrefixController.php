<?php

namespace App\Http\Controllers;

use App\Models\VoucherPrefix;
use Illuminate\Http\Request;

class VoucherPrefixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'voucherprefix.cfadmin.index' : 'home.index';

        $voucherprefixes = VoucherPrefix::all();
        return view($view, [
            'voucherprefixes' => $voucherprefixes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = ($this->isAdminRequest()) ? 'voucherprefix.cfadmin.create' : 'home.index';


        return view($view, [

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //// //
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);


        $input = $request->input();

        $voucherprefix = new VoucherPrefix();
        $voucherprefix->name = $input['name'];
        //$voucherprefix->logo = $input['logo'];
        $voucherprefix->amount = 0;
        if (!empty($input['remark']))
            $voucherprefix->remark = $input['remark'];

        $voucherprefix->status = 1;
        $voucherprefix->save();
        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/voucherprefix");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($voucherprefix)
    {
        //
        //
        $view = ($this->isAdminRequest()) ? 'voucherprefix.cfadmin.detail' : 'home.index';

        //$voucherprefix = VoucherPrefix::all();

//dd($product->tourcompanies->pluck('id')->toArray());
        return view($view, [
            'voucherprefix' => $voucherprefix,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $voucherprefix)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:191',
        ]);

        $input = $request->input();

        $voucherprefix->name = $input['name'];
        //$voucherprefix->logo = $input['logo'];
        $voucherprefix->amount = 0;
        if (!empty($input['remark']))
            $voucherprefix->remark = $input['remark'];

        $voucherprefix->status = 1;
        $voucherprefix->save();
        $request->session()->flash('alert-success', 'Project was successful updated!');
        return redirect("cfadmin/voucherprefix");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
