<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TourcompanyProduct extends Pivot
{
    //
    protected $table = 'tourcompany_product';

    public function tourcompany() {
        return $this->belongsTo('App\Models\Tourcompany');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }

    public function stocks()
    {
        return $this->hasMany('App\Models\Stock' , 'tourcompany_product_id');
    }

}
