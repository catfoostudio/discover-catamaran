<style>
    .social-list{
        display: inline-block;
    }
    .social-list li{
        position: inherit;
        padding: 0px;
    }
</style>
<footer class="footer" role="contentinfo">
    <!-- Wrapper -->
    <div class="wrap">
        <div class="row">
            <!-- OneFourth -->
            <div class="one-third">
                <h6 style="margin-bottom: -20px">DISCOVER CATAMARAN CO.,LTD.</h6>
                <ul class="social-list" style="line-height: 45px">
                    <li style="line-height: 25px;"><img src="{{ asset('images/dbd.png') }}" style="height:25px;display: inline-block;"> 0835561014024</li>
                    <li style="line-height: 25px;"><img src="{{ asset('images/ttt.png') }}" style="height:25px;display: inline-block;"> 83/1111</li>
                    <li style="line-height: 25px;"><img src="{{ asset('images/address.png') }}" style="height:25px;display: inline-block;"> 20/135 (Boat Lagoon) M2 Kohkaew, Muang, Phuket, Thailand 83000</li>
                    <li style="line-height: 25px;"><img src="{{ asset('images/tel.png') }}" style="height:25px;display: inline-block;"> 66-(0)80-076-1680 (EN/TH)<br>66-(0)87-461-9555 (CH/TH)</li>
                    <li style="line-height: 25px;"><img src="{{ asset('images/email.png') }}" style="height:25px;display: inline-block;"> discovercatamaran@gmail.com</li>
                    <li style="line-height: 25px;">Line: @catamaran</li>
                    <li style="line-height: 25px;">Wechat: allatphuket</li>
                </ul>
            </div>
            <div class="one-third">
                <h6>About us</h6>
                <ul>
                    <li><a href="{{ url('ourcompany') }}">ABOUT US</a></li>
                    <li><a href="{{ url('ourcompany') }}">WHO ARE WE</a></li>
                    <li><a href="{{ url('ourmission') }}">OUR VISION AND MISSION</a></li>
                    <li><a href="{{ url('policies') }}">POLICIES ( BOOKING-PAYMENT-REFUND)</a></li>
                    <li><a href="{{ url('contact') }}">BECOME OUR PARTNERS</a></li>
                    <li><a href="{{ url('premiumdaytrip') }}">PREMIUM DAY TRIP</a></li>
                    <li><a href="{{ url('privatecharter') }}">PRIVATE CHARTER</a></li>
                    <li><a href="{{ url('hotdeals') }}">HOT DEALS</a></li>
                </ul>
            </div>


            <!-- OneFourth -->
            <div class="one-third">
                <h6>Follow us</h6>
                {{--<div class="fb-like-box" data-href="https://www.facebook.com/discover-catamaran" data-width="270" data-height="300" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>--}}
                {{--<p>Lorem ipsum dolor sit amet, sectetuer adipiscing elit, sed diam nonummy  dolore magna aliquam erat volutpat. </p>--}}
                <ul class="social-list" style="line-height: 45px">
                    <li><a href="https://www.facebook.com/discovercatamaran" title="Facebook" ><i class="fa fa-facebook fa-fw circle"></i> Facebook</a></li>
                    <li><a href="https://instagram.com/discovercatamaran" title="Instagram" ><i class="fa fa-instagram fa-fw circle"></i>Instagram</a></li>
                    <li><a href="#" title="Twitter" ><i class="fa fa-twitter fa-fw circle"></i>Twitter</a></li>
                    <li><a href="#" title="Youtube" ><i class="fa fa-youtube-play fa-fw circle"></i>Youtube</a></li>
                    <li><a href="#" title="WEIBO    " ><i class="fa fa-weibo fa-fw circle"></i>WEIBO</a></li>
                </ul>

            </div>
            <!-- //OneFourth -->
        </div>
    </div>
    <!-- //Wrapper -->

    <div class="copy">
        <!-- Wrapper -->
        <div class="wrap">
            <p>Copyright 2018 Discover Catamaran, all rights reserved.</p>
            {{--<p>By <a href="http://www.themeenergy.com" title="www.themeenergy.com">themeenergy</a></p>--}}
        </div>
        <!-- //Wrapper -->
    </div>
</footer>