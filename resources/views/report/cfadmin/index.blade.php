@extends('default.cfadmin.index')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Export Voucher by Travel date
                {{--<small>Preview</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> cfAdmin</a></li>
                <li><a href="#">Report</a></li>
                <li class="active">Export vouchers data</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['method' => 'POST' , 'action' => ['ReportController@report']]) !!}

            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="margin-right: 10px;">Select method: </h3>
                            <label style="margin-right: 10px;">
                                <input type="radio" name="type" class="minimal" value="1" checked>
                                Specific
                            </label>
                            <label>
                                <input type="radio" name="type" class="minimal" value="2" >
                                Period
                            </label>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <div class="box-body form">
                            <div class="form-group" id="specific_section">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div id="regular_cust" {{ old('customer_type',2) == 1 ? 'style=display:none' : '' }}>
                                                    <label for="inputEmail3">Month</label>
                                                    <select name="month" class="form-control select2" style="width: 100%;"
                                                            name="customer">
                                                        @for($i=1;$i<=12;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Year</label>
                                                <select name="year" class="form-control select2">
                                                    @for($i=\Carbon\Carbon::now()->year - 3;$i<=\Carbon\Carbon::now()->year+2;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="period_section" style="display: none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Start</label>
                                                <input autocomplete="off" type="text" class="form-control datepicker" name="start">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">End</label>
                                                <input autocomplete="off" type="text" class="form-control datepicker" name="end">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputEmail3">User</label>
                                                <select class="form-control select2" name="user">
                                                @foreach($users as $user)
                                                        <option value="{{$user->id}}">{{ $user->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputEmail3">Product</label>
                                                <select class="form-control select2" name="product">
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}">{{ $product->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Sumbit</button>
                            {{--<button type="submit" class="btn btn-info">Sumbit and print</button>--}}
                            <button type="button" class="btn btn-default pull-right">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('script')
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>

        $(document).ready(function () {

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            $('input[name="type"]').on("change", function () {
                var type = $('input[name="type"]:checked').val();
                if(type == 1){
                    $('#specific_section').show();
                    $('#period_section').hide();
                }
                else{
                    $('#period_section').show();
                    $('#specific_section').hide();
                }

            });
        });
    </script>
@endsection